
PRODUCT_PACKAGES := \
#    FMRadio
#    MyTube \
#    VideoPlayer

PRODUCT_PACKAGES += \
    libmfvfactory

$(call inherit-product, $(SRC_TARGET_DIR)/product/common.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/telephony.mk)

# Overrides
PRODUCT_BRAND  := alps
PRODUCT_NAME   := $(TARGET_PRODUCT)
PRODUCT_DEVICE := $(TARGET_PRODUCT)

# This is for custom project language configuration.
PRODUCT_LOCALES := $(MTK_PRODUCT_LOCALES)
PRODUCT_LOCALES += $(MTK_PRODUCT_AAPT_CONFIG)



##############################################################################
##############################################################################
########Below Setting is from vanzo82_wet_jb5.mk.custom, dont edit directly########
##############################################################################
##############################################################################

PRODUCT_BRAND  := ZOPO
PRODUCT_PROPERTY_OVERRIDES += \
        ro.init.screen_brightness=255 \
        ro.init.no_sim_missing_notify=true \
        persist.sys.themePackageName=com.tmobile.theme.Sky \
        persist.sys.themeId=Sky
