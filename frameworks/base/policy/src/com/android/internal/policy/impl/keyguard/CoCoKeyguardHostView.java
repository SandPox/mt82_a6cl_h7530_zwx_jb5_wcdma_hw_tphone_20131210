/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.policy.impl.keyguard;

import android.app.admin.DevicePolicyManager;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.UserInfo;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.SystemProperties;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Slog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.RemoteViews.OnClickHandler;
import android.telephony.TelephonyManager;

import com.android.internal.R;
import com.android.internal.policy.impl.keyguard.KeyguardSecurityModel.SecurityMode;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.widget.LockPatternUtils;

import java.io.File;
import java.util.List;
/*coco locker begin*/
import android.view.inputmethod.InputMethodManager;
import android.os.Handler.Callback;
import android.os.Message;
import android.opengl.GLSurfaceView;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.ITelephony;
import android.os.ServiceManager;
import android.os.RemoteException;
import android.view.ViewManager;
import android.provider.Telephony.SIMInfo;
import com.android.internal.telephony.IccCardConstants;
/*coco locker end*/

public class CoCoKeyguardHostView extends KeyguardHostView {
    private static final String TAG = "CoCoKeyguardHostView";

    // Use this to debug all of keyguard
    public static boolean DEBUG = KeyguardViewMediator.DEBUG;

    // Found in KeyguardAppWidgetPickActivity.java
    static final int APPWIDGET_HOST_ID = 0x4B455947;

    private final int MAX_WIDGETS = 5;


    private boolean mIsVerifyUnlockOnly;
    private boolean mEnableFallback; // TODO: This should get the value from KeyguardPatternView
    private SecurityMode mCurrentSecuritySelection = SecurityMode.Invalid;
    

    protected OnDismissAction mDismissAction;

    protected int mFailedAttempts;

    private KeyguardSecurityModel mSecurityModel;


    private int mDisabledFeatures;

    private boolean mCameraDisabled;

    private boolean mSafeModeEnabled;
    private boolean mScreenOn;

 /*add sim info*/
    public static String cooeeSim1 = "";
    public static String cooeeSim2 = "";
/*add sim info*/

    /*coco locker begin*/
    private Context mContext;
    private ViewManager mViewManager;
    private LockPatternUtils mLockPatternUtils;
    private static final String LOG_TAG = "coco";
    private ViewAgent mViewAgent;
	//add for shuibo reflex
	public static ViewAgent  mZorderViewAgent;
    private View mLockView;

    private KeyguardUpdateMonitor mUpdateMonitor;
	
    private Callback mCoCoLockerCallback = new Callback(){
        @Override
        public boolean handleMessage(Message msg) {
            boolean ret = false;
            switch (msg.what) {
                case IWrap.REQUEST_KERNEL_EXIT:
                        if(mViewMediatorCallback != null)
                            mViewMediatorCallback.keyguardDone(true);
                        ret = true;
                    break;
                case IWrap.REQUEST_KERNEL_RESET_LIGHT:
                        if(mViewMediatorCallback != null)
                            mViewMediatorCallback.userActivity(0);
                        ret = true;
                    break;
                case IWrap.REQUEST_KERNEL_SEND_SIMCARD_NAME:
                        //msg.obj = getSimCardName();
                        ret = false;                       
                    break;

                default:

                    break;
            }
            return ret;
        } 
    };

	private KeyguardUpdateMonitorCallback mUpdateMonitorCallbacks =
		    new KeyguardUpdateMonitorCallback() {	
		@Override
		public void onRefreshCarrierInfoGemini(CharSequence plmn, CharSequence spn, int simId) {
		    Log.v("coco_siminfo","plmn = " + plmn + "spn = " + spn + " simId = " + simId);
                    if (plmn != null) {
                        if (PhoneConstants.GEMINI_SIM_1 == simId) {
                           cooeeSim1 = plmn.toString();
                        } else {
                           cooeeSim2 = plmn.toString();
                        }
                    }
		     String defaultStr = mContext.getResources().getText(R.string.lockscreen_carrier_default).toString();
		     /*if (mUpdateMonitor != null){
	                 final IccCardConstants.State state = mUpdateMonitor.getSimState();
	                 final IccCardConstants.State stateGemini = mUpdateMonitor.getSimState(PhoneConstants.GEMINI_SIM_2);
		          Log.v("coco_siminfo","state = " + state + "stateGemini = " + stateGemini);
			  if (simId == PhoneConstants.GEMINI_SIM_1){
			      if (state == IccCardConstants.State.READY){
			  	   cooeeSim1 = plmn.toString();
			      }else{
			  	   cooeeSim1 = "";
			      }
			  }
			  
			  if (simId == PhoneConstants.GEMINI_SIM_2){
			      if (stateGemini == IccCardConstants.State.READY){
			  	   cooeeSim2 = plmn.toString();
			      }else{
			  	   cooeeSim2 = "";
			      }
			  }			  
		     }*/
			  
                   if (cooeeSim1 != null && cooeeSim1.equals(defaultStr)){
                       cooeeSim1 = "";				   	
                   }
				   
                   if (cooeeSim2 != null && cooeeSim2.equals(defaultStr)){
                       cooeeSim2 = "";				   	
                   } 
				   
		    Log.i("coco_siminfo", "cooeeSim1= " + cooeeSim1+ " cooeeSim2 = " + cooeeSim2);
		    if (mViewAgent != null){
		        mViewAgent.notifyAppSimCardName(cooeeSim1 + " " + cooeeSim2);
		    }
		}
	    };
	
    /*coco locker end*/   
    public static void getSimCardName(Context context){
	boolean IsSim1Avaliable = false;
	boolean IsSim2Avaliable = false;
	KeyguardUpdateMonitor Monitor = KeyguardUpdateMonitor.getInstance(context);

	Log.v("coco","isSim1Locked SimState1" + Monitor.isSimLockedGemini(PhoneConstants.GEMINI_SIM_1) + Monitor.getSimState(PhoneConstants.GEMINI_SIM_1));
	if(!Monitor.isSimLockedGemini(PhoneConstants.GEMINI_SIM_1) && Monitor.getSimState(PhoneConstants.GEMINI_SIM_1) == IccCardConstants.State.READY){
		SIMInfo info1 = SIMInfo.getSIMInfoBySlot(context, PhoneConstants.GEMINI_SIM_1);
		if(info1 != null && info1.mDisplayName != null){
		   String str = info1.mDisplayName;
		   if(str.length()>4){
			String[] strs = str.split(" ");
			cooeeSim1 = strs[0];
		   }else{
		        cooeeSim1 = info1.mDisplayName;
			IsSim1Avaliable = true;
		   }
		}else{
		   
		   Log.v("coco","cooeeSim1 = null");
		}
		
	}else{
		cooeeSim1 = context.getResources().getText(R.string.lockscreen_missing_sim_message_short).toString();
		//cooeeSim1 = "";
	}
	Log.v("coco","isSim2Locked SimState2" + Monitor.isSimLockedGemini(PhoneConstants.GEMINI_SIM_2) + Monitor.getSimState(PhoneConstants.GEMINI_SIM_2));
	if(!Monitor.isSimLockedGemini(PhoneConstants.GEMINI_SIM_2) && Monitor.getSimState(PhoneConstants.GEMINI_SIM_2) == IccCardConstants.State.READY){
		SIMInfo info2 = SIMInfo.getSIMInfoBySlot(context, PhoneConstants.GEMINI_SIM_2);
		if(info2 != null && info2.mDisplayName != null){
		   String str2 = info2.mDisplayName;
		   if(str2.length()>4){
			String[] strs2 = str2.split(" ");
			cooeeSim2 = strs2[0];
		   }else{
		        cooeeSim2 = info2.mDisplayName;
			IsSim2Avaliable = true;
		   }
		}else{
		   Log.v("coco","cooeeSim2 = null");
		}
	}else{
		 cooeeSim2 = context.getResources().getText(R.string.lockscreen_missing_sim_message_short).toString();
		//cooeeSim2 = "";
	}
	
	if(IsSim1Avaliable && IsSim2Avaliable){
		cooeeSim1 = cooeeSim1 + " | ";	
	}else if(!IsSim1Avaliable && !IsSim2Avaliable){
		cooeeSim1 = cooeeSim1 + " - ";
		cooeeSim2 = context.getResources().getText(R.string.emergency_calls_only).toString();
	}else if(IsSim1Avaliable && !IsSim2Avaliable){
		cooeeSim2 = "";
	}else if(!IsSim1Avaliable && IsSim2Avaliable){
		cooeeSim1 = "";		
	}
    }


    /*package*/ interface OnDismissAction {
        /* returns true if the dismiss should be deferred */
        boolean onDismiss();
    }

    public CoCoKeyguardHostView(Context context) {
        this(context, null);
	mContext = context; 
    }

    public CoCoKeyguardHostView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context; 
        mLockPatternUtils = new LockPatternUtils(context); 
        mSecurityModel = new KeyguardSecurityModel(context);
        mUpdateMonitor = KeyguardUpdateMonitor.getInstance(context);
    }

    public void initCoCoHostView(ViewAgent viewAgent){
        mViewAgent = viewAgent;
        //add for shuibo reflex
        mZorderViewAgent = viewAgent;
        mViewAgent.setKernelCallback(mCoCoLockerCallback);
        Log.i("cocoTest","before onCreate");
        mViewAgent.onCreate();
        Log.i("cocoTest","after onCreate");
        mViewAgent.logApp("ViewWrap.onCreate finish");

        if(mLockView != null){
            this.removeView(mLockView);
            mLockView = null;
        }

        Log.i("cocoTest","before getView");
        mLockView = mViewAgent.getView();
        Log.i("cocoTest","after getView");
        Log.i(LOG_TAG,"mLockView:"+mLockView);
        this.addView(mLockView);
    }

    public boolean handleBackKey() {
        return false;
    }

    public boolean handleMenuKey() {
        // The following enables the MENU key to work for testing automation
        return false;
    }

    public void goToWidget(int appWidgetId) {
       
    }

     public boolean showNextSecurityScreenIfPresent() {
        SecurityMode securityMode = mSecurityModel.getSecurityMode();
        // Allow an alternate, such as biometric unlock
        securityMode = mSecurityModel.getAlternateFor(securityMode);
        if (SecurityMode.None == securityMode) {
            return false;
        } else {
           // showSecurityScreen(securityMode); // switch to the alternate security view
            return true;
        }
    }

    public void goToUserSwitcher() {
       // mAppWidgetContainer.setCurrentPage(getWidgetPosition(R.id.keyguard_multi_user_selector));
    }

    public void dismiss() {
       // showNextSecurityScreenOrFinish(false);
    }

     public void showAssistant() {
        // final Intent intent = ((SearchManager) mContext.getSystemService(Context.SEARCH_SERVICE))
        //   .getAssistIntent(mContext, UserHandle.USER_CURRENT);

        // if (intent == null) return;

        // final ActivityOptions opts = ActivityOptions.makeCustomAnimation(mContext,
        //         R.anim.keyguard_action_assist_enter, R.anim.keyguard_action_assist_exit,
        //         getHandler(), null);

        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // mActivityLauncher.launchActivityWithAnimation(
        //         intent, false, opts.toBundle(), null, null);
    }


    @Override
    public boolean onTouchEvent(MotionEvent ev) {
       return true;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (mViewMediatorCallback != null) {
            Log.i("coco","keyguardDoneDrawing");
            mViewMediatorCallback.keyguardDoneDrawing();
        }
    }


    @Override
    protected void onFinishInflate() {
        // Grab instances of and make any necessary changes to the main layouts. Create
        // view state manager and wire up necessary listeners / callbacks.
        ///M: add for power off alarm @{
       
    }


  
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
	Log.i("azmohan","registerCallback");
	KeyguardUpdateMonitor.getInstance(mContext).registerCallback(mUpdateMonitorCallbacks);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
	Log.i("azmohan","removeCallback");
	KeyguardUpdateMonitor.getInstance(mContext).removeCallback(mUpdateMonitorCallbacks);     
    }

    
    protected boolean mShowSecurityWhenReturn;

    @Override
    public void reset() {
       
    }


  
    @Override
    public void onScreenTurnedOff() {
        mScreenOn = false;

        Log.i(LOG_TAG, "onPause");
        if (mViewAgent!=null) {
            mViewAgent.onPause();
        }
    }

    @Override
    public void onScreenTurnedOn() {
        if (DEBUG) Log.d(TAG, "screen on, instance " + Integer.toHexString(hashCode()));
        /// M: Also set screen on flag @{
        mScreenOn = true;
        /// M @}
        // showPrimarySecurityScreen(false);
        // getSecurityView(mCurrentSecuritySelection).onResume(KeyguardSecurityView.SCREEN_ON);

        // // This is a an attempt to fix bug 7137389 where the device comes back on but the entire
        // // layout is blank but forcing a layout causes it to reappear (e.g. with with
        // // hierarchyviewer).
        // requestLayout();

        // if (mViewStateManager != null) {
        //     mViewStateManager.showUsabilityHints();
        // }
        Log.i(LOG_TAG, "onResume");
        if (mViewAgent!=null) {
            mViewAgent.onResume();
        }
    }
  
    @Override
    /*azmohan test*/
    public void show() {
        if (DEBUG) Log.d(TAG, "show()");
        Log.i("coco","CoCoKeyguardHostView show is called!");
        // Log.i(LOG_TAG, "onResume");
        // if (mViewAgent!=null) {
        //     mViewAgent.onResume();
        // }
       // showPrimarySecurityScreen(false);//azmohan test
        // if (mMultiPaneChallengeLayout != null) {
        //     mMultiPaneChallengeLayout.removeAllViews();
        //     mMultiPaneChallengeLayout.addView(mLockView);
        // } /// M: Video Orb Plugin for Tablet

    }

    private boolean isSecure() {
        SecurityMode mode = mSecurityModel.getSecurityMode();
        switch (mode) {
            case Pattern:
                return mLockPatternUtils.isLockPatternEnabled();
            case Password:
            case PIN:
                return mLockPatternUtils.isLockPasswordEnabled();
            case SimPinPukMe1:
            case SimPinPukMe2:
            case Account:
                return true;
            case None:
                return false;
            default:
                throw new IllegalStateException("Unknown security mode " + mode);
        }
    }

    @Override
    public void wakeWhenReadyTq(int keyCode) {
        if (DEBUG) Log.d(TAG, "onWakeKey");
        if (keyCode == KeyEvent.KEYCODE_MENU && isSecure()) {
            if (DEBUG) Log.d(TAG, "switching screens to unlock screen because wake key was MENU");
            //showSecurityScreen(SecurityMode.None);
        } else {
            if (DEBUG) Log.d(TAG, "poking wake lock immediately");
        }
        if (mViewMediatorCallback != null) {
            mViewMediatorCallback.wakeUp();
        }
    }

    @Override
    public void verifyUnlock() {
        SecurityMode securityMode = mSecurityModel.getSecurityMode();
        if (securityMode == KeyguardSecurityModel.SecurityMode.None) {
            if (mViewMediatorCallback != null) {
               // mViewMediatorCallback.keyguardDone(true);
            }
        } else if (securityMode != KeyguardSecurityModel.SecurityMode.Pattern
                && securityMode != KeyguardSecurityModel.SecurityMode.PIN
                && securityMode != KeyguardSecurityModel.SecurityMode.Password) {
            // can only verify unlock when in pattern/password mode
            if (mViewMediatorCallback != null) {
                mViewMediatorCallback.keyguardDone(false);
            }
        } else {
            // otherwise, go to the unlock screen, see if they can verify it
            mIsVerifyUnlockOnly = true;
           // showSecurityScreen(securityMode);
        }
    }



    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        
    }

    /**
     * M: add for power-off alarm
     */
    public boolean isAlarmUnlockScreen() {
        if (mSecurityModel.getSecurityMode() == KeyguardSecurityModel.SecurityMode.AlarmBoot) {
            return true;
        } else {
            return false;
        }
    }
    

    @Override
    /// M: clean callbacks to avoid memory leak
    public void cleanUp() {
        /// M: Resovle memory leak issue @{
        // if (mCurrentKeyguardSecurityView != null) {
        //     mCurrentKeyguardSecurityView.onPause();
        //     mCurrentKeyguardSecurityView.setKeyguardCallback(mNullCallback);
        // }
        Log.i("coco","onDestroy");
        if (mViewAgent!=null) {
            mViewAgent.onPause();// azmohan
            //mViewAgent.setZorder(false);
            if(mLockView != null){
                this.removeView(mLockView);
                mLockView = null;
            }
						
            mViewAgent.onDestroy();
            mViewAgent = null;
	    //add for shuibo reflex
	    mZorderViewAgent = null;
        }
       
    }

     @Override
    public long getUserActivityTimeout() {
        // Currently only considering user activity timeouts needed by widgets.
        // Could also take into account longer timeouts for certain security views.
        
        return -1;
    }

}
