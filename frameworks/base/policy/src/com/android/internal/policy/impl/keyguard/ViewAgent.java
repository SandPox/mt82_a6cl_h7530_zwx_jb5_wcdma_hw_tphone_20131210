package com.android.internal.policy.impl.keyguard;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Message;
import android.os.Handler.Callback;
import android.view.View;
import android.util.Log;

interface IWrap {
	void onCreate();

	void onDestroy();

	void onResume();

	void onPause();

	View getView();

	void setKernelCallback(Callback callback);

	Callback getAppService();

	int REQUEST_KERNEL_EXIT = 10000;
	int REQUEST_KERNEL_RESET_LIGHT = 10001;
	int REQUEST_KERNEL_SEND_SIMCARD_NAME = 10002;

	int NOTIFY_APP_LOGINFO = 20000;
	int NOTITY_APP_REMOTE_CONTEXT = 20001;
	int NOTIFY_APP_SIMCARD_NAME = 20002;
	int NOTIFY_APP_SETZORDER = 20003;
}

public class ViewAgent implements IWrap {

	private Method methodOnCreate;
	public static   String LOCK_PACKAGE_NAME = "com.coco.lock.view.miuiunlock";
    public static   String LOCK_WRAP_CLASS_NAME = "com.coco.lock.view.miuiunlock.ViewWrap";

	@Override
	public void onCreate() {
		try {
			methodOnCreate.invoke(obj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private Method methodOnDestroy;

	@Override
	public void onDestroy() {
		try {
			methodOnDestroy.invoke(obj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private Method methodOnResume;

	@Override
	public void onResume() {
		try {
			methodOnResume.invoke(obj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private Method methodOnPause;

	@Override
	public void onPause() {
		try {
			methodOnPause.invoke(obj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private Method methodGetView;

	@Override
	public View getView() {
		try {
			return (View) methodGetView.invoke(obj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}

	private Method methodSetKernelCallback;

	@Override
	public void setKernelCallback(Callback callback) {
		try {
			methodSetKernelCallback.invoke(obj, callback);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private Method methodGetAppService;

	@Override
	public Callback getAppService() {
		try {
			return (Callback) methodGetAppService.invoke(obj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}

	private Context context;

	private boolean loadContext(Context currentContext, String packageName) {
		try {
			context = currentContext.createPackageContext(packageName,
					Context.CONTEXT_INCLUDE_CODE
							| Context.CONTEXT_IGNORE_SECURITY);
			return true;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}

	private Class<?> wrapClass;
	private Constructor<?> wrapConstructor;

	private boolean loadClassType(String className) {
		try {
			wrapClass = Class
					.forName(className, true, context.getClassLoader());
			wrapConstructor = wrapClass.getDeclaredConstructor(Context.class);
			methodOnCreate = wrapClass.getDeclaredMethod("onCreate");
			methodOnDestroy = wrapClass.getDeclaredMethod("onDestroy");
			methodOnResume = wrapClass.getDeclaredMethod("onResume");
			methodOnPause = wrapClass.getDeclaredMethod("onPause");
			methodGetView = wrapClass.getDeclaredMethod("getView");
			methodSetKernelCallback = wrapClass.getDeclaredMethod(
					"setKernelCallback", Callback.class);
			methodGetAppService = wrapClass.getDeclaredMethod("getAppService");
			return true;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (SecurityException e) {
			e.printStackTrace();
			return false;
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			return false;
		}
	}

	private Object obj;

	private boolean loadInstance() {
		try {
			obj = wrapConstructor.newInstance(context);
			return true;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false;
		} catch (InstantiationException e) {
			e.printStackTrace();
			return false;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return false;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	//public static String simname;
	public static ViewAgent createInstance(Context context, String packageName,
			String className) {
		if (context == null || packageName == null || className == null) {
			return null;
		}

		ViewAgent va = new ViewAgent();
		if (va.loadContext(context, packageName) && va.loadClassType(className)
				&& va.loadInstance()) {
			va.loadAppService();
			va.setRemoteContext(context);
			/*add sim name*/
			//simname = CoCoKeyguardHostView.cooeeSim1 + "," + CoCoKeyguardHostView.cooeeSim2;
			//Log.v("azmohan","simname = " + simname);
			//va.notifyAppSimCardName(simname);
			/*add sim name*/
			return va;
		}

		return null;
	}
	
	private Callback appService;
	private void loadAppService() {
		appService = getAppService();
	}
	
	public void logApp(String text) {
		Message msg = Message.obtain();
		msg.what = IWrap.NOTIFY_APP_LOGINFO;
		msg.obj = text;
		appService.handleMessage(msg);
		msg.recycle();
	}

	public void notifyAppSimCardName(String name){
		Message msg = Message.obtain();
		msg.what = IWrap.NOTIFY_APP_SIMCARD_NAME;
		msg.obj = name;
		appService.handleMessage(msg);
		msg.recycle();
	}
	
	private void setRemoteContext(Context remote) {
		Message msg = Message.obtain();
		msg.what = IWrap.NOTITY_APP_REMOTE_CONTEXT;
		msg.obj = remote;
		appService.handleMessage(msg);
		msg.recycle();
	}
	//add for shuibo reflex
	public void setZorder(boolean order){
		Log.v("azmohan","setZorder = "+order);
		Message msg = Message.obtain();
		msg.what = IWrap.NOTIFY_APP_SETZORDER;
		msg.obj = order;
		appService.handleMessage(msg);
		msg.recycle();
	}
}
