/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.policy.impl.keyguard;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.security.auth.PrivateCredentialPermission;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Settings;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.content.res.Configuration;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.FileObserver;
import android.app.Activity;
import android.app.ActivityManager;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.android.internal.R;
import com.android.internal.widget.LockPatternUtils;

import android.net.Uri;
import android.database.Cursor;
import android.content.ContentResolver;
import android.provider.Settings;
import com.android.internal.policy.impl.keyguard.KeyguardSecurityModel.SecurityMode;
import com.mediatek.common.featureoption.FeatureOption;

/**
 * Manages creating, showing, hiding and resetting the keyguard.  Calls back
 * via {@link KeyguardViewMediator.ViewMediatorCallback} to poke
 * the wake lock and report that the keyguard is done, which is in turn,
 * reported to this class by the current {@link KeyguardViewBase}.
 */
public class KeyguardViewManager {
    private final static boolean DEBUG = KeyguardViewMediator.DEBUG;
    private static String TAG = "KeyguardViewManager";
    public static boolean USE_UPPER_CASE = true;

    // Timeout used for keypresses
    static final int DIGIT_PRESS_WAKE_MILLIS = 5000;

    private final Context mContext;
    private final ViewManager mViewManager;
    private final KeyguardViewMediator.ViewMediatorCallback mViewMediatorCallback;

    private WindowManager.LayoutParams mWindowLayoutParams;
    private boolean mNeedsInput = false;

    private FrameLayout mKeyguardHost;
    private KeyguardHostView mKeyguardView;

    private boolean mScreenOn = false;
    private LockPatternUtils mLockPatternUtils;


/* Vanzo:xulei on: Tue, 22 Oct 2013 10:35:52 +0800
 * add cooee locker
 */
    private KeyguardSecurityModel mSecurityModel;
    private final static boolean YUNLAN_DEBUG = false;
    private boolean loadCooeeCurrentLock() {

        String currentLock = Settings.System.getString(mContext.getContentResolver(), "cooee_current_lock");

        if (currentLock == null) {
            Log.i("coco","currentLock is null");
            return false;

        } else {
            Log.i("coco","currentLock:"+currentLock);
            String[] fields = currentLock.split(",");
            int length =  fields.length;

            if (length < 3) {
                return false;
            } else {
                ViewAgent.LOCK_PACKAGE_NAME = fields[1];
                ViewAgent.LOCK_WRAP_CLASS_NAME = fields[2];
                return true;
            }
        }
    }

    private View getCoCoHostView() {
        loadCooeeCurrentLock();
        Log.i("coco", "getCoCoHostView,mKeyguardView:" +mKeyguardView);
        if (mKeyguardView != null) {
            mKeyguardView.cleanUp();
            mKeyguardHost.removeView(mKeyguardView);
            mKeyguardView = null;
        }

        Log.i("coco","package_name:"+ViewAgent.LOCK_PACKAGE_NAME + "\nclass_name:"+ViewAgent.LOCK_WRAP_CLASS_NAME);
        ViewAgent mViewAgent = ViewAgent.createInstance(mContext, ViewAgent.LOCK_PACKAGE_NAME, ViewAgent.LOCK_WRAP_CLASS_NAME);
        if (mViewAgent != null) {
            CoCoKeyguardHostView hostView = new CoCoKeyguardHostView(mContext);
            hostView.setViewMediatorCallback(mViewMediatorCallback);
            hostView.initCoCoHostView(mViewAgent);
            return hostView;
        }
        Log.i("coco", "ViewAgent.createInstance error");
        return null;
    }

    private boolean isSecure() {
        SecurityMode mode = mSecurityModel.getSecurityMode();
        switch (mode) {
            case Pattern:
                return mLockPatternUtils.isLockPatternEnabled();
            case Password:
            case PIN:
                return mLockPatternUtils.isLockPasswordEnabled();
            case SimPinPukMe1:
            case SimPinPukMe2:
            case Account:
                return true;
            case None:
                return false;
            default:
                //alarm boot ;Don't throw exception in this case
                //throw new IllegalStateException("Unknown security mode " + mode); cuiqian
                return true;
        }
    }
// End of Vanzo:xulei

    public interface ShowListener {
        void onShown(IBinder windowToken);
    };

    /**
     * @param context Used to create views.
     * @param viewManager Keyguard will be attached to this.
     * @param callback Used to notify of changes.
     * @param lockPatternUtils
     */
    public KeyguardViewManager(Context context, ViewManager viewManager,
            KeyguardViewMediator.ViewMediatorCallback callback,
            LockPatternUtils lockPatternUtils) {
        mContext = context;
        mViewManager = viewManager;
        mViewMediatorCallback = callback;
        mLockPatternUtils = lockPatternUtils;
/* Vanzo:xulei on: Tue, 12 Nov 2013 17:30:42 +0800
 * add yunlan locker
 */
        if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
            mSecurityModel = new KeyguardSecurityModel(mContext);
            registerFileObserver();
        }
// End of Vanzo:xulei


/* Vanzo:xulei on: Tue, 22 Oct 2013 10:35:19 +0800
 * add cooee locker
 */
        if (FeatureOption.VANZO_COOEE_LOCKSCREEN_SUPPORT) {
            mSecurityModel = new KeyguardSecurityModel(context);
        }
// End of Vanzo:xulei
    }

    /**
     * Show the keyguard.  Will handle creating and attaching to the view manager
     * lazily.
     */
    public synchronized void show(Bundle options) {
        if (DEBUG) KeyguardUtils.xlogD(TAG, "show(); mKeyguardView=" + mKeyguardView);
/* Vanzo:xulei on: Tue, 12 Nov 2013 17:31:07 +0800
 * add yunlan locker
 */
        if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
            isYunlanOpen = getYunlanLockEnable(mContext);
        }
// End of Vanzo:xulei
        boolean enableScreenRotation = shouldEnableScreenRotation();
        if (DEBUG) KeyguardUtils.xlogD(TAG, "show() query screen rotation after");

        /// M: Incoming Indicator for Keyguard Rotation @{
        KeyguardUpdateMonitor.getInstance(mContext).setQueryBaseTime();
        /// @}
        maybeCreateKeyguardLocked(enableScreenRotation, false, options);
        
        if (DEBUG) KeyguardUtils.xlogD(TAG, "show() maybeCreateKeyguardLocked finish");
        
        maybeEnableScreenRotation(enableScreenRotation);

        // Disable common aspects of the system/status/navigation bars that are not appropriate or
        // useful on any keyguard screen but can be re-shown by dialogs or SHOW_WHEN_LOCKED
        // activities. Other disabled bits are handled by the KeyguardViewMediator talking
        // directly to the status bar service.
        final int visFlags = View.STATUS_BAR_DISABLE_HOME;
        if (DEBUG) KeyguardUtils.xlogD(TAG, "show:setSystemUiVisibility(" + Integer.toHexString(visFlags)+")");
        mKeyguardHost.setSystemUiVisibility(visFlags);
/* Vanzo:xulei on: Tue, 12 Nov 2013 21:38:16 +0800
 * add yunlan locker
 */
        if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
            updateWinLayoutP();
        }
// End of Vanzo:xulei
        mViewManager.updateViewLayout(mKeyguardHost, mWindowLayoutParams);
        mKeyguardHost.setVisibility(View.VISIBLE);
        mKeyguardView.show();
        mKeyguardView.requestFocus();
        if (DEBUG) KeyguardUtils.xlogD(TAG, "show() exit; mKeyguardView=" + mKeyguardView);
    }

    private boolean shouldEnableScreenRotation() {
        Resources res = mContext.getResources();
        return SystemProperties.getBoolean("lockscreen.rot_override",false)
                || res.getBoolean(com.android.internal.R.bool.config_enableLockScreenRotation);
    }

    class ViewManagerHost extends FrameLayout {
        
        public ViewManagerHost(Context context) {
            super(context);
/* Vanzo:Kern on: Mon, 23 Sep 2013 11:11:44 +0800
 * for fullscreen lockpaper
            setFitsSystemWindows(true);
 */
            setFitsSystemWindows(false);
// End of Vanzo: Kern
            /// M: Save initial config when view created
            mCreateOrientation = context.getResources().getConfiguration().orientation;
        }

        @Override
        protected boolean fitSystemWindows(Rect insets) {
            Log.v("TAG", "bug 7643792: fitSystemWindows(" + insets.toShortString() + ")");
            return super.fitSystemWindows(insets);
        }

        @Override
        protected void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
            if (DEBUG) {
                KeyguardUtils.xlogD(TAG, "onConfigurationChanged, old orientation=" + mCreateOrientation +
                        ", new orientation=" + newConfig.orientation);
            }
            /// M: Optimization, only create views when orientation changed
            if (mCreateOrientation != newConfig.orientation) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (KeyguardViewManager.this) {
                            if (mKeyguardHost.getVisibility() == View.VISIBLE) {
                                // only propagate configuration messages if we're currently showing
                                maybeCreateKeyguardLocked(shouldEnableScreenRotation(), true, null);
                            } else {
                                if (DEBUG) KeyguardUtils.xlogD(TAG, "onConfigurationChanged: view not visible");
                            }
                        }
                    }
                });
            } else {
                if (DEBUG) KeyguardUtils.xlogD(TAG, "onConfigurationChanged: orientation not changed");
            }
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent event) {
            if (mKeyguardView != null) {
                // Always process back and menu keys, regardless of focus
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    int keyCode = event.getKeyCode();
                    if (keyCode == KeyEvent.KEYCODE_BACK && mKeyguardView.handleBackKey()) {
                        return true;
                    } else if (keyCode == KeyEvent.KEYCODE_MENU && mKeyguardView.handleMenuKey()) {
                        return true;
                    }
                }
                // Always process media keys, regardless of focus
                /// M: [ALPS00601974] Avoid dispatch keyevent twice.
                return mKeyguardView.dispatchKeyEvent(event);
            }
            return super.dispatchKeyEvent(event);
        }
    }

    SparseArray<Parcelable> mStateContainer = new SparseArray<Parcelable>();

    private void maybeCreateKeyguardLocked(boolean enableScreenRotation, boolean force,
            Bundle options) {
        final boolean isActivity = (mContext instanceof Activity); // for test activity

        if (mKeyguardHost != null) {
            mKeyguardHost.saveHierarchyState(mStateContainer);
        }

        if (mKeyguardHost == null) {
            if (DEBUG) KeyguardUtils.xlogD(TAG, "keyguard host is null, creating it...");

            mKeyguardHost = new ViewManagerHost(mContext);

            int flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                    | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR
                    | WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN
                    | WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER;

            /// M: Modify to support DM lock, hide statusbr when dm lock power on @{
            KeyguardUpdateMonitor monitor = KeyguardUpdateMonitor.getInstance(mContext);
            if (monitor.dmIsLocked()) { //in the first created
                if (DEBUG) KeyguardUtils.xlogD(TAG, "show(); dmIsLocked ");
                flags &= ~WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN;
                flags |= WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
                flags |= WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR;
            } else if (KeyguardUpdateMonitor.isAlarmBoot()) {
                if (DEBUG) KeyguardUtils.xlogD(TAG, "show(); AlarmBoot ");
/* Vanzo:Kern on: Mon, 23 Sep 2013 16:13:58 +0800
 * for fullscreen lockpaper
                flags &= ~WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
 */
// End of Vanzo: Kern
                flags &= ~WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR;
                flags |= WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN;
            }
            /// M: @}
            if (!mNeedsInput) {
                flags |= WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
            }
            if (ActivityManager.isHighEndGfx()) {
                flags |= WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED;
            }

            final int stretch = ViewGroup.LayoutParams.MATCH_PARENT;
            final int type = isActivity ? WindowManager.LayoutParams.TYPE_APPLICATION
                    : WindowManager.LayoutParams.TYPE_KEYGUARD;
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams(
                    stretch, stretch, type, flags, PixelFormat.TRANSLUCENT);
            lp.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN;
            lp.windowAnimations = com.android.internal.R.style.Animation_LockScreen;
            if (ActivityManager.isHighEndGfx()) {
                lp.flags |= WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED;
                lp.privateFlags |=
                        WindowManager.LayoutParams.PRIVATE_FLAG_FORCE_HARDWARE_ACCELERATED;
            }
            lp.privateFlags |= WindowManager.LayoutParams.PRIVATE_FLAG_SET_NEEDS_MENU_KEY;
            if (isActivity) {
                lp.privateFlags |= WindowManager.LayoutParams.PRIVATE_FLAG_SHOW_FOR_ALL_USERS;
            }
            /// M: Poke user activity when operating Keyguard
            //lp.inputFeatures |= WindowManager.LayoutParams.INPUT_FEATURE_DISABLE_USER_ACTIVITY;
            lp.setTitle(isActivity ? "KeyguardMock" : "Keyguard");
            mWindowLayoutParams = lp;
/* Vanzo:xulei on: Tue, 12 Nov 2013 17:31:20 +0800
 * add yunlan locker
 */
            if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
                updateWinLayoutP();
            }
// End of Vanzo:xulei
            mViewManager.addView(mKeyguardHost, lp);
        }

        Log.i("coco_reset","maybeCreateKeyguardLocked,force:"+force +",mKeyguardView"+mKeyguardView);

        /// M: If force and keyguardView is not null, we should relase memory hold by old keyguardview
        if (force && mKeyguardView != null) {
            mKeyguardView.cleanUp();
/* Vanzo:xulei on: Tue, 22 Oct 2013 10:38:40 +0800
 * add cooee locker
 */
            if (FeatureOption.VANZO_COOEE_LOCKSCREEN_SUPPORT) {
                mKeyguardHost.removeView(mKeyguardView);
                mKeyguardView = null;
            }
// End of Vanzo:xulei
        }

/* Vanzo:xulei on: Tue, 22 Oct 2013 10:39:08 +0800
 * add cooee locker
        if (force || mKeyguardView == null) {
            inflateKeyguardView(options);
            mKeyguardView.requestFocus();
        }
 */

        if (FeatureOption.VANZO_COOEE_LOCKSCREEN_SUPPORT) {
            if(mLockPatternUtils.isCooeeLockScreenEnabled() && !isSecure() && ((mKeyguardView = (KeyguardHostView)getCoCoHostView())!= null)){
                Log.i("coco","mKeyguardView:"+mKeyguardView);
                mKeyguardHost.addView(mKeyguardView);
            } else {

                if (force || mKeyguardView == null) {
                    inflateKeyguardView(options);
                }
            }
        } else {
            if (force || mKeyguardView == null) {
                inflateKeyguardView(options);
            }
        }
// End of Vanzo:xulei
        updateUserActivityTimeoutInWindowLayoutParams();
/* Vanzo:xulei on: Tue, 12 Nov 2013 17:31:38 +0800
 * add yunlan locker
 */
        if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
            updateWinLayoutP();
        }
// End of Vanzo:xulei
        mViewManager.updateViewLayout(mKeyguardHost, mWindowLayoutParams);

        mKeyguardHost.restoreHierarchyState(mStateContainer);
    }

    private void inflateKeyguardView(Bundle options) {
        /// M: add for power-off alarm @{
        int resId = R.id.keyguard_host_view;
        int layoutId = R.layout.keyguard_host_view;
        if(KeyguardUpdateMonitor.isAlarmBoot()){
            layoutId = com.mediatek.internal.R.layout.power_off_alarm_host_view;
            resId = com.mediatek.internal.R.id.keyguard_host_view;
        }
        /// @}
        View v = mKeyguardHost.findViewById(resId);
        if (v != null) {
            mKeyguardHost.removeView(v);
        }
        // TODO: Remove once b/7094175 is fixed
        if (false) Slog.d(TAG, "inflateKeyguardView: b/7094175 mContext.config="
                + mContext.getResources().getConfiguration());
        
        /// M: Save new orientation
        mCreateOrientation = mContext.getResources().getConfiguration().orientation;
        
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(layoutId, mKeyguardHost, true);
        mKeyguardView = (KeyguardHostView) view.findViewById(resId);
        mKeyguardView.setLockPatternUtils(mLockPatternUtils);
        mKeyguardView.setViewMediatorCallback(mViewMediatorCallback);

        // HACK
        // The keyguard view will have set up window flags in onFinishInflate before we set
        // the view mediator callback. Make sure it knows the correct IME state.
        if (mViewMediatorCallback != null) {
            KeyguardPasswordView kpv = (KeyguardPasswordView) mKeyguardView.findViewById(
                    R.id.keyguard_password_view);

            if (kpv != null) {
                mViewMediatorCallback.setNeedsInput(kpv.needsInput());
            }
        }

        /// Extract this block to a single function
        updateKeyguardViewFromOptions(options);
    }

    public void updateUserActivityTimeout() {
        updateUserActivityTimeoutInWindowLayoutParams();

/* Vanzo:xulei on: Tue, 12 Nov 2013 17:32:52 +0800
 * add yunlan locker
 */
        if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
            updateWinLayoutP();
        }
// End of Vanzo:xulei
        mViewManager.updateViewLayout(mKeyguardHost, mWindowLayoutParams);
    }

    private void updateUserActivityTimeoutInWindowLayoutParams() {
        // Use the user activity timeout requested by the keyguard view, if any.
        if (mKeyguardView != null) {
            long timeout = mKeyguardView.getUserActivityTimeout();
            if (timeout >= 0) {
                mWindowLayoutParams.userActivityTimeout = timeout;
                return;
            }
        }

        // Otherwise, use the default timeout.
        mWindowLayoutParams.userActivityTimeout = KeyguardViewMediator.AWAKE_INTERVAL_DEFAULT_MS;
    }

    private void maybeEnableScreenRotation(boolean enableScreenRotation) {
        // TODO: move this outside
        if (enableScreenRotation) {
            if (DEBUG) KeyguardUtils.xlogD(TAG, "Rotation sensor for lock screen On!");
            mWindowLayoutParams.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_USER;
        } else {
            if (DEBUG) KeyguardUtils.xlogD(TAG, "Rotation sensor for lock screen Off!");
            mWindowLayoutParams.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR;
        }
/* Vanzo:xulei on: Tue, 12 Nov 2013 17:33:03 +0800
 * add yunlan locker
 */
        if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
            updateWinLayoutP();
        }
// End of Vanzo:xulei
        mViewManager.updateViewLayout(mKeyguardHost, mWindowLayoutParams);
    }

    public void setNeedsInput(boolean needsInput) {
        mNeedsInput = needsInput;
        if (mWindowLayoutParams != null) {
            if (needsInput) {
                mWindowLayoutParams.flags &=
                    ~WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
            } else {
                mWindowLayoutParams.flags |=
                    WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
            }
/* Vanzo:xulei on: Tue, 12 Nov 2013 17:33:03 +0800
 * add yunlan locker
 */
            if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
                updateWinLayoutP();
            }
// End of Vanzo:xulei

            try {
                mViewManager.updateViewLayout(mKeyguardHost, mWindowLayoutParams);
            } catch (java.lang.IllegalArgumentException e) {
                // TODO: Ensure this method isn't called on views that are changing...
                Log.w(TAG,"Can't update input method on " + mKeyguardHost + " window not attached");
            }
        }
    }

    /**
     * Reset the state of the view.
     */
    public synchronized void reset(Bundle options) {
        // User might have switched, check if we need to go back to keyguard
        // TODO: It's preferable to stay and show the correct lockscreen or unlock if none
        /// M: Avoid remove/add view when mKeyguardView is not null
        /// M: Also check if Dm lock is enabled, if dm lock is on, we should also force reset
        boolean forceReCreate = false;

/* Vanzo:xulei on: Tue, 22 Oct 2013 10:40:08 +0800
 * add cooee locker
 */
        if (FeatureOption.VANZO_COOEE_LOCKSCREEN_SUPPORT) {
            if (mKeyguardView != null && mKeyguardView instanceof CoCoKeyguardHostView) {
                maybeCreateKeyguardLocked(shouldEnableScreenRotation(), true, options);
                return;
            }
        }
// End of Vanzo:xulei
        if (options != null) {
            if (options.getBoolean(KeyguardViewMediator.RESET_FOR_DM_LOCK)
                || options.getBoolean(LockPatternUtils.KEYGUARD_SHOW_USER_SWITCHER)) {
                forceReCreate = true;
            }
        }
        if (DEBUG) KeyguardUtils.xlogD(TAG, "reset() mKeyguardView=" + mKeyguardView + ", forceReCreate=" + forceReCreate);
        if (!forceReCreate && mKeyguardView != null) {
            mKeyguardView.reset();
            updateKeyguardViewFromOptions(options);
        } else {
            maybeCreateKeyguardLocked(shouldEnableScreenRotation(), true, options);
        }
    }

    public synchronized void onScreenTurnedOff() {
        if (DEBUG) KeyguardUtils.xlogD(TAG, "onScreenTurnedOff()");
        mScreenOn = false;
        if (mKeyguardView != null) {
            mKeyguardView.onScreenTurnedOff();
        }
    }

    public synchronized void onScreenTurnedOn(
            final KeyguardViewManager.ShowListener showListener) {
        if (DEBUG) Log.d(TAG, "onScreenTurnedOn()");
        mScreenOn = true;
        if (mKeyguardView != null) {
            mKeyguardView.onScreenTurnedOn();

            if (mCreateOrientation != mContext.getResources().getConfiguration().orientation) {
                if (DEBUG) KeyguardUtils.xlogD(TAG, "onScreenTurnedOn orientation is different, recreate it. mCreateOrientation="+mCreateOrientation
                    +", newConfig="+mContext.getResources().getConfiguration().orientation);
                maybeCreateKeyguardLocked(shouldEnableScreenRotation(), true, null);
            }
            // Caller should wait for this window to be shown before turning
            // on the screen.
            if (showListener != null) {
                if (mKeyguardHost.getVisibility() == View.VISIBLE) {
                    // Keyguard may be in the process of being shown, but not yet
                    // updated with the window manager...  give it a chance to do so.
                    if (DEBUG) KeyguardUtils.xlogD(TAG, "onScreenTurnedOn mKeyguardView visible, post runnable");
                    mKeyguardHost.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mKeyguardHost.getVisibility() == View.VISIBLE) {
                                if (DEBUG) KeyguardUtils.xlogD(TAG, "onScreenTurnedOn mKeyguardView visible, showListener.onShown");
                                showListener.onShown(mKeyguardHost.getWindowToken());
                            } else {
                                if (DEBUG) KeyguardUtils.xlogD(TAG, "onScreenTurnedOn mKeyguardView !visible showListener.onShown");
                                showListener.onShown(null);
                            }
                        }
                    });
                } else {
                    if (DEBUG) KeyguardUtils.xlogD(TAG, "onScreenTurnedOn else mKeyguardView !visible showListener.onShown");
                    showListener.onShown(null);
                }
            }
        } else if (showListener != null) {
            KeyguardUtils.xlogD(TAG, "onScreenTurnedOn mKeyguardView=null showListener.onShown");
            showListener.onShown(null);
        }
    }

    public synchronized void verifyUnlock() {
        if (DEBUG) KeyguardUtils.xlogD(TAG, "verifyUnlock()");
        show(null);
        mKeyguardView.verifyUnlock();
    }

    /**
     * A key has woken the device.  We use this to potentially adjust the state
     * of the lock screen based on the key.
     *
     * The 'Tq' suffix is per the documentation in {@link android.view.WindowManagerPolicy}.
     * Be sure not to take any action that takes a long time; any significant
     * action should be posted to a handler.
     *
     * @param keyCode The wake key.  May be {@link KeyEvent#KEYCODE_UNKNOWN} if waking
     * for a reason other than a key press.
     */
    public boolean wakeWhenReadyTq(int keyCode) {
        if (DEBUG) KeyguardUtils.xlogD(TAG, "wakeWhenReady(" + keyCode + ")");
        if (mKeyguardView != null) {
            mKeyguardView.wakeWhenReadyTq(keyCode);
            return true;
        }
        KeyguardUtils.xlogD(TAG, "mKeyguardView is null in wakeWhenReadyTq");
        return false;
    }

    /**
     * Hides the keyguard view
     */
    public synchronized void hide() {
        if (DEBUG) KeyguardUtils.xlogD(TAG, "hide() mKeyguardView=" + mKeyguardView);

        if (mKeyguardHost != null) {
            mKeyguardHost.setVisibility(View.GONE);

            // We really only want to preserve keyguard state for configuration changes. Hence
            // we should clear state of widgets (e.g. Music) when we hide keyguard so it can
            // start with a fresh state when we return.
            mStateContainer.clear();

            // Don't do this right away, so we can let the view continue to animate
            // as it goes away.
            if (mKeyguardView != null) {
                final KeyguardViewBase lastView = mKeyguardView;
                mKeyguardView = null;
                mKeyguardHost.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (KeyguardViewManager.this) {
                            if (DEBUG) KeyguardUtils.xlogD(TAG, "hide() runnable lastView=" + lastView);
                            lastView.cleanUp();
                            mKeyguardHost.removeView(lastView);
                        }
                    }
                }, 500);
            }
        }
    }

    /**
     * Dismisses the keyguard by going to the next screen or making it gone.
     */
    public synchronized void dismiss() {
        KeyguardUtils.xlogD(TAG, "dismiss mScreenOn=" + mScreenOn);
        if (mScreenOn) {
            mKeyguardView.dismiss();
        }
    }

    /**
     * @return Whether the keyguard is showing
     */
    public synchronized boolean isShowing() {
        return (mKeyguardHost != null && mKeyguardHost.getVisibility() == View.VISIBLE);
    }

    public void showAssistant() {
        if (mKeyguardView != null) {
            mKeyguardView.showAssistant();
        }
    }

    /**
     * M: Update layout for KeyguardView, for DM lock/unlock to show/hide statsubar
     * 
     * @param dmLock
     */
    public void reLayoutScreen(boolean dmLock) {
        if (mWindowLayoutParams != null) {
            KeyguardUtils.xlogD(TAG, "reLayoutScreen, dmLock=" + dmLock + ", isAlarmBoot=" + KeyguardUpdateMonitor.isAlarmBoot());
            if (dmLock) {
                mWindowLayoutParams.flags &= ~WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN;
                mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
                mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR;
            } else if (KeyguardUpdateMonitor.isAlarmBoot()) {
/* Vanzo:Kern on: Mon, 23 Sep 2013 16:14:03 +0800
 * for fullscreen lockpaper
                mWindowLayoutParams.flags &= ~WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
 */
// End of Vanzo: Kern
                mWindowLayoutParams.flags &= ~WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR;
                mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN;
            } else {
                mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
                mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR;
                mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN;
            }
/* Vanzo:xulei on: Tue, 12 Nov 2013 17:32:52 +0800
 * add yunlan locker
 */
            if (FeatureOption.VANZO_YUNLAN_LOCKSCREEN_SUPPORT) {
                updateWinLayoutP();
            }
// End of Vanzo:xulei
            mViewManager.updateViewLayout(mKeyguardHost, mWindowLayoutParams);
        }
    }

    private void updateKeyguardViewFromOptions(Bundle options) {
        if (options != null) {
            int widgetToShow = options.getInt(LockPatternUtils.KEYGUARD_SHOW_APPWIDGET,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            if (widgetToShow != AppWidgetManager.INVALID_APPWIDGET_ID) {
                mKeyguardView.goToWidget(widgetToShow);
            }
        }
    }    

    /// M: add for ipo shut down update process
    public void ipoShutDownUpdate() {
        if (null != mKeyguardView) {
            mKeyguardView.ipoShutDownUpdate();
        }
    }

    /**
     * M: add for power-off alarm
     *
     * @return
     */
    public boolean isAlarmUnlockScreen() {
        if (null != mKeyguardView) {
            return mKeyguardView.isAlarmUnlockScreen();
        }
        return false;
    }
    
    // M: Save current orientation, so that we will only recreate views when orientation changed
    private int mCreateOrientation;

/* Vanzo:xulei on: Tue, 12 Nov 2013 17:35:21 +0800
 * add yunlan locker
 */
    public static boolean isYunlanOpen = true;

    public boolean getYunlanLockEnable(Context context) {
        boolean res = mLockPatternUtils.getYunlanLockScreenEnabled();
        if (YUNLAN_DEBUG) Log.d(TAG, " xu ---------res="+res);
        return res;
    }

    private void updateWinLayoutP() {
        if (YUNLAN_DEBUG)
            Log.d(TAG,
                    " -------------yunlan--------updateWinLayoutP--mSecurityModel="
                    + mSecurityModel);
        if (mSecurityModel.getSecurityMode() == KeyguardSecurityModel.SecurityMode.None
                && isYunlanOpen) {
            mWindowLayoutParams.windowAnimations = 0;
            mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
            mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
            if ((mWindowLayoutParams.flags & WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER) != 0) {
                mWindowLayoutParams.flags &= ~WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER;
            }
        } else {
            if ((mWindowLayoutParams.flags & WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER) == 0) {
                mWindowLayoutParams.flags |= WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER;
            }
        }
    }

    public static View mPreloadYunlanLockScreen = null;
    public static String savePackageName = null;
    public static BitmapDrawable mYunlanDrawable = null;
    public static int isSystemWallpaper = 0;
    public static BitmapDrawable mYunlanDrawable2 = null;
    public static boolean isLock = false;
    private Bitmap originalBgBitmap = null;

    public void preInitRes(Boolean isSystem, String filePath, String pkgName,
            ArrayList<String> list) {
        synchronized (this) {
            BitmapDrawable tmpBitmapDrawable = null;
            savePackageName = pkgName;
            if (isSystem) {
                if (originalBgBitmap != null && !originalBgBitmap.isRecycled()) {
                    originalBgBitmap.recycle();
                }
                originalBgBitmap = null;
                originalBgBitmap = getSystemLockScreenPaper(filePath);
                if (originalBgBitmap != null) {
                    tmpBitmapDrawable = new BitmapDrawable(mContext.getResources(),
                            originalBgBitmap);
                }
                isSystemWallpaper = 1;
            }

            if (YUNLAN_DEBUG)
                Log.d(TAG, " -----yunlan-----pkgName=" + pkgName);

            if (pkgName != null && list != null && tmpBitmapDrawable == null) {

                Resources mThemeRes = null;
                if (!TextUtils.isEmpty(pkgName)) {
                    mThemeRes = getThemeResources(mContext.getPackageManager(),
                            pkgName);
                }

                if (mThemeRes != null) {
                    tmpBitmapDrawable = (BitmapDrawable) (getDrawable(mThemeRes,
                                pkgName, list.get(0)));
                    isSystemWallpaper = 0;
                }

            }

            if (isLock) {
                if (YUNLAN_DEBUG)
                    Log.d(TAG, " -----yunlan-----isLock=" + isLock);
                if (mYunlanDrawable2 != null) {
                    mYunlanDrawable2.setCallback(null);
                    mYunlanDrawable2 =  null;
                }
                mYunlanDrawable2 = tmpBitmapDrawable;
            } else {
                if (YUNLAN_DEBUG)
                    Log.d(TAG, " -----yunlan-----isLock=" + isLock);
                if (mYunlanDrawable != null) {
                    mYunlanDrawable.setCallback(null);
                    mYunlanDrawable = null;
                }
                mYunlanDrawable = tmpBitmapDrawable;
            }
            if (DEBUG) Log.d(TAG, " -----yunlan-----isSystemWallpaper="+isSystemWallpaper+" mYunlanDrawable="+mYunlanDrawable
                    +"  KeyguardViewManager.isLock="+KeyguardViewManager.isLock);
        }
    }

    public Resources getThemeResources(PackageManager manager,
            String packageName) {
        try {
            return manager.getResourcesForApplication(packageName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Drawable getDrawable(Resources themeRes, String pkgName,
            String resName) {
        Drawable d = null;
        if (YUNLAN_DEBUG)
            Log.d(TAG, "--------xu----pkgName=" + pkgName + "   resName="
                    + resName);
        try {
            int id = themeRes.getIdentifier(resName, "drawable", pkgName);
            d = themeRes.getDrawable(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return d;
    }

    public Bitmap getSystemLockScreenPaper(String filePath) {

        boolean res = chmodFile(filePath);

        if (YUNLAN_DEBUG)
            Log.d(TAG, "xu----------------res=" + res);
        if (!res)
            return null;

        File file = new File(filePath);
        if (YUNLAN_DEBUG)
            Log.d(TAG, "xu----------------file.exists()=" + file.exists());
        if (!file.exists()) {
            return null;
        }
        ParcelFileDescriptor fd = null;
        Bitmap bitmap = null;
        try {
            fd = ParcelFileDescriptor.open(file,
                    ParcelFileDescriptor.MODE_READ_ONLY);
            bitmap = BitmapFactory.decodeFileDescriptor(fd.getFileDescriptor(),
                    null, null);
        } catch (FileNotFoundException e) {
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "xu------LockScreen wallpaper not exists,set default wallpaper");
            bitmap = null;
        } finally {
            try {
                fd.close();
            } catch (IOException ioex) {
                if (YUNLAN_DEBUG)
                    Log.d(TAG,
                            "xu-----Occur IOException when close FileDescriptor");
                ioex.printStackTrace();
            }
        }
        return bitmap;
    }

    public boolean chmodFile(String toFile) {
        boolean ret = true;
        try {
            String[] cmds = new String[3];
            cmds[0] = "sh";
            cmds[1] = "-c";
            Runtime rt = Runtime.getRuntime();
            cmds[2] = ("chmod 777 " + toFile);
            rt.exec(cmds);
        } catch (IOException e) {
            e.printStackTrace();
            ret = false;
        }
        return ret;
    }

    public View getCurScreen(Context context, Configuration configuration,
            KeyguardSecurityCallback callback, BitmapDrawable preloadBGBmp,
            int preloadType) {
        View view = null;
        Context mmsCtx = null;
        try {
            if (YUNLAN_DEBUG)
                Log.d(TAG, "-----yunlan------getCurScreen 1");
            mmsCtx = context.createPackageContext("com.yunlan.syslockmarket",
                    Context.CONTEXT_INCLUDE_CODE
                    | Context.CONTEXT_IGNORE_SECURITY);
            if (YUNLAN_DEBUG)
                Log.d(TAG, "-----yunlan------getCurScreen 2");

        } catch (NameNotFoundException e) {
            e.printStackTrace();
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "-----yunlan------getCurScreen NameNotFoundException");

            return null;
        }
        Class<?> maClass = null;
        try {
            if (YUNLAN_DEBUG)
                Log.d(TAG, "-----yunlan------getCurScreen 3");
            maClass = Class.forName(
                    "com.yunlan.syslockmarket.YLLockScreenView", true,
                    mmsCtx.getClassLoader());
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "wang-------------------------------getCurScreen thememarket 4");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "-----yunlan------getCurScreen 4 ClassNotFoundException");
            return null;
        }

        Class[] params = new Class[6];
        params[0] = Context.class;
        params[1] = Context.class;
        params[2] = Configuration.class;
        params[3] = KeyguardSecurityCallback.class;
        params[4] = BitmapDrawable.class;
        params[5] = Integer.class;
        Constructor<?> con = null;
        try {
            if (YUNLAN_DEBUG)
                Log.d(TAG, "-----yunlan------getCurScreen 5");
            con = maClass.getConstructor(params);
            // if (YUNLAN_DEBUG) Log.d(TAG,"-----yunlan------getCurScreen 5 " +
            // params);
            view = (View) con.newInstance(mmsCtx, context, null, null,
                    preloadBGBmp, preloadType);
            Method f = maClass.getDeclaredMethod("getInitFinish", null);
            Boolean init = (Boolean) f.invoke(view, null);
            if (YUNLAN_DEBUG)
                Log.d(TAG, "-----yunlan------getCurScreen init" + init);
            if (!init) {
                view = null;
            }
            if (YUNLAN_DEBUG)
                Log.d(TAG, "-----yunlan------getCurScreen 6");
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "-----yunlan------getCurScreen err1" + e.getMessage());
            return null;
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "-----yunlan------getCurScreen err2" + e.getMessage());
            return null;
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "-----yunlan------getCurScreen err3" + e.getMessage());
            return null;
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "-----yunlan------getCurScreen err4" + e.getMessage());
            return null;
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "-----yunlan------getCurScreen err5" + e.getMessage());
            return null;
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if (YUNLAN_DEBUG)
                Log.d(TAG,
                        "-----yunlan------getCurScreen err6" + e.getMessage());
            return null;
        }
        if (YUNLAN_DEBUG)
            Log.d(TAG, "-----yunlan------getCurScreen 7");

        return view;
    }

    public static SDCardFileObserver mFileObserver = null;
    private final String unlockWallpaperPathString = "/data/data/com.android.wallpaper.multipicker/files/lockpaper";

    public void registerFileObserver() {
        if (YUNLAN_DEBUG)
            Log.d(TAG, "-----yunlan--registerFileObserver ");
        mFileObserver = new SDCardFileObserver(unlockWallpaperPathString);
        if (mFileObserver != null)
            mFileObserver.startWatching();
    }

    class SDCardFileObserver extends FileObserver {
        // mask:指定要监听的事件类型，默认为FileObserver.ALL_EVENTS
        public SDCardFileObserver(String path, int mask) {
            super(path, mask);
        }

        public SDCardFileObserver(String path) {
            super(path);
        }

        @Override
            public void onEvent(int event, String path) {
                final int action = event & FileObserver.ALL_EVENTS;
                switch (action) {
                    case FileObserver.ACCESS:
                        // System.out.println("event: 文件或目录被访问, path: " + path);
                        break;

                    case FileObserver.DELETE:
                        // System.out.println("event: 文件或目录被删除, path: " + path);
                        break;

                    case FileObserver.OPEN:
                        // System.out.println("event: 文件或目录被打开, path: " + path);
                        break;

                    case FileObserver.MODIFY:
                        // if (YUNLAN_DEBUG) Log.d(TAG,"-----yunlan--event: path: " + path);
                        break;
                    case FileObserver.CLOSE_WRITE:
                        if (YUNLAN_DEBUG)
                            Log.d(TAG, "-----yunlan--close event: path: " + path);
                        preInitRes(true, unlockWallpaperPathString, null, null);

                        break;
                }
            }

    }
// End of Vanzo:xulei
}
