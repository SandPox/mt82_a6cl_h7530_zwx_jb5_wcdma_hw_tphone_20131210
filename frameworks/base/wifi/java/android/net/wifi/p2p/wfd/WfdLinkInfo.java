/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net.wifi.p2p.wfd;

import android.os.Parcelable;
import android.os.Parcel;
import android.util.Log;

/**
* A class representing Wi-Fi Display link status
* @hide
*/
public class WfdLinkInfo implements Parcelable {
    public String interfaceAddress = "";
    public String linkInfo = "";
    
    public WfdLinkInfo() {
    }
    
    public String toString() {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("interfaceAddress=").append(interfaceAddress);
        sbuf.append(" linkInfo=").append(linkInfo);
        return sbuf.toString();
    }
    
    public WfdLinkInfo(WfdLinkInfo source) {
        if (source != null) {
            interfaceAddress = source.interfaceAddress;
            linkInfo = source.linkInfo;
        }
    }
    
    /** Implement the Parcelable interface */
    public int describeContents() {
        return 0;
    }
    
    /** Implement the Parcelable interface */
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(interfaceAddress);
        dest.writeString(linkInfo);
    }
    
    /** Implement the Parcelable interface */
    public static final Creator<WfdLinkInfo> CREATOR =
        new Creator<WfdLinkInfo>() {
            public WfdLinkInfo createFromParcel(Parcel in) {
                WfdLinkInfo info = new WfdLinkInfo();
                info.interfaceAddress = in.readString();
                info.linkInfo = in.readString();
                return info;
            }

            public WfdLinkInfo[] newArray(int size) {
                return new WfdLinkInfo[size];
            }
        };
        

}