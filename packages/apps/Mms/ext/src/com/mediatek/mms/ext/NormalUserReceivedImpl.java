/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.mms.ext;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SqliteWrapper;
import android.net.Uri;

import com.mediatek.encapsulation.MmsLog;
import android.text.TextUtils;
import android.content.BroadcastReceiver;
import java.util.ArrayList;
import java.io.ByteArrayInputStream;   
import java.io.ByteArrayOutputStream;   
import java.io.IOException;   
import java.io.ObjectInputStream;   
import java.io.ObjectOutputStream; 
import android.os.UserHandle;
import android.provider.Telephony.Sms.Intents;

/// M:For MultiUser 3gdatasms, when receive SMS/MMS/CB in normal user @{
public class NormalUserReceivedImpl extends ContextWrapper implements INormalUserReceived {

    private static final String TAG = "Mms/NormalUserReceivedImpl";
    private Context mContext;
    private int mUserId = UserHandle.USER_OWNER;
    
    
    private void dispatchSmsPdus(byte[] smsPdu, String format, int simId) {
        MmsLog.d(TAG, "begin to dispatchSmsPdus");
        byte[][] pdus = new byte[1][];
        pdus[0] = smsPdu;
        
        Intent intent = new Intent(Intents.SMS_RECEIVED_ACTION);
        intent.putExtra("pdus", pdus);
        intent.putExtra("format", format);
        intent.putExtra("simId", simId);
        
        mContext.sendOrderedBroadcast(intent, "android.permission.RECEIVE_SMS");
        MmsLog.d(TAG, "end to dispatchSmsPdus");
    }
    
    private void dispatchMmsPdus(int simId, int transactionId, int pduType, byte[] header, byte[] data,
                                    String address, String service_center) {
        MmsLog.d(TAG, "begin to dispatchMmsPdus");
        
        Intent intent = new Intent(Intents.WAP_PUSH_RECEIVED_ACTION);
        intent.setType("application/vnd.wap.mms-message");
        intent.putExtra("transactionId", transactionId);
        intent.putExtra("pduType", pduType);
        intent.putExtra("header", header);
        intent.putExtra("data", data);
        intent.putExtra("address", address);
        intent.putExtra("service_center", service_center);
        intent.putExtra("simId", simId);
        
        mContext.sendOrderedBroadcast(intent, "android.permission.RECEIVE_MMS");
        MmsLog.d(TAG, "end to dispatchMmsPdus");
    }
    
    private void dispatchCBPdus(String action, byte[] cbPdu, int simId) {
        MmsLog.d(TAG, "begin to dispatchCBPdus");
        
        byte[][] pdus = new byte[1][];
        pdus[0] = cbPdu;
        String permission;
        Intent intent = new Intent(action);
        
        if (action.equals(Intents.SMS_CB_RECEIVED_ACTION)) {
            permission = "android.permission.RECEIVE_SMS";
        } else {
            MmsLog.e(TAG, "illegal action");
            return;
        }
        
        intent.putExtra("pdus", pdus);
        intent.putExtra("simId", simId);
        
        mContext.sendOrderedBroadcast(intent, permission);
        MmsLog.d(TAG, "end to dispatchCBPdus");
    }
    
    private void processReceivedSmsInUser() {
        Uri messageUri = null;
        ContentResolver resolver = mContext.getContentResolver();
        int id;
        byte[] smsPdu;
        String format;
        int simId;
        ArrayList idList = new ArrayList();
        
        Cursor cursor = SqliteWrapper.query(mContext, resolver, Uri.parse("content://usersms"),
                            new String[]{"_id", "pdus", "format", "simId"},
                            null, null, null);
        
        if (cursor == null || cursor.getCount() == 0) {
            MmsLog.e(TAG, "processReceivedSmsInUser cursor is null");
            return;
        }
        
        
        cursor.moveToFirst(); 
        for (int i = 0; i < cursor.getCount(); i++) {
            if (mUserId == UserHandle.USER_OWNER) {
                id = cursor.getInt(cursor.getColumnIndex("_id"));
                smsPdu = cursor.getBlob(cursor.getColumnIndex("pdus"));
                format = cursor.getString(cursor.getColumnIndex("format"));
                simId = cursor.getInt(cursor.getColumnIndex("simId"));
                
                idList.add(id);
                dispatchSmsPdus(smsPdu, format, simId);
                cursor.moveToNext();
                MmsLog.d(TAG, "move cursor, i = " + i);
            } else {
                break;
            }
        }  
        MmsLog.d(TAG, "processReceivedSmsInUser out of loop");     
        cursor.close();
        
        MmsLog.d(TAG, "begin to delete the message in temp db");
        for (int i = 0; i < idList.size(); i++) {
            MmsLog.d(TAG, "deleting the message in temp db, _id = " + idList.get(i));
            resolver.delete(Uri.parse("content://usersms"), "_id = ?", new String[]{String.valueOf(idList.get(i))});
        }
    }
    
    private void processReceivedMmsInUser() {
        Uri messageUri = null;
        ContentResolver resolver = mContext.getContentResolver();
        int id;
        int simId;
        int transactionId;
        int pduType;
        byte[] header;
        byte[] data;
        String address;
        String service_center;
        ArrayList idList = new ArrayList();
        
        Cursor cursor = SqliteWrapper.query(mContext, resolver, Uri.parse("content://usermms"),
                            null, null, null, null);
        
        if (cursor == null || cursor.getCount() == 0) {
            MmsLog.e(TAG, "processReceivedMmsInUser cursor is null");
            return;
        }
        
        
        cursor.moveToFirst(); 
        for (int i = 0; i < cursor.getCount(); i++) {
            if (mUserId == UserHandle.USER_OWNER) {
                id = cursor.getInt(cursor.getColumnIndex("_id"));
                simId = cursor.getInt(cursor.getColumnIndex("_id"));
                transactionId = cursor.getInt(cursor.getColumnIndex("transactionId"));
                pduType = cursor.getInt(cursor.getColumnIndex("pduType"));
                header = cursor.getBlob(cursor.getColumnIndex("header"));
                data = cursor.getBlob(cursor.getColumnIndex("data"));
                address = cursor.getString(cursor.getColumnIndex("address"));
                service_center = cursor.getString(cursor.getColumnIndex("service_center"));
                
                idList.add(id);
                dispatchMmsPdus(simId, transactionId, pduType, header, data, address, service_center);
                cursor.moveToNext();
                MmsLog.d(TAG, "move cursor, i = " + i);
            } else {
                break;
            }
        }  
        MmsLog.d(TAG, "processReceivedMmsInUser out of loop");     
        cursor.close();
        
        MmsLog.d(TAG, "begin to delete the message in temp db");
        for (int i = 0; i < idList.size(); i++) {
            MmsLog.d(TAG, "processReceivedMmsInUser deleting the message in temp db, _id = " + idList.get(i));
            resolver.delete(Uri.parse("content://usermms"), "_id = ?", new String[]{String.valueOf(idList.get(i))});
        }
    }
    
    private void processReceivedCBInUser() {
        Uri messageUri = null;
        ContentResolver resolver = mContext.getContentResolver();
        int id;
        byte[] smsPdu;
        String action;
        int simId;
        ArrayList idList = new ArrayList();
        
        Cursor cursor = SqliteWrapper.query(mContext, resolver, Uri.parse("content://usercb"),
                            null, null, null, null);
        
        if (cursor == null || cursor.getCount() == 0) {
            MmsLog.e(TAG, "processReceivedCBInUser cursor is null");
            return;
        }
        
        cursor.moveToFirst(); 
        for (int i = 0; i < cursor.getCount(); i++) {
            if (mUserId == UserHandle.USER_OWNER) {
                id = cursor.getInt(cursor.getColumnIndex("_id"));
                smsPdu = cursor.getBlob(cursor.getColumnIndex("pdus"));
                action = cursor.getString(cursor.getColumnIndex("action"));
                simId = cursor.getInt(cursor.getColumnIndex("simId"));
                
                idList.add(id);
                dispatchCBPdus(action, smsPdu, simId);
                cursor.moveToNext();
            } else {
                break;
            }
        }  
        MmsLog.d(TAG, "processReceivedCBInUser out of loop");     
        cursor.close();
        
        MmsLog.d(TAG, "begin to delete the message in temp db");
        for (int i = 0; i < idList.size(); i++) {
            MmsLog.d(TAG, "deleting the message in temp db, _id = " + idList.get(i));
            resolver.delete(Uri.parse("content://usercb"), "_id = ?", new String[]{String.valueOf(idList.get(i))});
        }
    }
    
    public NormalUserReceivedImpl(Context context) {
        super(context);
        mContext = context;
    }
    
    public void setCurrentUser(int userId) {
        MmsLog.d(TAG, "get into setCurrentUser, userId = " + userId);
        mUserId = userId;
    }
    
    public void handleUserSmsReceived(Intent intent) {
        MmsLog.d(TAG, "get into handleUserSmsReceived");
                
        Object[] objPdus = (Object[]) intent.getSerializableExtra("pdus");
        byte[] pdus = (byte[])objPdus[0];
        //byte[] pdus = intent.getByteArrayExtra("pdus");
        String format = intent.getStringExtra("format");
        int simId = intent.getIntExtra("simId", -1);
        MmsLog.d(TAG, "from intent simId = " + simId);
        
        ContentValues values = new ContentValues();
        values.put("pdus", pdus);
        values.put("format", format);
        values.put("simId", simId);
        
        ContentResolver resolver = mContext.getContentResolver();
        SqliteWrapper.insert(mContext, resolver, Uri.parse("content://usersms"), values);
    }
    
    public void handleUserMmsReceived(Intent intent) {
        MmsLog.d(TAG, "get into handleUserMmsReceived");
        
        int simId = intent.getIntExtra("simId", -1);
        int transactionId = intent.getIntExtra("transactionId", -1);
        int pduType = intent.getIntExtra("pduType", -1);
        byte[] header = intent.getByteArrayExtra("header");
        byte[] data = intent.getByteArrayExtra("data");
        String address = intent.getStringExtra("address");
        String service_center = intent.getStringExtra("service_center");
        
        ContentValues values = new ContentValues();
        values.put("simId", simId);
        values.put("transactionId", transactionId);
        values.put("pduType", pduType);
        values.put("header", header);
        values.put("data", data);
        values.put("address", address);
        values.put("service_center", service_center);
        
        ContentResolver resolver = mContext.getContentResolver();
        SqliteWrapper.insert(mContext, resolver, Uri.parse("content://usermms"), values);
        
        MmsLog.d(TAG, "end of handleUserMmsReceived");
    }
    
    public void handleUserCBReceived(Intent intent) {
        MmsLog.d(TAG, "get into handleUserCBReceived");
        
        int simId = intent.getIntExtra("simId", -1);
        Object[] objPdus = (Object[]) intent.getSerializableExtra("pdus");
        if (objPdus == null) {
            return;
        }
        byte[] pdus = (byte[])objPdus[0];
        
        ContentValues values = new ContentValues();
        values.put("action", intent.getAction());
        values.put("pdus", pdus);
        values.put("simId", simId);
        
        ContentResolver resolver = mContext.getContentResolver();
        SqliteWrapper.insert(mContext, resolver, Uri.parse("content://usercb"), values);
        
        MmsLog.d(TAG, "end to handleUserCBReceived");
    }

    public void handleSwitchToOwner() {
        MmsLog.d(TAG, "get into handleSwitchToOwner");
        
        processReceivedSmsInUser();
        processReceivedMmsInUser();
        processReceivedCBInUser();
    }
}
/// @}