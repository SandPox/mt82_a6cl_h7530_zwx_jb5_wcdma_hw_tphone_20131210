package com.mediatek.encapsulation.com.android.internal.telephony;

import android.os.ServiceManager;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.NeighboringCellInfo;
import android.telephony.CellInfo;
import com.android.internal.telephony.ITelephony;
import com.mediatek.encapsulation.EncapsulationConstant;
import com.mediatek.encapsulation.MmsLog;
import java.util.List;

//MTK-START [mtk04070][111117][ALPS00093395]MTK added
import android.os.Message;
//MTK-END [mtk04070][111117][ALPS00093395]MTK added

/**
 * ITelephony used to interact with the phone.  Mostly this is used by the
 * TelephonyManager class.  A few places are still using this directly.
 * Please clean them up if possible and use TelephonyManager instead.
 */
public class EncapsulatedTelephonyService {

    /** M: MTK reference ITelephony */
    private static ITelephony sTelephony;
    private static EncapsulatedTelephonyService sTelephonyService = new EncapsulatedTelephonyService();;

    private EncapsulatedTelephonyService() {}

    synchronized public static EncapsulatedTelephonyService getInstance() {
        if (sTelephony != null) {
            return sTelephonyService;
        } else {
            sTelephony = ITelephony.Stub.asInterface(ServiceManager.getService("phone"));
            if (null != sTelephony) {
                return sTelephonyService;
            } else {
                return null;
            }
        }
    }

    /**
     * Returns the IccCard type. Return "SIM" for SIM card or "USIM" for USIM card.
     */
    public String getIccCardType() throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.getIccCardType();
        } else {
            MmsLog.d("Encapsulation issue", "EncapsulatedTelephonyService -- getIccCardType()");
            return null;
        }
    }

    /**
     * Return ture if the ICC card is a test card
     */
    public boolean isTestIccCard() throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.isTestIccCard();
        } else {
            MmsLog.d("Encapsulation issue", "EncapsulatedTelephonyService -- isTestIccCard()");
            return false;
        }
    }

    /**
     * refer to getCallState();
     */
     public int getCallStateGemini(int simId) throws android.os.RemoteException {
         if (EncapsulationConstant.USE_MTK_PLATFORM) {
             return sTelephony.getCallStateGemini(simId);
         } else {
             return 0;
         }
     }

    /**
     * Check to see if the radio is on or not.
     * @return returns true if the radio is on.
     */
    public boolean isRadioOnGemini(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.isRadioOnGemini(simId);
        } else {
            MmsLog.d("Encapsulation issue", "EncapsulatedTelephonyService -- isRadioOnGemini(int)");
            return false;
        }
    }

    /**
     * Returns the IccCard type of Gemini phone. Return "SIM" for SIM card or "USIM" for USIM card.
     */
    public String getIccCardTypeGemini(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.getIccCardTypeGemini(simId);
        } else {
            MmsLog.d("Encapsulation issue", "EncapsulatedTelephonyService -- getIccCardTypeGemini(int)");
            return null;
        }
    }

    public Bundle getCellLocationGemini(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.getCellLocationGemini(simId);
        } else {
            return null;
        }
    }

    /**
     * Returns the neighboring cell information of the device.
     */
    public List<NeighboringCellInfo> getNeighboringCellInfoGemini(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.getNeighboringCellInfoGemini(simId);
        } else {
            return null;
        }
    }

    /**
    * Returns true if SIM card inserted
     * This API is valid even if airplane mode is on
    */
    public boolean isSimInsert(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.isSimInsert(simId);
        } else {
            MmsLog.d("Encapsulation issue", "EncapsulatedTelephonyService -- isSimInsert(int)");
            return false;
        }
    }

    /*Add by mtk80372 for Barcode number*/
    public String getSN() throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.getSN();
        } else {
            return null;
        }
    }

    /**
      * Returns the network type
      */
    public int getNetworkTypeGemini(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.getNetworkTypeGemini(simId);
        } else {
            return 0;
        }
    }

    public boolean hasIccCardGemini(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.hasIccCardGemini(simId);
        } else {
            return false;
        }
    }

    public int getDataStateGemini(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.getDataStateGemini(simId);
        } else {
            return 0;
        }
    }

    public int getDataActivityGemini(int simId) throws android.os.RemoteException {
        if (EncapsulationConstant.USE_MTK_PLATFORM) {
            return sTelephony.getDataActivityGemini(simId);
        } else {
            return 0;
        }
    }

   /**
     *get the services state for specified SIM
     * @param simId Indicate which sim(slot) to query
     * @return sim indicator state.
     *
    */
   public int getSimIndicatorStateGemini(int simId) throws android.os.RemoteException {
       if (EncapsulationConstant.USE_MTK_PLATFORM) {
           return sTelephony.getSimIndicatorStateGemini(simId);
       } else {
           return 0;
       }
   }

   /**
     *get the network service state for default SIM
     * @return service state.
     *
    */
   public Bundle getServiceState() throws android.os.RemoteException {
       if (EncapsulationConstant.USE_MTK_PLATFORM) {
           return sTelephony.getServiceState();
       } else {
           return null;
       }
   }

   /**
     * get the network service state for specified SIM
     * @param simId Indicate which sim(slot) to query
     * @return service state.
     *
    */
   public Bundle getServiceStateGemini(int simId) throws android.os.RemoteException {
       if (EncapsulationConstant.USE_MTK_PLATFORM) {
           return sTelephony.getServiceStateGemini(simId);
       } else {
           return null;
       }
   }

   public String getScAddressGemini(int simId) throws android.os.RemoteException {
       if (EncapsulationConstant.USE_MTK_PLATFORM) {
           return sTelephony.getScAddressGemini(simId);
       } else {
           return null;
       }
   }

   public void setScAddressGemini(String scAddr, int simId) throws android.os.RemoteException {
       if (EncapsulationConstant.USE_MTK_PLATFORM) {
           sTelephony.setScAddressGemini(scAddr, simId);
       } else {
       }
   }

   public int get3GCapabilitySIM() throws android.os.RemoteException {
       if (EncapsulationConstant.USE_MTK_PLATFORM) {
           return sTelephony.get3GCapabilitySIM();
       } else {
           MmsLog.d("Encapsulation issue", "EncapsulatedTelephonyService -- get3GCapabilitySIM()");
           return 0;
       }
   }

    /**
     * Place a call to the specified number.
     * @param number the number to be called.
     */
    public void call(String number) throws android.os.RemoteException {
        sTelephony.call(number);
    }

    public int getCallState() throws android.os.RemoteException {
        return sTelephony.getCallState();
    }

    public int getDataState() throws android.os.RemoteException {
        return sTelephony.getDataState();
    }

    /**
     * Return true if an ICC card is present
     * This API always return false if airplane mode is on.
     */
    public boolean hasIccCard() throws android.os.RemoteException {
        return sTelephony.hasIccCard();
    }
}
