package com.mediatek.encapsulation.com.android.ex.chips;

import com.android.ex.chips.MTKRecipient;
import com.mediatek.encapsulation.EncapsulationConstant;

public class EncapsulatedMTKRecipient extends MTKRecipient {

    public EncapsulatedMTKRecipient(String displayName, String destination) {
        super(displayName, destination);
    }
}
