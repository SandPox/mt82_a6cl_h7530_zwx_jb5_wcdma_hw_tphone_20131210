package com.mediatek.encapsulation.com.android.internal.telephony;

import com.android.internal.telephony.TelephonyIntents;
import com.mediatek.encapsulation.EncapsulationConstant;

public class EncapsulatedTelephonyIntents {
    public static final String ACTION_SIM_INDICATOR_STATE_CHANGED =
                            EncapsulationConstant.USE_MTK_PLATFORM ?
                            TelephonyIntents.ACTION_SIM_INDICATOR_STATE_CHANGED
                            : "android.intent.action.SIM_INDICATOR_STATE_CHANGED";

    public static final String INTENT_KEY_ICC_SLOT =
                            EncapsulationConstant.USE_MTK_PLATFORM ?
                            TelephonyIntents.INTENT_KEY_ICC_SLOT
                            : "slotId";
}