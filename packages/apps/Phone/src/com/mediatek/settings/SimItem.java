package com.mediatek.settings;

import com.android.internal.telephony.PhoneConstants;
import com.mediatek.telephony.SimInfoManager.SimInfoRecord;

public class SimItem {
        public boolean mIsSim = true;
        public SimInfoRecord mSiminfo;
        public int mState = PhoneConstants.SIM_INDICATOR_NORMAL;
        //constructor for sim
        public SimItem(SimInfoRecord siminfo) {
                this.mSiminfo = siminfo;
                if (siminfo == null) {
                    mIsSim = false;
                } else {
                    mIsSim = true;
                }
        }
}

