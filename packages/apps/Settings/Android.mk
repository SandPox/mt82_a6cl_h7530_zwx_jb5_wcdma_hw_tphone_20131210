LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_JAVA_LIBRARIES := bouncycastle \
                        telephony-common \
                        mediatek-framework \
                        CustomProperties

LOCAL_STATIC_JAVA_LIBRARIES := guava \
                               android-support-v4 \
                               jsr305 \
                               com.mediatek.settings.ext \
                               CellConnUtil

LOCAL_EMMA_COVERAGE_FILTER := @$(LOCAL_PATH)/emma_filter.txt,--$(LOCAL_PATH)/emma_filter_method.txt

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src)

ifeq (yes, strip$(MTK_LCA_RAM_OPTIMIZE))
LOCAL_AAPT_FLAGS := --utf16
endif

ifneq ($(MTK_BT_PROFILE_MANAGER), yes)
LOCAL_SRC_FILES := $(filter-out src/com/android/settings/bluetoothangel%, $(LOCAL_SRC_FILES))
else 
LOCAL_SRC_FILES := $(filter-out src/com/android/settings/bluetoothZ%, $(LOCAL_SRC_FILES))
endif

LOCAL_PACKAGE_NAME := Settings
LOCAL_CERTIFICATE := platform

LOCAL_PROGUARD_FLAG_FILES := proguard.flags

# Vanzo:Kern on: Thu, 10 Oct 2013 12:02:45 +0800
# add for SuperUser
LOCAL_AAPT_INCLUDE_ALL_RESOURCES := true
LOCAL_AAPT_FLAGS := --extra-packages com.koushikdutta.superuser:com.koushikdutta.widgets --auto-add-overlay
LOCAL_SRC_FILES += $(call all-java-files-under,../../../mediatek/external/koush/Superuser/Superuser/src) $(call all-java-files-under,../../../mediatek/external/koush/Widgets/Widgets/src)
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res $(LOCAL_PATH)/../../../mediatek/external/koush/Widgets/Widgets/res $(LOCAL_PATH)/../../../mediatek/external/koush/Superuser/Superuser/res
# End of Vanzo:Kern

include $(BUILD_PACKAGE)

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
