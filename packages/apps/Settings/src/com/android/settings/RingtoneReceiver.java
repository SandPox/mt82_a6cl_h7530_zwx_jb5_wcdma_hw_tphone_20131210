/**
 * yyg add file
 */
package com.android.settings;

import java.util.List;
import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.media.RingtoneManager;
import android.util.Log;

public class RingtoneReceiver extends BroadcastReceiver {
    private static final String PACKAGE_NAME = "com.yyg.ringexpert";

    PackageManager mPackageManager;

    @Override
        public void onReceive(Context context, Intent intent) {
            try {
                mPackageManager = context.getPackageManager();

                String action = intent.getAction();
                Configuration configure = context.getResources().getConfiguration();
                Locale locale = configure.locale;
                int mcc = configure.mcc;
                boolean addPreferred = ((mcc == 460) && locale.getLanguage()
                        .equals(Locale.CHINA.getLanguage()));
                Log.d("HomeActivity", "addPreferred:" + addPreferred + " action:"
                        + action);
                if (addPreferred) {
                    enableRingActivity(context, true);
                    addPreferred(context);
                } else {
                    removePreferred();
                    enableRingActivity(context, false);
                }
            }catch (Exception e) {

            }
        }


    private void addPreferred(Context context) {
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        IntentFilter filter = new IntentFilter(
                RingtoneManager.ACTION_RINGTONE_PICKER);
        filter.addCategory("android.intent.category.DEFAULT");
        List<ResolveInfo> resolveInfoList = context.getPackageManager()
            .queryIntentActivities(intent, 0);
        final int N = resolveInfoList.size();
        ComponentName[] set = new ComponentName[N];
        int bestMatch = 0;
        for (int i = 0; i < N; i++) {
            ResolveInfo r = resolveInfoList.get(i);
            set[i] = new ComponentName(r.activityInfo.packageName,
                    r.activityInfo.name);
            if (r.activityInfo.packageName.equals(PACKAGE_NAME))
                bestMatch = r.match;
        }

        Intent preferredIntent = new Intent();
        preferredIntent.setComponent(new ComponentName(PACKAGE_NAME,
                    "com.yyg.ringexpert.activity.EveRingToneActivity"));
        try {
            context.getPackageManager().addPreferredActivity(filter, bestMatch, set,
                    preferredIntent.getComponent());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void removePreferred() {
        mPackageManager.clearPackagePreferredActivities(PACKAGE_NAME);
    }

    private void enableRingActivity(Context context, boolean on) {
        PackageManager pm = context.getPackageManager();
        pm.setApplicationEnabledSetting(PACKAGE_NAME,
                on ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
