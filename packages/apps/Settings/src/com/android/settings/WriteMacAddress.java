package com.android.settings;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.gemini.GeminiPhone;
import com.mediatek.common.featureoption.FeatureOption;
import com.android.settings.NvRAMAgent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Telephony.SIMInfo;
import android.util.Log;

public class WriteMacAddress extends BroadcastReceiver {
    String TAG = "WriteMacAddress";

    private Phone mPhone = null;
    private GeminiPhone mGeminiPhone = null;
    public static int FILE_ID = SystemProperties.getInt(
            "ro.nid.wifi_mac_address", 29);
    public static boolean WRITE_MAC_SWITCH= SystemProperties.getBoolean(
            "ro.init.write_mac_address",true);
    private int offset = 7;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "WriteMacAddress switch value:"+WRITE_MAC_SWITCH) ;
        if (WRITE_MAC_SWITCH == false) {
            return;
        }
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            IBinder binder = ServiceManager.getService("NvRAMAgent");
            NvRAMAgent agent = NvRAMAgent.Stub.asInterface(binder);
            byte[] readBuff = null;
            try {
                readBuff = agent.readFile(FILE_ID);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            StringBuilder ss = new StringBuilder();
            ss.append(readBuff[4]);
            ss.append(readBuff[5]);
            ss.append(readBuff[6]);
            ss.append(readBuff[7]);
            ss.append(readBuff[8]);
            ss.append(readBuff[9]);
            Log.i(TAG, "ss values is >>>" + ss.toString());
            if ((readBuff!=null) && (ss.toString().equals("000000") || ss.toString().equals(""))) {
                Log.i(TAG, "write nvraming...");
                String values = stringRandom();
                byte[] macValues = values.getBytes();
                //byte[] buff = new byte[512];
                readBuff[4] = (byte) 0x00;
                readBuff[5] = (byte) 0x08;
                readBuff[6] = (byte) 0x22;
                for (int i = 0; i < 3; i++) {
                    readBuff[i + offset] = (byte) (Integer.parseInt(values
                            .substring(i + 0, i + 2)) & 0xFF);
                }
                try {
                    int flag = agent.writeFile(FILE_ID, readBuff);

                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String stringRandom() {
        String in = new String("0123456789");
        List<String> list = Arrays.asList(in.split(""));
        Collections.shuffle(list);
        String out = new String();
        for (String s : list) {
            out += s;
        }
        String temp = out.substring(0, 2) + out.substring(3, 5)
                + out.substring(7, 9);
        Log.i(TAG, ">>>>>>>>>>>>>>>>" + temp);
        return temp;
    }
}
