package com.android.settings;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class UpdateSystem extends Activity implements View.OnClickListener {

    private Button ok;
    private Button cancel;
    private TextView mTextView;
    private Intent intent;
    private Bundle mBundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.updatesettings);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        intent = new Intent("android.intent.action.MASTER_CLEAR");

        ok = (Button) findViewById(R.id.ok);
        cancel = (Button) findViewById(R.id.cancel);

        mTextView = (TextView) findViewById(R.id.textview);
        mTextView.setText(R.string.dangerous);

        mBundle = getIntent().getExtras();
        String number = mBundle.getString("update");
        number = number.equals("") ? "1" : number;
        intent.putExtra("func", Integer.parseInt(number));
        intent.putExtra("zero", mBundle.getBoolean("zero"));
        ok.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                sendBroadcast(intent);
                break;
            case R.id.cancel:
                finish();
                break;
            default:
                break;
        }
    }
}
