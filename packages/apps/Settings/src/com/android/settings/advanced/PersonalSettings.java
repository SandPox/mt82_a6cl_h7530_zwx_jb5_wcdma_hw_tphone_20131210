
package com.android.settings.advanced;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemProperties;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.util.Log;

import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;

public class PersonalSettings extends SettingsPreferenceFragment implements
        Preference.OnPreferenceChangeListener, OnPreferenceClickListener {

    private static final String TAG = "PersonalSettings";
    private Context mContext;

    private PreferenceCategory mAppsPerCategory;
    private SwitchPreference mSwitchBoxVolume;
    private PersonalSwitchPreference mSwitchStatusBar;
    private PersonalSwitchPreference mSuperuser;
    private PreferenceScreen mNotification;

    private static final String SHARED_PREFERENCES_NAME = "personal_settings";
    private static final String PERSONAL_SUPPORT_APPS = "personal_support_apps";
    private static final String PERSONAL_LEARN_ACTION = "personal_settings_learn_action";
    private static final String PERSONAL_SETTINGS_VOLUME = "personal_settings_volume";
    private static final String PERSONAL_SETTINGS_STATUS_BAR = "personal_settings_statusBar";
    private static final String PERSONAL_SETTINGS_NOTIFICATION = "personal_settings_notification";
    private static final String PERSONAL_SETTINGS_SUPERUSER = "personal_settings_superuser";

    private static final String KEY_PERSONAL_SETTINGS_SWITCH = "persist.personal_switch";
    private static final String KEY_PERSONAL_SETTINGS_VOLUME = "key_personal_settings_volume";
    private static final String KEY_PERSONAL_SETTINGS_STATUS_BAR = "key_personal_settings_statusBar";
    private static final String KEY_PERSONAL_SETTINGS_NOTIFICATION = "key_personal_settings_notification";
    private static final String KEY_PERSONAL_SETTINGS_SUPERUSER = "key_personal_settings_superuser";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
        ContentResolver resolver = getActivity().getContentResolver();
        addPreferencesFromResource(R.xml.personal_settings);

        mAppsPerCategory = (PreferenceCategory) findPreference(PERSONAL_SUPPORT_APPS);
        mSwitchBoxVolume = (SwitchPreference) findPreference(PERSONAL_SETTINGS_VOLUME);
        mSwitchStatusBar = (PersonalSwitchPreference) findPreference(PERSONAL_SETTINGS_STATUS_BAR);
        mNotification = (PreferenceScreen) findPreference(PERSONAL_SETTINGS_NOTIFICATION);
        mSuperuser = (PersonalSwitchPreference) findPreference(PERSONAL_SETTINGS_SUPERUSER);

        mSwitchBoxVolume.setChecked(SystemProperties.getInt("persist.sys.volume_key_wake", 0) == 1);
        mSwitchStatusBar.setChecked(SystemProperties.getInt("persist.sys.custom_transparency", 0) == 1);
        mSuperuser.setChecked(SystemProperties.getInt("persist.sys.root_access", 0) == 3);

        mSwitchBoxVolume.setOnPreferenceChangeListener(this);
        mSwitchStatusBar.setOnPreferenceChangeListener(this);
        mSwitchStatusBar.setOnPreferenceClickListener(this);
        mSuperuser.setOnPreferenceClickListener(this);
        mSuperuser.setOnPreferenceChangeListener(this);
/* Vanzo:Kern on: Thu, 10 Oct 2013 11:54:05 +0800
 * remove notification manager temporary
        mNotification.setOnPreferenceClickListener(this);
 */
// End of Vanzo: Kern

    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals(PERSONAL_SETTINGS_NOTIFICATION)) {
            openExternelActivity("com.android.settings",
                    "com.android.settings.advanced.notification.NotificationActivity");
        } else if (preference.getKey().equals(PERSONAL_SETTINGS_SUPERUSER)) {
            // do something
        } else if (preference.getKey().equals(PERSONAL_SETTINGS_STATUS_BAR)) {
            openExternelActivity("com.android.settings",
                    "com.android.settings.advanced.TransparencyActivity");
        }
        return false;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {

        boolean isChecked = String.valueOf(newValue).contains("true");
        if (mSwitchBoxVolume == preference) {
            SystemProperties.set("persist.sys.volume_key_wake", isChecked ? "1" : "0");
        } else if (mSwitchStatusBar == preference) {
            SystemProperties.set("persist.sys.custom_transparency",
                    ((Boolean) newValue == true) ? 1 + "" : 0 + "");
            Intent intent = new Intent();
            intent.setAction("android.intent.action.CUSTOM_TRANSPARENCY_CHANGED");
            getActivity().sendBroadcast(intent);
        } else if (mSuperuser == preference) {
            SystemProperties.set("persist.sys.root_access", (Boolean) newValue ? "3" : "0");
        }

        return true;
    }

    private void openExternelActivity(String pkgName, String className) {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName(pkgName, className);
        mContext.startActivity(intent);
    }
}
