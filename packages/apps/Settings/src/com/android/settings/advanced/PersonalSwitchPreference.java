
package com.android.settings.advanced;

import android.content.Context;
import android.preference.SwitchPreference;
import android.util.AttributeSet;

public class PersonalSwitchPreference extends SwitchPreference {
    private static final String TAG = "HotspotSwitchPreference";

    public PersonalSwitchPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public PersonalSwitchPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PersonalSwitchPreference(Context context) {
        super(context);
    }

    @Override
    protected void onClick() {
    }
}
