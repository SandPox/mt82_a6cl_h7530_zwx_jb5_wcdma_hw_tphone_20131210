
package com.android.settings.advanced.notification;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;
import java.util.Map;

public class RoundAdapte extends RoundListViewAdapter {

    public RoundAdapte(List<DownLoadAppInfo> data, Context context, List<String> headers,
            SQLiteDatabase database) {
        super(data, context, headers, database);
    }

    @Override
    public int numberOfRowsInSection() {
        return 2;
    }

    @Override
    public Map<Integer, Integer> numberOfSectionsInRow(Map<Integer, Integer> row) {
        return row;
    }
}
