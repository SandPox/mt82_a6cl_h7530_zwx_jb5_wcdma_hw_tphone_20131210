
package com.android.settings.advanced.notification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.android.settings.R;

public class NotificationActivity extends Activity {

    private Button mNotificationBtn;
    public final static int NOTIFTICATION = 0;
    public final static int AUTOSTART = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_layout_main);
        init();
    }

    private void init() {

        mNotificationBtn = (Button) findViewById(R.id.btn_notification);
        mNotificationBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("key", NOTIFTICATION);
                startActivity(new Intent(NotificationActivity.this, ManagerActivity.class));
            }
        });

    }

}
