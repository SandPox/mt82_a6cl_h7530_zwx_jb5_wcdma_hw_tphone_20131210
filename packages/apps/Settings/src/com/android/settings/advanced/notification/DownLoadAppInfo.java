
package com.android.settings.advanced.notification;

public class DownLoadAppInfo {
    private Object appIcon;
    private String appName;
    private String packageName;
    private boolean notificationState;

    public Object getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Object appIcon) {
        this.appIcon = appIcon;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isNotificationState() {
        return notificationState;
    }

    public void setNotificationState(boolean notificationState) {
        this.notificationState = notificationState;
    }

}
