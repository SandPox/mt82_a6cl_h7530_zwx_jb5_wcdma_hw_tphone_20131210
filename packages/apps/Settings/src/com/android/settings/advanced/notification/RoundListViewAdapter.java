
package com.android.settings.advanced.notification;

import android.app.INotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.ServiceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Switch;
import android.widget.TextView;

import com.android.settings.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class RoundListViewAdapter extends BaseAdapter {

    public static final String ROUND_TEXT_1 = "text1";
    final Integer TOP_CAP = 1;
    final Integer BOTTOM_CAP = 2;
    final Integer NO_CAP = 3;
    final Integer TOP_AND_BOTTOM = 4;
    private SQLiteDatabase sqLiteDatabase;
    private List<DownLoadAppInfo> mData;
    private Map<Integer, Integer> itemState; // state of round
    private Map<Integer, Integer> numSectionsInRow;
    private List<String> itemHeader;
    private Context mContext;
    private static final int STOP = 0, NOTSTOP = 1;
    private String TAG = "RoundListViewAdapter";

    public RoundListViewAdapter(List<DownLoadAppInfo> data, Context context, List<String> headers,
            SQLiteDatabase database) {
        mData = data;
        mContext = context;
        sqLiteDatabase = database;
        itemState = new HashMap<Integer, Integer>();
        numSectionsInRow = new HashMap<Integer, Integer>();
        itemHeader = headers;
        setItemState();
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHoder hoder;
        hoder = new ViewHoder();
        convertView = LayoutInflater.from(mContext).inflate(R.layout.notification_list_item, null);
        hoder.textView = (TextView) convertView.findViewById(R.id.textValue);
        hoder.iconIv = (ImageView) convertView.findViewById(R.id.appIcon);
        hoder.layoutProcess = (LinearLayout) convertView.findViewById(R.id.layoutProcess);
        hoder.textHeader = (TextView) convertView.findViewById(R.id.sectionHeader);
        hoder.switchBtn = (Switch) convertView.findViewById(R.id.switchButton);
        convertView.setTag(hoder);

        int shapeState = itemState.get(position);

        hoder.iconIv.setBackground((Drawable) mData.get(position).getAppIcon());
        hoder.textView.setText(mData.get(position).getAppName());

        Cursor cursor = sqLiteDatabase.query(MyDatabaseHelper.TABLE_NAME, new String[] {
                "packagename", "state"
        }, "packagename = ?", new String[] {
                mData.get(position).getPackageName()
        }, null, null, null);
        if (null != cursor) {
            while (cursor.moveToNext()) {
                int stopState = cursor.getInt(cursor.getColumnIndex("state"));
                hoder.switchBtn.setChecked(stopState == 1 ? true : false);
            }
        } else {
            hoder.switchBtn.setChecked(false);
        }
        cursor.close();
        hoder.switchBtn.setOnCheckedChangeListener(new MyonChecked(position));

        @SuppressWarnings("deprecation")
        LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        layout.setMargins(0, -2, 0, 0);
        // round the list
        switch (shapeState) {
            case 1:
                hoder.layoutProcess.setBackgroundResource(R.drawable.top_round);
                hoder.textHeader.setVisibility(View.VISIBLE);
                layout.setMargins(0, 0, 0, 0);
                hoder.layoutProcess.setLayoutParams(layout);
                break;
            case 2:
                hoder.layoutProcess.setBackgroundResource(R.drawable.bottom_round);
                hoder.layoutProcess.setLayoutParams(layout);
                hoder.textHeader.setVisibility(View.GONE);
                break;
            case 3:
                hoder.layoutProcess.setBackgroundResource(R.drawable.no_round);
                hoder.textHeader.setVisibility(View.GONE);
                hoder.layoutProcess.setLayoutParams(layout);
                break;
            case 4:
                hoder.layoutProcess.setBackgroundResource(R.drawable.bottom_and_top_round);
                hoder.textHeader.setVisibility(View.VISIBLE);
                layout.setMargins(0, 0, 0, 0);
                hoder.layoutProcess.setLayoutParams(layout);
            default:
                break;
        }

        if (itemHeader != null && (shapeState == 1 || shapeState == 4)) {
            String header = itemHeader.get(getHeaderIndexForPosition(position));
            hoder.textHeader.setText(header);
        } else {
            // textheader is gone now
            hoder.textHeader.setText("");
        }
        return convertView;
    }

    class MyonChecked implements OnCheckedChangeListener {
        int selPosition;

        public MyonChecked(int position) {
            super();
            this.selPosition = position;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            INotificationManager nm = INotificationManager.Stub.asInterface(ServiceManager
                    .getService(Context.NOTIFICATION_SERVICE));

            if (isChecked) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("packagename", mData.get(selPosition).getPackageName());
                contentValues.put("state", 1);
                int row = sqLiteDatabase.update(MyDatabaseHelper.TABLE_NAME, contentValues,
                        "packagename = ?", new String[] {
                            String.valueOf(mData.get(selPosition).getPackageName())
                        });
                if (row < 1) {
                    sqLiteDatabase.insert(MyDatabaseHelper.TABLE_NAME, null, contentValues);
                }

                try {
                    nm.setNotificationsEnabledForPackage(mData.get(selPosition).getPackageName(),
                            false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                ContentValues contentValues = new ContentValues();
                contentValues.put("packagename", mData.get(selPosition).getPackageName());
                contentValues.put("state", 0);
                int row = sqLiteDatabase.update(MyDatabaseHelper.TABLE_NAME, contentValues,
                        "packagename = ?", new String[] {
                            String.valueOf(mData.get(selPosition).getPackageName())
                        });
                if (row < 1) {
                    sqLiteDatabase.insert(MyDatabaseHelper.TABLE_NAME, null, contentValues);
                }

                try {
                    nm.setNotificationsEnabledForPackage(mData.get(selPosition).getPackageName(),
                            true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            ManagerActivity.mHandler.sendEmptyMessage(ManagerActivity.REFRESH);
        }

    }

    public abstract int numberOfRowsInSection();

    public abstract Map<Integer, Integer> numberOfSectionsInRow(Map<Integer, Integer> row);

    static class ViewHoder {
        TextView textView;
        ImageView iconIv;
        TextView textHeader;
        LinearLayout layoutProcess;
        Switch switchBtn;
    }

    public void setItemState() {
        int row = numberOfRowsInSection();
        if (row == 0) {
            return;
        }
        if (mData.size() == 0) {
            return;
        }
        Cursor cursor = sqLiteDatabase.query(MyDatabaseHelper.TABLE_NAME, new String[] {
                "packagename", "state"
        }, "state = 0", null, null, null, null);
        if (cursor.getCount() > 0) {
            numSectionsInRow.put(NOTSTOP, cursor.getCount());
            numSectionsInRow.put(STOP, mData.size() - cursor.getCount());
        } else {
            numSectionsInRow.put(NOTSTOP, mData.size());
            numSectionsInRow.put(STOP, 0);
        }
        cursor.close();
        int position = 0;
        int rowCount = 0;
        for (int i = 0; i < row; i++) {
            switch (i) {
                case 0:
                    rowCount = numberOfSectionsInRow(numSectionsInRow).get(NOTSTOP);
                    break;
                case 1:
                    rowCount = numberOfSectionsInRow(numSectionsInRow).get(STOP);
                    break;
                default:
                    break;
            }

            for (int j = 0; j < rowCount; j++) {
                if (position == mData.size()) {
                    break;
                }
                if (j == 0 && rowCount == 1) {
                    itemState.put(position++, TOP_AND_BOTTOM);
                    break;
                }
                if (j == 0) {
                    itemState.put(position++, TOP_CAP);
                } else if (j == rowCount - 1) {
                    itemState.put(position++, BOTTOM_CAP);
                } else {
                    itemState.put(position++, NO_CAP);
                }
            }
        }

        if (itemState != null) {
            int lastState = itemState.get(position - 1);

            boolean isBottom = false;
            if (lastState == TOP_CAP || lastState == NO_CAP) {
                itemState.put(position - 1, TOP_AND_BOTTOM);
                isBottom = false;
            } else {
                isBottom = true;
            }

            int remainder = mData.size() - position;
            if (position < mData.size()) {
                for (int i = 0; i < remainder; i++) {
                    if (remainder == 1) {
                        itemState.put(position++, TOP_AND_BOTTOM);
                        return;
                    }
                    if (i == 0 && isBottom) {
                        itemState.put(position++, TOP_CAP);
                    } else if (i == remainder - 1) {
                        itemState.put(position++, BOTTOM_CAP);
                    } else {
                        itemState.put(position++, NO_CAP);
                    }
                }
            }
        }

    }

    private int getHeaderIndexForPosition(int position) {

        int row = numberOfRowsInSection();

        int sumCount = 0;
        int rowCount = 0;
        for (int i = 0; i < row; i++) {
            switch (i) {
                case 0:
                    rowCount = numberOfSectionsInRow(numSectionsInRow).get(NOTSTOP);
                    break;
                case 1:
                    rowCount = numberOfSectionsInRow(numSectionsInRow).get(STOP);
                    break;
                default:
                    break;
            }
            sumCount += rowCount;
            if (sumCount > position) {
                return i;
            }
        }
        return row;
    }

    @Override
    public void notifyDataSetChanged() {
        setItemState();
        super.notifyDataSetChanged();
    }
}
