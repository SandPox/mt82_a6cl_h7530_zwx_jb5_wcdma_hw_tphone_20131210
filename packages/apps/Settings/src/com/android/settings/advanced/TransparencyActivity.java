
package com.android.settings.advanced;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemProperties;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.android.settings.R;

public class TransparencyActivity extends Activity implements DialogInterface.OnClickListener {

    private CompoundButton mSyncCheckBox;
    private SeekBar mStatusBar;
    private SeekBar mLauncherDrawer;
    private SeekBar mLockscreen;
    private View mDialogBg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.transparency_settings);

        mDialogBg = findViewById(R.id.bg_layout);

        View content = getLayoutInflater().inflate(R.layout.transparency_dialog, null);

        final AlertDialog seekBarDialog = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK)
                .setCancelable(false)
                .setTitle(R.string.transparency_dialog_title)
                .setView(content)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, this)
                .create();
        seekBarDialog.show();

        mLauncherDrawer = (SeekBar) content.findViewById(R.id.launcher_drawer);
        mLauncherDrawer.setMax(255);
        int alpha = SystemProperties.getInt("persist.sys.launcher_alpha", 128);
        mLauncherDrawer.setProgress(alpha);
        mLauncherDrawer.setOnSeekBarChangeListener(mSeekBarListener);

        mDialogBg.setBackgroundColor(alpha << 24);

        mStatusBar = (SeekBar) content.findViewById(R.id.status_bar);
        mStatusBar.setMax(255);
        mStatusBar.setProgress(SystemProperties.getInt("persist.sys.status_bar_alpha", 128));

        mLockscreen = (SeekBar) content.findViewById(R.id.lockscreen);
        mLockscreen.setMax(255);
        mLockscreen.setProgress(SystemProperties.getInt("persist.sys.lockscreen_alpha", 128));

        mSyncCheckBox = (CompoundButton) content.findViewById(R.id.cb_sync);
        mSyncCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mSyncCheckBox.isChecked()) {
                    mStatusBar.setEnabled(false);
                    mLockscreen.setEnabled(false);
                    mStatusBar.setProgress(mLauncherDrawer.getProgress());
                    mLockscreen.setProgress(mLauncherDrawer.getProgress());
                } else {
                    mStatusBar.setEnabled(true);
                    mLockscreen.setEnabled(true);
                }
            }
        });

        boolean sync = getSharedPreferences("advanced_settings", Context.MODE_PRIVATE)
                .getBoolean("transparency_sync", false);
        mSyncCheckBox.setChecked(sync);
    }

    private OnSeekBarChangeListener mSeekBarListener = new OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mDialogBg.setBackgroundColor(progress << 24);
            if (mSyncCheckBox.isChecked()) {
                mStatusBar.setProgress(progress);
                mLockscreen.setProgress(progress);
            }
        }
    };

    @Override
    public void onClick(DialogInterface dialogInterface, int button) {
        if (button == DialogInterface.BUTTON_POSITIVE) {
            SystemProperties.set("persist.sys.launcher_alpha", mLauncherDrawer.getProgress() + "");
            SystemProperties.set("persist.sys.status_bar_alpha", mStatusBar.getProgress() + "");
            SystemProperties.set("persist.sys.lockscreen_alpha", mLockscreen.getProgress() + "");

            getSharedPreferences("advanced_settings", Context.MODE_PRIVATE).edit()
                    .putBoolean("transparency_sync", mSyncCheckBox.isChecked()).commit();

            Intent intent = new Intent();
            intent.setAction("android.intent.action.CUSTOM_TRANSPARENCY_CHANGED");
            sendBroadcast(intent);
        }
        finish();
    }
}
