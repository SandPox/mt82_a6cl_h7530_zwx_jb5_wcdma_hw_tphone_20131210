
package com.android.settings.advanced.notification;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.android.settings.R;

import java.util.ArrayList;
import java.util.List;

public class ManagerActivity extends Activity {
    private RoundAdapte mAdapter;
    private ListView mListView;
    private Button mBackBtn;
    private List<String> mListHeader;
    private List<DownLoadAppInfo> mDownAppInfosList;
    private SQLiteDatabase mSqLiteDatabase = null;
    private MyDatabaseHelper mDatabaseHelper = null;
    public static Handler mHandler;
    public static final int REFRESH = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_layout_manager);
        int choice = getIntent().getIntExtra("key", NotificationActivity.NOTIFTICATION);
        if (NotificationActivity.NOTIFTICATION == choice) {

        } else if (NotificationActivity.AUTOSTART == choice) {

        }
        mDatabaseHelper = new MyDatabaseHelper(getApplicationContext());
        mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();
        mDownAppInfosList = getDownLoadAppInfoList();
        saveAppInfoInDB();// save app in db
        init();

        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case REFRESH:
                        mDownAppInfosList = getDownLoadAppInfoList();
                        mAdapter = new RoundAdapte(mDownAppInfosList, ManagerActivity.this,
                                mListHeader, mSqLiteDatabase);
                        mListView.setAdapter(mAdapter);
                        break;

                    default:
                        break;
                }

            }
        };
    }

    private void saveAppInfoInDB() {
        int size = mDownAppInfosList.size();

        Cursor cursor = mSqLiteDatabase.rawQuery("select * from " + MyDatabaseHelper.TABLE_NAME,
                null);
        if (cursor.getCount() < size) {

            for (int i = 0; i < size; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("packagename", mDownAppInfosList.get(i).getPackageName());
                contentValues.put("state", 0);
                int row = mSqLiteDatabase.update(MyDatabaseHelper.TABLE_NAME, contentValues,
                        "packagename = ?", new String[] {
                            String.valueOf(mDownAppInfosList.get(i).getPackageName())
                        });

                if (row < 1) {
                    mSqLiteDatabase.insert(MyDatabaseHelper.TABLE_NAME, null, contentValues);
                }
            }
        }
    }

    private void init() {

        mBackBtn = (Button) findViewById(R.id.btn_back);
        mBackBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ManagerActivity.this.finish();
            }
        });

        mListView = (ListView) findViewById(R.id.roundList);
        mListHeader = new ArrayList<String>();
        mListHeader.add(getResources().getString(R.string.display));
        mListHeader.add(getResources().getString(R.string.nodisplay));

        mAdapter = new RoundAdapte(mDownAppInfosList, this, mListHeader, mSqLiteDatabase);
        mListView.setAdapter(mAdapter);
    }

    /*
     * get user's application information
     */
    private List<DownLoadAppInfo> getDownLoadAppInfoList() {
        List<PackageInfo> packs = getPackageManager().getInstalledPackages(0);

        List<DownLoadAppInfo> downLoadAppInfoList = new ArrayList<DownLoadAppInfo>(packs.size());

        // get all application information

        int count = packs.size();
        for (int i = 0; i < count; i++) {
            Intent mainintent = new Intent(Intent.ACTION_MAIN, null);
            mainintent.addCategory(Intent.CATEGORY_LAUNCHER);
            PackageInfo pinfo = packs.get(i);
            ApplicationInfo appInfo = pinfo.applicationInfo;
            if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) > 0) {
                // system's application information,ignore
            } else {
                // user's application information
                DownLoadAppInfo downLoadAppInfo = new DownLoadAppInfo();
                downLoadAppInfo.setAppIcon(packs.get(i).applicationInfo
                        .loadIcon(getPackageManager()));
                downLoadAppInfo.setAppName(String.valueOf(packs.get(i).applicationInfo
                        .loadLabel(getPackageManager())));
                downLoadAppInfo.setPackageName(packs.get(i).applicationInfo.packageName);
                Cursor cursor = mSqLiteDatabase.query(MyDatabaseHelper.TABLE_NAME, new String[] {
                        "packagename", "state"
                }, "packagename = ?", new String[] {
                        packs.get(i).applicationInfo.packageName
                }, null, null, null);
                // judge the application's notification is stop
                int state = 0;
                if (null != cursor) {
                    while (cursor.moveToNext()) {
                        state = cursor.getInt(cursor.getColumnIndex("state"));
                    }
                }
                cursor.close();
                if (state == 0) {
                    // application with notification to the top
                    downLoadAppInfoList.add(0, downLoadAppInfo);
                } else {
                    // application with stopped notification to the bottom
                    downLoadAppInfoList.add(downLoadAppInfoList.size(), downLoadAppInfo);
                }
            }
        }

        return downLoadAppInfoList;
    }
}
