package com.android.settings;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.gemini.GeminiPhone;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.common.featureoption.FeatureOption;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class WriteIMEIDialog extends Activity implements OnClickListener,TextWatcher{

    private String TAG = "WriteIMEI";
    private EditText imei1;
    private EditText imei2;
    private Button saveBtn;
    private Button cancelBtn;
    private Intent intent;
    private static final int AGENT_BIN = 1;
    private Phone mPhone = null;
    private GeminiPhone mGeminiPhone = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.write_imei);
        intent = getIntent();
        saveBtn = (Button) this.findViewById(R.id.saveBtn);
        cancelBtn = (Button) this.findViewById(R.id.cancelBtn);
        imei1 = (EditText) this.findViewById(R.id.imei1);
        imei2 = (EditText) this.findViewById(R.id.imei2);
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            imei1.addTextChangedListener(this);
            imei2.addTextChangedListener(this);
        } else {
            imei2.setVisibility(View.GONE);
            imei1.addTextChangedListener(this);
        }
        saveBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.saveBtn:
                writeIMEI();
                break;
            case R.id.cancelBtn:
                finish();
                break;
        }
    }

    void writeIMEI() {
        String cmd[] = {
                "AT+EGMR=1,", ""
        };
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            mGeminiPhone = (GeminiPhone) PhoneFactory.getDefaultPhone();
            boolean isSaveToBinNeeded = false;
            String imei1Str = mGeminiPhone.getDeviceIdGemini(PhoneConstants.GEMINI_SIM_1);
            String imei2Str = mGeminiPhone.getDeviceIdGemini(PhoneConstants.GEMINI_SIM_2);

            Log.e(TAG, "IMEI1 ====== " + imei1Str);
            Log.e(TAG, "IMEI2 ====== " + imei2Str);

            if (imei1Str == null || imei1Str.equals("")) {
                cmd[0] = "AT+EGMR=1,7,\"" + imei1.getText().toString().trim() + "\"";
                mGeminiPhone.invokeOemRilRequestStringsGemini(cmd, null,
                        PhoneConstants.GEMINI_SIM_1);
                Log.e(TAG, "Send write IMEI1 command:" + cmd[0]);
                isSaveToBinNeeded = true;
            }

            if (imei2Str == null || imei2Str.equals("")) {
                cmd[0] = "AT+EGMR=1,10,\"" + imei2.getText().toString().trim() + "\"";
                mGeminiPhone.invokeOemRilRequestStringsGemini(cmd, null,
                        PhoneConstants.GEMINI_SIM_2);
                Log.e(TAG, "Send write IMEI2 command:" + cmd[0]);
                isSaveToBinNeeded = true;
            }

            if (isSaveToBinNeeded) {
                BackupToBinThread thread = new BackupToBinThread();
                thread.sendEmptyMessageDelayed(AGENT_BIN, 10 * 1000);
                Intent in = new Intent("com.mediatek.factorymode.start.bin");
                this.sendBroadcast(in);
                finish();
            } else {
                Intent in = new Intent("com.mediatek.factorymode.get.imei");
                Bundle bundle = new Bundle();
                bundle.putString("imei1", imei1Str);
                bundle.putString("imei2", imei2Str);
                in.putExtras(bundle);
                this.sendBroadcast(in);
                finish();
            }
        } else {
            mPhone = PhoneFactory.getDefaultPhone();
            String values = imei1.getText().toString().trim();
            try {
                if (mPhone.getImei() == null || mPhone.getImei().equals("")) {
                    cmd[0] = "AT+EGMR=1,7,\"" + values + "\"";
                    mPhone.invokeOemRilRequestStrings(cmd, null);
                    BackupToBinThread thread = new BackupToBinThread();
                    thread.sendEmptyMessageDelayed(AGENT_BIN, 10 * 1000);
                    finish();
                } else {
                    Intent in = new Intent("com.mediatek.factorymode.get.imei");
                    Bundle bundle = new Bundle();
                    bundle.putString("imei1", mPhone.getImei());
                    in.putExtras(bundle);
                    this.sendBroadcast(intent);
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            if (imei1.length() == 15 && imei2.length() == 15) {
                saveBtn.setEnabled(true);
            } else {
                saveBtn.setEnabled(false);
            }
        } else {
            if (imei1.length() == 15) {
                saveBtn.setEnabled(true);
            } else {
                saveBtn.setEnabled(false);
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub
    }

    private class BackupToBinThread extends Handler {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == AGENT_BIN) {
                IBinder binder = ServiceManager.getService("NvRAMBackup");
                NvRAMBackup agent = NvRAMBackup.Stub.asInterface(binder);
                boolean ret = false;
                boolean flag = false;

                try {
                    Log.e(TAG, "begin saveToBin");
                    ret = agent.saveToBin();
                    flag = true;
                    Intent intent = new Intent("com.mediatek.factorymode.write.success");
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("result", flag);
                    intent.putExtras(bundle);
                    WriteIMEIDialog.this.sendBroadcast(intent);
                    Log.e(TAG, "end saveToBin, return value: " + ret);
                } catch (Exception e) {
                    Intent intent = new Intent("com.mediatek.factorymode.write.success");
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("result", flag);
                    intent.putExtras(bundle);
                    WriteIMEIDialog.this.sendBroadcast(intent);
                }
            }
        }
    }

}
