
package com.android.settings;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.gemini.GeminiPhone;
import com.mediatek.common.featureoption.FeatureOption;
import com.android.settings.NvRAMAgent;
import com.android.settings.NvRAMBackup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Telephony.SIMInfo;
import android.util.Log;

public class WriteIMEI extends BroadcastReceiver {
    String TAG = "WriteIMEI";

    private Phone mPhone = null;

    private GeminiPhone mGeminiPhone = null;


    @Override
    public void onReceive(Context context, Intent intent) {
/* Vanzo:tanjianhua on: Wed, 10 Jul 2013 15:22:12 +0800
 * Added for generate IMEI automatically
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
 */
        if (intent.getAction().equals("com.mediatek.factorymode.gen.imei.auto")) {
// End of Vanzo: tanjianhua

            String cmd[] = { "AT+EGMR=1,", "" };

            if (FeatureOption.MTK_GEMINI_SUPPORT) {

                mGeminiPhone = (GeminiPhone) PhoneFactory.getDefaultPhone();
/* Vanzo:tanjianhua on: Wed, 10 Jul 2013 15:23:06 +0800
 * Added for generate IMEI automatically
 */
                boolean isSaveToBinNeeded = false;
// End of Vanzo: tanjianhua
                String imei1Str = mGeminiPhone.getDeviceIdGemini(PhoneConstants.GEMINI_SIM_1);
                String imei2Str = mGeminiPhone.getDeviceIdGemini(PhoneConstants.GEMINI_SIM_2);

                Log.e(TAG, "IMEI1 ====== " + imei1Str);
                Log.e(TAG, "IMEI2 ====== " + imei2Str);

                if (imei1Str == null || imei1Str.equals("")) {
                    cmd[0] = "AT+EGMR=1,7,\"" + stringRandom() + "\"";
                    mGeminiPhone.invokeOemRilRequestStringsGemini(cmd, null, PhoneConstants.GEMINI_SIM_1);
                    Log.e(TAG, "Send write IMEI1 command:" + cmd[0]);

/* Vanzo:tanjianhua on: Wed, 10 Jul 2013 15:23:41 +0800
 * Added for generate IMEI automatically
 */
                    isSaveToBinNeeded = true;
// End of Vanzo: tanjianhua
                }

                if (imei2Str == null || imei2Str.equals("")) {
                    cmd[0] = "AT+EGMR=1,10,\"" + stringRandom() + "\"";
                    mGeminiPhone.invokeOemRilRequestStringsGemini(cmd, null, PhoneConstants.GEMINI_SIM_2);
                    Log.e(TAG, "Send write IMEI2 command:" + cmd[0]);

/* Vanzo:tanjianhua on: Wed, 10 Jul 2013 15:24:06 +0800
 * Added for generate IMEI automatically
 */
                    isSaveToBinNeeded = true;
// End of Vanzo: tanjianhua
                }

/* Vanzo:tanjianhua on: Wed, 10 Jul 2013 15:24:24 +0800
 * Added for generate IMEI automatically
 */
                if (true == isSaveToBinNeeded) {
                    new BackupToBinThread().start();
                }
// End of Vanzo: tanjianhua
            } else {
                mPhone = PhoneFactory.getDefaultPhone();
                String values = stringRandom();
/* Vanzo:tanjianhua on: Fri, 05 Jul 2013 20:16:42 +0800
 * bugfix #41902
                if (mPhone.getImei().equals("") || mPhone.getImei() == null) {
                    cmd[0] = "AT+EGMR=1,7,\"" + values + "\"";
                    mPhone.invokeOemRilRequestStrings(cmd, null);

                    new BackupToBinThread().start();
                }
 */
                try {
                    if (mPhone.getImei() == null || mPhone.getImei().equals("")) {
                        cmd[0] = "AT+EGMR=1,7,\"" + values + "\"";
                        mPhone.invokeOemRilRequestStrings(cmd, null);
                        new BackupToBinThread().start();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
// End of Vanzo: tanjianhua
            }
        }
    }

    public String stringRandom() {
        String in = new String("012345678901234");
        List<String> list = Arrays.asList(in.split(""));
        Collections.shuffle(list);
        String out = new String();
        for (String s : list) {
            out += s;
        }
        return out;
    }

/* Vanzo:tanjianhua on: Wed, 10 Jul 2013 15:24:48 +0800
 * Added for generate IMEI automatically
 */
    private class BackupToBinThread extends Thread {

        @Override
        public void run() {

            super.run();

            try {
                //Sleep 10 seconds
                //Some initializaton should be finished before saving to bin region
                Thread.sleep(10 * 1000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

            IBinder binder = ServiceManager.getService("NvRAMBackup");
            NvRAMBackup agent = NvRAMBackup.Stub.asInterface(binder);

            try {
                Log.e(TAG, "begin saveToBin");
                boolean ret = agent.saveToBin();
                Log.e(TAG, "end saveToBin, return value: " + ret);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } /* End run */
    }
// End of Vanzo: tanjianhua

}
