package com.mediatek.gemini;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.PhoneConstants;
import com.android.settings.R;
import com.mediatek.telephony.SimInfoManager;
import com.mediatek.telephony.SimInfoManager.SimInfoRecord;
import com.mediatek.xlog.Xlog;

import java.util.Collections;
import java.util.List;

public class SimCardListAdpater extends BaseAdapter {
    private List<SimInfoRecord> mSimList;
    private Context mContext;
    private ITelephony mTelephony;
    private static final int NUM_WIDTH = 4;
    private static final String TAG = "SimCardListAdpater";

    public SimCardListAdpater(Context context) {
        mContext = context;
        updateSimInfo();
    }

    @Override
    public int getCount() {
        return mSimList.size();
    }

    public void updateSimInfo() {
        mSimList = SimInfoManager.getInsertedSimInfoList(mContext);
        Collections.sort(mSimList, new GeminiUtils.SIMInfoComparable());
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mSimList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.sim_card_information, null);
            holder = new ViewHolder();
            setViewHolderId(holder, convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        SimInfoRecord simInfo = mSimList.get(position);
        setViewProperty(holder,simInfo);
        return convertView;
    }

	private void setViewProperty(ViewHolder holder, SimInfoRecord simInfo) {
        if (simInfo.mDisplayName != null) {
            holder.mTextName.setText(simInfo.mDisplayName);
        }
        if (simInfo.mNumber != null && !simInfo.mNumber.isEmpty()) {
            holder.mTextNum.setVisibility(View.VISIBLE);
            holder.mTextNum.setText(simInfo.mNumber);
        } else {
            holder.mTextNum.setVisibility(View.GONE);
        }
        int colorRes = GeminiUtils.getSimColorResource(simInfo.mColor);
        if (colorRes == -1) {
            Xlog.e(TAG,"colorRes == -1 and simInfo.mColor = " + simInfo.mColor);
        } else {
            holder.mImageSim.setBackgroundResource(colorRes);
        }
        int simState = getSimIndicator(simInfo.mSimSlotId);
        Xlog.d(TAG, "simState=" + simState);
        int statusRes = GeminiUtils.getStatusResource(simState);
        Xlog.d(TAG, "statusRes=" + statusRes);
        if (statusRes == -1) {
            holder.mImageStatus.setVisibility(View.GONE);
        } else {
            holder.mImageStatus.setVisibility(View.VISIBLE);
            holder.mImageStatus.setImageResource(statusRes);
        }
        setTextNumFormat(holder.mTextNumFormat, simInfo.mDispalyNumberFormat,
                simInfo.mNumber);
    }	

    private int getSimIndicator(int slotId) {
        Xlog.d(TAG, "getSimIndicator---slotId=" + slotId);
        int state = PhoneConstants.SIM_INDICATOR_UNKNOWN;
        try {
            if (mTelephony == null) {
                mTelephony = ITelephony.Stub.asInterface(ServiceManager
                        .getService("phone"));
            }
            state = mTelephony.getSimIndicatorStateGemini(slotId);
        } catch (RemoteException e) {
            Xlog.e(TAG, "RemoteException");
        } catch (NullPointerException ex) {
            Xlog.e(TAG, "NullPointerException");
        }
        return state;
    }

    private void setTextNumFormat(TextView textNumFormat, int format,
            String number) {
        textNumFormat.setVisibility(View.VISIBLE);
        if (number != null) {
            switch (format) {
            case SimInfoManager.DISPALY_NUMBER_NONE:
                textNumFormat.setVisibility(View.GONE);
                break;
            case SimInfoManager.DISPLAY_NUMBER_FIRST:
                if (number.length() >= NUM_WIDTH) {
                    textNumFormat.setText(number.substring(0, NUM_WIDTH));
                } else {
                    textNumFormat.setText(number);
                }
                break;
            case SimInfoManager.DISPLAY_NUMBER_LAST:
                if (number.length() >= NUM_WIDTH) {
                    textNumFormat.setText(number.substring(number.length()
                            - NUM_WIDTH));
                } else {
                    textNumFormat.setText(number);
                }
                break;
            default:
                break;
            }
        }
    }

    private void setViewHolderId(ViewHolder holder, View convertView) {
        holder.mTextName = (TextView) convertView.findViewById(R.id.simNameSel);
        holder.mTextNum = (TextView) convertView.findViewById(R.id.simNumSel);
        holder.mImageStatus = (ImageView) convertView
                .findViewById(R.id.simStatusSel);
        holder.mTextNumFormat = (TextView) convertView
                .findViewById(R.id.simNumFormatSel);
        holder.mImageSim = (RelativeLayout) convertView
                .findViewById(R.id.simIconSel);
    }

    class ViewHolder {
        TextView mTextName;
        TextView mTextNum;
        RelativeLayout mImageSim;
        ImageView mImageStatus;
        TextView mTextNumFormat;
    }

}
