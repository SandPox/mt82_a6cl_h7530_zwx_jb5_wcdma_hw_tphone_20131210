package com.mediatek.gemini;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.android.internal.telephony.PhoneConstants;
import com.mediatek.telephony.SimInfoManager;
import com.mediatek.telephony.SimInfoManager.SimInfoRecord;
import com.mediatek.xlog.Xlog;

import java.util.Comparator;
import java.util.List;

/**
 * Contains utility functions for getting framework resource
 */
public class GeminiUtils {

    private static final int COLORNUM = 7;
    static final int TYPE_VOICECALL = 1;
    static final int TYPE_VIDEOCALL = 2;
    static final int TYPE_SMS = 3;
    static final int TYPE_GPRS = 4;
    static final int INTERNET_CALL_COLOR = 8;
    static final int NO_COLOR = -1;
    static final int IMAGE_GRAY = 75;// 30% of 0xff in transparent
    static final int ORIGINAL_IMAGE = 255;
    static int sG3SlotID = PhoneConstants.GEMINI_SIM_1;
    public static final String EXTRA_SIMID = "simid";
    // stands for slot Id (0 or 1 ) not sim id which is int not long
    public static final String EXTRA_SLOTID = "slotid";
    public static final String INTENT_CARD_SELECT = "com.mediatek.action.SIM_SELECTION";
    public static final int REQUEST_SIM_SELECT = 7777;
    public static final int UNDEFINED_SLOT_ID = -1;
    public static final int UNDEFINED_SIM_ID = -1;
    
    public static final int ERROR_SLOT_ID = -2;
    
    //  for sim pin unlock   
    public  static final int PIN1_REQUEST_CODE = 302;
    
    private static final String TAG = "GeminiUtils";

    public static int getStatusResource(int state) {

        switch (state) {
        case PhoneConstants.SIM_INDICATOR_RADIOOFF:
            return com.mediatek.internal.R.drawable.sim_radio_off;
        case PhoneConstants.SIM_INDICATOR_LOCKED:
            return com.mediatek.internal.R.drawable.sim_locked;
        case PhoneConstants.SIM_INDICATOR_INVALID:
            return com.mediatek.internal.R.drawable.sim_invalid;
        case PhoneConstants.SIM_INDICATOR_SEARCHING:
            return com.mediatek.internal.R.drawable.sim_searching;
        case PhoneConstants.SIM_INDICATOR_ROAMING:
            return com.mediatek.internal.R.drawable.sim_roaming;
        case PhoneConstants.SIM_INDICATOR_CONNECTED:
            return com.mediatek.internal.R.drawable.sim_connected;
        case PhoneConstants.SIM_INDICATOR_ROAMINGCONNECTED:
            return com.mediatek.internal.R.drawable.sim_roaming_connected;
        default:
            return -1;
        }
    }

    public static int getSimColorResource(int color) {

        if ((color >= 0) && (color <= COLORNUM)) {
            return SimInfoManager.SimBackgroundDarkRes[color];
        } else {
            return -1;
        }

    }

    public static int getTargetSlotId(Context context) {
        List<SimInfoRecord> simInfoList = SimInfoManager.getInsertedSimInfoList(context);
        int simSize = simInfoList.size();
        int slotId = UNDEFINED_SLOT_ID;
        if (simSize == 1) {
            slotId = simInfoList.get(0).mSimSlotId;
        }
        return slotId;
    }
    
    public static void goBackSimSelection(Activity activity, boolean needFinish) {
        if (!activity.getFragmentManager().popBackStackImmediate()) {
            Intent it = activity.getIntent();
            int slotId = it.getIntExtra(GeminiUtils.EXTRA_SLOTID, GeminiUtils.ERROR_SLOT_ID);
            Xlog.d(TAG, "slotid is " + slotId);
            if (slotId != GeminiUtils.ERROR_SLOT_ID) {
                activity.finish();
            } else {
                backToSimcardUnlock(activity , needFinish);
            }
        }
    }

    public static class SIMInfoComparable implements Comparator<SimInfoRecord> {

        @Override
        public int compare(SimInfoRecord sim1, SimInfoRecord sim2) {
            return sim1.mSimSlotId - sim2.mSimSlotId;
        }
    }
    
    public static void backToSimcardUnlock(Activity activity, boolean needFinish) {
        List<SimInfoRecord> simInfoList = SimInfoManager
                .getInsertedSimInfoList(activity);
        int simSize = simInfoList.size();
        if (simSize > 1) {
            Intent intent = new Intent();
            intent.setClassName(activity.getApplication().getPackageName(),
                    activity.getClass().getCanonicalName());
            Xlog.d(TAG, "packageName: "
                    + activity.getApplication().getPackageName()
                    + "className: " + activity.getClass().getCanonicalName());
            activity.startActivity(intent);
            if (needFinish) {
                activity.finish();
            }
        } else {
            activity.finish();
        }
    }
    
    /**
     * Get sim slot Id by passing the sim info id
     * 
     * @param simInfoId
     *            sim info id
     * @param simInfoList
     *            a SIMInfo list
     * @return the sim slot id or -1
     */
    public static int getSimSlotIdBySimInfoId(long simId, List<SimInfoRecord> simInfoList) {
        for (SimInfoRecord siminfo : simInfoList) {
            if (siminfo.mSimInfoId == simId) {
                return siminfo.mSimSlotId;
            }
        }
        return UNDEFINED_SLOT_ID;
    }

    /**
     * Get sim info id by passing the sim slot id
     * 
     * @param slotId
     *            sim slot id
     * @param simInfoList
     *            a SIMInfo list
     * @return the sim info id or -1
     */
    public static long getSiminfoIdBySimSlotId(int slotId, List<SimInfoRecord> simInfoList) {
        for (SimInfoRecord siminfo : simInfoList) {
            if (siminfo.mSimSlotId == slotId) {
                return siminfo.mSimInfoId;
            }
        }
        return UNDEFINED_SIM_ID;
    }
}
