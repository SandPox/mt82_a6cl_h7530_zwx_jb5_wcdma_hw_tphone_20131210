package com.mediatek.gemini;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.internal.telephony.TelephonyIntents;
import com.android.settings.SettingsPreferenceFragment;
import com.mediatek.telephony.SimInfoManager;
import com.mediatek.telephony.SimInfoManager.SimInfoRecord;
import com.mediatek.xlog.Xlog;

public class SelectSimCardFragment extends SettingsPreferenceFragment implements
        OnItemClickListener {
    private ListView mListView;
    private static final String TAG = "SelectSimCardFragment";
    private SimCardListAdpater mAdapter;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TelephonyIntents.ACTION_SIM_INDICATOR_STATE_CHANGED
                    .equals(action)) {
                if (mListView != null) {
                    mAdapter.notifyDataSetChanged();
                }
            } else if (TelephonyIntents.ACTION_SIM_INFO_UPDATE.equals(action)) {
                // for sim hot swap case only otherwise no way to jump to card
                // selection
                if (isNoSimInserted()) {
                    finish();
                } else {
                    mAdapter.updateSimInfo();
                    mAdapter.notifyDataSetChanged();
                }
            }
        }

    };

    private boolean isNoSimInserted() {
        int simNum = SimInfoManager.getInsertedSimCount(getActivity());
        Xlog.d(TAG, "simNum = " + simNum);
        return simNum == 0;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mIntentFilter = new IntentFilter(
                TelephonyIntents.ACTION_SIM_INDICATOR_STATE_CHANGED);
        mIntentFilter.addAction(TelephonyIntents.ACTION_SIM_INFO_UPDATE);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(mReceiver, mIntentFilter);
        if (isNoSimInserted()) {
            /*
             * only happened when sim card plug out at background, and switch to
             * this fragment, otherwise no sim card should not enter the
             * selection screen
             */
            finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mListView = (ListView) view.findViewById(android.R.id.list);
        mAdapter = new SimCardListAdpater(getActivity());
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
            long id) {
        Xlog.d(TAG, "position = " + position);
        Intent intent = new Intent();
        SimInfoRecord siminfo = (SimInfoRecord) mAdapter.getItem(position);
        intent.putExtra(GeminiUtils.EXTRA_SLOTID, siminfo.mSimSlotId);
        intent.putExtra(GeminiUtils.EXTRA_SIMID, siminfo.mSimInfoId);
        getActivity().setResult(getActivity().RESULT_OK, intent);
        finish();
    }
}
