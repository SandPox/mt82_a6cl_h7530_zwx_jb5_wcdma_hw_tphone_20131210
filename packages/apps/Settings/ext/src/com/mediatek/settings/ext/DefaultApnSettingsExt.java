package com.mediatek.settings.ext;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.Menu;

import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.common.featureoption.FeatureOption;
import com.mediatek.xlog.Xlog;

/* Dummy implmentation , do nothing */
public class DefaultApnSettingsExt implements IApnSettingsExt {
    
    private static final String TAG = "DefaultApnSettingsExt";
    private static final String TYPE_MMS = "mms";
    private static final String CMMAIL_TYPE = "cmmail";
    private static final String RCSE_TYPE = "rcse";

    public boolean isAllowEditPresetApn(String type, String apn, String numeric, int sourcetype) {
        Xlog.d(TAG, "isAllowEditPresetApn");
        return true;
    }

    public void removeTetherApnSettings(PreferenceScreen prefSc, Preference preference) {
        Xlog.d(TAG, "removeTetherApnSettings");
        prefSc.removePreference(preference);
    }

    public boolean isSelectable(String type) {
        return !TYPE_MMS.equals(type);
    }

    public IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter(
                TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);
        
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED); 
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED); 
        filter.addAction(TRANSACTION_START);
        filter.addAction(TRANSACTION_STOP);
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            filter.addAction(Intent.ACTION_DUAL_SIM_MODE_CHANGED);
        }
        ///M: add for hot swap {
        filter.addAction(TelephonyIntents.ACTION_SIM_INFO_UPDATE);
        ///@}
        return filter;
    }

    public BroadcastReceiver getBroadcastReceiver(BroadcastReceiver receiver) {
        return receiver;
    }

    public boolean getScreenEnableState(int slotId, Activity activity) {        
        boolean simReady = true;
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            simReady = TelephonyManager.SIM_STATE_READY == TelephonyManager.getDefault().getSimStateGemini(slotId);    
        } else {
            simReady = TelephonyManager.SIM_STATE_READY == TelephonyManager.getDefault().getSimState();    
        }
        boolean airplaneModeEnabled = android.provider.Settings.System.getInt(activity.getContentResolver(),
                android.provider.Settings.System.AIRPLANE_MODE_ON, -1) == 1;

        boolean isMMsNoTransac = isMMSNotTransaction(activity);

        Xlog.w(TAG, "isMMsNoTransac = " + isMMsNoTransac);        
        Xlog.w(TAG, "airplaneModeEnabled = " + airplaneModeEnabled);        
        Xlog.w(TAG, "simReady = " + simReady);        
        return isMMsNoTransac && !airplaneModeEnabled && simReady;
    }

    private boolean isMMSNotTransaction(Activity activity) {
        boolean isMMSNotProcess = true;
        ConnectivityManager cm = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE_MMS);
            if (networkInfo != null) {
                NetworkInfo.State state = networkInfo.getState();
                Xlog.d(TAG,"mms state = " + state);
                isMMSNotProcess = (state != NetworkInfo.State.CONNECTING
                    && state != NetworkInfo.State.CONNECTED);
            }
        }
        return isMMSNotProcess;
    }

    public String getFillListQuery(String numeric, boolean isMVNO, String spn, String imsi, String pnn) {
        boolean flagImsi = false;
        boolean flagSpn = false;
        boolean flagPnn = false;
        String sqlStr = "";

        if (FeatureOption.MTK_MVNO_SUPPORT) {
            if (isMVNO) {
                if (!imsi.isEmpty()) {
                    flagImsi = true;
                    sqlStr += " imsi=\"" + imsi + "\"";
                }
                if (!spn.isEmpty()) {
                    flagSpn = true;
                    if (flagImsi) {
                        sqlStr += " or spn=\"" + spn + "\"";
                    } else {
                        sqlStr += " spn=\"" + spn + "\"";
                    }
                    
                }
                if (!pnn.isEmpty()) {
                    flagPnn = true;
                    if (flagImsi || flagSpn) {
                        sqlStr += " or pnn=\"" + pnn + "\"";
                    } else {
                        sqlStr += " pnn=\"" + pnn + "\"";
                    }
                    
                }
            } else {
                sqlStr = "(spn is NULL or spn=\"\") and (imsi is NULL or imsi=\"\") and (pnn is NULL or pnn=\"\") ";
            }
        }
        String result = "numeric=\"" + numeric + "\" and ( " + sqlStr + ")";
        Xlog.e(TAG,"getQuery result: " + result);
        return result;
    }

    public void addMenu(Menu menu, Activity activity, int add, int restore, String numeric) {
        menu.add(0, MENU_NEW, 0,
                activity.getResources().getString(add))
                .setIcon(android.R.drawable.ic_menu_add);
        menu.add(0, MENU_RESTORE, 0,
                activity.getResources().getString(restore))
                .setIcon(android.R.drawable.ic_menu_upload);
    }

    public void addApnTypeExtra(Intent it) {
    }

    public void updateTetherState(Activity activity) {
    }

    public void initTetherField(Activity activity) {
    }
    
    public Uri getRestoreCarrierUri(int slotId) {
        Uri preferredUri = null;
        if (FeatureOption.MTK_GEMINI_SUPPORT) {
            preferredUri = PREFERRED_URI_LIST[slotId];
        } else {
            preferredUri = Uri.parse(PREFERRED_APN_URI);
        }
        return preferredUri;
    }

    public boolean isSkipApn(String type, IRcseOnlyApnExtension rcseExt) {
        return CMMAIL_TYPE.equals(type) 
                || (RCSE_TYPE.equals(type) && !rcseExt.isRcseOnlyApnEnabled());
    }
}

