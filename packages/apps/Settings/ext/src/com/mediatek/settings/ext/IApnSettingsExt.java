package com.mediatek.settings.ext;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.telephony.TelephonyManager;
import android.view.Menu;

import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.common.featureoption.FeatureOption;

public interface IApnSettingsExt {
    public static final String PREFERRED_APN_URI = "content://telephony/carriers/preferapn";
    public static final String PREFERRED_APN_URI_GEMINI_SIM1 = "content://telephony/carriers_sim1/preferapn";
    public static final String PREFERRED_APN_URI_GEMINI_SIM2 = "content://telephony/carriers_sim2/preferapn";
    public static final String PREFERRED_APN_URI_GEMINI_SIM3 = "content://telephony/carriers_sim3/preferapn";
    public static final String PREFERRED_APN_URI_GEMINI_SIM4 = "content://telephony/carriers_sim4/preferapn";

    public static final String TRANSACTION_START = "com.android.mms.transaction.START";
    public static final String TRANSACTION_STOP = "com.android.mms.transaction.STOP";

    public static final int MENU_NEW = Menu.FIRST;
    public static final int MENU_RESTORE = Menu.FIRST + 1;

    // preferred list for gemini
    public static final Uri PREFERRED_URI_LIST[] = {
            Uri.parse(PREFERRED_APN_URI_GEMINI_SIM1),
            Uri.parse(PREFERRED_APN_URI_GEMINI_SIM2),
            Uri.parse(PREFERRED_APN_URI_GEMINI_SIM3),
            Uri.parse(PREFERRED_APN_URI_GEMINI_SIM4)

    };
    /**
     * 
     * @param type
     * @param apn name to query
     * @param numeric
     * @param sourcetype 
     * @return if the specified apn could be edited.
     */
     boolean isAllowEditPresetApn(String type, String apn, String numeric, int sourcetype);

    /**
     * remove tethering apn setting in not orange load.
     * @param prefSc
     * @param preference
     */
    void removeTetherApnSettings(PreferenceScreen prefSc, Preference preference);
    
    /**
     * judge the apn can be selected or not.
     * @param type 
     */
    boolean isSelectable(String type);

    /**
     * the IntentFilter customize by plugin.
     * @return IntentFilter.
     */
    IntentFilter getIntentFilter();

    /**
     * get BrocastReceiver customize by plugin.
     * @param receiver the receiver in default 
     * @return BrocastReceiver.
     */
    BroadcastReceiver getBroadcastReceiver(BroadcastReceiver receiver);

    /**
     * get query statement for db.
     * @param numeric mcc+mnc on sim card 
     * @param isMVNO sim card is mvno or not  
     * @param spn spn value on sim card  
     * @param imsi imsi value on sim card  
     * @param pnn pnn value on sim card  
     * @return query statement.
     */
    String getFillListQuery(String numeric, boolean isMVNO, String spn, String imsi, String pnn);

    /**
     * add option menu item.
     * @param menu
     * @param activity  
     * @param add  the add menu item resource id
     * @param restore the resore menu item resource id 
     * @param numeric  
     */
    void addMenu(Menu menu, Activity activity, int add, int restore, String numeric);

    /**
     * add intent extra for start ApnEditor.
     * @param it  the intent to start ApnEditor 
     */
    void addApnTypeExtra(Intent it);

    /**
     * update Tether apn State.
     * @param it
     */
    void updateTetherState(Activity activity);

    /**
     * init some fields for tether apn.
     * @param activity
     */
    void initTetherField(Activity activity);

    /**
     * get restore uri customized by plugin.
     * @param slotId 
     */
    Uri getRestoreCarrierUri(int slotId);

    /**
     * judge the screen if enable or disable.
     * @param slotId 
     * @param activity 
     * @return true screen should enable.
     * @return false screen should disable.
     */
    boolean getScreenEnableState(int slotId, Activity activity);       

    /**
     * judge if we should hide the apn.
     * @param type 
     * @param rcseExt
     * @return true hide the apn.
     * @return false show the apn.
     */
    boolean isSkipApn(String type, IRcseOnlyApnExtension rcseExt);
}
