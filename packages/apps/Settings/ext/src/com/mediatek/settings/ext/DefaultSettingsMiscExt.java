package com.mediatek.settings.ext;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.provider.Telephony;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;

import com.mediatek.common.featureoption.FeatureOption;
import com.mediatek.xlog.Xlog;

import java.util.ArrayList;
import java.util.HashMap;

/* Dummy implmentation , do nothing */
public class DefaultSettingsMiscExt implements ISettingsMiscExt {
    
    private static final String TAG = "DefaultSettingsMiscExt";

    public boolean isWifiToggleCouldDisabled(Context context) {
        return true;
    }

    public String getTetherWifiSSID(Context ctx) {
        return ctx.getString(
                    com.android.internal.R.string.wifi_tether_configure_ssid_default);
    }

    public void setTimeoutPrefTitle(Preference pref) {
    }
    
   
    public String getDataUsageBackgroundStrByTag(String defStr, String tag) {
        return defStr;
    }

	
    public TabSpec DataUsageUpdateTabInfo(Activity activity, String tag, TabSpec tab, TabWidget tabWidget, String title) {
        return tab;
    }

    public void DataUsageUpdateSimText(int simColor, TextView title) {
        return;
    }

    public String getFactoryResetTitle(String defaultTitle) {
        Xlog.d(TAG, "title = " + defaultTitle);
        return defaultTitle;
    }
}

