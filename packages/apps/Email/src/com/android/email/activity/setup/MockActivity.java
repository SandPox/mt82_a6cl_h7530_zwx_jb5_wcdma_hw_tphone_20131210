package com.android.email.activity.setup;

import android.app.Activity;

/**
 * Used for Test only. in some case , we need launch a mock activity to start
 * the target activity.
 */
public class MockActivity extends Activity {

}
