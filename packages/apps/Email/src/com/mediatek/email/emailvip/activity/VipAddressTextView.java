package com.mediatek.email.emailvip.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.android.email.R;
import com.android.emailcommon.mail.Address;
import com.android.emailcommon.utility.Utility;
import com.android.email.activity.ChipsAddressTextView;

/**
 * M : This is a RecipientEditTextView which has a custom onItemClickListener.
 * add selected item into database directly.
 */
public class VipAddressTextView extends ChipsAddressTextView {
    private VipListFragment mListFragment = null;
    ///M: Define context
    private Context mContext;
    public VipAddressTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public void setTargetFragment(VipListFragment listFragment) {
        mListFragment = listFragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        super.onItemClick(parent, view, position, id);
        // Add the clicked item into database directly when user click the popup list item.
        Address[] addresses = getAddresses(this);
        if (addresses.length > 0 && mListFragment != null) {
            mListFragment.onAddVip(addresses);
            setText("");
        }
    }

    private Address[] getAddresses(TextView view) {
        /** M: check the address is valid or not, and alter user. @{ */
        String addressList = view.getText().toString().trim();
        if (!Address.isAllValid(addressList)) {
            view.setText("");
            Utility.showToast(mContext, R.string.message_compose_error_invalid_email);
        }
        /** @} */
        Address[] addresses = Address.parse(addressList, false);
        return addresses;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // if KEYCODE_ENTER, don't auto complete the address.
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            return false;
        }
        return super.onKeyUp(keyCode, event);
    }
}

