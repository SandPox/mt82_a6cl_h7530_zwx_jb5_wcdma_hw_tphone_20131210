LOCAL_PATH:= $(call my-dir)
#This is used for building out an library for RCS-e contact

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src)

#/* Vanzo:fangfangjie on: Thu, 31 Oct 2013 11:45:47 +0800
# * zdws fae contacts add libcomisw
# */
LOCAL_JAVA_LIBRARIES := telephony-common
LOCAL_STATIC_JAVA_LIBRARIES := \
    com.android.phone.common \
    com.android.vcard \
    android-common \
    guava \
    android-support-v13 \
    android-support-v4 \
    android-ex-variablespeed \
    com.android.phone.common \
    libcomisw \
    CellConnUtil \
    com.android.contacts.ext
#// End of Vanzo: fangfangjie

LOCAL_JAVA_LIBRARIES += mediatek-framework
LOCAL_JAVA_LIBRARIES += mediatek-common

LOCAL_REQUIRED_MODULES := libvariablespeed

LOCAL_PACKAGE_NAME := Contacts
LOCAL_CERTIFICATE := shared

LOCAL_PROGUARD_FLAG_FILES := proguard.flags

include $(BUILD_PACKAGE)

#/* Vanzo:fangfangjie on: Thu, 31 Oct 2013 11:47:23 +0800
# * zdws fae contacts
# */
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES  := libcomisw:com_isw_sdk_v2.3.3.0.jar
include $(BUILD_MULTI_PREBUILT)
#// End of Vanzo: fangfangjie
# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
