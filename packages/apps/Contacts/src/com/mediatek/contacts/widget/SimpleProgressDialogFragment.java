package com.mediatek.contacts.widget;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.WindowManager;

import com.android.contacts.R;
import com.mediatek.contacts.util.LogUtils;

public class SimpleProgressDialogFragment extends DialogFragment {

    private static String TAG = SimpleProgressDialogFragment.class.getSimpleName();
    private static String DIALOG_TAG = "progress_dialog";
    
    private static SimpleProgressDialogFragment getInstance(FragmentManager fm) {
        SimpleProgressDialogFragment dialog = getExistDialogFragment(fm);
        if (dialog == null) {
            dialog = new SimpleProgressDialogFragment();
        }
        return dialog;
    }

    private static SimpleProgressDialogFragment getExistDialogFragment(FragmentManager fm) {
        return (SimpleProgressDialogFragment) fm.findFragmentByTag(DIALOG_TAG);
    }

    public static void show(FragmentManager fm) {
        SimpleProgressDialogFragment dialog = getInstance(fm);
        if (dialog.isAdded()) {
            LogUtils.d(TAG, "dialog is already shown: " + dialog);
        } else {
            LogUtils.d(TAG, "dialog created and shown: " + dialog);
            dialog.show(fm, DIALOG_TAG);
            dialog.setCancelable(false);
        }
    }

    public static void dismiss(FragmentManager fm) {
        SimpleProgressDialogFragment dialog = getExistDialogFragment(fm);
        if (dialog == null) {
            LogUtils.d(TAG, "dialog never shown before, no need dismiss");
            return;
        }
        if (dialog.isAdded()) {
            LogUtils.d(TAG, "force dismiss dialog: " + dialog);
            dialog.dismissAllowingStateLoss();
        } else {
            LogUtils.d(TAG, "dialog not added, dismiss failed: " + dialog);
        }
    }

    public SimpleProgressDialogFragment() {
    }

    @Override
    public String toString() {
        return String.valueOf(hashCode());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getActivity().getString(R.string.please_wait));
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
    }

}
