package com.mediatek.contacts.util;

import java.util.ArrayList;
import java.util.List;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;

public class DisableViewsUtil extends Handler {

    private static final String TAG = DisableViewsUtil.class.getSimpleName();
    private static final int ACTION_DISABLE = 0;
    private static final int ACTION_ENABLE = 1;
    private static final long DISABLE_DURATION = 500;
    private List<View> mViews;

    private DisableViewsUtil(List<View> views) {
        super(Looper.getMainLooper());
        mViews = views;
    }

    public void disableTemporarily() {
        disableAll();
        sendEmptyMessageDelayed(ACTION_ENABLE, DISABLE_DURATION);
    }

    private void enableAll() {
        for (View v : mViews) {
            if (v != null) {
                v.setEnabled(true);
            }
        }
    }

    private void disableAll() {
        for (View v : mViews) {
            if (v != null) {
                v.setEnabled(false);
            }
        }
    }

    @Override
    public void handleMessage(Message msg) {
        LogUtils.d(TAG, "msg = " + msg.what);
        switch (msg.what) {
        case ACTION_ENABLE:
            enableAll();
            break;
        case ACTION_DISABLE:
            disableAll();
            break;
        default:
            LogUtils.w(TAG, "not supported message: " + msg.what);
            break;
        }
    }

    public static class Builder {
        List<View> mViews = new ArrayList<View>();

        public Builder addView(View view) {
            if (view != null) {
                mViews.add(view);
            } else {
                LogUtils.w(TAG, "view is null, abort adding");
            }
            return this;
        }

        public Builder addViews(List<View> views) {
            mViews.addAll(views);
            return this;
        }

        public DisableViewsUtil build() {
            return new DisableViewsUtil(mViews);
        }
    }
}
