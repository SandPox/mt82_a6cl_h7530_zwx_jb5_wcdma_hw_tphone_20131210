
package com.android.contacts;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

public class NvRAMAdapter {

    private static int FILE_ID = SystemProperties.getInt("ro.nid.productinfo", 25);

    private static String TAG = "NvRAMAdapter";

    public static String readNvRAM(int offset, int length) {
        IBinder binder = ServiceManager.getService("NvRAMAgent");
        NvRAMAgent agent = NvRAMAgent.Stub.asInterface(binder);
        binder = ServiceManager.getService("NvRAMBackup");
        byte[] readBuff = null;
        try {
            readBuff = agent.readFile(FILE_ID);
            Log.e(TAG, "readBuff size is >>> " + readBuff.length);
        } catch (RemoteException e) {
            Log.e(TAG, "readbuff can't read...");
        }
        char charBuf[] = new char[length];
        for (int i = 0; i < charBuf.length; i++) {
            charBuf[i] = (char) readBuff[offset + i];
        }
        StringBuffer sb = new StringBuffer();
        sb.append(charBuf, 0, length);
        return sb.toString();
    }

    public static void writeNvRam(int offset, String resultFlag) {
        IBinder binder = ServiceManager.getService("NvRAMAgent");
        NvRAMAgent agent = NvRAMAgent.Stub.asInterface(binder);
        binder = ServiceManager.getService("NvRAMBackup");
        byte[] writeBuff = null;
        try {
            writeBuff = agent.readFile(FILE_ID);
            Log.e(TAG, "readBuff size is >>> " + writeBuff.length);
        } catch (RemoteException e) {
            Log.e(TAG, "readbuff can't read...");
        }
        byte[] resultFlagByte = resultFlag.getBytes();
        for (int i = 0; i < resultFlagByte.length; i++) {
            Log.i("wangyi", "resultFlagByte >>>>>>>>>>>>" + resultFlagByte[i]);
        }
        for (int i = 0; i < resultFlagByte.length; i++) {
            writeBuff[offset + i] = resultFlagByte[i];
            Log.e(TAG, "writeBuff[] >>>> " + writeBuff[offset + i]);
        }
        try {
            int flag = agent.writeFile(FILE_ID, writeBuff);
            Log.e(TAG, ".... Write SN ....");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
