
package com.android.contacts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NvRAMReceiver extends BroadcastReceiver {

    private static String TAG = "NvRAMReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            if ((!NvRAMAdapter.readNvRAM(552, 8).equals("aabbccdd"))
                    && ((NvRAMAdapter.readNvRAM(62, 2)).equals("10"))) {
                NvRAMAdapter.writeNvRam(552, "aabbccdd");
            }
        }
    }
}
