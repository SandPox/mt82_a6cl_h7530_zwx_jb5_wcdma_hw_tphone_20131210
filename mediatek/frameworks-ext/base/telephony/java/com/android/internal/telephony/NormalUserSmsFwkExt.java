/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.android.internal.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.os.PowerManager;
import android.os.UserHandle;
import android.provider.Telephony.Sms.Intents;

import com.android.internal.telephony.PhoneConstants;
import com.mediatek.xlog.Xlog;



public class NormalUserSmsFwkExt implements INormalUserSmsFwkExt {
    private static final String TAG = "NormalUserSmsFwkExt";
    
    private Context mContext = null;
    private PowerManager.WakeLock mWakeLock;
    private int mUserId = UserHandle.USER_OWNER;
    private final int WAKE_LOCK_TIMEOUT = 5000;
    
    
    private final BroadcastReceiver mUserReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(Intent.ACTION_USER_SWITCHED.equals(action)){
                mUserId = intent.getIntExtra(Intent.EXTRA_USER_HANDLE, UserHandle.USER_OWNER);
                Xlog.d(TAG, "onReceive user switch userId = " + mUserId);
                
                Intent i = new Intent("mediatek.action.USER_SWITCH");
                i.putExtra(Intent.EXTRA_USER_HANDLE, mUserId);
                mContext.sendOrderedBroadcast(i, null);
            }
            /*if (mUserId == UserHandle.USER_OWNER) {
                Intent i = new Intent("mediatek.action.USER_SWITCH_TO_OWNER");
                mWakeLock.acquire(WAKE_LOCK_TIMEOUT);
                mContext.sendOrderedBroadcast(i, null);
            }*/
        }
    };
    
    private void createWakelock() {
        PowerManager pm = (PowerManager)mContext.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SMSDispatcher");
        mWakeLock.setReferenceCounted(true);
    }
    
    public NormalUserSmsFwkExt(Context context) {
        if(context == null) {
            Xlog.d(TAG, "FAIL! context is null");
            return;
        }
        mContext = context;
        createWakelock();
        
        IntentFilter userFilter = new IntentFilter();
        userFilter.addAction(Intent.ACTION_USER_SWITCHED);
        mContext.registerReceiver(mUserReceiver, userFilter);
    }
    
    public void dispatch(Intent intent, int simId, String permission) {
        String action = intent.getAction();
        Xlog.d(TAG, "call dispatch: action = " + action + " permission = " + permission);
        
        if (action.equals(Intents.SMS_RECEIVED_ACTION)) { // SMS
            intent.setAction("mediatek.Telephony.NORMALUSER_SMS_RECEIVED");
        } else if (action.equals(Intents.WAP_PUSH_RECEIVED_ACTION)) { // MMS
            intent.setAction("mediatek.Telephony.NORMALUSER_MMS_RECEIVED");
        } else if (action.equals(Intents.SMS_CB_RECEIVED_ACTION)) { // CB
            intent.setAction("mediatek.Telephony.NORMALUSER_CB_RECEIVED");
        } 
        
        intent.putExtra(PhoneConstants.GEMINI_SIM_ID_KEY, simId);
        
        // Hold a wake lock for WAKE_LOCK_TIMEOUT seconds, enough to give any
        // receivers time to take their own wake locks.
        mWakeLock.acquire(WAKE_LOCK_TIMEOUT);
        mContext.sendOrderedBroadcast(intent, permission);
    }
    
    public boolean isCurrentNormalUser() {
        return mUserId != UserHandle.USER_OWNER;
    }
    
}
