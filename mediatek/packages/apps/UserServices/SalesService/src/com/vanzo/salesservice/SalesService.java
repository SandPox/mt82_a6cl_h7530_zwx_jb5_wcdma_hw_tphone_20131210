
package com.vanzo.salesservice;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SalesService extends Activity {
    private Context mContext = this;



    private static final String[] strs = new String[] {
            "安徽", "福建", "甘肃", "广东", "广西","贵州", "海南", "河北", "河南", "湖北","湖南", "吉林",
            "江苏", "江西", "辽宁", "内蒙古", "山东", "山西", "陕西","四川", "天津", "新疆","云南", "浙江", "重庆"
    };

    @Override
        public void onCreate(Bundle savedInstanceState) {super.onCreate(savedInstanceState);
                setContentView(R.layout.sales_service);
                setTitle(R.string.province);


        ListView lv = (ListView) findViewById(R.id.sales_list);//得到ListView对象的引用 /*为ListView设置Adapter来绑定数据*/ 

        lv.setAdapter(new ArrayAdapter<String>(this,
                        R.layout.list_item, strs));
         
         //添加点击  
         lv.setOnItemClickListener(new OnItemClickListener() {
             @Override  
             public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,  
                     long arg3) {  
                 Intent i = new Intent();
                 i.setClassName(mContext, "com.vanzo.salesservice.SalesDetial");
                 i.putExtra("position", arg2);
                 startActivity(i);
             }
         }); 
         }
}
