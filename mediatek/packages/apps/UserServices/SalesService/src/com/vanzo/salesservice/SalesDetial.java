package com.vanzo.salesservice;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


public class SalesDetial extends Activity {
    private TextView tv;
    private int[] newAddressArray = {
            R.raw.anhui, R.raw.fujian, R.raw.gansu,
            R.raw.guangdong, R.raw.guangxi, R.raw.guizhou,
            R.raw.hainan, R.raw.hebei, R.raw.henan,
            R.raw.hubei, R.raw.hunan,
            R.raw.jilin, R.raw.jiangsu, R.raw.jiangxi,
            R.raw.liaoning, R.raw.neimeng, R.raw.shandong,
            R.raw.shanxi, R.raw.shanxi2,
            R.raw.sichuan, R.raw.tianjin, R.raw.xinjiang,
            R.raw.yunnan, R.raw.zhejiang, R.raw.chongqin
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sales_service_text);
        setTitle(R.string.detail);
        Intent i = getIntent();
        int position = i.getIntExtra("position",0);
        show_province(newAddressArray[position]);
        
    }

    private void show_province(int province) {
        String line = null; 
        try{ 
            InputStream in = getResources().openRawResource(province); 
            BufferedReader d = new BufferedReader(new InputStreamReader(in));
            while (true) {
                line = d.readLine();
                addTextView(line);
                if(line == null) break;
            }    
            in.close();            
        }catch(Exception e){ 
              e.printStackTrace();         
        } 
    }

    private void addTextView(String line) {
        String filter1 = line.substring(line.indexOf("\t") + 1);
        String filter2 = filter1.substring(filter1.indexOf("\t") + 1);
        String filter3 = filter2.substring(filter2.indexOf("\t") + 1);
        String scity = filter1.substring(0, filter1.indexOf("\t"));
        String saddr = filter2.substring(0, filter2.indexOf("\t"));
        String stel = filter3.substring(0, filter3.indexOf("\t"));
        
        LinearLayout sales_text = (LinearLayout)findViewById(R.id.sales_text);
        sales_text.addView(createView(scity, saddr, stel));
    }
    
    private View createView(String scity, String saddr, String stel) { 

        View view =LayoutInflater.from(this).inflate(R.layout.item, null);
         
        TextView city = (TextView)view.findViewById(R.id.city);
        TextView addr = (TextView)view.findViewById(R.id.addr);
        TextView tel = (TextView)view.findViewById(R.id.tel);

        city.setText(scity); 
        addr.setText(saddr); 
        tel.setText(stel);
        return view; 
    } 
}
