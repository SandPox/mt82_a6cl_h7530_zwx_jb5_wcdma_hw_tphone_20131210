package com.vanzo.salesservice;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private Context mContext = this;



    private static final String[] strs = new String[] {
            "宏为简介", "全国售后网点信息", "全国统一客户服务热线", "售后服务说明"
    };//定义一个String数组用来显示ListView的内容private ListView lv;/** Called when the activity is first created. */

    @Override
        public void onCreate(Bundle savedInstanceState) {super.onCreate(savedInstanceState);
                setContentView(R.layout.main);

        ListView lv = (ListView) findViewById(R.id.sales_main);//得到ListView对象的引用 /*为ListView设置Adapter来绑定数据*/ 

        lv.setAdapter(new ArrayAdapter<String>(this,
                        R.layout.list_item, strs));
         
         //添加点击  
         lv.setOnItemClickListener(new OnItemClickListener() {
             @Override  
             public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,  
                     long arg3) {  
                 Intent intent = new Intent();
                 if (arg2 == 0) {
                     intent.setClassName(mContext, "com.vanzo.salesservice.SpecialNumber");
                 } else if (arg2 == 1) {
                     intent.setClassName(mContext, "com.vanzo.salesservice.SalesService");
                 } else if (arg2 == 2) {
                     intent.setClassName(mContext, "com.vanzo.salesservice.SalesServiceHotline");
                 } else if (arg2 == 3) {
                     intent.setClassName(mContext, "com.vanzo.salesservice.Help");
                 }
                 startActivity(intent);
             }
         }); 
         }
}
