package com.userinstructions.demo;

import android.app.Activity;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import com.userinstructions.demo.R;

public class Main extends Activity implements OnTouchListener ,OnClickListener{
	// 屏幕宽度
	public static int screenWidth;
	// 屏幕高度
	public static int screenHeight;
	private MyGallery gallery;
	private ImageView mShowPre;
	private ImageView mShowNext;
	private Button mFirstPage;
	private Button mLastPage;
	private Button mExit;
	public static TextView mCurrentPage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);

		gallery = (MyGallery) findViewById(R.id.gallery);
		gallery.setVerticalFadingEdgeEnabled(false);// 取消竖直渐变边框
		gallery.setHorizontalFadingEdgeEnabled(false);// 取消水平渐变边框
		gallery.setAdapter(new GalleryAdapter(this));
		mShowPre = (ImageView) findViewById(R.id.show_pre);
		mShowPre.setOnClickListener(this);
		mShowNext = (ImageView) findViewById(R.id.show_next);
		mShowNext.setOnClickListener(this);
		mFirstPage = (Button) findViewById(R.id.first_button);
		mFirstPage.setOnClickListener(this);
		mLastPage = (Button) findViewById(R.id.last_button);
		mLastPage.setOnClickListener(this);
		mCurrentPage = (TextView) findViewById(R.id.current_page_title);
		mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
		mExit = (Button) findViewById(R.id.title_back);
		mExit.setOnClickListener(this);
		// gallery.setOnTouchListener(this);
		// gallery.setOnItemSelectedListener(new GalleryChangeListener());
		// FrameLayout.LayoutParams params=(FrameLayout.LayoutParams)
		// gallery.getLayoutParams();
		// params.height=400;
		// params.width=300;

		screenWidth = getWindow().getWindowManager().getDefaultDisplay().getWidth();
		screenHeight = getWindow().getWindowManager().getDefaultDisplay().getHeight();

	}

	float beforeLenght = 0.0f; // 两触点距离
	float afterLenght = 0.0f; // 两触点距离
	boolean isScale = false;
	float currentScale = 1.0f;// 当前图片的缩放比率

	private class GalleryChangeListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			currentScale = 1.0f;
			isScale = false;
			beforeLenght = 0.0f;
			afterLenght = 0.0f;

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {

		// Log.i("","touched---------------");
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_POINTER_DOWN:// 多点缩放
			beforeLenght = spacing(event);
			if (beforeLenght > 5f) {
				isScale = true;
			}
			mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
			break;
		case MotionEvent.ACTION_MOVE:
			if (isScale) {
				afterLenght = spacing(event);
				if (afterLenght < 5f)
					break;
				float gapLenght = afterLenght - beforeLenght;
				if (gapLenght == 0) {
				    mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
					break;
				} else if (Math.abs(gapLenght) > 5f) {
					// FrameLayout.LayoutParams params =
					// (FrameLayout.LayoutParams) gallery.getLayoutParams();
					float scaleRate = gapLenght / 854;// 缩放比例
					// Log.i("",
					// "scaleRate："+scaleRate+" currentScale:"+currentScale);
					// Log.i("", "缩放比例：" +
					// scaleRate+" 当前图片的缩放比例："+currentScale);
					// params.height=(int)(800*(scaleRate+1));
					// params.width=(int)(480*(scaleRate+1));
					// params.height = 400;
					// params.width = 300;
					// gallery.getChildAt(0).setLayoutParams(new
					// Gallery.LayoutParams(300, 300));
					Animation myAnimation_Scale = new ScaleAnimation(currentScale, currentScale + scaleRate, currentScale, currentScale + scaleRate, Animation.RELATIVE_TO_SELF, 0.5f,
							Animation.RELATIVE_TO_SELF, 0.5f);
					// Animation myAnimation_Scale = new
					// ScaleAnimation(currentScale, 1+scaleRate, currentScale,
					// 1+scaleRate);
					myAnimation_Scale.setDuration(100);
					myAnimation_Scale.setFillAfter(true);
					myAnimation_Scale.setFillEnabled(true);
					// gallery.getChildAt(0).startAnimation(myAnimation_Scale);

					// gallery.startAnimation(myAnimation_Scale);
					currentScale = currentScale + scaleRate;
					// gallery.getSelectedView().setLayoutParams(new
					// Gallery.LayoutParams((int)(480), (int)(800)));
					// Log.i("",
					// "===========:::"+gallery.getSelectedView().getLayoutParams().height);
					// gallery.getSelectedView().getLayoutParams().height=(int)(800*(currentScale));
					// gallery.getSelectedView().getLayoutParams().width=(int)(480*(currentScale));
					gallery.getSelectedView().setLayoutParams(new Gallery.LayoutParams((int) (480 * (currentScale)), (int) (854 * (currentScale))));
					// gallery.getSelectedView().setLayoutParams(new
					// Gallery.LayoutParams((int)(320*(scaleRate+1)),
					// (int)(480*(scaleRate+1))));
					// gallery.getSelectedView().startAnimation(myAnimation_Scale);
					// isScale = false;
					beforeLenght = afterLenght;
				}
				mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
				return true;
			}
			break;
		case MotionEvent.ACTION_POINTER_UP:
			isScale = false;
			mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
			break;
		}
		mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
		return false;
	}

	/**
	 * 就算两点间的距离
	 */
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        int viewId = v.getId();
        switch (viewId) {
            case R.id.show_pre:
                if (gallery.getSelectedItemPosition() >= 1) {
                    gallery.setSelection(gallery.getSelectedItemPosition() - 1);
                    mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
                    android.util.Log.i("wangyi", "onClick");
                }
                break;
            case R.id.show_next:
                if (gallery.getSelectedItemPosition() < gallery.getCount() - 1) {
                    gallery.setSelection(gallery.getSelectedItemPosition() + 1);
                    mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
                    android.util.Log.i("wangyi", "onClick");
                }
                break;
            case R.id.first_button :
                gallery.setSelection(0);
                mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
                break;
            case R.id.last_button :
                gallery.setSelection(gallery.getCount() - 1);
                mCurrentPage.setText(String.valueOf(gallery.getSelectedItemPosition() + 1));
                break;
            case R.id.title_back :
                finish();
                break;
            default:
                return;
        }
    }
}