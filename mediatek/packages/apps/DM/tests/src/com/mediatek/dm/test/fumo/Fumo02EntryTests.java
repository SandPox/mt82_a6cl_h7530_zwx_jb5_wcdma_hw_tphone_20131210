package com.mediatek.dm.test.fumo;

//import com.jayway.android.robotium.solo.Solo;

import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.content.Context;
import android.content.Intent;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.util.Log;

import com.mediatek.dm.DmConst;
import com.mediatek.dm.data.PersistentContext;
import com.mediatek.dm.fumo.DmEntry;
import com.redbend.vdm.DownloadDescriptor;

import junit.framework.Assert;

public class Fumo02EntryTests extends ActivityInstrumentationTestCase2<DmEntry> {

    private static final String TAG = "Fumo02EntryTests";
    private static final String VERSION_PREFERENCE = "current_version";
    private static final String UPDATE_PREFERENCE = "system_update";
    private static final String INSTALL_PREFERENCE = "update_install";

    /**
     * ALPS.JB.FPB.p9-p7
     */
    DmEntry mActivity;
    Context mContext;
    Instrumentation mInstrumentation;
    PersistentContext mPersistentContext;
    PreferenceScreen mParentPreference;

    // private Solo mSolo;

    public Fumo02EntryTests() {
        super(DmEntry.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        mInstrumentation = this.getInstrumentation();
        mContext = mInstrumentation.getTargetContext();
        mActivity = this.getActivity();
        mInstrumentation.waitForIdleSync();
        // mSolo = new Solo(mInstrumentation, mActivity);
        mParentPreference = mActivity.getPreferenceScreen();
        mPersistentContext = PersistentContext.getInstance(mContext);
         setActivityInitialTouchMode(false);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void test01Start() throws Exception {
        Log.v(TAG,"test01Start");
//        ActivityMonitor monitor = mInstrumentation.addMonitor("com.mediatek.dm.fumo.DmEntry", null,
//                false);
        mPersistentContext.deleteDeltaPackage();
        mInstrumentation.waitForIdleSync();

        Intent intent = new Intent();
        intent.setAction(DmConst.IntentAction.DM_SWUPDATE);
        mContext.sendBroadcast(intent);

        Thread.sleep(300);
        mInstrumentation.waitForIdleSync();
//        Assert.assertTrue(mInstrumentation.checkMonitorHit(monitor, 1));
//        mActivity.finish();
    }

    public void test02DmSessionStart() throws Exception {
        mPersistentContext.setDMSessionStatus(PersistentContext.STATE_DM_NIA_START);
        mInstrumentation.waitForIdleSync();
        Log.d(TAG, "++++++++++++ setDMSessionStatus STATE_DM_NIA_START++++++++++++"
                + PersistentContext.STATE_DM_NIA_START);
    }

    public void test03DmSessionComplete() throws Exception {
        mPersistentContext.setDMSessionStatus(PersistentContext.STATE_DM_NIA_COMPLETE);
        mInstrumentation.waitForIdleSync();
        Log.d(TAG, "++++++++++++ setDMSessionStatus STATE_DM_NIA_COMPLETE++++++++++++"
                + PersistentContext.STATE_DM_NIA_COMPLETE);

        // assertEquals(PersistentContext.STATE_DM_NIA_COMPLETE,
        // mPersistentContext.getDMSessionStatus());
    }

    public void test04NewVersionDetected() throws Exception {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_NEW_VERSION_DETECTED);
        mInstrumentation.waitForIdleSync();

    }

    public void test05ResumeDownload() throws Exception {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_RESUME_DOWNLOAD);
        DownloadDescriptor dd = new DownloadDescriptor();
        dd.field = Fumo03FlowTests.FIELD;
        dd.size = Fumo03FlowTests.TOTAL;
        mPersistentContext.setDownloadDescriptor(dd);
        mInstrumentation.waitForIdleSync();

        // mSolo = new Solo(mInstrumentation, mActivity);
        // mSolo.clickOnButton(mActivity.getString(R.string.download));

        // assertEquals(PersistentContext.STATE_RESUME_DOWNLOAD,
        // mPersistentContext.getDLSessionStatus());
    }

    public void test06PauseDownload() throws Exception {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_PAUSE_DOWNLOAD);
        mInstrumentation.waitForIdleSync();

        // mSolo = new Solo(mInstrumentation, mActivity);
        // mSolo.clickOnButton(mActivity.getString(R.string.suspend));

        // assertEquals(PersistentContext.STATE_PAUSE_DOWNLOAD,
        // mPersistentContext.getDLSessionStatus());
    }

    public void test07DownloadComplete() {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_DL_PKG_COMPLETE);
        mInstrumentation.waitForIdleSync();
    }

    public void test08DeletePkgWhenPause() {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_PAUSE_DOWNLOAD);
        mInstrumentation.waitForIdleSync();
    }

    public void test09DeletePkgWhenComplete() {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_DL_PKG_COMPLETE);
        mInstrumentation.waitForIdleSync();
    }

    public void test10ClickUpdatePrefWhenPaused() {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_PAUSE_DOWNLOAD);
        mInstrumentation.waitForIdleSync();

        final Runnable mUpdateResults = new Runnable() {
            @UiThreadTest
            public void run() {
                Preference p = mActivity.findPreference(UPDATE_PREFERENCE);
                mActivity.onPreferenceTreeClick(null, p);
            }
        };
        mUpdateResults.run();

        mInstrumentation.waitForIdleSync();
    }

    public void test11ClickUpdatePrefWhenCompleted() {
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_DL_PKG_COMPLETE);
        mInstrumentation.waitForIdleSync();

        final Runnable mUpdateResults = new Runnable() {
            @UiThreadTest
            public void run() {
                Preference p = mActivity.findPreference(UPDATE_PREFERENCE);
                mActivity.onPreferenceTreeClick(null, p);
            }
        };
        mUpdateResults.run();
        mInstrumentation.waitForIdleSync();
    }

    public void test12ClickInstallPref() {
        ActivityMonitor monitor = mInstrumentation.addMonitor("com.mediatek.dm.fumo.DmClient",
                null, false);
        mPersistentContext.setDLSessionStatus(PersistentContext.STATE_DL_PKG_COMPLETE);
        mInstrumentation.waitForIdleSync();

        final Runnable mUpdateResults = new Runnable() {
            @UiThreadTest
            public void run() {
                Preference p = mActivity.findPreference(INSTALL_PREFERENCE);
                mActivity.onPreferenceTreeClick(null, p);
            }
        };
        mUpdateResults.run();

        mInstrumentation.waitForIdleSync();

        mPersistentContext.deleteDeltaPackage();
        mInstrumentation.waitForIdleSync();
        Assert.assertTrue(mInstrumentation.checkMonitorHit(monitor, 1));
    }
}
