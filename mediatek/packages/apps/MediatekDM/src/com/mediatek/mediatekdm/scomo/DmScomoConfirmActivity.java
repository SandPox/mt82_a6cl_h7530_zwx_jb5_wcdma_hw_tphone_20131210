
package com.mediatek.mediatekdm.scomo;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.mediatek.mediatekdm.DmActivity;
import com.mediatek.mediatekdm.DmConst;
import com.mediatek.mediatekdm.DmConst.NotificationInteractionType;
import com.mediatek.mediatekdm.IScomoManager;
import com.mediatek.mediatekdm.R;
import com.mediatek.mediatekdm.util.DialogFactory;

public class DmScomoConfirmActivity extends DmActivity {
    private static final String TAG = DmConst.TAG.SCOMO + "/DmScomoConfirmActivity";
    
    private static final int DIALOG_DOWNLOAD_FAILED = 0;
    private static final int DIALOG_NEW_DP_FOUND = 1;
    private static final int DIALOG_INSTALL_FAILED = 2;
    private static final int DIALOG_INSTALL_OK = 3;
    private static final int DIALOG_GENERIC_ERROR = 4;
    private static final int DIALOG_NETWORK_ERROR = 5;
    
    private int mDialogId = -1;

    @SuppressWarnings("deprecation")
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(NotificationInteractionType.TYPE_SCOMO_NOTIFICATION);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Intent intent = getIntent();
        int action = intent.getIntExtra("action", -1);
        Log.e(TAG, "action is " + action);
        switch (action) {
            case DmScomoState.NEW_DP_FOUND:
                mDialogId = DIALOG_NEW_DP_FOUND;
                break;
            case DmScomoState.DOWNLOAD_FAILED:
                mDialogId = DIALOG_DOWNLOAD_FAILED;
                break;
            case DmScomoState.IDLE:
                if (intent.hasExtra("reason")) {
                    String reason = intent.getStringExtra("reason");
                    Log.d(TAG, "IDLE state with reason: " + reason);
                    if (reason.equals("DM_NETWORK_ERROR")) {
                        mDialogId = DIALOG_NETWORK_ERROR;
                    } else if (reason.equals("DM_FAILED")) {
                        mDialogId = DIALOG_GENERIC_ERROR;
                    } else if (reason.equals("INSTALL_FAILED")) {
                        mDialogId = DIALOG_INSTALL_FAILED;
                    } else if (reason.equals("INSTALL_OK")) {
                        mDialogId = DIALOG_INSTALL_OK;
                    }
                } else {
                    Log.d(TAG, "IDLE state with no reason");
                }
                break;
            default:
                mDialogId = -1;
                break;
        }
        
        if (mDialogId != -1) {
            showDialog(mDialogId);
            if (!bindService()) {
                Log.e(TAG, "Bind failed. Exit!");
                finish();
            }
        } else {
            finish();
        }
    }
    
    @Override
    public void onServiceDisconnected() {
        Log.d(TAG, "onServiceDisconnected()");
        super.onServiceDisconnected();
    };

    
    protected void onDestroy() {
        if (mService != null) {
            unbindService();
        }
        super.onDestroy();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case DIALOG_NEW_DP_FOUND:
                dialog = onConfirmDownload();
                break;
            case DIALOG_INSTALL_FAILED:
                dialog = onInstallFailed();
                break;
            case DIALOG_DOWNLOAD_FAILED:
                dialog = onDownloadFailed();
                break;
            case DIALOG_NETWORK_ERROR:
                dialog = onNetworkError();
                break;
            case DIALOG_INSTALL_OK:
                dialog = onInstallOk();
                break;
            case DIALOG_GENERIC_ERROR:
                dialog = onGenericError();
                break;
            default:
                Log.e(TAG, "Invalid dialog id :" + id);
                break;
        }
        return dialog;
    }
    
    private Dialog onNetworkError() {
        Log.d(TAG, "onNetworkError()");
        return DialogFactory.newAlert(this)
                .setCancelable(false)
                .setTitle(R.string.software_update)
                .setMessage(R.string.networkerror)
                .setNeutralButton(R.string.ok, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v(TAG, "Neutral button clicked, DM failed due to network error!");
                        DmScomoConfirmActivity.this.finish();
                    }
                })
                .create();
    }

    private Dialog onGenericError() {
        Log.v(TAG, "onGenericError()");
        return DialogFactory.newAlert(this)
                .setCancelable(false)
                .setTitle(R.string.software_update)
                .setMessage(R.string.unknown_error)
                .setNeutralButton(R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                })
                .create();
    }

    private Dialog onDownloadFailed() {
        Log.v(TAG, "onDownloadFailed()");
        return DialogFactory.newAlert(this)
                .setCancelable(false)
                .setTitle(R.string.software_update)
                .setMessage(R.string.download_failed)
                .setNeutralButton(R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v(TAG,
                                "Neutral button clicked, download_failed!");
                        if (mService != null) {
                            IScomoManager scomo = mService.getScomoManager();
                            // scomo.pauseDlPkg();// cancel DL session
                            scomo.cancelDlPkg();// trigger report
                            scomo.setScomoState(DmScomoState.IDLE, null, null);
                        }
                        finish();
                    }
                })
                .create();
    }

    private Dialog onInstallOk() {
        Log.v(TAG, "onInstallOk");
        return DialogFactory.newAlert(this)
                .setCancelable(false)
                .setTitle(R.string.software_update)
                .setMessage(R.string.install_complete)
                .setNeutralButton(R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }).create();
    }

    private Dialog onInstallFailed() {
        Log.v(TAG, "onInstallFailed");

        return DialogFactory.newAlert(this)
                .setCancelable(false)
                .setTitle(R.string.software_update)
                .setMessage(R.string.install_failed)
                .setNeutralButton(R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                })
                .create();
    }

    private Dialog onConfirmDownload() {
        Log.v(TAG, "onConfirmDownload");

        return DialogFactory.newAlert(this)
                .setCancelable(false)
                .setTitle(R.string.software_update)
                .setMessage(R.string.confirm_download_msg)
                .setPositiveButton(R.string.ok, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v(TAG, "positive button clicked, start to download");
                        if (mService != null) {
                            mService.getScomoManager().startDlPkg();
                        } else {
                            Log.d(TAG, "mService is null");
                        }
                        DmScomoConfirmActivity.this.finish();
                    }
                })
                .setNegativeButton(R.string.cancel, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mService != null) {
                            IScomoManager scomo = mService.getScomoManager();
                            switch (which) {
                                case DialogInterface.BUTTON_NEGATIVE:
                                    Log.v(TAG, "negative button clicked,reset scomo state & cancelDLScomoPkg");
                                    scomo.cancelDlPkg();// trigger report
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            Log.d(TAG, "mService is null");
                        }
                        finish();
                    }
                })
                .create();
    }
}
