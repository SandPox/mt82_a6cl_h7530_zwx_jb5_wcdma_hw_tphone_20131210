package com.android.wallpaper.multipicker;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridLayout.LayoutParams;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Handler;
import android.os.Message;

public class WallpaperGridView extends Activity implements View.OnClickListener{

    private boolean mIsWallpaperSet;
    private GridView mGridView;
    private ImageButton mGalleryBtn;
    private String[] mImages;
    private String[] mThumbs;
    private int mChoose;
    private int mWidth;
    private TextView mTitleTv;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallpapergridview);

        initialization();

        if (WallpaperChooser.WALLPAPER == mChoose) {
            mTitleTv.setText(getResources().getString(R.string.title_bar_wallpaper));
        } else {
            mTitleTv.setText(getResources().getString(R.string.title_bar_lockpaper));
        }

        mGridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (mIsWallpaperSet) {
                    return;
                }
                mIsWallpaperSet = true;
                Intent intent = new Intent();
                intent.setClass(WallpaperGridView.this, Preview.class);
                int imageResId = getResources().getIdentifier(mImages[position], "drawable",
                    getApplication().getPackageName());
                intent.putExtra("imageResId", imageResId);
                intent.putExtra("position", position);
                intent.putExtra("choose", mChoose);
                startActivity(intent);
            }
        });

    }

    public void initialization() {
        mChoose = getIntent().getIntExtra("choose", WallpaperChooser.WALLPAPER);
        mWidth = getWindowManager().getDefaultDisplay().getWidth();

        mGridView = (GridView) findViewById(R.id.gridview);
        mImages = getResources().getStringArray(R.array.wallpapers);
        mThumbs = getResources().getStringArray(R.array.wallpapers);
        mGridView.setAdapter(new ImageAdapter(this));

        mTitleTv = (TextView) findViewById(R.id.title_choice_wallpaper);
        mGalleryBtn = (ImageButton) findViewById(R.id.imagebutton);
        Button backBtn = (Button) findViewById(R.id.back_btn);

        mGalleryBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imagebutton:
                Intent intent = new Intent(Intent.ACTION_SET_WALLPAPER);
                intent.setClassName("com.android.gallery3d",
                    "com.android.gallery3d.app.Wallpaper");
                intent.putExtra("aphonewallpaper", true);
                if (WallpaperChooser.WALLPAPER == getIntent().getIntExtra("choose",
                        WallpaperChooser.WALLPAPER)) {
                    intent.putExtra("setwallpaper", 1);
                } else {
                    intent.putExtra("setwallpaper", 2);
                }
                startActivity(intent);
                break;
            case R.id.back_btn:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsWallpaperSet = false;
    }

    private class ImageAdapter extends BaseAdapter {
        private Context mContext;

        ImageAdapter(Context context) {
            mContext = context;
        }

        public int getCount() {
            return mThumbs.length;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView image;

            if (convertView == null) {
                image = new ImageView(mContext);
            } else {
                image = (ImageView) convertView;
            }

            String thumbName = mThumbs[position] + "_small";
            int thumbRes = getResources().getIdentifier(thumbName, "drawable",
                    getApplication().getPackageName());
            image.setImageResource(thumbRes);
            return image;
        }
    }
}
