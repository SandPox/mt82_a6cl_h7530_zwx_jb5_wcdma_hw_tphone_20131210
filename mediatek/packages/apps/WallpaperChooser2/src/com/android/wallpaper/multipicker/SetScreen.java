package com.android.wallpaper.multipicker;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.view.WindowManager;
import android.view.Window;
import android.widget.TextView;
import android.graphics.Matrix;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SetScreen extends Activity implements View.OnClickListener {
    private final int ANIM_DURATION = 1000;
    private int WALLPAPER = 1;
    private int LOCKPAPER = 1 << 1;
    private boolean mIsRepeat;
    private int mImageResId;
    private int mChoose;
    private int mIsVerticalScreenPng;
    private Button mProcessBtn;
    private Button mAllBtn;
    private Button mCancelBtn;
    private SharedPreferences mPreferences;
    private Editor mEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.set_screen);

        initialization();
    }

    public void initialization() {
        mProcessBtn = (Button) findViewById(R.id.set_process);
        mAllBtn = (Button) findViewById(R.id.set_all);
        mCancelBtn = (Button) findViewById(R.id.cancel);
        mImageResId = getIntent().getIntExtra("imageResId", 0);
        mIsVerticalScreenPng = getIntent().getIntExtra("isverticalscreen", 2);
        mChoose = getIntent().getIntExtra("choose", WallpaperChooser.WALLPAPER);
        // for the "two process one interface"
        if (WallpaperChooser.WALLPAPER == mChoose) {
            mProcessBtn.setText(getResources().getString(R.string.set_lockpaper));
            mChoose = LOCKPAPER;
        } else if(WallpaperChooser.LOCKSCREEN == mChoose){
            mProcessBtn.setText(getResources().getString(R.string.set_wallpaper));
            mChoose = WALLPAPER;
        }

        // for the "apply button"
        if (getIntent().getBooleanExtra("setWallpaper", false)) {
            dismissPopupWindow(WALLPAPER);
        } else if (getIntent().getBooleanExtra("setLockpaper", false)) {
            dismissPopupWindow(LOCKPAPER);
        }
        mProcessBtn.setOnClickListener(this);
        mAllBtn.setOnClickListener(this);
        mCancelBtn.setOnClickListener(this);
    }

    public void exitAnimation() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.exit_anim);
        RelativeLayout setScreen = (RelativeLayout) findViewById(R.id.set_screen);
        setScreen.setBackgroundColor(0x00000000);
        setScreen.findViewById(R.id.cancel).setClickable(false);
        anim.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {

            }
        });
        anim.setFillAfter(true);
        setScreen.startAnimation(anim);
    }

    private void selectWallpaper(int setScreen) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), mImageResId);
        try {
            if ((setScreen & LOCKPAPER) == LOCKPAPER) {
                WallpaperChooser.saveBitmap(bitmap, "lockpaper", mIsVerticalScreenPng);
            }
            if ((setScreen & WALLPAPER) == WALLPAPER) {
                WallpaperManager wpm = (WallpaperManager) getSystemService(WALLPAPER_SERVICE);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                wpm.setStream(new ByteArrayInputStream(out.toByteArray()));
                WallpaperChooser.saveBitmap(bitmap, "wallpaper", mIsVerticalScreenPng);
                out.close();
            }
        } catch (IOException e) {
            Log.e("WallpaperChooser", "Failed to set wallpaper: " + e);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.set_process:
                dismissPopupWindow(mChoose);
                break;
            case R.id.set_all:
                dismissPopupWindow(WALLPAPER | LOCKPAPER);
                break;
            case R.id.cancel:
                setResult(RESULT_CANCELED);
                exitAnimation();
                break;
            default:
                break;
        }
    }

    private void dismissPopupWindow(final int setScreen) {
        if (mIsRepeat) {
            return;
        }
        mIsRepeat = true;
        exitAnimation();
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View viewOK = layoutInflater.inflate(R.layout.layout_ok, null);
        TextView content = (TextView)viewOK.findViewById(R.id.dialog_content);
        content.setText(getResources().getString(R.string.save_photo));
        final Toast t = new Toast(SetScreen.this);
        t.setView(viewOK);
        t.setDuration(Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();

        Handler handler = new Handler();
        final Runnable exitAnim = new Runnable() {
            public void run() {
                selectWallpaper(setScreen);
                t.cancel();
            }
        };
        setResult(RESULT_OK);
        handler.postDelayed(exitAnim, ANIM_DURATION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsRepeat = false;
    }

}
