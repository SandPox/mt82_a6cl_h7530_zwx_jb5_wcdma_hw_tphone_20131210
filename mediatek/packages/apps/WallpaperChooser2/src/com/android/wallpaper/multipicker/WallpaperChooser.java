package com.android.wallpaper.multipicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap.CompressFormat;
import android.widget.TableRow;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.graphics.BitmapFactory;

public class WallpaperChooser extends Activity implements View.OnClickListener {
    public static final int WALLPAPER = 0, LOCKSCREEN = 1;
    private String TAG = "WallpaperChooser";
    private SharedPreferences mPreferences;
    private Editor mEdit;
    private String mFilePath;
    private BroadcastReceiver mSaveLiveWallpaperReceiver = new SaveLiveWallpaperReceiver();
    private Button mBackBtn;
    private TableRow mWallpaperSettings;
    private TableRow mDynamicSettings;
    private TableRow mLockpaperSettings;
    private LinearLayout mWallpaper;
    private LinearLayout mDynamic;
    private LinearLayout mLockpaper;
    private ImageView mDynamicImg;
    private ImageView mWallpaperImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallpaper_mi_chooser);
        initialization();

        registerLivepaperListener();
    }

    public void initialization() {
        mPreferences = getSharedPreferences("preferences", MODE_WORLD_READABLE);
        mEdit = mPreferences.edit();

        mBackBtn = (Button) findViewById(R.id.back_btn);
        mWallpaperSettings = (TableRow) findViewById(R.id.wallpaper_settings);
        mDynamicSettings = (TableRow) findViewById(R.id.dynamic_settings);
        mLockpaperSettings = (TableRow) findViewById(R.id.lockpaper_settings);

        mWallpaper = (LinearLayout) findViewById(R.id.wallpaper);
        mDynamic = (LinearLayout) findViewById(R.id.dynamic);
        mLockpaper = (LinearLayout) findViewById(R.id.lockpaper);

        mDynamicImg = (ImageView) findViewById(R.id.dynamic_img);
        mWallpaperImg = (ImageView) findViewById(R.id.wallpaper_img);

        mBackBtn.setOnClickListener(this);
        mWallpaperSettings.setOnClickListener(this);
        mDynamicSettings.setOnClickListener(this);
        mLockpaperSettings.setOnClickListener(this);
    }
    private void registerLivepaperListener() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DYNAMICWALLPAPER");
        intentFilter.addAction("android.intent.action.WALLPAPER");
        registerReceiver(mSaveLiveWallpaperReceiver, intentFilter);
    }


    private class SaveLiveWallpaperReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
                mFilePath =intent.getStringExtra("dynamicwallpaper");
            if (action.equals("android.intent.action.DYNAMICWALLPAPER")) {
                mEdit.putString("filepath", mFilePath);
                mEdit.putBoolean("iswallpaper", false);
            } else if (action.equals("android.intent.action.WALLPAPER")) {
                mEdit.putBoolean("iswallpaper", true);
            }
            mEdit.commit();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_SET_WALLPAPER);
        switch (v.getId()) {
            case R.id.wallpaper_settings:
                intent.setClass(this, WallpaperGridView.class);
                intent.putExtra("choose", WALLPAPER);
                startActivityForResult(intent, 0);
                break;
            case R.id.dynamic_settings:
                intent.setClass(this,LiveWallpaperActivity.class);
                startActivity(intent);
                break;
            case R.id.lockpaper_settings:
                intent.setClass(this, WallpaperGridView.class);
                intent.putExtra("choose", LOCKSCREEN);
                startActivityForResult(intent, 0);
                break;
            case R.id.back_btn:
                finish();
                break;
            default:
                break;
        }
    }

    protected static void saveBitmap(Bitmap bmp, String string, int verticalscreen) {
        File file_dir = new File(
                "/data/data/com.android.wallpaper.multipicker/files");
        if (!file_dir.exists()) {
            file_dir.mkdir();
        }
        File liviFile = new File(
                "/data/data/com.android.wallpaper.multipicker/files", string);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = null;
/* Vanzo:Kern on: Mon, 23 Sep 2013 20:50:15 +0800
 * TODO: replace this line with your comment
        if (DisplayMetrics.DENSITY_DEVICE == 240) {
            bitmap = Bitmap.createBitmap(bmp, 0, 0,
                    (int) bmp.getHeight() * 1 / 3, bmp.getHeight());
        } else {
            bitmap = Bitmap.createBitmap(bmp, 0, 0,
                    (int) bmp.getHeight() / 2, bmp.getHeight());
        }
 */
        if (string.equals("lockpaper") && verticalscreen == 2) {
            bitmap = Bitmap.createBitmap(bmp, (int) bmp.getWidth() / 4, 0, (int) bmp.getWidth() / 2, bmp.getHeight());
        } else {
            bitmap = bmp;
        }
// End of Vanzo: Kern
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        FileOutputStream fosto;
        try {
            fosto = new FileOutputStream(liviFile);
            fosto.write(baos.toByteArray());
            fosto.flush();
            fosto.close();
            Runtime.getRuntime().exec(
                    "chmod 777 /data/data/com.android.wallpaper.multipicker/files");
            Runtime.getRuntime().exec(
                    "chmod 777 /data/data/com.android.wallpaper.multipicker/files/" + string);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Drawable drawable = getWallpaper("lockpaper");
        if (drawable == null) {
            drawable = getWallpaper();
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            saveBitmap(bitmap, "lockpaper", 2);
        }
        mLockpaper.setBackgroundDrawable(drawable);

        drawable = getWallpaper("wallpaper");
        if (drawable == null) {
            drawable = getWallpaper();
        }
        mWallpaper.setBackgroundDrawable(drawable);

        mFilePath = mPreferences.getString("filepath", "");
        drawable = getWallpaper(mFilePath);
        if (drawable == null) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.default_dynamic);
            drawable = new BitmapDrawable(getResources(), bm);
            mEdit.putBoolean("iswallpaper", true);
            mEdit.commit();
        }
        mDynamic.setBackgroundDrawable(drawable);

        boolean isWallpaper = mPreferences.getBoolean("iswallpaper", true);
        if (isWallpaper) {
            mWallpaperImg.setVisibility(View.VISIBLE);
            mDynamicImg.setVisibility(View.GONE);
        } else {
            mDynamicImg.setVisibility(View.VISIBLE);
            mWallpaperImg.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSaveLiveWallpaperReceiver != null) {
            unregisterReceiver(mSaveLiveWallpaperReceiver);
            mSaveLiveWallpaperReceiver = null;
        }
    }

    private Drawable getWallpaper(String wallpaper) {
        Drawable mDrawable = null;
        String filePath = "/data/data/com.android.wallpaper.multipicker/files/"
                        + wallpaper;
        try {
            File f = new File(filePath);
            FileInputStream is = new FileInputStream(f);
            if (is != null) {
                mDrawable = Drawable.createFromStream(is, wallpaper);
                is.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mDrawable;
    }
}
