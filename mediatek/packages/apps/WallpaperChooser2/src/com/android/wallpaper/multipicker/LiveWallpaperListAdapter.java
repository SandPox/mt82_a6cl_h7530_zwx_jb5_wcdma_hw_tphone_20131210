/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.wallpaper.multipicker;

import android.app.WallpaperInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.service.wallpaper.WallpaperService;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.graphics.PixelFormat;
import com.mediatek.common.featureoption.FeatureOption;
import com.mediatek.xlog.Xlog;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import android.util.DisplayMetrics;

public class LiveWallpaperListAdapter extends BaseAdapter implements ListAdapter {
    private static final String LOG_TAG = "LiveWallpaperListAdapter";
    private static final String VIDEO_LIVE_WALLPAPER_PACKAGE = "com.mediatek.vlw";
    private final LayoutInflater mInflater;
    private final PackageManager mPackageManager;
    private List<LiveWallpaperInfo> mWallpapers;

    @SuppressWarnings("unchecked")
    public LiveWallpaperListAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPackageManager = context.getPackageManager();

        List<ResolveInfo> list = mPackageManager.queryIntentServices(
                new Intent(WallpaperService.SERVICE_INTERFACE),
                PackageManager.GET_META_DATA);

        mWallpapers = generatePlaceholderViews(list.size());

        new LiveWallpaperEnumerator(context).execute(list);
    }

    private List<LiveWallpaperInfo> generatePlaceholderViews(int amount) {
        /// M: just new an empty ArrayList and dynamicly add items to fix the issue caused by VLW. @{
        ArrayList<LiveWallpaperInfo> list = new ArrayList<LiveWallpaperInfo>();
        /// @}
        return list;
    }

    public int getCount() {
        if (mWallpapers == null) {
            return 0;
        }
        return mWallpapers.size();
    }

    public Object getItem(int position) {
        return mWallpapers.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

/* Vanzo:hexiuhui on: Fri, 13 Sep 2013 19:10:28 +0800
 * TODO: add MiWallpaper
 */
    public Bitmap drawableToBitmap(Drawable drawable) {

        Bitmap bitmap = Bitmap.createBitmap(
                drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(),
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }
// End of Vanzo: hexiuhui
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.live_wallpaper_entry, parent, false);

            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LiveWallpaperInfo wallpaperInfo = mWallpapers.get(position);
        if (holder.thumbnail != null) {
            holder.thumbnail.setImageDrawable(wallpaperInfo.thumbnail);
        }

        if (holder.title != null && wallpaperInfo.info != null) {
            holder.title.setText(wallpaperInfo.info.loadLabel(mPackageManager));
            if (holder.thumbnail == null) {
                holder.title.setCompoundDrawablesWithIntrinsicBounds(null, wallpaperInfo.thumbnail,
                    null, null);
            }
        }

        return convertView;
    }

    public class LiveWallpaperInfo {
        public Drawable thumbnail;
        public WallpaperInfo info;
        public Intent intent;
    }

    private class ViewHolder {
        TextView title;
        ImageView thumbnail;
    }

    private class LiveWallpaperEnumerator extends
            AsyncTask<List<ResolveInfo>, LiveWallpaperInfo, Void> {
        private Context mContext;
        private int mWallpaperPosition;

        public LiveWallpaperEnumerator(Context context) {
            super();
            mContext = context;
            mWallpaperPosition = 0;
        }

        @Override
        protected Void doInBackground(List<ResolveInfo>... params) {
            final PackageManager packageManager = mContext.getPackageManager();

            List<ResolveInfo> list = params[0];

            final Resources res = mContext.getResources();
            BitmapDrawable galleryIcon = (BitmapDrawable) res.getDrawable(
                    R.drawable.livewallpaper_placeholder);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
            paint.setTextAlign(Paint.Align.CENTER);
            Canvas canvas = new Canvas();

            Collections.sort(list, new Comparator<ResolveInfo>() {
                final Collator mCollator;

                {
                    mCollator = Collator.getInstance();
                }

                public int compare(ResolveInfo info1, ResolveInfo info2) {
                    return mCollator.compare(info1.loadLabel(packageManager),
                            info2.loadLabel(packageManager));
                }
            });

            /// M: moved the size computing out of the for loop. @{
            int thumbWidth = res.getDimensionPixelSize(
                    R.dimen.live_wallpaper_thumbnail_width);
            int thumbHeight = res.getDimensionPixelSize(
                    R.dimen.live_wallpaper_thumbnail_height);
            /// @}
            for (ResolveInfo resolveInfo : list) {
                WallpaperInfo info = null;
                try {
                    info = new WallpaperInfo(mContext, resolveInfo);
                } catch (XmlPullParserException e) {
                    Log.w(LOG_TAG, "Skipping wallpaper " + resolveInfo.serviceInfo, e);
                    continue;
                } catch (IOException e) {
                    Log.w(LOG_TAG, "Skipping wallpaper " + resolveInfo.serviceInfo, e);
                    continue;
                }
    
                /// M: skip Video Live Wallpaper. @{
                if (VIDEO_LIVE_WALLPAPER_PACKAGE.equals(info.getPackageName())) {
                    Xlog.w(LOG_TAG, "Skipping wallpaper " + info.getPackageName());
                    continue;
                }
                /// @}

                /// M: add MTK Stereo 3D support. @{
                if (FeatureOption.MTK_S3D_SUPPORT) {
                    if (!info.getServiceInfo().exported) {
                        Xlog.w(LOG_TAG, "Skipping wallpaper " + resolveInfo.serviceInfo);
                        continue;
                    }
                }
                /// @}

                LiveWallpaperInfo wallpaper = new LiveWallpaperInfo();
                wallpaper.intent = new Intent(WallpaperService.SERVICE_INTERFACE);
                wallpaper.intent.setClassName(info.getPackageName(), info.getServiceName());
                wallpaper.info = info;

                /// M: fix the thumbnail image null bug. @{
                BitmapDrawable thumb = (BitmapDrawable)info.loadThumbnail(mPackageManager);
                BitmapDrawable thumbNew;
                if (thumb != null) {
                    Bitmap bitmap = Bitmap.createScaledBitmap(thumb.getBitmap(), thumbWidth, thumbHeight, false);
                    thumbNew = new BitmapDrawable(res, bitmap);
                } else {
                    Bitmap thumbnail = Bitmap.createBitmap(thumbWidth, thumbHeight,
                            Bitmap.Config.ARGB_8888);

                    paint.setColor(res.getColor(R.color.live_wallpaper_thumbnail_background));
                    canvas.setBitmap(thumbnail);
                    canvas.drawPaint(paint);

                    galleryIcon.setBounds(0, 0, thumbWidth, thumbHeight);
                    galleryIcon.setGravity(Gravity.CENTER);
                    galleryIcon.draw(canvas);

                    String title = info.loadLabel(packageManager).toString();

                    paint.setColor(res.getColor(R.color.live_wallpaper_thumbnail_text_color));
                    paint.setTextSize(
                            res.getDimensionPixelSize(R.dimen.live_wallpaper_thumbnail_text_size));

                    canvas.drawText(title, (int) (thumbWidth * 0.5),
                            thumbHeight - res.getDimensionPixelSize(
                                    R.dimen.live_wallpaper_thumbnail_text_offset), paint);

                    thumbNew = new BitmapDrawable(res, thumbnail);
                }

                wallpaper.thumbnail = thumbNew;
/* Vanzo:hexiuhui on: Fri, 13 Sep 2013 19:21:59 +0800
 * TODO: add MiWallpaper
 */
                    Bitmap mithumbnail = drawableToBitmap(wallpaper.thumbnail);
                    saveBitmap(mithumbnail, info.getServiceName());
// End of Vanzo: hexiuhui
                /// @}
                publishProgress(wallpaper);
            }


            return null;
        }


        @Override
        protected void onProgressUpdate(LiveWallpaperInfo...infos) {
            for (LiveWallpaperInfo info : infos) {
                info.thumbnail.setDither(true);
                if (mWallpaperPosition < mWallpapers.size()) {
                    mWallpapers.set(mWallpaperPosition, info);
                } else {
                    mWallpapers.add(info);
                }
                mWallpaperPosition++;
                LiveWallpaperListAdapter.this.notifyDataSetChanged();
            }
        }
    }
/* Vanzo:hexiuhui on: Fri, 13 Sep 2013 19:22:42 +0800
 * TODO: add MiWallpaper
 */
    protected static void saveBitmap(Bitmap bmp, String string) {

        File file_dir = new File("/data/data/com.android.wallpaper.multipicker/files");
        if (!file_dir.exists()) {
            file_dir.mkdir();
        }
        File liviFile = new File("/data/data/com.android.wallpaper.multipicker/files", string);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = null;
        if (string.equals(string)) {
             bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight());
         } else {
             bitmap = bmp;
         }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        FileOutputStream fosto;
        try {
            fosto = new FileOutputStream(liviFile);

            fosto.write(baos.toByteArray());
            fosto.flush();
            fosto.close();
            Runtime.getRuntime().exec(
                    "chmod 777 /data/data/com.android.wallpaper.multipicker/files");
            Runtime.getRuntime().exec(
                    "chmod 777 /data/data/com.android.wallpaper.multipicker/files" + string);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
// End of Vanzo: hexiuhui
}
