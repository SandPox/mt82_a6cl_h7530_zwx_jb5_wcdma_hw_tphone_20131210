package com.android.wallpaper.multipicker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.FrameLayout;
import android.view.WindowManager;
import android.view.Window;
import android.view.View.OnTouchListener;
import android.view.animation.TranslateAnimation;
import android.os.Handler;
import android.os.Message;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Gravity;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.widget.Toast;
import android.app.WallpaperManager;

public class Preview extends Activity implements View.OnClickListener {
    private String TAG = "Preview";
    private int mImageResId, mChoose;
    private int mLastIdx = -1;
    private int mDesiredMinimumWidth;
    private int mDesiredMinimumHeight;
    private int mIsVerticalScreenPng;
    private final int ANIM_DURATION = 500;
    private final int MSG_SINGLECLICK = 1;
    private final int MSG_DOUBLECLICK = 2;
    private static final int GET_CODE = 0;
    private static final int DOUBLE_CLICK_TIME = 350;
    private boolean mShowGlanceOver = true;
    private boolean mIsRepeat;
    private boolean mWaitDouble = true;
    private Button mApplyBtn, mBackBtn;
    private ImageButton mAnywayBtn, mMoreBtn;
    private ScrollLayout mScroll;
    private String[] mImages;
    private Bitmap[] mBitmaps;
    private FrameLayout mPreviewLayout;
    private RelativeLayout mApplyLayout;
    private RelativeLayout mGlanceOverTitle;
    private ImageView mGlanceOverImg;
    private ImageView mScrollImg;
    private Bitmap mBitMap;
    private SharedPreferences mPreferences;
    private Editor mEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview);

        int position = getIntent().getIntExtra("position", 0);
        initialization();
        setGlanceoverImg(mChoose);
        initScroll();

        genPagesDelayed(position);
        mScroll.setToScreen(position);
        mScroll.setPageListener(new mPageListener());
    }

    class mPageListener implements ScrollLayout.PageListener {
        @Override
        public void page(int page) {
            mImageResId = getResources().getIdentifier(mImages[page], "drawable",
                getApplication().getPackageName());
            mIsRepeat = false;
            genPagesDelayed(page);
            Bitmap resbitmap = BitmapFactory.decodeResource(getResources(), mImageResId);
            if (resbitmap.getWidth() == mDesiredMinimumWidth) {
                mIsVerticalScreenPng = 1;
                mAnywayBtn.setClickable(false);
                mAnywayBtn.setVisibility(View.GONE);
            } else {
                mIsVerticalScreenPng = 2;
                if (WallpaperChooser.LOCKSCREEN == mChoose) {
                    mAnywayBtn.setClickable(false);
                    mAnywayBtn.setVisibility(View.GONE);
                } else {
                    mAnywayBtn.setClickable(true);
                    mAnywayBtn.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    public void initialization() {
        Preview.this.overridePendingTransition(R.anim.push_in, R.anim.push_out);

        mDesiredMinimumWidth = getWindowManager().getDefaultDisplay().getWidth();
        mDesiredMinimumHeight = getWindowManager().getDefaultDisplay().getHeight();
        mImageResId = getIntent().getIntExtra("imageResId", 0);
        mChoose = getIntent().getIntExtra("choose", WallpaperChooser.WALLPAPER);
        mPreferences = getSharedPreferences("preferences", MODE_WORLD_READABLE);
        mEdit = mPreferences.edit();

        mBackBtn = (Button) findViewById(R.id.back_btn);
        mAnywayBtn = (ImageButton) findViewById(R.id.anyway_btn);
        mApplyBtn = (Button) findViewById(R.id.apply_btn);
        mMoreBtn = (ImageButton) findViewById(R.id.more_btn);
        mScroll = (ScrollLayout) findViewById(R.id.scroll);
        mApplyLayout = (RelativeLayout) findViewById(R.id.apply);
        mGlanceOverTitle = (RelativeLayout) findViewById(R.id.glanceover_title);
        mGlanceOverImg = (ImageView) findViewById(R.id.glanceover_img);
        mPreviewLayout = (FrameLayout) findViewById(R.id.preview_layout);
        mImages = getResources().getStringArray(R.array.wallpapers);
        mBitmaps = new Bitmap[mImages.length];

        mBackBtn.setOnClickListener(this);
        mAnywayBtn.setOnClickListener(this);
        mApplyBtn.setOnClickListener(this);
        mMoreBtn.setOnClickListener(this);
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what){
                case MSG_SINGLECLICK:
                    singleClick();
                    break;
                case MSG_DOUBLECLICK:
                    doubleClick();
                    break;
            }
        };
    };

    private void genPagesDelayed(int idx) {
        if (mLastIdx == idx) return;
        // first create
        if (mLastIdx == -1) {
            genPage(idx - 1);
            genPage(idx);
            genPage(idx + 1);
        } else {
            if (idx > mLastIdx) {
                // forward
                recyclePage(idx - 2);
                genPage(idx + 1);
            } else {
                // backward
                recyclePage(idx + 2);
                genPage(idx - 1);
            }
        }
        mLastIdx = idx;
    }

    private void recyclePage(int idx) {
        if (true) return;
        if (idx < 0 || idx >= mImages.length) return;
        if (mBitmaps[idx] == null) return;
        mBitmaps[idx].recycle();
        mBitmaps[idx] = null;
        ImageView iv = ((ImageView) mScroll.getChildAt(idx));
        iv.setImageDrawable(null);
    }

    private void genPage(int idx) {
        if (idx < 0 || idx >= mImages.length) return;
        if (mBitmaps[idx] != null) return;
        int resId = getResources().getIdentifier(mImages[idx], "drawable", getApplication().getPackageName());
        Bitmap bmpFromRes = BitmapFactory.decodeResource(getResources(), resId);
        int width = getWindowManager().getDefaultDisplay().getWidth();
        if (bmpFromRes.getWidth() == width){
            mBitmaps[idx] = Bitmap.createBitmap(bmpFromRes, 0, 0,
                    bmpFromRes.getWidth(), bmpFromRes.getHeight());
        } else {
            mBitmaps[idx] = Bitmap.createBitmap(bmpFromRes, (int) bmpFromRes.getWidth() / 4, 0,
                    (int) bmpFromRes.getWidth() / 2, bmpFromRes.getHeight());
        }
        mBitmaps[idx].setHasAlpha(false);

        ((ImageView) mScroll.getChildAt(idx)).setImageDrawable(new BitmapDrawable(mBitmaps[idx]));
        //bmpFromRes.recycle();
    }

    public void initScroll() {
        for (String image : mImages) {
            mScrollImg = new ImageView(this);
            mScroll.addView(mScrollImg);
            mScrollImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                // TODO Auto-generated method stub
                    if (mWaitDouble == true) {
                        mWaitDouble = false;
                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(DOUBLE_CLICK_TIME);
                                    if (mWaitDouble == false) {
                                        mWaitDouble = true;
                                        Message message = new Message();
                                        message.what = MSG_SINGLECLICK;
                                        mHandler.sendMessage(message);
                                    }
                                } catch(InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();
                    } else {
                        mWaitDouble = true;
                        Message message = new Message();
                        message.what = MSG_DOUBLECLICK;
                        mHandler.sendMessage(message);
                    }
                }
            });
        }
    }
    private void singleClick() {
        mIsRepeat = false;
        if (mShowGlanceOver) {
            showGlanceover();
        } else {
            showApply();
        }
    }

    private void doubleClick() {
    }

    public void setGlanceoverImg(int choose) {
        if (WallpaperChooser.WALLPAPER == mChoose) {
            mGlanceOverImg.setBackgroundResource(R.drawable.wallpaper_detail_desktop_mask);
        } else {
            mGlanceOverImg.setBackgroundResource(R.drawable.wallpaper_detail_lockscreen_mask);
        }
    }

    public void showGlanceover() {
        Animation mApplyanimation = new TranslateAnimation(0.0f, 0.0f, 0.0f,
                mApplyLayout.getHeight());
        mApplyanimation.setDuration(ANIM_DURATION);
        mApplyanimation.setFillAfter(true);
        mApplyLayout.startAnimation(mApplyanimation);

        Animation mTitleanimation = new TranslateAnimation(0.0f, 0.0f, 0.0f,
                -mGlanceOverTitle.getHeight());
        mTitleanimation.setDuration(ANIM_DURATION);
        mTitleanimation.setFillAfter(true);
        mGlanceOverTitle.startAnimation(mTitleanimation);

        final Animation anim = AnimationUtils.loadAnimation(this, R.anim.push_in);
        Handler handler = new Handler();
        Runnable showGlanceover = new Runnable() {
            public void run() {
                mGlanceOverImg.setAnimation(anim);
                anim.startNow();
                mGlanceOverImg.setVisibility(View.VISIBLE);
            }
        };
        handler.postDelayed(showGlanceover, 0);
        mShowGlanceOver = false;
    }

    public void showApply() {
        Animation mApplyanimation = new TranslateAnimation(0.0f, 0.0f, mApplyLayout.getHeight(), 0.0f);
        mApplyanimation.setDuration(ANIM_DURATION);
        mApplyanimation.setFillAfter(true);
        mApplyLayout.startAnimation(mApplyanimation);

        Animation mTitleanimation = new TranslateAnimation(0.0f, 0.0f, -mGlanceOverTitle.getHeight(), 0.0f);
        mTitleanimation.setDuration(ANIM_DURATION);
        mTitleanimation.setFillAfter(true);
        mGlanceOverTitle.startAnimation(mTitleanimation);

        final Animation anim = AnimationUtils.loadAnimation(this, R.anim.push_out);
        Handler handler = new Handler();
        Runnable hideGlanceover = new Runnable() {
            public void run() {
                mGlanceOverImg.setVisibility(View.GONE);
                mGlanceOverImg.setAnimation(anim);
                anim.startNow();
            }
        };
        handler.postDelayed(hideGlanceover, 0);
        mShowGlanceOver = true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK: {
                if (mIsRepeat) {
                    return false;
                }
                mIsRepeat = true;
                finish();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        if (mIsRepeat) {
            return;
        }
        mIsRepeat = true;
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.more_btn:
                intent.setClass(this, SetScreen.class);
                intent.putExtra("imageResId", mImageResId);
                intent.putExtra("choose", mChoose);
                startActivityForResult(intent, GET_CODE);
                break;
            case R.id.back_btn:
                finish();
                break;
            case R.id.anyway_btn:
                setAnyway();
                break;
            case R.id.apply_btn:
                apply();
                break;
            default:
                break;
        }
    }

    public void apply() {
        Intent intent = new Intent();
        intent.setClass(this, SetScreen.class);
        intent.putExtra("imageResId", mImageResId);
        intent.putExtra("isverticalscreen", mIsVerticalScreenPng);
        // for the "two process one interface"
        WallpaperManager wpm = (WallpaperManager) getSystemService(WALLPAPER_SERVICE);
        if (WallpaperChooser.WALLPAPER == mChoose) {
            Intent mAnywayIntent = new Intent();
            if (mPreferences.getBoolean("isanyway", false) == true || mIsVerticalScreenPng == 1) {
                wpm.suggestDesiredDimensions(mDesiredMinimumWidth, mDesiredMinimumHeight);
                mAnywayIntent.setAction("android.intent.action.SET_VERTICAL_SCREEN");
            } else {
                wpm.suggestDesiredDimensions(mDesiredMinimumWidth * 2, mDesiredMinimumHeight);
                mAnywayIntent.setAction("android.intent.action.SET_HORIZONTAL_SCREEN");
            }
            sendBroadcast(mAnywayIntent);

            Intent intent1 = new Intent();
            intent1.setAction("android.intent.action.WALLPAPER");
            sendBroadcast(intent1);
            intent.putExtra("setWallpaper", true);
        } else {
            intent.putExtra("setLockpaper", true);
        }
        startActivityForResult(intent, GET_CODE);
    }

    public void setAnyway() {
        mIsRepeat = false;
        if (mPreferences.getBoolean("isanyway", false) == true) {
            mEdit.putBoolean("isanyway", false);
            showAnywayToast(true);
            mAnywayBtn.setImageResource(R.drawable.horizontal_button);
        } else {
            mEdit.putBoolean("isanyway", true);
            showAnywayToast(false);
            mAnywayBtn.setImageResource(R.drawable.vertical_button);
        }
        mEdit.commit();
    }

    public void showAnywayToast(boolean anyway) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View viewOK = layoutInflater.inflate(R.layout.layout_ok, null);
        TextView content = (TextView)viewOK.findViewById(R.id.dialog_content);
        if (anyway == true) {
            content.setText(getResources().getString(R.string.set_horizontal_screen));
        } else if (anyway == false) {
            content.setText(getResources().getString(R.string.set_vertical_screen));
        }
        final Toast t = new Toast(Preview.this);
        t.setView(viewOK);
        t.setDuration(Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();

        Handler handler = new Handler();
        final Runnable exitAnim = new Runnable() {
            public void run() {
                t.cancel();
            }
        };
        handler.postDelayed(exitAnim, ANIM_DURATION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsRepeat = false;
        if (mPreferences.getBoolean("isanyway", false) == true) {
            mAnywayBtn.setImageResource(R.drawable.vertical_button);
        } else {
            mAnywayBtn.setImageResource(R.drawable.horizontal_button);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GET_CODE:
                if (resultCode == RESULT_OK) {
                    Handler handler = new Handler();
                    Runnable exitAnim = new Runnable() {
                        public void run() {
                            finish();
                        }
                    };
                    handler.postDelayed(exitAnim, ANIM_DURATION);
                }
                break;
            default:
                break;
        }
    }
}


