/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.worldphone;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.internal.telephony.worldphone.ModemSwitchHandler;
import com.mediatek.engineermode.R;
import com.mediatek.xlog.Xlog;

import java.util.ArrayList;

import com.mediatek.common.telephony.IWorldPhone;
import com.mediatek.common.featureoption.FeatureOption;
import com.android.internal.telephony.gemini.MTKPhoneFactory;
import com.android.internal.telephony.gemini.GeminiPhone;
import com.android.internal.telephony.gsm.GSMPhone;

public class ModemSwitch extends Activity {
    private final static String TAG = "EM/ModemSwitch";
    private final static int MODEM_TYPE_FDD_INDEX = 0;
    private final static int MODEM_TYPE_TDD_INDEX = 1;
    private final static int MODEM_SELECT_AUTO = 2;
    private final static int MODEM_SWITCH_SUCCEED = 0;
    private Spinner mSpinner = null;
    private int mCurrentPos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modem_switch);
        mSpinner = (Spinner) findViewById(R.id.Spinner_ModemSwitch);

        ArrayList<String> items = new ArrayList<String>();
        items.add(getString(R.string.modem_type_fdd));
        items.add(getString(R.string.modem_type_tdd));
        items.add(getString(R.string.select_mode_auto));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(mItemSelectedListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int modemType = ModemSwitchHandler.getModem();
        Xlog.d(TAG, "Get modem type: " + modemType);

        if (modemType == ModemSwitchHandler.MODEM_SWITCH_MODE_FDD) {
            mSpinner.setSelection(MODEM_TYPE_FDD_INDEX);
        } else if (modemType == ModemSwitchHandler.MODEM_SWITCH_MODE_TDD) {
            mSpinner.setSelection(MODEM_TYPE_TDD_INDEX);
        } else {
            Toast.makeText(this, "Query Modem type failed: " + modemType, Toast.LENGTH_SHORT).show();
        }
        mCurrentPos = mSpinner.getSelectedItemPosition();
    }

    private OnItemSelectedListener mItemSelectedListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (mCurrentPos == position) {
                // if invoked by setSelection() in onResume, do nothing
                return;
            }
            mCurrentPos = position;

            boolean result = false;
            switch (position) {
            case MODEM_TYPE_FDD_INDEX:
                Xlog.d(TAG, "Set modem type: " + ModemSwitchHandler.MODEM_SWITCH_MODE_FDD);
                if (FeatureOption.MTK_GEMINI_SUPPORT) {
                    (((GeminiPhone)(MTKPhoneFactory.getDefaultPhone())).mWorldPhone).setNetworkSelectionMode(IWorldPhone.SELECTION_MODE_MANUAL);
                } else {
                    (((GSMPhone)(MTKPhoneFactory.getDefaultPhone())).mWorldPhone).setNetworkSelectionMode(IWorldPhone.SELECTION_MODE_MANUAL);
                }
                ModemSwitchHandler.switchModem(ModemSwitchHandler.MODEM_SWITCH_MODE_FDD);
                if (ModemSwitchHandler.getModem() == ModemSwitchHandler.MODEM_SWITCH_MODE_FDD) {
                    result = true;
                }
                break;
            case MODEM_TYPE_TDD_INDEX:
                Xlog.d(TAG, "Set modem type: " + ModemSwitchHandler.MODEM_SWITCH_MODE_TDD);
                if (FeatureOption.MTK_GEMINI_SUPPORT) {
                    (((GeminiPhone)(MTKPhoneFactory.getDefaultPhone())).mWorldPhone).setNetworkSelectionMode(IWorldPhone.SELECTION_MODE_MANUAL);
                } else {
                    (((GSMPhone)(MTKPhoneFactory.getDefaultPhone())).mWorldPhone).setNetworkSelectionMode(IWorldPhone.SELECTION_MODE_MANUAL);
                }
                ModemSwitchHandler.switchModem(ModemSwitchHandler.MODEM_SWITCH_MODE_TDD);
                if (ModemSwitchHandler.getModem() == ModemSwitchHandler.MODEM_SWITCH_MODE_TDD) {
                    result = true;
                }
                break;
            case MODEM_SELECT_AUTO:
                if (FeatureOption.MTK_GEMINI_SUPPORT) {
                    (((GeminiPhone)(MTKPhoneFactory.getDefaultPhone())).mWorldPhone).setNetworkSelectionMode(IWorldPhone.SELECTION_MODE_AUTO);
                } else {
                    (((GSMPhone)(MTKPhoneFactory.getDefaultPhone())).mWorldPhone).setNetworkSelectionMode(IWorldPhone.SELECTION_MODE_AUTO);
                }
                result = true;
                break;
            default:
                break;
            }

            Xlog.d(TAG, "Set modem type result: " + result);
            if (result) {
                Toast.makeText(ModemSwitch.this, "Switch succeed.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ModemSwitch.this, "Switch failed.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing
        }
    };
}
