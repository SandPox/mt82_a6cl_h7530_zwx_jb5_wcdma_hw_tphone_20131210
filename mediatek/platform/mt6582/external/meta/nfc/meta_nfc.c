/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
/*****************************************************************************
 *
 * Filename:
 * ---------
 *   meta_nfc.c
 *
 * Project:
 * --------
 *   YUSU
 *
 * Description:
 * ------------
 *   NFC META implementation.
 *
 * Author:
 * -------
 *  
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by CC/CQ. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:$
 * $Modtime:$
 * $Log:$
 *
 * 10 25 2011 nina.hsu
 * [ALPS00080644] [New_Feature][NFC] Merge NFC feature.
 * .
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by CC/CQ. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include <signal.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include <sys/un.h>
#include <errno.h>   /* Error number definitions */


#include <ctype.h>
#include <dirent.h>
#include "meta_nfc_para.h"
#include "WM2Linux.h"
#include "./../../../external/mtknfc/inc/mtk_nfc_sys.h"

#define USING_LOCAL_SOCKET// For Local socket

static NFC_CNF nfc_cnf;
static int nfc_service_sockfd = -1;
static pthread_t  read_cnf_thread_handle;
static unsigned char bStop_ReadThread = 0;

#ifdef META_NFC_SELF_TEST_EN
static NFC_CNF_CB meta_nfc_cnf_cb = NULL;
#endif
extern BOOL WriteDataToPC(void *Local_buf,unsigned short Local_len,void *Peer_buf,unsigned short Peer_len);

#ifdef META_NFC_SELF_TEST_EN
void META_NFC_Register(NFC_CNF_CB callback)
{
    meta_nfc_cnf_cb = callback;
}
#endif
static void* META_NFC_read_cnf(void *arg)
{
    int rec_bytes = 0;
    // Read resonse
    META_LOG("META_NFC_CMD:NFC read thread start");
    bStop_ReadThread = 0;
    while(bStop_ReadThread == 0)
    {    
        ilm_struct nfc_ilm_rec;
        nfc_msg_struct nfc_msg;
        unsigned char nfc_msg_length;
        unsigned char fgSupport = 1;
        UINT8 buffer[1024];
        int bufsize=1024;
        
         #if 0
        //clean struct buffer
        memset(&nfc_ilm_rec, 0, sizeof(ilm_struct));
        //read fd
        //if get response break
        rec_bytes = read(nfc_service_sockfd,(char*)&nfc_ilm_rec, sizeof(ilm_struct));             
        #else        
        rec_bytes = read(nfc_service_sockfd, &buffer[0], bufsize);           
        #endif
        
        if (rec_bytes > 0)
        {
            // check msg id
            #if 0
            META_LOG("META_NFC_CMD:NFC read (msg_id,dest_mod_id) = (%d,%d)",nfc_ilm_rec.msg_id, nfc_ilm_rec.dest_mod_id);
            if ((nfc_ilm_rec.msg_id == MSG_ID_NFC_TEST_RSP) && (nfc_ilm_rec.dest_mod_id == MOD_NFC_APP))
            {
                nfc_msg_length = sizeof(nfc_msg_struct);
                memcpy( &nfc_msg, (nfc_msg_struct*)nfc_ilm_rec.local_para_ptr, nfc_msg_length);
                META_LOG("META_NFC_CMD:NFC read msg_type=%d,length=%d", nfc_msg.msg_type,nfc_msg_length);
                switch (nfc_msg.msg_type)
                {
                    case MSG_ID_NFC_SETTING_RSP:
                    { 
                        nfc_cnf.op = NFC_OP_SETTING;
                        memcpy(&nfc_cnf.result.m_setting_cnf, (nfc_setting_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_setting_response));
                        META_LOG("META_NFC_CMD:NFC NFC_OP_SETTING =%d/%d/%d/%d/%d/%d/%d/%d/%d/", 
                            nfc_cnf.result.m_setting_cnf.status,
                            nfc_cnf.result.m_setting_cnf.debug_enable,
                            nfc_cnf.result.m_setting_cnf.fw_ver,
                            nfc_cnf.result.m_setting_cnf.get_capabilities,
                            nfc_cnf.result.m_setting_cnf.sw_ver,
                            nfc_cnf.result.m_setting_cnf.hw_ver,
                            nfc_cnf.result.m_setting_cnf.fw_ver,
                            nfc_cnf.result.m_setting_cnf.reader_mode,
                            nfc_cnf.result.m_setting_cnf.card_mode);                        
                        break;
                    }
                    case MSG_ID_NFC_NOTIFICATION_RSP:
                    {
                        nfc_cnf.op = NFC_OP_REG_NOTIFY;
                        memcpy(&nfc_cnf.result.m_reg_notify_cnf, (nfc_reg_notif_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length),sizeof(nfc_reg_notif_response));
                        META_LOG("META_NFC_CMD:NFC NFC_OP_DISCOVERY =%d/", 
                            nfc_cnf.result.m_reg_notify_cnf.status);                              
                        break;
                    }
                    case MSG_ID_NFC_SE_SET_RSP:
                    {
                        nfc_cnf.op = NFC_OP_SECURE_ELEMENT;
                        memcpy(&nfc_cnf.result.m_se_set_cnf, (nfc_se_set_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_se_set_response));
                        META_LOG("META_NFC_CMD:NFC NFC_OP_SECURE_ELEMENT =%d/", 
                            nfc_cnf.result.m_se_set_cnf.status);                             
                        break;
                    }
                    case MSG_ID_NFC_DISCOVERY_RSP:
                    {
                        nfc_cnf.op = NFC_OP_DISCOVERY;
                        memcpy(&nfc_cnf.result.m_dis_notify_cnf, (nfc_dis_notif_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_dis_notif_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_DISCOVERY =%d/%d/", 
                            nfc_cnf.result.m_dis_notify_cnf.status,
                            nfc_cnf.result.m_dis_notify_cnf.type);                        
                        break;
                    }
                    case MSG_ID_NFC_TAG_READ_RSP:
                    {
                        nfc_cnf.op = NFC_OP_TAG_READ;
                        memcpy(&nfc_cnf.result.m_tag_read_cnf, (nfc_tag_read_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_tag_read_response)); 
                        META_LOG("META_NFC_CMD:NFC NFC_OP_TAG_READ =%d/%d/", 
                            nfc_cnf.result.m_tag_read_cnf.status,
                            nfc_cnf.result.m_tag_read_cnf.type);                            
                        break;
                    }
                    case MSG_ID_NFC_TAG_WRITE_RSP:
                    {
                        nfc_cnf.op = NFC_OP_TAG_WRITE;
                        memcpy(&nfc_cnf.result.m_tag_write_cnf, (nfc_tag_write_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_tag_write_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_TAG_WRITE =%d/%d/", 
                            nfc_cnf.result.m_tag_write_cnf.status,
                            nfc_cnf.result.m_tag_write_cnf.type);                         
                        break;
                    }                
                    case MSG_ID_NFC_TAG_DISCONN_RSP:
                    {
                        nfc_cnf.op = NFC_OP_TAG_DISCONN;
                        memcpy(&nfc_cnf.result.m_tag_discon_cnf, (nfc_tag_disconnect_request*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_tag_disconnect_request));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_TAG_DISCONN =%d/", 
                            nfc_cnf.result.m_tag_discon_cnf.status);                           
                        break;
                    } 
                    case MSG_ID_NFC_TAG_F2NDEF_RSP:
                    {
                        nfc_cnf.op = NFC_OP_TAG_FORMAT_NDEF;
                        memcpy(&nfc_cnf.result.m_tag_fromat2Ndef_cnf, (nfc_tag_fromat2Ndef_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_tag_fromat2Ndef_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_TAG_FORMAT_NDEF =%d/", 
                            nfc_cnf.result.m_tag_fromat2Ndef_cnf.status);                          
                        break;
                    } 
                    case MSG_ID_NFC_TAG_RAWCOM_RSP:
                    {
                        nfc_cnf.op = NFC_OP_TAG_RAW_COMM;
                        memcpy(&nfc_cnf.result.m_tag_raw_com_cnf, (nfc_tag_raw_com_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_tag_raw_com_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_TAG_RAW_COMM =%d/%d/", 
                            nfc_cnf.result.m_tag_raw_com_cnf.status, 
                            nfc_cnf.result.m_tag_raw_com_cnf.type);                        
                        break;
                    }    
                    case MSG_ID_NFC_P2P_COMMUNICATION_RSP:
                    {
                        nfc_cnf.op = NFC_OP_P2P_COMM;
                        memcpy(&nfc_cnf.result.m_p2p_com_cnf, (nfc_p2p_com_response*)nfc_ilm_rec.local_para_ptr, sizeof(nfc_p2p_com_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_P2P_COMM =%d/%d/", 
                            nfc_cnf.result.m_p2p_com_cnf.status, 
                            nfc_cnf.result.m_p2p_com_cnf.length);    
                        
                        break;
                    }  
                    case MSG_ID_NFC_RD_COMMUNICATION_RSP:
                    {
                        nfc_cnf.op = NFC_OP_RD_COMM;
                        memcpy(&nfc_cnf.result.m_rd_com_cnf, (nfc_rd_com_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_rd_com_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_RD_COMM =%d/%d/", 
                            nfc_cnf.result.m_rd_com_cnf.status, 
                            nfc_cnf.result.m_rd_com_cnf.length);                                   
                        break;
                    }
                    case MSG_ID_NFC_TX_ALWAYSON_TEST_RSP:
                    {
                        nfc_cnf.op = NFC_OP_TX_ALWAYSON_TEST;
                        memcpy(&nfc_cnf.result.m_script_cnf, (nfc_script_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_script_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_TX_ALWAYSON_TEST =%d/", 
                            nfc_cnf.result.m_script_cnf.result);                           
                        break;
                    }   
                    case MSG_ID_NFC_TX_ALWAYSON_WO_ACK_TEST_RSP:
                    {
                        nfc_cnf.op = NFC_OP_TX_ALWAYSON_WO_ACK_TEST;
                        memcpy(&nfc_cnf.result.m_script_cnf, (nfc_script_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_script_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_TX_ALWAYSON_WO_ACK_TEST =%d/", 
                            nfc_cnf.result.m_script_cnf.result);                         
                        break;
                    }
                    case MSG_ID_NFC_CARD_EMULATION_MODE_TEST_RSP:
                    {
                        nfc_cnf.op = NFC_OP_CARD_MODE_TEST;
                        memcpy(&nfc_cnf.result.m_script_cnf, (nfc_script_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_script_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_CARD_MODE_TEST =%d/", 
                            nfc_cnf.result.m_script_cnf.result);                        
                        break;
                    }      
                    case MSG_ID_NFC_READER_MODE_TEST_RSP:
                    {
                        nfc_cnf.op = NFC_OP_READER_MODE_TEST;
                        memcpy(&nfc_cnf.result.m_script_cnf, (nfc_script_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_script_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_READER_MODE_TEST =%d/", 
                            nfc_cnf.result.m_script_cnf.result);                         
                        break;
                    }          
                    case MSG_ID_NFC_P2P_MODE_TEST_RSP:
                    {
                        nfc_cnf.op = NFC_OP_P2P_MODE_TEST;
                        memcpy(&nfc_cnf.result.m_script_cnf, (nfc_script_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_script_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_P2P_MODE_TEST =%d/", 
                            nfc_cnf.result.m_script_cnf.result);                         
                        break;
                    }    
                    case MSG_ID_NFC_SWP_SELF_TEST_RSP:
                    {
                        nfc_cnf.op = NFC_OP_SWP_SELF_TEST;
                        memcpy(&nfc_cnf.result.m_script_cnf, (nfc_script_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_script_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_SWP_SELF_TEST =%d/", 
                            nfc_cnf.result.m_script_cnf.result);                         
                        break;
                    }       
                    case MSG_ID_NFC_ANTENNA_SELF_TEST_RSP:
                    {
                        nfc_cnf.op = NFC_OP_ANTENNA_SELF_TEST;
                        memcpy(&nfc_cnf.result.m_script_cnf, (nfc_script_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_script_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_ANTENNA_SELF_TEST =%d/", 
                            nfc_cnf.result.m_script_cnf.result);                           
                        break;
                    }                     
                    case MSG_ID_NFC_TAG_UID_RW_RSP:
                    {
                        nfc_cnf.op = NFC_OP_TAG_UID_RW;
                        memcpy(&nfc_cnf.result.m_script_uid_cnf, (nfc_script_uid_response*)(nfc_ilm_rec.local_para_ptr + nfc_msg_length), sizeof(nfc_script_uid_response));                            
                        META_LOG("META_NFC_CMD:NFC NFC_OP_TAG_UID_RW =%d/", 
                            nfc_cnf.result.m_script_cnf.result);                         
                        break;
                    }
                    case MSG_ID_NFC_CARD_MODE_TEST_RSP:
                    case MSG_ID_NFC_STOP_TEST_RSP:
                    default:
                    {
                        fgSupport = 0;
                        META_LOG("META_NFC_CMD:Don't support CNF CMD %d",nfc_msg.msg_type);
                        break;
                    }
                }
                if (fgSupport == 1)
                {
                    META_LOG("META_NFC_CMD:NFC read nfc_cnf.op=%d,nfc_msg.msg_type=%d", nfc_cnf.op,nfc_msg.msg_type);
                    nfc_cnf.status = META_SUCCESS;
                    #ifdef META_NFC_SELF_TEST_EN
                    if (meta_nfc_cnf_cb)
                    {
                        meta_nfc_cnf_cb(&nfc_cnf, NULL, 0);
                    }
                    else
                    #endif
                    {
                        WriteDataToPC(&nfc_cnf, sizeof(NFC_CNF), NULL, 0);
                    }
                }
                else 
                {

                    META_LOG("META_NFC_CMD:Don't Write to PC MSGID,%d,",nfc_msg.msg_type);
                }
            }
            else 
            {
                META_LOG("META_NFC_CMD:Don't support MSGID,%d,DestID,%d",nfc_ilm_rec.msg_id, nfc_ilm_rec.dest_mod_id);
            }
            #endif
            MTK_NFC_MSG_T *nfc_msg_loc = (MTK_NFC_MSG_T *)buffer;
            
            META_LOG("META_NFC_CMD:NFC read (msg_id,length) = (%d,%d)",nfc_msg_loc->type, nfc_msg_loc->length);
            //New UI!!
            switch(nfc_msg_loc->type)
            {
               case MTK_NFC_EM_ALS_READER_MODE_RSP:
               {
                   nfc_cnf.op = NFC_OP_ALS_READER_MODE;
                   memcpy(&nfc_cnf.result.m_nNfc_als_readerm_rsp, (s_mtk_nfc_em_als_readerm_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_als_readerm_rsp));                            
                   META_LOG("META_NFC_CMD:NFC NFC_OP_ALS_READER_MODE_REQ =%02x/", 
                            nfc_cnf.result.m_nNfc_als_readerm_rsp.result);                                   
                   break;
               }
               case MTK_NFC_EM_ALS_READER_MODE_NTF:
               {
                   nfc_cnf.op = NFC_OP_ALS_READER_MODE;
                   memcpy(&nfc_cnf.result.m_nNfc_als_readerm_ntf, (s_mtk_nfc_em_als_readerm_ntf*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_als_readerm_ntf));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_ALS_READER_MODE_NTF =%02x,%02x,%02x,%02x,/", 
                    nfc_cnf.result.m_nNfc_als_readerm_ntf.isNDEF,
                    nfc_cnf.result.m_nNfc_als_readerm_ntf.result,
                    nfc_cnf.result.m_nNfc_als_readerm_ntf.Uid,
                    nfc_cnf.result.m_nNfc_als_readerm_ntf.UidLen); 
                   {
                      int i;
                      for(i=0;i<nfc_cnf.result.m_nNfc_als_readerm_ntf.UidLen;i++)
                      {
                          META_LOG("Uid[%d][%02x]",i,nfc_cnf.result.m_nNfc_als_readerm_ntf.Uid[i]);
                      }
                   }
                   break;
               }
               case MTK_NFC_EM_ALS_READER_MODE_OPT_RSP:
               {                
                   nfc_cnf.op = NFC_OP_ALS_READER_MODE_OPT;
                   memcpy(&nfc_cnf.result.m_nNfc_als_readerm_opt_rsp, (s_mtk_nfc_em_als_readerm_opt_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_als_readerm_opt_rsp));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_ALS_READER_MODE_OPT_RSP =%02x,/",
                    nfc_cnf.result.m_nNfc_als_readerm_opt_rsp.result);     
                   break;
               }
               case MTK_NFC_EM_ALS_P2P_MODE_RSP:
               {
                   nfc_cnf.op = NFC_OP_ALS_P2P_MODE;
                   memcpy(&nfc_cnf.result.m_nNfc_als_p2p_rsp, (s_mtk_nfc_em_als_p2p_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_als_p2p_rsp));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_ALS_P2P_MODE_RSP =%02x,/", 
                    nfc_cnf.result.m_nNfc_als_p2p_rsp.result);                                  
                   break;
               }
               case MTK_NFC_EM_ALS_P2P_MODE_NTF:
               {
                   nfc_cnf.op = NFC_OP_ALS_P2P_MODE;
                   memcpy(&nfc_cnf.result.m_nNfc_als_p2p_ntf, (s_mtk_nfc_em_als_p2p_ntf*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_als_p2p_ntf));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_ALS_P2P_MODE_NTF =%02x,/", 
                    nfc_cnf.result.m_nNfc_als_p2p_ntf.link_status);   
                   break;
               }
               case MTK_NFC_EM_ALS_CARD_MODE_RSP:
               {
                   nfc_cnf.op = NFC_OP_ALS_CARD_MODE;
                   memcpy(&nfc_cnf.result.m_nNfc_als_cardm_rsp, (s_mtk_nfc_em_als_cardm_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_als_cardm_rsp));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_ALS_CARD_MODE_RSP =%02x,/", 
                    nfc_cnf.result.m_nNfc_als_cardm_rsp.result);                                   
                   break;
               }
               case MTK_NFC_EM_POLLING_MODE_RSP:
               {
                   nfc_cnf.op = NFC_OP_POLLING_MODE;
                   memcpy(&nfc_cnf.result.m_nNfc_polling_rsp, (s_mtk_nfc_em_polling_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_polling_rsp));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_POLLING_MODE_RSP =%02x,/", 
                    nfc_cnf.result.m_nNfc_polling_rsp.result);                                    
                   break;
               }
               case MTK_NFC_EM_POLLING_MODE_NTF:
               {
                   nfc_cnf.op = NFC_OP_POLLING_MODE;
                   memcpy(&nfc_cnf.result.m_nNfc_polling_func_ntf, (s_mtk_nfc_em_polling_func_ntf*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_polling_func_ntf));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_POLLING_MODE_NTF =%02x,%02x,%02x,/", 
                    nfc_cnf.result.m_nNfc_polling_func_ntf.card,
                    nfc_cnf.result.m_nNfc_polling_func_ntf.p2p,
                    nfc_cnf.result.m_nNfc_polling_func_ntf.reader);                                  
                   break;
               }
               case MTK_NFC_EM_TX_CARRIER_ALS_ON_RSP:
               {
                   nfc_cnf.op = NFC_OP_TX_CARRIER_ALS_ON;
                   memcpy(&nfc_cnf.result.m_nNfc_tx_carr_als_on_rsp, (s_mtk_nfc_em_tx_carr_als_on_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_tx_carr_als_on_rsp));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_TX_CARRIER_ALS_ON_RSP =%02x,/", 
                    nfc_cnf.result.m_nNfc_tx_carr_als_on_rsp.result);                                       
                   break;
               }
               case MTK_NFC_EM_VIRTUAL_CARD_RSP:
               {
                   nfc_cnf.op = NFC_OP_VIRTUAL_CARD;
                   memcpy(&nfc_cnf.result.m_nNfc_virtual_card_rsp, (s_mtk_nfc_em_virtual_card_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_virtual_card_rsp));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_VIRTUAL_CARD_RSP =%02x,/", 
                    nfc_cnf.result.m_nNfc_virtual_card_rsp.result);                                
                   break;
               }
               case MTK_NFC_EM_PNFC_CMD_RSP:
               {
                   nfc_cnf.op = NFC_OP_PNFC_CMD;
                   memcpy(&nfc_cnf.result.m_nNfc_pnfc_new_rsp, (s_mtk_nfc_em_pnfc_new_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_em_pnfc_new_rsp));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_EM_PNFC_CMD_RSP =%02x,%02x,%s,/", 
                    nfc_cnf.result.m_nNfc_pnfc_new_rsp.result,
                    nfc_cnf.result.m_nNfc_pnfc_new_rsp.datalen,
                    nfc_cnf.result.m_nNfc_pnfc_new_rsp.data);                                    
                   break;
               }
               case MTK_NFC_TESTMODE_SETTING_RSP:
               {
                   nfc_cnf.op = NFC_OP_TESTMODE_SETTING;
                   memcpy(&nfc_cnf.result.m_nNfc_test_mode_Setting_rsp, (s_mtk_nfc_test_mode_Setting_rsp_t*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_test_mode_Setting_rsp_t));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_TESTMODE_SETTING_RSP =%02x", 
                    nfc_cnf.result.m_nNfc_test_mode_Setting_rsp.result);                                   
                   break;
               }
               case MTK_EM_LOOPBACK_TEST_RSP:
               {
                   nfc_cnf.op = NFC_OP_LOOPBACK_TEST;
                   memcpy(&nfc_cnf.result.m_nNfc_loopback_test_rsp, (s_mtk_nfc_loopback_test_rsp_t*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_loopback_test_rsp_t));                            
                   META_LOG("META_NFC_CMD:NFC MTK_EM_LOOPBACK_TEST_RSP =%02x", 
                    nfc_cnf.result.m_nNfc_loopback_test_rsp.result);                                    
                   break;
               }
               case MTK_NFC_FM_SWP_TEST_RSP:
               {
                   nfc_cnf.op = NFC_OP_SWP_TEST;
                   memcpy(&nfc_cnf.result.m_nNfc_swp_test_rsp, (s_mtk_nfc_fm_swp_test_rsp*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_fm_swp_test_rsp));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_FM_SWP_TEST_RSP =%02x", 
                    nfc_cnf.result.m_nNfc_swp_test_rsp.result);                                   
                   break;
               }
               case MTK_NFC_SW_VERSION_RESPONSE:
               {
                   nfc_cnf.op = NFC_OP_SW_VERSION;
                   memcpy(&nfc_cnf.result.m_nNfc_sw_Version_rsp, (s_mtk_nfc_sw_Version_rsp_t*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_sw_Version_rsp_t));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_SW_VERSION_RESPONSE =%02x,%02x,%s,", 
                    nfc_cnf.result.m_nNfc_sw_Version_rsp.fw_ver,
                    nfc_cnf.result.m_nNfc_sw_Version_rsp.hw_ver,
                    nfc_cnf.result.m_nNfc_sw_Version_rsp.mw_ver);                                    
                   break;
               }
               case MTK_NFC_GET_SELIST_RSP:
               {
                   nfc_cnf.op = NFC_OP_GET_SELIST;
                   memcpy(&nfc_cnf.result.m_nNfc_se_get_list_req, (s_mtk_nfc_jni_se_get_list_rsp_t*)((UINT8 *)nfc_msg_loc + sizeof(MTK_NFC_MSG_T)), sizeof(s_mtk_nfc_jni_se_get_list_rsp_t));                            
                   META_LOG("META_NFC_CMD:NFC MTK_NFC_GET_SELIST_RSP =%02x,%02x,", 
                    nfc_cnf.result.m_nNfc_se_get_list_req.SeCount,
                    nfc_cnf.result.m_nNfc_se_get_list_req.status);                                  
                   break;
               }
               default:
                  fgSupport = 0;
                  META_LOG("META_NFC_CMD:NFC default case");                                    
                  break;
 
            }
            
            if (fgSupport == 1)
            {
                META_LOG("META_NFC_CMD:NFC read nfc_cnf.op=%d,nfc_msg.msg_type=%d", nfc_cnf.op,nfc_msg.msg_type);
                nfc_cnf.status = META_SUCCESS;
                #ifdef META_NFC_SELF_TEST_EN
                if (meta_nfc_cnf_cb)
                {
                    meta_nfc_cnf_cb(&nfc_cnf, NULL, 0);
                }
                else
                #endif
                {
                    WriteDataToPC(&nfc_cnf, sizeof(NFC_CNF), NULL, 0);
                }
            }
            else 
            {

                META_LOG("META_NFC_CMD:Don't Write to PC MSGID,%d,",nfc_msg.msg_type);
            }
        }        
      //  else
      //  {
        //    usleep(100000); // wake up every 0.1sec     
       // }
    }
    bStop_ReadThread = 1;
    META_LOG("META_NFC_CMD:NFC read thread stop");
    pthread_exit(NULL);
   return NULL;

}
/********************************************************************************
//FUNCTION:
//		META_NFC_init
//DESCRIPTION:
//		NFC Init for META test.
//
//PARAMETERS:
//		void
//RETURN VALUE:
//		true : success
//      false: failed
//
********************************************************************************/
#ifdef USING_LOCAL_SOCKET
#define MTKNFC_COMM_SOCK    "/data/mtknfc_server"
#endif
int META_NFC_init(void)
{
    
    pid_t pid;
    //int portno;

    // Run nfc service process
    if ((pid = fork()) < 0) 
    {
        META_LOG("META_NFC_init: fork fails: %d (%s)\n", errno, strerror(errno));
        return (-2);
    } 
    else if (pid == 0)  /*child process*/
    {
        int err;
    
        META_LOG("nfc_open: execute: %s\n", "/system/xbin/nfcservice");
        err = execl("/system/xbin/nfcstackp", "nfcstackp", "NFC_TEST_MODE", NULL);
        if (err == -1)
        {
            META_LOG("META_NFC_init: execl error: %s\n", strerror(errno));
            return (-3);
        }
        return 0;
    } 
    else  /*parent process*/
    {
        META_LOG("META_NFC_init: pid = %d\n", pid);
    }

    // Create socket

    
    #ifdef USING_LOCAL_SOCKET
    struct sockaddr_un address;
    int len;
    
    //printf("nfc_open: SELF TEST COD"); 
    //nfc_sockfd = socket(AF_UNIX, SOCK_STREAM, 0); 
    nfc_service_sockfd = socket(AF_LOCAL, SOCK_STREAM, 0); 
    if (nfc_service_sockfd < 0)     
    {        
       META_LOG("nfc_open: ERROR opening socket");        
       return (-4);    
    } 

    address.sun_family = AF_LOCAL;//AF_UNIX;
    strcpy (address.sun_path, MTKNFC_COMM_SOCK);
    len = sizeof (address);
            
    sleep(3);  // sleep 5sec for libmnlp to finish initialization        
      
    META_LOG("connecting(%s)...\r\n",address.sun_path);   
            
    /* Now connect to the server */    
    if (connect(nfc_service_sockfd, (struct sockaddr *)&address, sizeof(address)) < 0)     
    {         
       META_LOG("NFC_Open: ERROR connecting\r\n");         
       return (-6);    
    }    

    #else
    struct sockaddr_in serv_addr;
    struct hostent *server;	
    
    nfc_service_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (nfc_service_sockfd < 0) 
    {
        META_LOG("META_NFC_init: ERROR opening socket");
        return (-4);
    }
    server = gethostbyname("127.0.0.1");
    if (server == NULL) {
        META_LOG("META_NFC_init: ERROR, no such host\n");
        return (-5);
    }


    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(SOCKET_NFC_PORT);

    sleep(3);  // sleep 5sec for nfcservice to finish initialization
    
    /* Now connect to the server */
    if (connect(nfc_service_sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    {
         META_LOG("META_NFC_init: ERROR connecting");
         return (-6);
    }

    #endif
    META_LOG("META_NFC_init: create read command thread\n"); 
    if(pthread_create(&read_cnf_thread_handle, NULL, META_NFC_read_cnf,
          NULL) != 0)
    {
       META_LOG("META_NFC_init:Fail to create read command thread");
       return (-7);
    }   

    META_LOG("META_NFC_init: done\n"); 
	return (0);
}

/********************************************************************************
//FUNCTION:
//		META_NFC_deinit
//DESCRIPTION:
//		NFC deinit for META test.
//
//PARAMETERS:
//		void
//RETURN VALUE:
//		void
//     
********************************************************************************/
void META_NFC_deinit()
{
    int err=0;
    /* stop RX thread */
    bStop_ReadThread = 1;
    
    /* wait until thread exist */
    pthread_join(read_cnf_thread_handle, NULL);

    /* Close socket port */
    if (nfc_service_sockfd > 0)
    {
        close (nfc_service_sockfd);
        nfc_service_sockfd = -1;
    }
    // kill service process
    META_LOG("META_NFC_deinit: kill: %s\n", "/system/xbin/nfcservice");
    err = execl("kill /system/xbin/nfcservice", "nfcservice", NULL);
    if (err == -1)
    {
        META_LOG("META_NFC_init: kill error: %s\n", strerror(errno));
    }
    return;   
}
/********************************************************************************
//FUNCTION:
//		META_NFC_CMD
//DESCRIPTION:
//		SEND MESSAGE to NFC driver
//      RECEIVE MESSAGE to NFC driver
//PARAMETERS:
//		void
//RETURN VALUE:
//		void
//     
********************************************************************************/
void META_NFC_CMD(ilm_struct* nfc_ilm_req_ptr)
{

    int ret = 0;
    int rec_bytes = 0;
    int rety_count = 0;
    META_LOG("META_NFC_CMD:write CMD");

    // Write request command
    ret = write(nfc_service_sockfd, (const char*)nfc_ilm_req_ptr, sizeof(ilm_struct));

    if ( ret <= 0)
    {
        META_LOG("META_NFC_CMD:write failure,%d",ret);
        return;
    }
    else
    {
        META_LOG("META_NFC_CMD:write CMD done,%d",ret);
    }
    return;
}

void META_NFC_SEND_SOCKET(MTK_NFC_MSG_T *msg)
{
    int32_t ret;
    if (msg == NULL)
    {
       // MNL_DEBUG_OUTPUT(MDBG_MSG, DBG_ERR,"MNLMsgS", "NULL", "");
        return MTK_NFC_ERROR;
    }
    ret = write(nfc_service_sockfd, msg, (sizeof(MTK_NFC_MSG_T) + msg->length)); 
    
    META_LOG("mtk_nfc_sys_msg_send: ret,%d\n",ret); 

    free( (VOID*)msg);

    return;
}


/********************************************************************************
//FUNCTION:
//		META_NFC_OP
//DESCRIPTION:
//		META NFC test main process function.
//
//PARAMETERS:
//		req: NFC Req struct
//      peer_buff: peer buffer pointer
//      peer_len: peer buffer length
//RETURN VALUE:
//		void
//      
********************************************************************************/
void META_NFC_OP(NFC_REQ *req, char *peer_buff, unsigned short peer_len) 
{     
    ilm_struct nfc_ilm_loc;
    nfc_msg_struct nfc_msg;
    memset(&nfc_cnf, 0, sizeof(NFC_CNF));
    memset(&nfc_msg, 0, sizeof(nfc_msg_struct));
    nfc_cnf.header.id = FT_NFC_CNF_ID;
    nfc_cnf.header.token = req->header.token;
    nfc_cnf.op = req->op;

    memset(&nfc_ilm_loc, 0, sizeof(ilm_struct));    
    nfc_ilm_loc.msg_id = MSG_ID_NFC_TEST_REQ;
    nfc_ilm_loc.src_mod_id = MOD_NFC_APP;
    nfc_ilm_loc.dest_mod_id = MOD_NFC;    
    
    switch(req->op)
    {
        META_LOG("META_NFC_OP:NFC request op=%d", req->op);
        case NFC_OP_SETTING:
        {
            //Write handle function here
            nfc_msg.msg_length = sizeof(nfc_setting_request);
            nfc_msg.msg_type = MSG_ID_NFC_SETTING_REQ;
            META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);
            memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
            memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)), (char*)&req->cmd.m_setting_req, sizeof(nfc_setting_request));
            META_NFC_CMD(&nfc_ilm_loc);
            break;
       }
       case NFC_OP_REG_NOTIFY:
	   {
           //Write handle function here
	       nfc_msg.msg_length = sizeof(nfc_reg_notif_request);
           nfc_msg.msg_type = MSG_ID_NFC_NOTIFICATION_REQ;
           
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),(char*)&req->cmd.m_reg_notify_req, sizeof(nfc_reg_notif_request));
           META_NFC_CMD(&nfc_ilm_loc);
           break;
       }
       case NFC_OP_SECURE_ELEMENT:
       {
            //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_se_set_request);
           nfc_msg.msg_type = MSG_ID_NFC_SE_SET_REQ;

           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_se_set_req, sizeof(nfc_se_set_request));
           META_NFC_CMD(&nfc_ilm_loc);
           break;
       }
       case NFC_OP_DISCOVERY:
       {
        //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_dis_notif_request);
           nfc_msg.msg_type = MSG_ID_NFC_DISCOVERY_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_dis_notify_req, sizeof(nfc_dis_notif_request));
           META_NFC_CMD(&nfc_ilm_loc);
           break;
       }
       case NFC_OP_TAG_READ:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_tag_read_request);
           nfc_msg.msg_type = MSG_ID_NFC_TAG_READ_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_tag_read_req, sizeof(nfc_tag_read_request));
           break;
       }
       case NFC_OP_TAG_WRITE:
       {
            //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_tag_write_request);
           nfc_msg.msg_type = MSG_ID_NFC_TAG_WRITE_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_tag_write_req, sizeof(nfc_tag_write_request));
           META_NFC_CMD(&nfc_ilm_loc);
           break;
       }
       case NFC_OP_TAG_DISCONN:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_tag_disconnect_request);
           nfc_msg.msg_type = MSG_ID_NFC_TAG_DISCONN_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_tag_discon_req, sizeof(nfc_tag_disconnect_request));
           META_NFC_CMD(&nfc_ilm_loc);
           break;
       }
       case NFC_OP_TAG_FORMAT_NDEF:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_tag_fromat2Ndef_request);
           nfc_msg.msg_type = MSG_ID_NFC_TAG_F2NDEF_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_tag_fromat2Ndef_req, sizeof(nfc_tag_fromat2Ndef_request));
           META_NFC_CMD(&nfc_ilm_loc);
           break;
       }
       case NFC_OP_TAG_RAW_COMM:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_tag_raw_com_request);
           nfc_msg.msg_type = MSG_ID_NFC_TAG_RAWCOM_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_tag_raw_com_req, sizeof(nfc_tag_raw_com_request));
           META_NFC_CMD(&nfc_ilm_loc);
           break;
       }
       case NFC_OP_P2P_COMM:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_p2p_com_request);
           nfc_msg.msg_type = MSG_ID_NFC_P2P_COMMUNICATION_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_p2p_com_req, sizeof(nfc_p2p_com_request));
           if ((peer_buff != NULL) && (peer_len != 0))
           {
               memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct) + sizeof(nfc_p2p_com_request)), peer_buff, peer_len);          
           }
           META_NFC_CMD(&nfc_ilm_loc);
           break;
       }
       case NFC_OP_RD_COMM:
       {
            //Write handle function here
            nfc_msg.msg_length = sizeof(nfc_rd_com_request);
            nfc_msg.msg_type = MSG_ID_NFC_RD_COMMUNICATION_REQ;
            META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
            
            memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
            memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)), &req->cmd.m_rd_com_req, sizeof(nfc_rd_com_request));
            if ((peer_buff != NULL) && (peer_len != 0))
            {
                memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)+ sizeof(nfc_rd_com_request)), peer_buff, peer_len);          
            } 
            META_NFC_CMD(&nfc_ilm_loc);
            break;
       }
       case NFC_OP_TX_ALWAYSON_TEST:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_script_request);
           nfc_msg.msg_type = MSG_ID_NFC_TX_ALWAYSON_TEST_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_nfc_tx_alwayson_req, sizeof(nfc_tx_alwayson_request));
           META_NFC_CMD(&nfc_ilm_loc);           
           break;
       }
       case NFC_OP_TX_ALWAYSON_WO_ACK_TEST:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_script_request);
           nfc_msg.msg_type = MSG_ID_NFC_TX_ALWAYSON_WO_ACK_TEST_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_nfc_tx_alwayson_req, sizeof(nfc_tx_alwayson_request));
           META_NFC_CMD(&nfc_ilm_loc);           
           break;
       }
       case NFC_OP_CARD_MODE_TEST:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_script_request);
           nfc_msg.msg_type = MSG_ID_NFC_CARD_EMULATION_MODE_TEST_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_nfc_card_emulation_req, sizeof(nfc_card_emulation_request));
           META_NFC_CMD(&nfc_ilm_loc);           
           break;
       }
       case NFC_OP_READER_MODE_TEST:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_script_request);
           nfc_msg.msg_type = MSG_ID_NFC_READER_MODE_TEST_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_script_req, sizeof(nfc_script_request));
           META_NFC_CMD(&nfc_ilm_loc);           
           break;
       }
       case NFC_OP_P2P_MODE_TEST: 
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_script_request);
           nfc_msg.msg_type = MSG_ID_NFC_P2P_MODE_TEST_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_script_req, sizeof(nfc_script_request));
           META_NFC_CMD(&nfc_ilm_loc);           
           break;
       }
       case NFC_OP_SWP_SELF_TEST:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_script_request);
           nfc_msg.msg_type = MSG_ID_NFC_SWP_SELF_TEST_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_script_req, sizeof(nfc_script_request));
           META_NFC_CMD(&nfc_ilm_loc);           
           break;
       }        
       case NFC_OP_ANTENNA_SELF_TEST:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_script_request);
           nfc_msg.msg_type = MSG_ID_NFC_ANTENNA_SELF_TEST_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
           
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_script_req, sizeof(nfc_script_request));
           META_NFC_CMD(&nfc_ilm_loc);           
           break;
       }
       case NFC_OP_TAG_UID_RW:
       {
           //Write handle function here
           nfc_msg.msg_length = sizeof(nfc_script_uid_request);
           nfc_msg.msg_type = MSG_ID_NFC_TAG_UID_RW_REQ;
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", nfc_msg.msg_type, nfc_msg.msg_length);           
                
           memcpy(nfc_ilm_loc.local_para_ptr, (char*)&nfc_msg, sizeof(nfc_msg_struct));
           memcpy((nfc_ilm_loc.local_para_ptr + sizeof(nfc_msg_struct)),&req->cmd.m_script_uid_req, sizeof(nfc_script_uid_request));
           META_NFC_CMD(&nfc_ilm_loc);           
           break;
       }    


       /*
       New UI
       */
       case NFC_OP_ALS_READER_MODE:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_als_readerm_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_em_als_readerm_req);
           msg->type = MTK_NFC_EM_ALS_READER_MODE_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_als_readerm_req, sizeof(s_mtk_nfc_em_als_readerm_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);  

           META_LOG("%02x,%02x,%02x,%02x,%02x,%02x,%02x,",
                   req->cmd.m_nNfc_als_readerm_req.action,
                   req->cmd.m_nNfc_als_readerm_req.supporttype,
                   req->cmd.m_nNfc_als_readerm_req.typeA_datarate,
                   req->cmd.m_nNfc_als_readerm_req.typeB_datarate,
                   req->cmd.m_nNfc_als_readerm_req.typeF_datarate,
                   req->cmd.m_nNfc_als_readerm_req.typeV_datarate,
                   req->cmd.m_nNfc_als_readerm_req.typeV_subcarrier);
           
           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_ALS_P2P_MODE:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_als_p2p_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_em_als_p2p_req);
           msg->type = MTK_NFC_EM_ALS_P2P_MODE_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_als_p2p_req, sizeof(s_mtk_nfc_em_als_p2p_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   

           META_LOG("%02x,%02x,%02x,%02x,%02x,%02x,%02x,",
                   req->cmd.m_nNfc_als_p2p_req.action,
                   req->cmd.m_nNfc_als_p2p_req.isDisableCardM,
                   req->cmd.m_nNfc_als_p2p_req.mode,
                   req->cmd.m_nNfc_als_p2p_req.role,
                   req->cmd.m_nNfc_als_p2p_req.supporttype,
                   req->cmd.m_nNfc_als_p2p_req.typeA_datarate,
                   req->cmd.m_nNfc_als_p2p_req.typeF_datarate);
                      
           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_ALS_CARD_MODE:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_als_cardm_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_em_als_cardm_req);
           msg->type = MTK_NFC_EM_ALS_CARD_MODE_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_als_cardm_req, sizeof(s_mtk_nfc_em_als_cardm_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   

           META_LOG("%02x,%02x,%02x,%02x,",
                   req->cmd.m_nNfc_als_cardm_req.action,
                   req->cmd.m_nNfc_als_cardm_req.fgvirtualcard,
                   req->cmd.m_nNfc_als_cardm_req.supporttype,
                   req->cmd.m_nNfc_als_cardm_req.SWNum);

           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_POLLING_MODE:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_polling_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_em_polling_req);
           msg->type = MTK_NFC_EM_POLLING_MODE_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_polling_req, sizeof(s_mtk_nfc_em_polling_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   

           META_LOG("%02x,%02x,%02x,%02x,%02x,%02x,%02x",
                   req->cmd.m_nNfc_polling_req.action,
                   req->cmd.m_nNfc_polling_req.cardM,
                   req->cmd.m_nNfc_polling_req.enablefunc,
                   req->cmd.m_nNfc_polling_req.p2pM,
                   req->cmd.m_nNfc_polling_req.Period,
                   req->cmd.m_nNfc_polling_req.phase,
                   req->cmd.m_nNfc_polling_req.readerM);

           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_TX_CARRIER_ALS_ON:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_tx_carr_als_on_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_em_tx_carr_als_on_req);
           msg->type = MTK_NFC_EM_POLLING_MODE_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_tx_carr_als_on_req, sizeof(s_mtk_nfc_em_tx_carr_als_on_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   

           META_LOG("%02x,",
                   req->cmd.m_nNfc_tx_carr_als_on_req.action);

           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_VIRTUAL_CARD:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_virtual_card_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_em_virtual_card_req);
           msg->type = MTK_NFC_EM_VIRTUAL_CARD_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_virtual_card_req, sizeof(s_mtk_nfc_em_virtual_card_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   

           META_LOG("%02x,%02x,%02x,",
                   req->cmd.m_nNfc_virtual_card_req.action,
                   req->cmd.m_nNfc_virtual_card_req.supporttype,
                   req->cmd.m_nNfc_virtual_card_req.typeF_datarate);

           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_PNFC_CMD:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_pnfc_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_em_pnfc_req);
           msg->type = MTK_NFC_EM_PNFC_CMD_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_pnfc_req, sizeof(s_mtk_nfc_em_pnfc_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   

           META_LOG("%02x,%02x,%02x,",
                   req->cmd.m_nNfc_pnfc_req.rEmPnfcReq,
                   req->cmd.m_nNfc_pnfc_req.u4action,
                   req->cmd.m_nNfc_pnfc_req.u4ReqMsg);

           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_SW_VERSION:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T));
           //Write handle function here
           msg->length = 0;//sizeof(s_mtk_nfc_test_mode_Setting_req_t);
           msg->type = MTK_NFC_SW_VERSION_QUERY;           
           //memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_test_mode_Setting_req, sizeof(s_mtk_nfc_test_mode_Setting_req_t));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   
           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_TESTMODE_SETTING:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_test_mode_Setting_req_t));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_test_mode_Setting_req_t);
           msg->type = MTK_NFC_TESTMODE_SETTING_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_test_mode_Setting_req, sizeof(s_mtk_nfc_test_mode_Setting_req_t));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   

           META_LOG("%02x,%02x,",
                   req->cmd.m_nNfc_test_mode_Setting_req.forceDownLoad,
                   req->cmd.m_nNfc_test_mode_Setting_req.TagAutoPresenceChk);

           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_LOOPBACK_TEST:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_loopback_test_req_t));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_loopback_test_req_t);
           msg->type = MTK_EM_LOOPBACK_TEST_REQ;           
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_loopback_test_req, sizeof(s_mtk_nfc_loopback_test_req_t));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   
           
           META_LOG("%02x,",
                   req->cmd.m_nNfc_loopback_test_req.action);
           
           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_SWP_TEST:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_fm_swp_test_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_fm_swp_test_req);
           msg->type = MTK_NFC_FM_SWP_TEST_REQ;
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_swp_test_req, sizeof(s_mtk_nfc_fm_swp_test_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   

           
           META_LOG("%02x,",
                   req->cmd.m_nNfc_swp_test_req.action);
           
           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_ALS_READER_MODE_OPT:
       {
           MTK_NFC_MSG_T *msg = NULL;
           msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_als_readerm_opt_req));
           //Write handle function here
           msg->length = sizeof(s_mtk_nfc_em_als_readerm_opt_req);
           msg->type = MTK_NFC_EM_ALS_READER_MODE_OPT_REQ;
           memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_als_readerm_opt_req, sizeof(s_mtk_nfc_em_als_readerm_opt_req));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   
           
           META_LOG("%02x,",
                   req->cmd.m_nNfc_als_readerm_opt_req.action);
           
           META_NFC_SEND_SOCKET(msg);
       }
       break;
       case NFC_OP_GET_SELIST:
       {
           MTK_NFC_MSG_T *msg = NULL;
           //msg = malloc(sizeof(MTK_NFC_MSG_T) + sizeof(s_mtk_nfc_em_als_readerm_opt_req));
           msg = malloc(sizeof(MTK_NFC_MSG_T));
           //Write handle function here
           msg->length = 0;//sizeof(s_mtk_nfc_em_als_readerm_opt_req);
           msg->type = MTK_NFC_GET_SELIST_REQ;
           //memcpy(((unsigned char*)msg + sizeof(MTK_NFC_MSG_T)), (unsigned char*)&req->cmd.m_nNfc_se_get_list_req, sizeof(s_mtk_nfc_jni_se_get_list_req_t));
           META_LOG("META_NFC_OP:NFC msg_type,msg_length = (%d,%d)", msg->type, msg->length);   
           META_NFC_SEND_SOCKET(msg);
       }
       break;
       default:
       {
           nfc_cnf.status = META_FAILED;
           WriteDataToPC(&nfc_cnf, sizeof(NFC_CNF), NULL, 0);
           META_LOG("Unknow OP ID,%02x",req->op);
           
           break;
       }
    }
    return;
}


