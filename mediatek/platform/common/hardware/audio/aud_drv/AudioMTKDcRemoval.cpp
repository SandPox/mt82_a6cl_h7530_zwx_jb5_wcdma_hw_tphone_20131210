#define LOG_TAG  "AudioMTKDCRemoval"

#include <cutils/compiler.h>


#include "AudioMTKDcRemoval.h"
#include <cutils/xlog.h>


#define ENABLE_DC_REMOVE

namespace android
{

DcRemove::DcRemove()
    : mHandle(NULL)
{
}
DcRemove::~DcRemove()
{
    close();
}

status_t  DcRemove::init(uint32 channel, uint32 samplerate, uint32 dcrMode)
{
    Mutex::Autolock _l(&mLock);
    if (!mHandle)
    {
        mHandle = DCR_Open(channel, samplerate, dcrMode);
    }
    else
    {
        mHandle = DCR_ReConfig(mHandle, channel, samplerate, dcrMode);
        // SXLOGD("DcRemove::Reconfig channel=%u,samplerate=%u,dcrMode=%u", channel, samplerate, dcrMode);
    }
    if (!mHandle)
    {
        SXLOGW("Fail to get DCR Handle");
        return NO_INIT;
    }
    return NO_ERROR;
}

status_t  DcRemove::close()
{
    Mutex::Autolock _l(&mLock);
    SXLOGV("DcRemove::deinit");
    if (mHandle)
    {
        DCR_Close(mHandle);
        mHandle = NULL;
    }
    return NO_ERROR;
}

size_t DcRemove::process(const void *inbuffer, size_t bytes, void *outbuffer)
{
    Mutex::Autolock _l(&mLock);
#ifdef ENABLE_DC_REMOVE
    if (mHandle)
    {
        size_t outputBytes = 0;
        uint32 inputBufSize  = bytes;
        uint32 outputBufSize = bytes;
        outputBytes = DCR_Process(mHandle, (short *)inbuffer, &inputBufSize, (short *)outbuffer, &outputBufSize);
        SXLOGV("DcRemove::process inputBufSize = %d,outputBufSize=%d,outputBytes=%d ", inputBufSize, outputBufSize, outputBytes);
        return outputBytes;
    }
    SXLOGW("DcRemove::process Dcr not initialized");
#endif
    return 0;
}

}

