#include "WCNChipController.h"

#include "AudioIoctl.h"
#include <sys/stat.h> 
#include <fcntl.h>

#include <utils/threads.h>

#include <linux/fm.h>

#define LOG_TAG "WCNChipController"

namespace android
{

/*==============================================================================
 *                     Property keys
 *============================================================================*/

/*==============================================================================
 *                     Const Value
 *============================================================================*/

static const uint32_t kMaxFMChipVolume = 15;

/*==============================================================================
 *                     Enumerator
 *============================================================================*/

/*==============================================================================
 *                     Singleton Pattern
 *============================================================================*/

WCNChipController *WCNChipController::mWCNChipController = NULL;

WCNChipController *WCNChipController::GetInstance()
{
    static Mutex mGetInstanceLock;
    Mutex::Autolock _l(mGetInstanceLock);

    if (mWCNChipController == NULL)
    {
        mWCNChipController = new WCNChipController();
    }
    ASSERT(mWCNChipController != NULL);
    return mWCNChipController;
}

/*==============================================================================
 *                     Constructor / Destructor / Init / Deinit
 *============================================================================*/

WCNChipController::WCNChipController()
{
    ALOGD("%s()", __FUNCTION__);
}

WCNChipController::~WCNChipController()
{
    ALOGD("%s()", __FUNCTION__);
}

/*==============================================================================
 *                     WCN FM Chip Control
 *============================================================================*/

bool WCNChipController::GetFmChipPowerInfo()
{
    static const size_t BUF_LEN = 1;

    char rbuf[BUF_LEN] = {'\0'};
    char wbuf[BUF_LEN] = {'1'};
    const char *FM_POWER_STAUTS_PATH = "/proc/fm";

    ALOGD("+%s()", __FUNCTION__);

    int fd = open(FM_POWER_STAUTS_PATH, O_RDONLY, 0);
    if (fd < 0)
    {
        ALOGE("-%s(), open(%s) fail!! fd = %d", __FUNCTION__, FM_POWER_STAUTS_PATH, fd);
        return false;
    }

    int ret = read(fd, rbuf, BUF_LEN);
    if (ret != BUF_LEN)
    {
        ALOGE("-%s(), read(%s) fail!! ret = %d", __FUNCTION__, FM_POWER_STAUTS_PATH, ret);
        return false;
    }
    close(fd);

    const bool fm_power_on = (strncmp(wbuf, rbuf, BUF_LEN) == 0) ? true : false;

    ALOGD("-%s(), fm_power_on = %d", __FUNCTION__, fm_power_on);
    return fm_power_on;
}

status_t WCNChipController::SetFmChipConfiguration()
{
    ALOGD("+%s()", __FUNCTION__);

    // File descriptor
    int fd_fm = open(FM_DEVICE_NAME, O_RDWR);
    ALOGD("%s(), open(%s), fd_fm = %d", __FUNCTION__, FM_DEVICE_NAME, fd_fm);
    if (fd_fm == -1008) // WCN chip reset
    {
        ALOGW("%s(), WCN chip reset, return!!", __FUNCTION__);
        return UNKNOWN_ERROR;
    }
    ASSERT(fd_fm >= 0);

    int fd_audio = ::open(kAudioDeviceName, O_RDWR);
    ALOGD("%s(), open(%s), fd_audio = %d", __FUNCTION__, kAudioDeviceName, fd_audio);
    ASSERT(fd_audio >= 0);


    // intial setting
    struct fm_i2s_setting fm_chip_i2s_setting; // defined in alps/mediatek/kernel/include/linux/fm.h
    fm_chip_i2s_setting.onoff  = FM_I2S_ON;
#if defined(MTK_MERGE_INTERFACE_SUPPORT)
    // Merge I2S In
    fm_chip_i2s_setting.mode   = FM_I2S_SLAVE;
    fm_chip_i2s_setting.sample = FM_I2S_44K;
#else
    // 2nd I2S In
    fm_chip_i2s_setting.mode   = FM_I2S_MASTER;
    fm_chip_i2s_setting.sample = FM_I2S_32K;
#endif


    // Config FM chip
    int ret = ioctl(fd_fm, FM_IOCTL_I2S_SETTING, &fm_chip_i2s_setting);
    ALOGD("%s(), ioctl: FM_IOCTL_I2S_SETTING, ret = %d", __FUNCTION__, ret);

    // Reset FM Chip
    ::ioctl(fd_audio, AUDDRV_RESET_FMCHIP_MERGEIF);

    // Set GPIO
    ::ioctl(fd_audio, AUDDRV_SET_FM_I2S_GPIO);


    close(fd_fm);
    close(fd_audio);

    ALOGD("-%s()", __FUNCTION__);
    return NO_ERROR;
}

status_t WCNChipController::SetFmChipVolume(const uint32_t fm_chip_volume)
{
    ALOGD("+%s(), fm_chip_volume = %u", __FUNCTION__, fm_chip_volume);

    WARNING("No need to set FM Chip Volume in Audio Driver");

    ASSERT(0 <= fm_chip_volume && fm_chip_volume <= kMaxFMChipVolume);

    ASSERT(GetFmChipPowerInfo() == true);

    int fd_fm = open(FM_DEVICE_NAME, O_RDWR);
    ALOGD("%s(), open(%s), fd_fm = %d", __FUNCTION__, FM_DEVICE_NAME, fd_fm);
    if (fd_fm == -1008) // WCN chip reset
    {
        ALOGW("%s(), WCN chip reset, return!!", __FUNCTION__);
        return UNKNOWN_ERROR;
    }
    ASSERT(fd_fm >= 0);

    int ret = ::ioctl(fd_fm, FM_IOCTL_SETVOL, (uint32_t *)&fm_chip_volume);
    ALOGD("%s(), ioctl: FM_IOCTL_SETVOL, ret = %d", __FUNCTION__, ret);
    ASSERT(ret == 0);

    close(fd_fm);

    ALOGD("-%s(), fm_chip_volume = %u", __FUNCTION__, fm_chip_volume);
    return NO_ERROR;
}

} // end of namespace android
