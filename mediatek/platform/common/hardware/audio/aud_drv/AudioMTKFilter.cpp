


#define LOG_TAG  "AudioMTKFilter"

#include <media/AudioSystem.h>
#include <cutils/compiler.h>

#include "audio_custom_exp.h"
#include "AudioCustParam.h"
#include "CFG_AUDIO_File.h"
#include"AudioMTKFilter.h"
#include <cutils/xlog.h>

#include "AudioType.h"

namespace
{

//Configure ACF Work Mode
#if defined(ENABLE_AUDIO_COMPENSATION_FILTER) && defined(ENABLE_AUDIO_DRC_SPEAKER)
//5
#define AUDIO_COMPENSATION_FLT_MODE AUDIO_CMP_FLT_LOUDNESS_COMP_BASIC

#elif defined(ENABLE_AUDIO_COMPENSATION_FILTER)
// 4
#define AUDIO_COMPENSATION_FLT_MODE AUDIO_CMP_FLT_LOUDNESS_COMP

#elif defined(ENABLE_AUDIO_DRC_SPEAKER)
// 3
#define AUDIO_COMPENSATION_FLT_MODE AUDIO_CMP_FLT_LOUDNESS_LITE

#endif

}


namespace android
{

AudioMTKFilter::AudioMTKFilter(
    AudioCompFltType_t type,
    AudioComFltMode_t mode,
    uint32_t sampleRate,
    uint32_t channel,
    uint32_t format,
    size_t bufferSize)
    : mType(type),
      mMode(mode),
      mSampleTate(sampleRate),
      mChannel(channel),
      mFormat(format),
      mBufferSize(bufferSize),
      mFilter(NULL),
      mStart(false),
      mActive(false)
{
    init();
}

AudioMTKFilter::~AudioMTKFilter()
{
    if (mFilter)
    {
        mFilter->Stop();
        mFilter->Deinit();
    }
}

status_t AudioMTKFilter::init()
{
    Mutex::Autolock _l(&mLock);
    if (mType < AUDIO_COMP_FLT_NUM)
    {
        mFilter = new AudioCompensationFilter(mType, mBufferSize);
        if (NULL != mFilter)
        {
            mFilter->Init();
            mFilter->LoadACFParameter();
            return NO_ERROR;
        }
    }
    return NO_INIT;
}

void AudioMTKFilter::start()
{
    Mutex::Autolock _l(mLock);
    if (mFilter && !mActive)
    {
        SXLOGV("AudioMTKFilter::start() type %d mode %d", mType, mMode);
        mFilter->SetWorkMode(mChannel, mSampleTate, mMode);
        mFilter->Start();
        mStart  = true;
        mActive = true;
    }
    return;
}

void AudioMTKFilter::stop()
{
    Mutex::Autolock _l(mLock);
    if (mFilter && mActive)
    {
        SXLOGV("AudioMTKFilter::stop() type %d mode %d", mType, mMode);
        mFilter->Stop();
        mFilter->ResetBuffer();
        mStart  = false;
        mActive = false;
    }
    return;
}

void AudioMTKFilter::pause()
{
    Mutex::Autolock _l(mLock);
    if (mFilter && mActive)
    {
        SXLOGV("AudioMTKFilter::pause() type %d mode %d", mType, mMode);
        mFilter->Pause();
        mActive = false;
    }
}

void AudioMTKFilter::resume()
{
    Mutex::Autolock _l(mLock);
    if (mFilter && !mActive)
    {
        SXLOGV("AudioMTKFilter::resume() type %d mode %d", mType, mMode);
        mFilter->Resume();
        mActive = true;
    }
}

bool AudioMTKFilter::isStart()
{
    Mutex::Autolock _l(mLock);
    return mStart;
}

bool AudioMTKFilter::isActive()
{
    Mutex::Autolock _l(mLock);
    return mActive;
}

void AudioMTKFilter::setParameter(void *param)
{
    Mutex::Autolock _l(mLock);
    if (mFilter)
    {
        SXLOGV("AudioMTKFilter::setParameter type %d mode %d", mType, mMode);
        if (mActive)
        {
            mFilter->Stop();
            mFilter->ResetBuffer();
        }
        mFilter->SetACFPreviewParameter((AUDIO_ACF_CUSTOM_PARAM_STRUCT *)param);
        if (mActive)
        {
            mFilter->SetWorkMode(mChannel, mSampleTate, mMode);
            mFilter->Start();
        }
    }
}

uint32_t AudioMTKFilter::process(void *inBuffer, uint32_t bytes, void *outBuffer)
{
    // if return 0, means CompFilter can't do anything. Caller should use input buffer to write to Hw.
    // do post process
    Mutex::Autolock _l(mLock);
    if (mFilter && mActive)
    {
        SXLOGV("AudioMTKFilter::process type %d mode %d", mType, mMode);
        int shift = mFormat == AUDIO_FORMAT_PCM_16_BIT ? 1 : 0;
        int inSample =  bytes >> shift;
        int outSample = 0;
        if (inSample >= 1024)
        {
            mFilter->Process((short *)inBuffer, &inSample, (short *)outBuffer, &outSample);
            return outSample << shift; // sameples to bytes
        }
    }
    return 0;
}


//filter manager
#undef  LOG_TAG
#define LOG_TAG  "AudioMTKFilterManager"

AudioMTKFilterManager::AudioMTKFilterManager(
    uint32_t sampleRate,
    uint32_t channel,
    uint32_t format,
    size_t bufferSize)
    : mSamplerate(sampleRate),
      mChannel(channel),
      mFormat(format),
      mBufferSize(bufferSize),
      mFixedParam(false),
      mSpeakerFilter(NULL),
      mHeadphoneFilter(NULL),
      mEnhanceFilter(NULL),
      mBuffer(NULL),
      mDevices(0)
{
    init();
}

AudioMTKFilterManager::~AudioMTKFilterManager()
{
    if (mSpeakerFilter)
    {
        mSpeakerFilter->stop();
        delete mSpeakerFilter;
        mSpeakerFilter = NULL;
    }
    if (mHeadphoneFilter)
    {
        mHeadphoneFilter->stop();
        delete mHeadphoneFilter;
        mHeadphoneFilter = NULL;
    }
    if (mEnhanceFilter)
    {
        mEnhanceFilter->stop();
        delete mEnhanceFilter;
        mEnhanceFilter = NULL;
    }
    if (mBuffer)
    {
        delete[] mBuffer;
        mBuffer = NULL;
    }

}

bool AudioMTKFilterManager::init()
{

#if defined(ENABLE_AUDIO_COMPENSATION_FILTER)||defined(ENABLE_AUDIO_DRC_SPEAKER)
    mSpeakerFilter = new AudioMTKFilter(AUDIO_COMP_FLT_AUDIO, AUDIO_COMPENSATION_FLT_MODE,
                                        mSamplerate, mChannel, mFormat, mBufferSize);
    ASSERT(mSpeakerFilter != NULL);
#endif

#if defined(ENABLE_HEADPHONE_COMPENSATION_FILTER)
    mHeadphoneFilter = new AudioMTKFilter(AUDIO_COMP_FLT_HEADPHONE, AUDIO_CMP_FLT_LOUDNESS_COMP_HEADPHONE,
                                          mSamplerate, mChannel, mFormat, mBufferSize);
    ASSERT(mHeadphoneFilter != NULL);
#endif

#if defined(MTK_AUDENH_SUPPORT) //For reduce resource
    mEnhanceFilter = new AudioMTKFilter(AUDIO_COMP_FLT_AUDENH, AUDIO_CMP_FLT_LOUDNESS_COMP_AUDENH,
                                        mSamplerate, mChannel, mFormat, mBufferSize);

    ASSERT(mEnhanceFilter != NULL);

    mBuffer = new uint8_t[mBufferSize];
    unsigned int result = 0 ;
#if 0
    char value[PROPERTY_VALUE_MAX];
    int result = 0 ;
    property_get("persist.af.audenh.ctrl", value, "1");
    result = atoi(value);
#else
    AUDIO_AUDENH_CONTROL_OPTION_STRUCT audioParam;
    if (GetAudEnhControlOptionParamFromNV(&audioParam))
    {
        result = audioParam.u32EnableFlg;
    }
#endif
    mFixedParam = (result ? true : false);
#endif

    SXLOGD("init() fixedParam %d", mFixedParam);

    return NO_ERROR;
}

void AudioMTKFilterManager::start()
{
    uint32_t device = mDevices;
    SXLOGV("start() device 0x%x", device);

    if (device & AUDIO_DEVICE_OUT_SPEAKER)
    {
        //stop hcf & enhangce
        if (mHeadphoneFilter) { mHeadphoneFilter->stop(); }
        if (mEnhanceFilter) { mEnhanceFilter->stop(); }
        // start acf
        if (mSpeakerFilter) { mSpeakerFilter->start(); }
    }
    else if ((device & AUDIO_DEVICE_OUT_WIRED_HEADSET) || (device & AUDIO_DEVICE_OUT_WIRED_HEADPHONE))
    {
        // stop acf
        if (mSpeakerFilter) { mSpeakerFilter->stop(); }
        // start hcf
        if (mHeadphoneFilter) { mHeadphoneFilter->start(); }

        if (mEnhanceFilter)
        {
            if (false == mFixedParam)
            {
                if (mEnhanceFilter->isStart()) { mEnhanceFilter->pause(); }
            }
            else
            {
                if (!mEnhanceFilter->isStart())
                {
                    mEnhanceFilter->start();
                }
                else
                {
                    mEnhanceFilter->resume();
                }
            }
        }
    }
}

void AudioMTKFilterManager::stop()
{
    SXLOGV("stop()");

    if (mSpeakerFilter) { mSpeakerFilter->stop(); }
    if (mHeadphoneFilter) { mHeadphoneFilter->stop(); }
    if (mEnhanceFilter) { mEnhanceFilter->stop(); }
}

bool  AudioMTKFilterManager::isFilterStart(uint32_t type)
{
    if (type == AUDIO_COMP_FLT_AUDIO && mSpeakerFilter)
    {
        return mSpeakerFilter->isStart();
    }
    else if (type == AUDIO_COMP_FLT_HEADPHONE && mHeadphoneFilter)
    {
        return mHeadphoneFilter->isStart();
    }
    else if (type == AUDIO_COMP_FLT_AUDENH && mEnhanceFilter)
    {
        return mEnhanceFilter->isStart();
    }
    return false;
}

void AudioMTKFilterManager::setParamFixed(bool flag)
{
    SXLOGV("setParamFixed() flag %d", flag);

#if defined(MTK_AUDENH_SUPPORT)
    mFixedParam = flag;
#if 0
    if (flag)
    {
        property_set("persist.af.audenh.ctrl", "1");
    }
    else
    {
        property_set("persist.af.audenh.ctrl", "0");
    }
#else
    AUDIO_AUDENH_CONTROL_OPTION_STRUCT audioParam;
    audioParam.u32EnableFlg = flag ? 1 : 0;
    SetAudEnhControlOptionParamToNV(&audioParam);
#endif
#else
    SXLOGW("Unsupport AudEnh Feature");
#endif
}

bool AudioMTKFilterManager::isParamFixed()
{
    return mFixedParam;
}

void AudioMTKFilterManager::setDevice(uint32_t devices)
{
    mDevices = devices;
    return;
}

uint32_t  AudioMTKFilterManager::process(void *inBuffer, uint32_t bytes, void *outBuffer)
{
    SXLOGV("+process() insize %u", bytes);
    uint32_t outputSize = 0;
    uint32_t device = mDevices;
    if (device & AUDIO_DEVICE_OUT_SPEAKER)
    {
        if (mSpeakerFilter && mSpeakerFilter->isActive())
        {
            outputSize = mSpeakerFilter->process(inBuffer, bytes, outBuffer);
        }
    }
    else if ((device & AUDIO_DEVICE_OUT_WIRED_HEADSET) || (device & AUDIO_DEVICE_OUT_WIRED_HEADPHONE))
    {
        if (mEnhanceFilter && mEnhanceFilter->isActive())
        {
            outputSize = mEnhanceFilter->process(inBuffer, bytes, outBuffer);
        }
        if (mHeadphoneFilter && mHeadphoneFilter->isActive())
        {
            if (CC_UNLIKELY(outputSize == 0))
            {
                outputSize = mHeadphoneFilter->process(inBuffer, bytes, outBuffer);
            }
            else
            {
                void *in = outBuffer;
                void *out = mBuffer;
                outputSize = mHeadphoneFilter->process(in, outputSize, out);
                if (outputSize > 0) { memcpy(outBuffer, out, outputSize); }
            }
        }
    }
    SXLOGV("-process() outsize %u", outputSize);
    return outputSize;

}

void AudioMTKFilterManager::setParameter(uint32_t type, void *param)
{
    SXLOGV("setParameter() type %u", type);

    if (type == AUDIO_COMP_FLT_AUDIO && mSpeakerFilter)
    {
        return mSpeakerFilter->setParameter(param);
    }
    else if (type == AUDIO_COMP_FLT_HEADPHONE && mHeadphoneFilter)
    {
        return mHeadphoneFilter->setParameter(param);
    }
    else if (type == AUDIO_COMP_FLT_AUDENH && mEnhanceFilter)
    {
        return mEnhanceFilter->setParameter(param);
    }
}

}

