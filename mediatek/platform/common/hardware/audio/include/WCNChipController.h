#ifndef ANDROID_WCN_CHIP_CONTROLLER_H
#define ANDROID_WCN_CHIP_CONTROLLER_H

#include "AudioType.h"

namespace android
{

class WCNChipController
{
    public:
        virtual ~WCNChipController();

        static WCNChipController *GetInstance();

        virtual bool     GetFmChipPowerInfo();

        virtual status_t SetFmChipConfiguration();

        virtual status_t SetFmChipVolume(const uint32_t fm_chip_volume);


    protected:
        WCNChipController();


    private:
        static WCNChipController *mWCNChipController; // singleton

};

} // end namespace android

#endif // end of ANDROID_WCN_CHIP_CONTROLLER_H
