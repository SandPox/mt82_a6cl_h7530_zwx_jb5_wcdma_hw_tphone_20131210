#
# libmpoencoder
#
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= MpoEncoder.cpp
    
LOCAL_C_INCLUDES += $(MTK_PATH_SOURCE)/protect-bsp/external/mhal/src/core/common/libmpo \
	$(JNI_H_INCLUDE) 

LOCAL_C_INCLUDES += \
        $(MTK_ROOT)/external/mhal/src/core/common/libmpo \
        $(MTK_ROOT)/external/mhal/src/core/common/libmpo/mpodecoder \

LOCAL_SHARED_LIBRARIES:= \
	libcutils \
        libmpo

LOCAL_PRELINK_MODULE := false

LOCAL_MODULE:= libmpoencoder

LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)

