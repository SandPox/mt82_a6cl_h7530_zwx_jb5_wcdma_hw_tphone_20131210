/* Build
 * ./mk -o=HAVE_CMMB_FEATURE=yes mt6589_phone_qhdv2 mm ./mediatek/frameworks/base/tests/JavaLibraryProGuard/
 * Execute
 * adb shell am instrument -w -e class com.mediatek.javalibrary.proguard.test.ProGuardTest com.mediatek.javalibrary.proguard.test/android.test.InstrumentationTestRunner
 */

package com.mediatek.javalibrary.proguard.test;

import android.app.Instrumentation;
import android.test.AndroidTestCase;
import android.util.Log;
import java.lang.reflect.Method;
import junit.framework.Assert;

//Target Package CMMB
import com.mediatek.mbbms.ESGHandler;
//Target Package PushParser
import com.mediatek.pushparser.si.SiDateDecoderUtil;
//Target Package Gemini
import com.android.internal.telephony.gemini.GeminiCallSubUtil;
//Target Package pluginmanager
import com.mediatek.pluginmanager.PluginManager;
import android.content.pm.Signature;
//Target Package ANRManager
import android.app.ActivityManager;
import android.app.Activity;
import android.content.Context;
import com.android.server.am.ANRManager;

public class ProGuardTest extends AndroidTestCase {
    private static final String TAG = "JavaLibraryProGuardTest";
    private boolean CMMBObjNameObfuscated = true;
    private boolean PushParserObjNameObfuscated = true;
    private boolean GeminiObjNameObfuscated = true;
    private boolean PluginManagerObjNameObfuscated = true;
    private boolean ANRManagerObjNameObfuscated = true;

    private boolean CMMBAMethodName = false;
    private boolean PushParserAMethodName = false;
    private boolean GeminiAMethodName = false;
    private boolean PluginManagerAMethodName = false;
    private boolean ANRManagerAMethodName = false;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Log.i(TAG, " Setup JavaLibrary ProGuard Test Case ");

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        Log.i(TAG, " TearDown JavaLibrary ProGuard Test Case ");
    }

    public void testCMMBProGuard() throws Throwable {

        Log.i(TAG, " testCMMBProGuard ");

        ESGHandler checkClass = new ESGHandler(null);
        Method[] methods = checkClass.getClass().getDeclaredMethods();

        for ( Method checkMethodName : methods) {
            Log.i(TAG,"CMMB checkMethodName = " + checkMethodName.getName());
            if (CMMBObjNameObfuscated && checkMethodName.getName().equals("insertValue")){
              CMMBObjNameObfuscated = false;
            }
            if (!CMMBAMethodName && checkMethodName.getName().equals("a")){
              CMMBAMethodName = true;
            }
        }

        Log.i(TAG,"CMMB checkMethodName = insertValue is obfusted : " + CMMBObjNameObfuscated);
        Log.i(TAG,"CMMB checkMethodName = a is                    : " + CMMBAMethodName);
        assertTrue("CMMB object name is not obfuscated",(CMMBObjNameObfuscated & CMMBAMethodName));

    }

    public void testPushParserProGuard() throws Throwable {

        Log.i(TAG, " testPushParserProGuard ");

        SiDateDecoderUtil checkClass = new SiDateDecoderUtil();
        Method[] methods = checkClass.getClass().getDeclaredMethods();

        for ( Method checkMethodName : methods) {
            Log.i(TAG,"PushParser checkMethodName = " + checkMethodName.getName());
            if (PushParserObjNameObfuscated && checkMethodName.getName().equals("WbXmlDateDecoder")){
              PushParserObjNameObfuscated = false;
            }
            if (!PushParserAMethodName && checkMethodName.getName().equals("a")){
              PushParserAMethodName = true;
            }
        }

        Log.i(TAG,"PushParser checkMethodName = WbXmlDateDecoder is obfusted : " + PushParserObjNameObfuscated);
        Log.i(TAG,"PushParser checkMethodName = a is                         : " + PushParserAMethodName);
        assertTrue("PushParser object name is not obfuscated",(PushParserObjNameObfuscated & PushParserAMethodName) );

    }


    public void testGeminiProGuard() throws Throwable {

        Log.i(TAG, " testGeminiProGuard ");

        GeminiCallSubUtil checkClass = new GeminiCallSubUtil();
        Method[] methods = checkClass.getClass().getDeclaredMethods();

        for ( Method checkMethodName : methods) {
            Log.i(TAG,"Gemini checkMethodName = " + checkMethodName.getName());
            if (GeminiObjNameObfuscated && checkMethodName.getName().equals("getBackgroundCall")){
              GeminiObjNameObfuscated = false;
              //Log.i(TAG,"Gemini checkMethodName hit = " + checkMethodName.getName() + "GeminiObjNameObfuscated = " + GeminiObjNameObfuscated);
            }
            if (!GeminiAMethodName && checkMethodName.getName().equals("a")){
              GeminiAMethodName = true;
            }
        }

        Log.i(TAG,"Gemini checkMethodName = getBackgroundCall is obfusted : " + GeminiObjNameObfuscated);
        Log.i(TAG,"Gemini checkMethodName = a is                          : " + GeminiAMethodName);
        assertTrue("Gemini object name is not obfuscated",(GeminiObjNameObfuscated & GeminiAMethodName));

    }

    public void testPluginManagerProGuard() throws Throwable {

        Log.i(TAG, " testPluginManagerProGuard ");
        Signature [] mSignature = null;

        PluginManager<String> checkClass = PluginManager.create(getContext(),"testPluginManagerProGuard", mSignature);
        Method[] methods = checkClass.getClass().getDeclaredMethods();

        for ( Method checkMethodName : methods) {
            if (PluginManagerObjNameObfuscated && checkMethodName.getName().equals("checkPermission")){
              PluginManagerObjNameObfuscated = false;
            }
            if (!PluginManagerAMethodName && checkMethodName.getName().equals("a")){
              PluginManagerAMethodName = true;
            }
        }

        Log.i(TAG,"PluginManager checkMethodName = checkPermission is obfusted : " + PluginManagerObjNameObfuscated);
        Log.i(TAG,"PluginManager checkMethodName = a is                        : " + PluginManagerAMethodName);
        assertTrue("PluginManager object name is not obfuscated",(PluginManagerObjNameObfuscated & PluginManagerAMethodName));

    }

  public void testANRManagerProGuard() throws Throwable {

      Log.i(TAG, " testANRManagerProGuard ");

      ANRManager checkClass = new ANRManager(null);
      Method[] methods = checkClass.getClass().getDeclaredMethods();

      for ( Method checkMethodName : methods) {
          Log.i(TAG,"ANRManager checkMethodName = " + checkMethodName.getName());
          if (ANRManagerObjNameObfuscated && checkMethodName.getName().equals("preDumpStackTraces")
            && (checkMethodName.getParameterTypes().length==2)){
            ANRManagerObjNameObfuscated = false;
          }
          if (!ANRManagerAMethodName && checkMethodName.getName().equals("a")){
            ANRManagerAMethodName = true;
          }
      }

      Log.i(TAG,"ANRManager checkMethodName = preDumpStackTraces is obfusted : " + ANRManagerObjNameObfuscated);
      Log.i(TAG,"ANRManager checkMethodName = a is                           : " + ANRManagerAMethodName);
      assertTrue("ANRManager object name is not obfuscated",(ANRManagerObjNameObfuscated & ANRManagerAMethodName));

  }

}

