package com.mediatek.telephony;
import android.content.Context;
import android.util.Log;
import android.content.Intent;
import android.content.ContentResolver;
import java.util.List;
import android.provider.Settings;
import android.os.SystemProperties;
import android.provider.Telephony.SIMInfo;
import com.mediatek.telephony.SimInfoManager.SimInfoRecord;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.common.featureoption.FeatureOption;
/**
 *@hide
 */
public class DefaultSmsSimSettings {
    private static final String TAG = "DefaultSmsSimSettings";

    public static void setSmsTalkDefaultSim(ContentResolver contentResolver,
            List<SimInfoRecord> simInfos, long[] simIdForSlot, int nSIMCount) {
        if (!FeatureOption.MTK_BSP_PACKAGE) {
            String optr = SystemProperties.get("ro.operator.optr");
            Log.i("TAG", "nSIMCount" + nSIMCount + " , optr = " + optr);
            long oldSmsDefaultSIM = Settings.System.getLong(contentResolver,
                    Settings.System.SMS_SIM_SETTING, Settings.System.DEFAULT_SIM_NOT_SET);
            Log.i("TAG", "oldSmsDefaultSIM" + oldSmsDefaultSIM);
            long defSIM = Settings.System.DEFAULT_SIM_NOT_SET;
            if (nSIMCount > 1) {
                if (oldSmsDefaultSIM == Settings.System.DEFAULT_SIM_NOT_SET) {
                    if ("OP01".equals(optr)) {
                        Settings.System.putLong(contentResolver, Settings.System.SMS_SIM_SETTING,
                                Settings.System.SMS_SIM_SETTING_AUTO);
                    } else {
                        Settings.System.putLong(contentResolver, Settings.System.SMS_SIM_SETTING,
                                Settings.System.DEFAULT_SIM_SETTING_ALWAYS_ASK);
                    }
                }
                if ("OP01".equals(optr)) {
                    defSIM = Settings.System.SMS_SIM_SETTING_AUTO;
                } else {
                    defSIM = Settings.System.DEFAULT_SIM_SETTING_ALWAYS_ASK;
                }
            } else if (nSIMCount == 1) {
                long simId = simInfos.get(0).mSimInfoId;
                Settings.System.putLong(contentResolver, Settings.System.SMS_SIM_SETTING, simId);
                defSIM = simInfos.get(0).mSimInfoId;
            }
            Log.i("TAG", "defSIM" + defSIM);
            if (isSimRemoved(oldSmsDefaultSIM, simIdForSlot, PhoneConstants.GEMINI_SIM_NUM)) {
                Settings.System.putLong(contentResolver, Settings.System.SMS_SIM_SETTING, defSIM);
            }
        }
    }

    private static boolean isSimRemoved(long defSimId, long[] curSim, int numSim) {
        // there is no default sim if defSIMId is less than zero
        if (defSimId <= 0) {
            return false;
        }

        boolean isDefaultSimRemoved = true;
        for (int i = 0; i < numSim; i++) {
            if (defSimId == curSim[i]) {
                isDefaultSimRemoved = false;
                break;
            }
        }
        return isDefaultSimRemoved;
    }
}
