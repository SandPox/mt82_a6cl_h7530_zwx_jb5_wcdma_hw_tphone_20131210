/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ngin3d;

/**
 * This class implements an XYZ triple, used to specify a point in a 3D space.
 */
public class Point {

    /**
     * The value of X axis.
     */
    public float x;

    /**
     * The value of Y axis.
     */
    public float y;

    /**
     * The value of Z axis.
     */
    public float z;

    /**
     * The value of Point is normalized or not.
     */
    public boolean isNormalized;

    /**
     * Construct a (0, 0, 0) point.
     */
    public Point() {
        // Do nothing by default
    }

    /**
     * Construct a (0, 0, 0) point with specified normalized flag.
     *
     * @param isNormalized true for normalized
     */
    public Point(boolean isNormalized) {
        this.isNormalized = isNormalized;
    }

    /**
     * Simple constructor.
     * Build a point from its coordinates
     *
     * @param x The value of X axis
     * @param y The value of Y axis
     */
    public Point(float x, float y) {
        set(x, y, 0);
    }

    public Point(float x, float y, boolean isNormalized) {
        set(x, y, 0);
        this.isNormalized = isNormalized;
    }

    /**
     * Simple constructor.
     * Build a point from its coordinates
     *
     * @param x The value of X axis
     * @param y The value of Y axis
     * @param z The value of Z axis
     */
    public Point(float x, float y, float z) {
        set(x, y, z);
    }

    public Point(float x, float y, float z, boolean isNormalized) {
        set(x, y, z);
        this.isNormalized = isNormalized;
    }

    public Point(Point other) {
        set(other.x, other.y, other.z);
        this.isNormalized = other.isNormalized;
    }

    public final void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Multiplicative constructor
     * Build a point from another one and a scale factor.
     * The point created will be a * u
     *
     * @param a scale factor
     * @param u base (unscaled) point
     */
    public Point(float a, Point u) {
        this.x = a * u.x;
        this.y = a * u.y;
        this.z = a * u.z;
    }


    /* The setters/getters are deprecated.  The aim is to remove them
     * to avoid confusion, as the raw values are public anyway.
     */

    @Deprecated
    /**
     * Set the X value of the Point.
     *
     * @param x the value of x
     * @deprecated use the public raw value instead
     */
    public void setX(float x) {
        this.x = x;
    }

    @Deprecated
    /**
     * Get the X value of the Point.
     *
     * @return X of the point
     * @deprecated use the public raw value instead
     */
    public float getX() {
        return x;
    }

    @Deprecated
    /**
     * Set the Y value of the Point.
     *
     * @param y the value of y
     * @deprecated use the public raw value instead
     */
    public void setY(float y) {
        this.y = y;
    }

    @Deprecated
    /**
     * Get the Y value of the Point.
     *
     * @return Y of the point
     * @deprecated use the public raw value instead
     */
    public float getY() {
        return y;
    }

    @Deprecated
    /**
     * Set the Z value of the Point.
     *
     * @param z the value of z
     * @deprecated use the public raw value instead
     */
    public void setZ(float z) {
        this.z = z;
    }

    @Deprecated
    /**
     * Get the Z value of the Point.
     *
     * @return Z of the point
     * @deprecated use the public raw value instead
     */
    public float getZ() {
        return z;
    }


    /**
     * Add two Points. h = j.add(v)
     *
     * @param v Point to add
     * @return a new Point h being j+v
     */
    public Point add(Point v) {
        return new Point(x + v.x, y + v.y, z + v.z);
    }

    /**
     * Add two Points with scaling. h = j.add(factor,v)
     *
     * @param factor scale factor to apply to v before adding it
     * @param v      Point to add
     * @return a new Point h being j+(factor*v)
     */
    public Point add(float factor, Point v) {
        return new Point(x + factor * v.x, y + factor * v.y, z + factor * v.z);
    }

    /**
     * Subtract two Points. h = j.subtract(v)
     *
     * @param v Point to subtract
     * @return a new Point h being j-v
     */
    public Point subtract(Point v) {
        return new Point(x - v.x, y - v.y, z - v.z);
    }

    /**
     * Subtract two Points with scaling. h = j.subtract(factor, v)
     *
     * @param factor scale factor to apply to v before subtracting it
     * @param v      point to subtract
     * @return a new Point h being j-(factor*v)
     */
    public Point subtract(float factor, Point v) {
        return new Point(x - factor * v.x, y - factor * v.y, z - factor * v.z);
    }

    /**
     * Multiply the instance by a scalar
     *
     * @param a scalar
     * @return a new vector
     */
    public Point multiply(float a) {
        return new Point(a * x, a * y, a * z);
    }

    /**
     * Compute the distance between two points.
     *
     * @param v1 first point
     * @param v2 second point
     * @return the distance between v1 and v2
     */
    public static float distance(Point v1, Point v2) {
        final float dx = v2.x - v1.x;
        final float dy = v2.y - v1.y;
        final float dz = v2.z - v1.z;
        return (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (isNormalized != point.isNormalized) return false;
        if (Float.compare(point.x, x) != 0) return false;
        if (Float.compare(point.y, y) != 0) return false;
        if (Float.compare(point.z, z) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (x == +0.0f ? 0 : Float.floatToIntBits(x));
        result = 31 * result + (y == +0.0f ? 0 : Float.floatToIntBits(y));
        result = 31 * result + (z == +0.0f ? 0 : Float.floatToIntBits(z));
        result = 31 * result + (isNormalized ? 1 : 0);
        return result;
    }

    /**
     * Convert the point property to string for output
     * @return   output string
     */
    @Override
    public String toString() {
        return "Point:[" + this.x + ", " + this.y + ", " + this.z + "], isNormalized : " + isNormalized;
    }

    /**
     * Convert the point property to JSON formatted String
     * @return   output JSON formatted String
     */
    public String toJson() {
        return "{Point:[" + this.x + ", " + this.y + ", " + this.z + "], isNormalized : " + isNormalized + "}";
    }

    public static Point newFromString(String positionString) {
        float[] xyz = Utils.parseStringToFloat(positionString);
        return new Point(xyz[0], xyz[1], xyz[2]);
    }
}
