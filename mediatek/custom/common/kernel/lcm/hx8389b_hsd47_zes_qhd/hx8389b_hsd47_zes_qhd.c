/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/


#ifdef BUILD_LK
	#include <platform/mt_gpio.h>
	#include <platform/mt_pmic.h>
	
	#define Lcd_Log printf
#else
    #include <linux/string.h>
	#include <linux/kernel.h>
	#include <mach/mt_gpio.h>
	#include <mach/mt_pm_ldo.h>
	
	#define Lcd_Log printk
#endif

#include "lcm_drv.h"

// ---------------------------------------------------------------------------
//  Local Constants
// ---------------------------------------------------------------------------

#define FRAME_WIDTH  										(540)
#define FRAME_HEIGHT 										(960)

#define REGFLAG_DELAY             							0xFE///0xFE
#define REGFLAG_END_OF_TABLE      							 0xFFF    // END OF REGISTERS MARKER  0xFF 

#define LCM_ID_HX8389B	0x89//0x48655

#define LCM_DSI_CMD_MODE									0

#ifndef TRUE
    #define   TRUE     1
#endif
 
#ifndef FALSE
    #define   FALSE    0
#endif

// ---------------------------------------------------------------------------
//  Local Variables
// ---------------------------------------------------------------------------

static LCM_UTIL_FUNCS lcm_util = {0};
//static unsigned int vcom_level = 0x45;

#define SET_RESET_PIN(v)    								(lcm_util.set_reset_pin((v)))

#define UDELAY(n) 											(lcm_util.udelay(n))
#define MDELAY(n) 											(lcm_util.mdelay(n))


// ---------------------------------------------------------------------------
//  Local Functions
// ---------------------------------------------------------------------------

#define dsi_set_cmdq_V2(cmd, count, ppara, force_update)	lcm_util.dsi_set_cmdq_V2(cmd, count, ppara, force_update)
#define dsi_set_cmdq(pdata, queue_size, force_update)		lcm_util.dsi_set_cmdq(pdata, queue_size, force_update)
#define wrtie_cmd(cmd)										lcm_util.dsi_write_cmd(cmd)
#define write_regs(addr, pdata, byte_nums)					lcm_util.dsi_write_regs(addr, pdata, byte_nums)
#define read_reg											lcm_util.dsi_read_reg()
#define read_reg_v2(cmd, buffer, buffer_size)				lcm_util.dsi_dcs_read_lcm_reg_v2(cmd, buffer, buffer_size)

static struct LCM_setting_table {
    unsigned cmd;
    unsigned char count;
    unsigned char para_list[64];
};


static struct LCM_setting_table lcm_initialization_setting[] = {
//mx1031b_sb_s6 //suxian 
//********************
{0xB9,3,{0xFF,0x83,0x89}},
	{REGFLAG_DELAY, 10, {}},

	{0xBA, 7, {0x41,0x93,0x00,0x16,0xA4,0x10,0x18}}, 
	{REGFLAG_DELAY, 15, {}},

{0xc6,1,{0x08}},

{0xde,3,{0x05,0x58,0x10}},

{0xB1,19,{0x00,0x00,0x07,0xe8,0x40,
					0x10,0x11,0xD2,0xf2,0x32,
					0x3A,0x29,0x29,0x42,0x01,
					0x3A,0xF7,0x00,0xE6}}, 
	{REGFLAG_DELAY, 15, {}},

{0xB7,3,{0x00,0x00,0x4d}}, 

{0xB2,7,{0x00,0x00,0x78,0x0C,0x07,
				0x3F,0x40}},   

{0xb4,23,{0x80,0x08,0x00,0x32,0x10,
					0x04,0x32,0x10,0x00,0x32,
					0x10,0x00,0x37,0x0a,0x40,
					0x08,0x37,0x0A,0x40,0x14,
					0x46,0x50,0x0a}},


{0xD5,56,{0x00,0x00,0x00,0x00,0x01,
					0x00,0x00,0x00,0x60,0x00,
					0x99,0x88,0xAA,0xBB,0x88,
					0x23,0x88,0x01,0x88,0x67,
					0x88,0x45,0x01,0x23,0x88,
					0x88,0x88,0x88,0x88,0x88,
					0x99,0x88,0xBB,0xAA,0x54,
					0x88,0x76,0x88,0x10,0x88,
					0x32,0x32,0x10,0x88,0x88,
					0x88,0x88,0x88,0x00,0x04,
					0x00,0x00,0x00,0x00,0x00,
					0x00}},

{0xE0,34,{0x05,0x1E,0x21,0x39,0x3F,
					0x3F,0x2F,0x4A,0x09,0x0E,
					0x0F,0x12,0x14,0x11,0x14,
					0x10,0x18,0x05,0x1E,0x21,
					0x39,0x3F,0x3F,0x2F,0x4A,
					0x09,0x0E,0x0F,0x12,0x14,
					0x11,0x14,0x10,0x18}},
	{REGFLAG_DELAY, 15, {}},

{0xCC,1,{0x02}},
	{REGFLAG_DELAY, 25, {}},

{0xb6,4,{0x00,0x9C,0x00,0x9C}},
	{REGFLAG_DELAY, 10, {}},

{0x11,	0,	{0x00}},
{REGFLAG_DELAY, 120, {}},

{0x29,	0,	{0x00}},
{REGFLAG_DELAY, 100, {}},
	{REGFLAG_END_OF_TABLE, 0x00, {}},
	
};




static struct LCM_setting_table lcm_sleep_out_setting[] = {
    // Sleep Out
	{0x11, 0, {0x00}},
    {REGFLAG_DELAY, 120, {}},

    // Display ON
	{0x29, 0, {0x00}},
    {REGFLAG_DELAY, 100, {}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_sleep_in_setting[] = {
	// Display off sequence
	{0x28, 0, {0x00}},
	{REGFLAG_DELAY, 120, {}},
    // Sleep Mode On
	{0x10, 0, {0x00}},
	{REGFLAG_DELAY, 120, {}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};

static void push_table(struct LCM_setting_table *table, unsigned int count, unsigned char force_update)
{
	unsigned int i;

    for(i = 0; i < count; i++) {
		
        unsigned cmd;
        cmd = table[i].cmd;
		
        switch (cmd) {
			
            case REGFLAG_DELAY :
                MDELAY(table[i].count);
                break;
				
            case REGFLAG_END_OF_TABLE :
                break;
				
            default:
				dsi_set_cmdq_V2(cmd, table[i].count, table[i].para_list, force_update);
       	}
    }
	
}


// ---------------------------------------------------------------------------
//  LCM Driver Implementations
// ---------------------------------------------------------------------------

static void lcm_init_setting()
{
		unsigned int data_array[16];

      data_array[0]=0x00043902;//Enable external Command
        data_array[1]=0x8983FFB9; 
        dsi_set_cmdq(&data_array, 2, 1);

        data_array[0]=0x00083902;//Enable external Command//3
        data_array[1]=0x009341ba; 
        data_array[2]=0x1810a416; 
        dsi_set_cmdq(&data_array, 3, 1);

        data_array[0]=0x00023902;//Enable external Command
        data_array[1]=0x000008c6; 
        dsi_set_cmdq(&data_array, 2, 1);

        data_array[0]=0x00043902;//Enable external Command
        data_array[1]=0x105805de; 
        dsi_set_cmdq(&data_array, 2, 1);
                //MDELAY(1);//3000

        data_array[0]=0x00143902;
        data_array[1]=0x070000B1;
        data_array[2]=0x111040e8;
        data_array[3]=0x322ef2d2;  //3a32
        data_array[4]=0x01422929; 
        data_array[5]=0xe600f73a;  
        dsi_set_cmdq(&data_array, 6, 1);


        data_array[0]=0x00083902;
        data_array[1]=0x780000B2;
        data_array[2]=0x403f070c;
        dsi_set_cmdq(&data_array, 3, 1);

        data_array[0]=0x00043902;//Enable external Command//3
        data_array[1]=0x4d0000b7;
        dsi_set_cmdq(&data_array, 2, 1);


        data_array[0]=0x00183902;//Enable external Command//3
        data_array[1]=0x000882b4;   //0x000480b4
        data_array[2]=0x32041032; 
        data_array[3]=0x10320010; 
        data_array[4]=0x400a3700; 
        data_array[5]=0x400a3708; //0x380a1301
        data_array[6]=0x0a504614; 
        dsi_set_cmdq(&data_array, 7, 1);

        data_array[0]=0x00393902;//Enable external Command//3
        data_array[1]=0x000000d5; 
        data_array[2]=0x00000100; 
        data_array[3]=0x99006000; 
        data_array[4]=0x88bbaa88; 
        data_array[5]=0x88018823; 
        data_array[6]=0x01458867; 
        data_array[7]=0x88888823; 
        data_array[8]=0x99888888; 
        data_array[9]=0x5488aabb; 
        data_array[10]=0x10887688; 
        data_array[11]=0x10323288; 
        data_array[12]=0x88888888; 
        data_array[13]=0x00043c88; 
        data_array[14]=0x00000000; 
        data_array[15]=0x00000000; 
        dsi_set_cmdq(&data_array, 16, 1);

        data_array[0]=0x00053902;//Enable external Command
        data_array[1]=0x009c00b6; //98
        data_array[2]=0x0000009c; 
        dsi_set_cmdq(&data_array, 3, 1);

        data_array[0]=0x00233902;
        data_array[1]=0x211e05e0; 
        data_array[2]=0x2f3f3f39; 
        data_array[3]=0x0f0e094a; 
        data_array[4]=0x14111412; 
        data_array[5]=0x1e051810; 
        data_array[6]=0x3f3f3921; 
        data_array[7]=0x0e094a2f; 
        data_array[8]=0x1114120f; 
        data_array[9]=0x00181014; 
        dsi_set_cmdq(&data_array, 10, 1);

        data_array[0]=0x00023902;//e
        data_array[1]=0x000002cc;//0e
        dsi_set_cmdq(&data_array, 2, 1);

        //data_array[0]=0x00023902;
        //data_array[1]=0x00000035;
        //dsi_set_cmdq(&data_array, 2, 1);

        //data_array[0]=0x00033902;
        //data_array[1]=0x00000244;
        //dsi_set_cmdq(&data_array, 2, 1);


        data_array[0] = 0x00110500; //0x11,exit sleep mode,1byte
        dsi_set_cmdq(&data_array, 1, 1);
        MDELAY(120);//5000

        data_array[0] = 0x00290500; //0x11,exit sleep mode,1byte
        dsi_set_cmdq(&data_array, 1, 1);
        MDELAY(20);//5000
}

static void lcm_set_util_funcs(const LCM_UTIL_FUNCS *util)
{
    memcpy(&lcm_util, util, sizeof(LCM_UTIL_FUNCS));
}


static void lcm_get_params(LCM_PARAMS *params)
{
		memset(params, 0, sizeof(LCM_PARAMS));
	
		params->type   = LCM_TYPE_DSI;

		params->width  = FRAME_WIDTH;
		params->height = FRAME_HEIGHT;

		// enable tearing-free
		params->dbi.te_mode 				= LCM_DBI_TE_MODE_DISABLED;  //LCM_DBI_TE_MODE_VSYNC_ONLY;
		params->dbi.te_edge_polarity		= LCM_POLARITY_RISING;

#if (LCM_DSI_CMD_MODE)
		params->dsi.mode   = CMD_MODE;
#else
		params->dsi.mode   = SYNC_EVENT_VDO_MODE;//CMD_MODE;//SYNC_PULSE_VDO_MODE;
#endif
	
		// DSI
		/* Command mode setting */
		params->dsi.LANE_NUM				= LCM_TWO_LANE;
		//The following defined the fomat for data coming from LCD engine.
		params->dsi.data_format.color_order = LCM_COLOR_ORDER_RGB;
		params->dsi.data_format.trans_seq   = LCM_DSI_TRANS_SEQ_MSB_FIRST;
		params->dsi.data_format.padding     = LCM_DSI_PADDING_ON_LSB;
		params->dsi.data_format.format      = LCM_DSI_FORMAT_RGB888;

		// Highly depends on LCD driver capability.
		// Not support in MT6573
			params->dsi.packet_size=256;

		// Video mode setting		
			params->dsi.intermediat_buffer_num = 0;//because DSI/DPI HW design change, this parameters should be 0 when video mode in MT658X; or memory leakage

		params->dsi.PS=LCM_PACKED_PS_24BIT_RGB888;

		params->dsi.word_count=540*3;	//DSI CMD mode need set these two bellow params, different to 6577
			params->dsi.vertical_sync_active				= 9;//5;  //---3
			params->dsi.vertical_backporch					= 7;//5; //---14
			params->dsi.vertical_frontporch 				= 7;//5;  //----8
			params->dsi.vertical_active_line				= FRAME_HEIGHT; 
	
			params->dsi.horizontal_sync_active				= 40;//20;  //----2
			params->dsi.horizontal_backporch				= 45;//46; //----28
			params->dsi.horizontal_frontporch				= 48;//31; //----50
			params->dsi.horizontal_active_pixel 			= FRAME_WIDTH;
	
	
			//params->dsi.HS_PRPR=3;
			//params->dsi.CLK_HS_POST=22;
			//params->dsi.DA_HS_EXIT=20;
		// Bit rate calculation
			params->dsi.pll_div1=0; 	// div1=0,1,2,3;div1_real=1,2,4,4
		params->dsi.pll_div2=1;		// div2=0,1,2,3;div2_real=1,2,4,4
			params->dsi.fbk_div =19;	// fref=26MHz, fvco=fref*(fbk_div+1)*2/(div1_real*div2_real)	

		/* ESD or noise interference recovery For video mode LCM only.  // Send TE packet to LCM in a period of n frames and check the response. 
		params->dsi.lcm_int_te_monitor = FALSE; 
		params->dsi.lcm_int_te_period = 1; // Unit : frames 

		// Need longer FP for more opportunity to do int. TE monitor applicably. 
		if(params->dsi.lcm_int_te_monitor) 
		params->dsi.vertical_frontporch *= 2; 

		// Monitor external TE (or named VSYNC) from LCM once per 2 sec. (LCM VSYNC must be wired to baseband TE pin.) 
		params->dsi.lcm_ext_te_monitor = FALSE; 
		// Non-continuous clock 
		params->dsi.noncont_clock = TRUE; 
		params->dsi.noncont_clock_period = 2; // Unit : frames*/
}


static void lcm_init(void)
{

	SET_RESET_PIN(1);
	MDELAY(10);
	SET_RESET_PIN(0);
	MDELAY(50);
	SET_RESET_PIN(1);
	MDELAY(120);

	lcm_init_setting();
	//push_table(lcm_initialization_setting, sizeof(lcm_initialization_setting) / sizeof(struct LCM_setting_table), 1);



}


static void lcm_suspend(void)
{
	
/*			unsigned int data_array[16];
	SET_RESET_PIN(1);
	MDELAY(10);
	SET_RESET_PIN(0);
	MDELAY(50);
	SET_RESET_PIN(1);
	MDELAY(120);
	
	data_array[0] = 0x00100500; //0x10,entry sleep mode,1byte
	dsi_set_cmdq(&data_array, 1, 1);
	MDELAY(120);//150*/
	push_table(lcm_sleep_in_setting, sizeof(lcm_sleep_in_setting) / sizeof(struct LCM_setting_table), 1);

}


static void lcm_resume(void)
{
  push_table(lcm_sleep_out_setting, sizeof(lcm_sleep_out_setting) / sizeof(struct LCM_setting_table), 1);
}




static unsigned int lcm_compare_id(void)
{
	int array[16];
	char buffer[5];
	char id_high=0;
	char id_low=0;
	int id=0;

	SET_RESET_PIN(1);
	SET_RESET_PIN(0);
	MDELAY(10);
	SET_RESET_PIN(1);
	MDELAY(60);

	array[0]=0x00043902;
	array[1]=0x8983FFB9;// page enable
	dsi_set_cmdq(&array, 2, 1);
	MDELAY(10);
	
	array[0]=0x00083902;
	array[1]=0x009341BA;// page enable
	array[2]=0x1800A416;
	dsi_set_cmdq(&array, 3, 1);
	MDELAY(1);

	array[0] = 0x00023700;// return byte number
	dsi_set_cmdq(&array, 1, 1);
	MDELAY(10);

	read_reg_v2(0xF4, buffer, 2);
	id = buffer[0]; 

	return (LCM_ID_HX8389B == id)?1:0;
}

LCM_DRIVER hx8389b_hsd47_zes_qhd_lcm_drv =
{
	.name		= "hx8389b_hsd47_zes_qhd",
	.set_util_funcs = lcm_set_util_funcs,
	.get_params     = lcm_get_params,
	.init           = lcm_init,
	.suspend        = lcm_suspend,
	.resume         = lcm_resume,
#if (LCM_DSI_CMD_MODE)
	.update         = lcm_update,
#endif
	.compare_id     = lcm_compare_id,
};

