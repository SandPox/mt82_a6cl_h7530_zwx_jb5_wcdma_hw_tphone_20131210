/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#if defined(BUILD_LK)
#include <platform/mt_gpio.h>
#include <platform/mt_pmic.h>
#else
#include <mach/mt_gpio.h>
#include <mach/mt_pm_ldo.h>
#endif

#if !defined(BUILD_LK)
#include <linux/string.h>
#endif
#include "lcm_drv.h"

#if defined(BUILD_LK)
	#define LCM_DEBUG  printf
	#define LCM_FUNC_TRACE() printf("huyl [uboot] %s\n",__func__)
#else
	#define LCM_DEBUG  printk
	#define LCM_FUNC_TRACE() printk("huyl [kernel] %s\n",__func__)
#endif

// ---------------------------------------------------------------------------
//  Local Constants
// ---------------------------------------------------------------------------

#define FRAME_WIDTH  										(768)
#define FRAME_HEIGHT 										(1280)

#define REGFLAG_DELAY             							0XFE
#define REGFLAG_END_OF_TABLE      							0xFF   // END OF REGISTERS MARKER

#define LCM_DSI_CMD_MODE									0

// ---------------------------------------------------------------------------
//  Local Variables
// ---------------------------------------------------------------------------

static LCM_UTIL_FUNCS lcm_util = {0};

#define SET_RESET_PIN(v)    								(lcm_util.set_reset_pin((v)))

#define UDELAY(n) 											(lcm_util.udelay(n))
#define MDELAY(n) 											(lcm_util.mdelay(n))


// ---------------------------------------------------------------------------
//  Local Functions
// ---------------------------------------------------------------------------

#define dsi_set_cmdq_V2(cmd, count, ppara, force_update)	(lcm_util.dsi_set_cmdq_V2(cmd, count, ppara, force_update))
#define dsi_set_cmdq(pdata, queue_size, force_update)		(lcm_util.dsi_set_cmdq(pdata, queue_size, force_update))
#define wrtie_cmd(cmd)										(lcm_util.dsi_write_cmd(cmd))
#define write_regs(addr, pdata, byte_nums)					(lcm_util.dsi_write_regs(addr, pdata, byte_nums))
#define read_reg(cmd)											(lcm_util.dsi_read_reg(cmd))
#define read_reg_v2(cmd, buffer, buffer_size)   			(lcm_util.dsi_dcs_read_lcm_reg_v2(cmd, buffer, buffer_size))        

static unsigned int lcm_read(void);

static struct LCM_setting_table {
    unsigned cmd;
    unsigned char count;
    unsigned char para_list[64];
};


static struct LCM_setting_table lcm_initialization_setting[] = {
	
	
	//*************************************//
	// LG4593 Initial Code, Auto Boosting
	//************************************//
	
	{0xB0,5,{0x43,0x00,0x80,0x00,0x00}},
	
	//#Set Address mode
	{0x36,1,{0x0A}},
	
	{0xB1,3,{0x77,0x38,0x22}},			  
	
	//#Display Control
	{0xB3,2,{0x0A,0x9F}}, //#0x9F_1280,,0x7F_1024  Ž°¿ÚÉèÖÃ  0A 768  0c 720
	{0xB4,1,{0x00}},
	{0xB5,5,{0x65,0x14,0x14,0x00,0x20}},
	{0xB6,7,{0x00,0x14,0x0F,0x16,0x13,0x9F,0x9F}},
	
	//#Power Control
	{0xC0,3,{0x00,0x0A,0x10}},			  
	
	{0xC3,12,{0x12,0x66,0x03,0x20,0x00,0x66,0x85,0x33,0x11,0x38,0x38,0x00}},
	
	{0xC4,5,{0x22,0x24,0x11,0x11,0x6A}},  
	
	{0xC7,3,{0x10,0x00,0x14}},
	
	//# Gamma setting
	{0xD0,9,{0x00,0x16,0x66,0x23,0x14,0x16,0x50,0x31,0x03}}, 
	{0xD1,9,{0x00,0x16,0x66,0x23,0x14,0x06,0x50,0x31,0x03}},
	{0xD2,9,{0x00,0x16,0x66,0x23,0x14,0x16,0x50,0x31,0x03}},
	{0xD3,9,{0x00,0x16,0x66,0x23,0x14,0x06,0x50,0x31,0x03}},
	{0xD4,9,{0x00,0x16,0x66,0x23,0x14,0x16,0x50,0x31,0x03}},
	{0xD5,9,{0x00,0x16,0x66,0x23,0x14,0x06,0x50,0x31,0x03}},
	
	//# IEF
	{0xE0,1,{0x0F}},
	{0xC8,5,{0x22,0x22,0x22,0x33,0x94}},
	{0xE4,3,{0x02,0x85,0x82}},
	{0xE5,3,{0x01,0x83,0x81}},
	{0xE6,3,{0x04,0x08,0x04}},
	
	{0xF1,2,{0x10,0x00}},
//	{0x36,1,{0x00}},
 //   {0x3A,1,{0x77}},
	{0x11,1,{0x00}},//SLEEP OUT
	{REGFLAG_DELAY,200,{}},
																												
	{0x29,1,{0x00}},//Display ON 
	{REGFLAG_DELAY,20,{}}, 
	{REGFLAG_END_OF_TABLE, 0x00, {}} 

};


static struct LCM_setting_table lcm_set_window[] = {
	{0x2A,	4,	{0x00, 0x00, (FRAME_WIDTH>>8), (FRAME_WIDTH&0xFF)}},
	{0x2B,	4,	{0x00, 0x00, (FRAME_HEIGHT>>8), (FRAME_HEIGHT&0xFF)}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_sleep_out_setting[] = {
    // Sleep Out
	{0x11, 1, {0x00}},
    {REGFLAG_DELAY, 120, {}},

    // Display ON
	{0x29, 1, {0x00}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_deep_sleep_mode_in_setting[] = {
	// Display off sequence
	{0x28, 1, {0x00}},

    // Sleep Mode On
	{0x10, 1, {0x00}},

	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_backlight_level_setting[] = {
	{0x51, 1, {0xFF}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static void push_table(struct LCM_setting_table *table, unsigned int count, unsigned char force_update)
{
	unsigned int i;

    for(i = 0; i < count; i++) {
		
        unsigned cmd;
        cmd = table[i].cmd;
		
        switch (cmd) {
			
            case REGFLAG_DELAY :
                MDELAY(table[i].count);
                break;
				
            case REGFLAG_END_OF_TABLE :
                break;
				
            default:
				dsi_set_cmdq_V2(cmd, table[i].count, table[i].para_list, force_update);
       	}
    }
	
}


// ---------------------------------------------------------------------------
//  LCM Driver Implementations
// ---------------------------------------------------------------------------

static void lcm_set_util_funcs(const LCM_UTIL_FUNCS *util)
{
    memcpy(&lcm_util, util, sizeof(LCM_UTIL_FUNCS));
}


static void lcm_get_params(LCM_PARAMS *params)
{

		memset(params, 0, sizeof(LCM_PARAMS));
	
		params->type   = LCM_TYPE_DSI;

		params->width  = FRAME_WIDTH;
		params->height = FRAME_HEIGHT;

		// enable tearing-free
		params->dbi.te_mode 				= LCM_DBI_TE_MODE_DISABLED;
		//params->dbi.te_edge_polarity		= LCM_POLARITY_RISING;

        #if (LCM_DSI_CMD_MODE)
		params->dsi.mode   = CMD_MODE;
        #else
		params->dsi.mode   = BURST_VDO_MODE; //SYNC_PULSE_VDO_MODE;//BURST_VDO_MODE; 
        #endif
	
		// DSI
		/* Command mode setting */
		//1 Three lane or Four lane
		params->dsi.LANE_NUM				= LCM_FOUR_LANE;
		//The following defined the fomat for data coming from LCD engine.
		params->dsi.data_format.color_order = LCM_COLOR_ORDER_RGB;
		params->dsi.data_format.trans_seq   = LCM_DSI_TRANS_SEQ_MSB_FIRST;
		params->dsi.data_format.padding     = LCM_DSI_PADDING_ON_LSB;
		params->dsi.data_format.format      = LCM_DSI_FORMAT_RGB888;

		// Highly depends on LCD driver capability.
		// Not support in MT6573
//		params->dsi.packet_size=256;

		// Video mode setting		
		params->dsi.intermediat_buffer_num = 0;//because DSI/DPI HW design change, this parameters should be 0 when video mode in MT658X; or memory leakage

		params->dsi.PS=LCM_PACKED_PS_24BIT_RGB888;
		params->dsi.word_count=720*3;//768	

		
		params->dsi.vertical_sync_active				= 3;// 3    2		3
		params->dsi.vertical_backporch					= 8;// 20   1		12
		params->dsi.vertical_frontporch					= 8; // 1  12		2
		params->dsi.vertical_active_line				= FRAME_HEIGHT; 

		params->dsi.horizontal_sync_active				= 11;// 50  2		10
		params->dsi.horizontal_backporch				= 65 ;//			50
		params->dsi.horizontal_frontporch				= 65 ;//			50
		params->dsi.horizontal_active_pixel				= FRAME_WIDTH;


//params->dsi.HS_PRPR=3;
//params->dsi.CLK_HS_POST=22;
//params->dsi.DA_HS_EXIT=20;



	    //params->dsi.LPX=8; 

		// Bit rate calculation
		//1 Every lane speed
		params->dsi.pll_div1=0;		// div1=0,1,2,3;div1_real=1,2,4,4 ----0: 546Mbps  1:273Mbps		26
		params->dsi.pll_div2=1;		// div2=0,1,2,3;div1_real=1,2,4,4							1
		params->dsi.fbk_div =19;    // fref=26MHz, fvco=fref*(fbk_div+1)*2/(div1_real*div2_real)	

}


static void lcm_init(void)
{
	unsigned int data_array[16];

//#if defined(BUILD_LK)
//	upmu_set_rg_vgp6_vosel(5);
//	upmu_set_rg_vgp6_en(1);
//#else
//	hwPowerOn(MT65XX_POWER_LDO_VGP6, VOL_2800, "Lance_LCM");
//#endif
    SET_RESET_PIN(1);
    MDELAY(50);
	SET_RESET_PIN(0);
	MDELAY(100);
	SET_RESET_PIN(1);
	MDELAY(200);

#if 0
	/*For DI Issue, Use dsi_set_cmdq() instead of dsi_set_cmdq_v2()*/
	data_array[0] = 0x00032300;//MIPI config
	dsi_set_cmdq(&data_array, 1, 1);

	data_array[0] = 0x00200500;
	dsi_set_cmdq(&data_array, 1, 1);
	data_array[0] = 0x00361500;
	dsi_set_cmdq(&data_array, 1, 1);


	data_array[0] = 0x703A1500;
	dsi_set_cmdq(&data_array, 1, 1);
	
	data_array[0] = 0xFF511500;//
	dsi_set_cmdq(&data_array, 1, 1);
	data_array[0] = 0x24531500;//
	dsi_set_cmdq(&data_array, 1, 1);
	data_array[0] = 0x00551500;//
	dsi_set_cmdq(&data_array, 1, 1);
	data_array[0] = 0x555E1500;//
	dsi_set_cmdq(&data_array, 1, 1);

	data_array[0] = 0x00043902;
	data_array[1] = 0x0A4306B1;
	dsi_set_cmdq(&data_array, 2, 1);

	data_array[0] = 0x00033902;
	data_array[1] = 0x0000C8B2;
	dsi_set_cmdq(&data_array, 2, 1);
	data_array[0] = 0x02B31500;//
	dsi_set_cmdq(&data_array, 1, 1);
	data_array[0] = 0x04B31500;//
	dsi_set_cmdq(&data_array, 1, 1);
	data_array[0] = 0x00063902;
	data_array[1] = 0x101040B5;
	data_array[2] = 0x00000000;
	dsi_set_cmdq(&data_array, 3, 1);
	data_array[0] = 0x00073902;
	data_array[1] = 0x020F0BB6;
	data_array[2] = 0x00E81040;
	dsi_set_cmdq(&data_array, 3, 1);


	data_array[0] = 0x00063902;
	data_array[1] = 0x0A0A07C3;
	data_array[2] = 0x0000020A;
	dsi_set_cmdq(&data_array, 3, 1);
	data_array[0] = 0x00073902;
	data_array[1] = 0x182412C4;
	data_array[2] = 0x00490218;
	dsi_set_cmdq(&data_array, 3, 1);
	data_array[0] = 0x6DC51500;//
	dsi_set_cmdq(&data_array, 1, 1);

	data_array[0] = 0x00043902;
	data_array[1] = 0x036342C6;
	dsi_set_cmdq(&data_array, 2, 1);

	data_array[0] = 0x000A3902;
	data_array[1] = 0x650522D0;
	data_array[2] = 0x21040003;
	data_array[3] = 0x00000200;
	dsi_set_cmdq(&data_array, 4, 1);
	data_array[0] = 0x000A3902;
	data_array[1] = 0x650522D1;
	data_array[2] = 0x21040003;
	data_array[3] = 0x00000200;
	dsi_set_cmdq(&data_array, 4, 1);
	data_array[0] = 0x000A3902;
	data_array[1] = 0x650522D2;
	data_array[2] = 0x21040003;
	data_array[3] = 0x00000200;
	dsi_set_cmdq(&data_array, 4, 1);
	data_array[0] = 0x000A3902;
	data_array[1] = 0x650522D3;
	data_array[2] = 0x21040003;
	data_array[3] = 0x00000200;
	dsi_set_cmdq(&data_array, 4, 1);
	data_array[0] = 0x000A3902;
	data_array[1] = 0x650522D4;
	data_array[2] = 0x21040003;
	data_array[3] = 0x00000200;
	dsi_set_cmdq(&data_array, 4, 1);
	data_array[0] = 0x000A3902;
	data_array[1] = 0x650522D5;
	data_array[2] = 0x21040003;
	data_array[3] = 0x00000200;
	dsi_set_cmdq(&data_array, 4, 1);
#else
	push_table(lcm_initialization_setting, sizeof(lcm_initialization_setting) / sizeof(struct LCM_setting_table), 1);
#endif
}

static void lcm_suspend(void)
{
#if 0
	unsigned int data_array[16];
	data_array[0] = 0x00010500;//SW reset
	dsi_set_cmdq(&data_array, 1, 1);
	MDELAY(10);//Must > 5ms
#endif

#if 1
    SET_RESET_PIN(1);
    MDELAY(20);
	SET_RESET_PIN(0);
	MDELAY(10);
	SET_RESET_PIN(1);
	MDELAY(50);

#endif
	push_table(lcm_deep_sleep_mode_in_setting, sizeof(lcm_deep_sleep_mode_in_setting) / sizeof(struct LCM_setting_table), 1);

}


static void lcm_resume(void)
{
	lcm_init();

	//push_table(lcm_sleep_out_setting, sizeof(lcm_sleep_out_setting) / sizeof(struct LCM_setting_table), 1);

}


static void lcm_update(unsigned int x, unsigned int y,
                       unsigned int width, unsigned int height)
{
	unsigned int x0 = x;
	unsigned int y0 = y;
	unsigned int x1 = x0 + width - 1;
	unsigned int y1 = y0 + height - 1;

	unsigned char x0_MSB = ((x0>>8)&0xFF);
	unsigned char x0_LSB = (x0&0xFF);
	unsigned char x1_MSB = ((x1>>8)&0xFF);
	unsigned char x1_LSB = (x1&0xFF);
	unsigned char y0_MSB = ((y0>>8)&0xFF);
	unsigned char y0_LSB = (y0&0xFF);
	unsigned char y1_MSB = ((y1>>8)&0xFF);
	unsigned char y1_LSB = (y1&0xFF);

	unsigned int data_array[16];

	data_array[0]= 0x00053902;
	data_array[1]= (x1_MSB<<24)|(x0_LSB<<16)|(x0_MSB<<8)|0x2a;
	data_array[2]= (x1_LSB);
	data_array[3]= 0x00053902;
	data_array[4]= (y1_MSB<<24)|(y0_LSB<<16)|(y0_MSB<<8)|0x2b;
	data_array[5]= (y1_LSB);
	data_array[6]= 0x002c3909;

	dsi_set_cmdq(&data_array, 7, 0);

}


static void lcm_setbacklight(unsigned int level)
{
	unsigned int default_level = 145;
	unsigned int mapped_level = 0;

	//for LGE backlight IC mapping table
	if(level > 255) 
			level = 255;

	if(level >0) 
			mapped_level = default_level+(level)*(255-default_level)/(255);
	else
			mapped_level=0;

	// Refresh value of backlight level.
	lcm_backlight_level_setting[0].para_list[0] = mapped_level;

	push_table(lcm_backlight_level_setting, sizeof(lcm_backlight_level_setting) / sizeof(struct LCM_setting_table), 1);
}


static void lcm_setpwm(unsigned int divider)
{
	// TBD
}


static unsigned int lcm_getpwm(unsigned int divider)
{
	// ref freq = 15MHz, B0h setting 0x80, so 80.6% * freq is pwm_clk;
	// pwm_clk / 255 / 2(lcm_setpwm() 6th params) = pwm_duration = 23706
	unsigned int pwm_clk = 23706 / (1<<divider);	
	return pwm_clk;
}

static unsigned int lcm_read(void)
{
	unsigned int id = 0;
	unsigned char buffer[2]={0x88};
	unsigned int array[16];

	array[0] = 0x00013700;// read id return two byte,version and id
	dsi_set_cmdq(array, 1, 1);
//	id = read_reg(0xF4);
	read_reg_v2(0x52, buffer, 1);
	id = buffer[0]; //we only need ID

  //  return (LCM_ID == id)?1:0;
}

static unsigned int lcm_compare_id(void)
{
	unsigned int id0,id1,id=0;
	unsigned char buffer[5];
	unsigned int array[16];  

	SET_RESET_PIN(1);
	SET_RESET_PIN(0);
	MDELAY(1);
	
	SET_RESET_PIN(1);
	MDELAY(20); 

    return 1;
}

LCM_DRIVER lg4593_lg47_xyl_hd_lcm_drv = 
{
    .name			= "lg4593_lg47_xyl",
	.set_util_funcs = lcm_set_util_funcs,
	.get_params     = lcm_get_params,
	.init           = lcm_init,
	.suspend        = lcm_suspend,
	.resume         = lcm_resume,
//	.compare_id     = lcm_compare_id,
#if (LCM_DSI_CMD_MODE)
    .update         = lcm_update,
#endif
};


