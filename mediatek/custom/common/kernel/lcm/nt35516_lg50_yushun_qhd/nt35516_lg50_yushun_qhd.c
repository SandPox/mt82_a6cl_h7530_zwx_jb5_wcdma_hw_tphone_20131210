/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
#ifdef BUILD_LK
#else
    #include <linux/string.h>
    #include <linux/kernel.h>
    #if defined(BUILD_UBOOT)
        #include <asm/arch/mt_gpio.h>
    #else
        #include <mach/mt_gpio.h>
    #endif
#endif
#include "lcm_drv.h"

// ---------------------------------------------------------------------------
//  Local Constants
// ---------------------------------------------------------------------------

#define FRAME_WIDTH  										(540)
#define FRAME_HEIGHT 										(960)

#define REGFLAG_DELAY             							0xfE
#define REGFLAG_END_OF_TABLE      							0xfff   // END OF REGISTERS MARKER

#define GPIO_LCD_RST_EN      GPIO131
#define LCM_DSI_CMD_MODE									1
#define LCM_NT35516_ID       (0x5516)

// ---------------------------------------------------------------------------
//  Local Variables
// ---------------------------------------------------------------------------

static LCM_UTIL_FUNCS lcm_util = {0};

#define SET_RESET_PIN(v)    (lcm_util.set_reset_pin((v)))

#define UDELAY(n) (lcm_util.udelay(n))
#define MDELAY(n) (lcm_util.mdelay(n))


// ---------------------------------------------------------------------------
//  Local Functions
// ---------------------------------------------------------------------------

#define dsi_set_cmdq_V2(cmd, count, ppara, force_update)	lcm_util.dsi_set_cmdq_V2(cmd, count, ppara, force_update)
#define dsi_set_cmdq(pdata, queue_size, force_update)		lcm_util.dsi_set_cmdq(pdata, queue_size, force_update)
#define wrtie_cmd(cmd)									lcm_util.dsi_write_cmd(cmd)
#define write_regs(addr, pdata, byte_nums)				lcm_util.dsi_write_regs(addr, pdata, byte_nums)
#define read_reg											lcm_util.dsi_read_reg()
#define read_reg_v2(cmd, buffer, buffer_size)   				lcm_util.dsi_dcs_read_lcm_reg_v2(cmd, buffer, buffer_size)

static struct LCM_setting_table {
    unsigned cmd;
    unsigned char count;
    unsigned char para_list[64];
};
static struct LCM_setting_table lcm_compare_id_setting[] = {
    // Display off sequence
    {0xf0,  5,      {0x55,0xaa,0x52,0x08,0x01}},
    {REGFLAG_DELAY, 10, {}},
    {REGFLAG_END_OF_TABLE, 0x00, {}}
};

static struct LCM_setting_table lcm_initialization_setting[] = {

	/*
Note :

Data ID will depends on the following rule.

count of parameters > 1 => Data ID = 0x39
count of parameters = 1 => Data ID = 0x15
count of parameters = 0 => Data ID = 0x05

Structure Format :

{DCS command, count of parameters, {parameter list}}
{REGFLAG_DELAY, milliseconds of time, {}},

...

Setting ending by predefined flag

{REGFLAG_END_OF_TABLE, 0x00, {}}
*/

{ 0xFF,  4,{0xAA, 0x55, 0x25, 0x01}},
{ 0xF2,  35,{0x00, 0x00, 0x4A, 0x0A, 0xA8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x00, 0x00, 0x00, 0x0B, 0x00, 0x00, 
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
     0x00, 0x40, 0x01, 0x51, 0x00, 0x01, 0x00, 
   0x01}},        
{ 0xF3,  7,{0x02, 0x03, 0x07, 0x45, 0x88, 
     0xD1, 0x0D}},   

//-------------------------------------
// CMD2, Page 0
//-------------------------------------
{ 0xF0,  5,{0x55, 0xAA, 0x52, 0x08, 0x00}},   
{ 0xB1,  3,{0xEC, 0x00, 0x00}},//0xCC   
{ 0xB8,  4,{0x01, 0x02, 0x02, 0x02}},   
//{ 0xBB,  3,{0x63, 0x03, 0x63}},   
{ 0xBc,  3,{0x02, 0x02, 0x02}},  //00 

{ 0xC9, 6,{ 0x63, 0x06, 0x0D, 0x1A, 0x17, 0x00}},   

//-------------------------------------
// CMD2, Page 1
//-------------------------------------
{ 0xF0, 5,{ 0x55, 0xAA, 0x52, 0x08, 0x01}},   
{ 0xB0, 3,{ 0x05, 0x05, 0x05}},   
{ 0xB1, 3,{ 0x05, 0x05, 0x05}},   
{ 0xB2, 3,{ 0x01, 0x01, 0x01}},   
{ 0xB3, 3,{ 0x0D, 0x0D, 0x0D}},   
{ 0xB4, 3,{ 0x09, 0x09, 0x09}},   
{ 0xB6, 3,{ 0x44, 0x44, 0x44}},   
{ 0xB7, 3,{ 0x34, 0x34, 0x34}},   
{ 0xB8, 3,{ 0x10, 0x10, 0x10}},   
{ 0xB9, 3,{ 0x26, 0x26, 0x26}},   
{ 0xBA, 3,{ 0x34, 0x34, 0x34}},   
{ 0xBC, 3,{ 0x00, 0x90, 0x00}},   //B5
{ 0xBD, 3,{ 0x00, 0x8e, 0x00}},   //D2
{ 0xBE, 1,{ 0x4c}}, //4a  59  
{ 0xC0, 2,{ 0x04, 0x00}},   //00  08
//{ 0xCA, 2,{0x04, 0x00}},   
{ 0xCA, 1,{0x00}},


{ 0xD0,4,{ 0x06, 0x10, 0x0D, 0x0F}},   

{ 0xD1,16,{ 0x00, 0xA7, 0x00, 0xF0, 0x01, 0x18, 0x01, 0x30, 0x01, 0x40, 0x01, 0x5F, 0x01, 0x70, 0x01, 0x9E}},
{ 0xD5,16,{ 0x00, 0xA7, 0x01, 0x23, 0x01, 0x48, 0x01, 0x5F, 0x01, 0x6E, 0x01, 0x8B, 0x01, 0x9B, 0x01, 0xC5}},
{ 0xD9,16,{ 0x00, 0xA7, 0x00, 0xF0, 0x01, 0x18, 0x01, 0x30, 0x01, 0x40, 0x01, 0x5F, 0x01, 0x70, 0x01, 0x9E}},
{ 0xE0,16,{ 0x00, 0xA7, 0x01, 0x23, 0x01, 0x48, 0x01, 0x5F, 0x01, 0x6E, 0x01, 0x8B, 0x01, 0x9B, 0x01, 0xC5}},
{ 0xE4,16,{ 0x00, 0xA7, 0x00, 0xF0, 0x01, 0x18, 0x01, 0x30, 0x01, 0x40, 0x01, 0x5F, 0x01, 0x70, 0x01, 0x9E}},
{ 0xE8,16,{ 0x00, 0xA7, 0x01, 0x23, 0x01, 0x48, 0x01, 0x5F, 0x01, 0x6E, 0x01, 0x8B, 0x01, 0x9B, 0x01, 0xC5}},
                                                                                                      
{ 0xD2,16,{ 0x01, 0xBA, 0x01, 0xE7, 0x02, 0x0A, 0x02, 0x42, 0x02, 0x6F, 0x02, 0x70, 0x02, 0x9B, 0x02, 0xCC}},
{ 0xD6,16,{ 0x01, 0xE0, 0x02, 0x0A, 0x02, 0x2B, 0x02, 0x5F, 0x02, 0x89, 0x02, 0x8A, 0x02, 0xB2, 0x02, 0xE0}},
{ 0xDD,16,{ 0x01, 0xBA, 0x01, 0xE7, 0x02, 0x0A, 0x02, 0x42, 0x02, 0x6F, 0x02, 0x70, 0x02, 0x9B, 0x02, 0xCC}},
{ 0xE1,16,{ 0x01, 0xE0, 0x02, 0x0A, 0x02, 0x2B, 0x02, 0x5F, 0x02, 0x89, 0x02, 0x8A, 0x02, 0xB2, 0x02, 0xE0}},
{ 0xE5,16,{ 0x01, 0xBA, 0x01, 0xE7, 0x02, 0x0A, 0x02, 0x42, 0x02, 0x6F, 0x02, 0x70, 0x02, 0x9B, 0x02, 0xCC}},
{ 0xE9,16,{ 0x01, 0xE0, 0x02, 0x0A, 0x02, 0x2B, 0x02, 0x5F, 0x02, 0x89, 0x02, 0x8A, 0x02, 0xB2, 0x02, 0xE0}},
                                                                                                      
{ 0xD3,16,{ 0x02, 0xE8, 0x03, 0x19, 0x03, 0x36, 0x03, 0x5E, 0x03, 0x78, 0x03, 0x97, 0x03, 0xA6, 0x03, 0xB3}},
{ 0xD7,16,{ 0x02, 0xFA, 0x03, 0x28, 0x03, 0x43, 0x03, 0x68, 0x03, 0x81, 0x03, 0x9E, 0x03, 0xAC, 0x03, 0xB8}},
{ 0xDE,16,{ 0x02, 0xE8, 0x03, 0x19, 0x03, 0x36, 0x03, 0x5E, 0x03, 0x78, 0x03, 0x97, 0x03, 0xA6, 0x03, 0xB3}},
{ 0xE2,16,{ 0x02, 0xFA, 0x03, 0x28, 0x03, 0x43, 0x03, 0x68, 0x03, 0x81, 0x03, 0x9E, 0x03, 0xAC, 0x03, 0xB8}},
{ 0xE6,16,{ 0x02, 0xE8, 0x03, 0x19, 0x03, 0x36, 0x03, 0x5E, 0x03, 0x78, 0x03, 0x97, 0x03, 0xA6, 0x03, 0xB3}},
{ 0xEA,16,{0x02, 0xFA, 0x03, 0x28, 0x03, 0x43, 0x03, 0x68, 0x03, 0x81, 0x03, 0x9E, 0x03, 0xAC, 0x03, 0xB8}},
{0xD4,4,{0x03, 0xD0, 0x03, 0xCF}}, //P4  V255	
{0xD8,4,{0x03, 0xD0, 0x03, 0xCF}}, //P4  V255	
{0xDF,4,{0x03, 0xD0, 0x03, 0xCF}}, //P4  V255	
{0xE3,4,{0x03, 0xD0, 0x03, 0xCF}}, //P4  V255	
{0xE7,4,{0x03, 0xD0, 0x03, 0xCF}}, //P4  V255	
{0xEB,4,{0x03, 0xD0, 0x03, 0xCF}}, //P4  V255

{0x35,1,{0x00}},
{0x4c,  1,      {0x11}},   //vivid color  by zxy
{0x11,1,{0x00}}, //Tearing Effect On        Sleep Out
{REGFLAG_DELAY, 120, {}},
{0x29,1,{0x00}}, //Display On   Display On.
{REGFLAG_DELAY, 40, {}},
{REGFLAG_END_OF_TABLE, 0x00, {}}

};


static struct LCM_setting_table lcm_set_window[] = {
	{0x2A,	4,	{0x00, 0x00, (FRAME_WIDTH>>8), (FRAME_WIDTH&0xFF)}},
	{0x2B,	4,	{0x00, 0x00, (FRAME_HEIGHT>>8), (FRAME_HEIGHT&0xFF)}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_sleep_out_setting[] = {


	    // Sleep Out
	{0x11, 0, {0x00}},
    {REGFLAG_DELAY, 200, {}},

    // Display ON
	{0x29, 0, {0x00}},
	{REGFLAG_DELAY, 100, {}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_deep_sleep_mode_in_setting[] = {

	// Display off sequence
	{0x28, 0, {0x00}},
	{REGFLAG_DELAY, 100, {}},

    // Sleep Mode On
	{0x10, 0, {0x00}},
	{REGFLAG_DELAY, 200, {}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};

static void push_table(struct LCM_setting_table *table, unsigned int count, unsigned char force_update)
{
	unsigned int i;

    for(i = 0; i < count; i++) {

        unsigned cmd;
        cmd = table[i].cmd;

        switch (cmd) {

            case REGFLAG_DELAY :
                MDELAY(table[i].count);
                break;

            case REGFLAG_END_OF_TABLE :
                break;

            default:
				dsi_set_cmdq_V2(cmd, table[i].count, table[i].para_list, force_update);
       	}

    }

}


// ---------------------------------------------------------------------------
//  LCM Driver Implementations
// ---------------------------------------------------------------------------

static void lcm_set_util_funcs(const LCM_UTIL_FUNCS *util)
{
	memcpy(&lcm_util, util, sizeof(LCM_UTIL_FUNCS));
}

static void lcm_get_params(LCM_PARAMS *params)
{
	memset(params, 0, sizeof(LCM_PARAMS));
	params->type   = LCM_TYPE_DSI;
	params->width  = FRAME_WIDTH;
	params->height = FRAME_HEIGHT;

	// enable tearing-free
	params->dbi.te_mode 				= LCM_DBI_TE_MODE_DISABLED;
	//params->dbi.te_mode 				= LCM_DBI_TE_MODE_VSYNC_ONLY;
	params->dbi.te_edge_polarity		= LCM_POLARITY_RISING;

#if (LCM_DSI_CMD_MODE)
	params->dsi.mode   = CMD_MODE;
#else
	params->dsi.mode   = SYNC_PULSE_VDO_MODE;
#endif

			// DSI
		/* Command mode setting */
		params->dsi.LANE_NUM				= LCM_TWO_LANE;
		//The following defined the fomat for data coming from LCD engine.
		params->dsi.data_format.color_order = LCM_COLOR_ORDER_RGB;
		params->dsi.data_format.trans_seq   = LCM_DSI_TRANS_SEQ_MSB_FIRST;
		params->dsi.data_format.padding     = LCM_DSI_PADDING_ON_LSB;
		params->dsi.data_format.format      = LCM_DSI_FORMAT_RGB888;
		//params->dsi.compatibility_for_nvk=1;

		params->dsi.intermediat_buffer_num = 2;//because DSI/DPI HW design change, this parameters should be 0 when video mode in MT658X; or memory leakage


		params->dsi.PS=LCM_PACKED_PS_24BIT_RGB888;
		params->dsi.word_count=720*3;


                params->dsi.vertical_sync_active                                = 2;  //---3
                params->dsi.vertical_backporch                                  = 10; //---14
                params->dsi.vertical_frontporch                                 = 44;  //----8
                params->dsi.vertical_active_line                                = FRAME_HEIGHT;

                params->dsi.horizontal_sync_active                              = 2;  //----2
                params->dsi.horizontal_backporch                                = 60; //----28
                params->dsi.horizontal_frontporch                               = 20; //----50
                params->dsi.horizontal_active_pixel                             = FRAME_WIDTH;



	// Bit rate calculation
	//1 Every lane speed
	params->dsi.pll_div1=0;		// div1=0,1,2,3;div1_real=1,2,4,4 ----0: 546Mbps  1:273Mbps
	params->dsi.pll_div2=1;		// div2=0,1,2,3;div1_real=1,2,4,4
	params->dsi.fbk_div =18;//18;    // fref=26MHz, fvco=fref*(fbk_div+1)*2/(div1_real*div2_real)

		/* ESD or noise interference recovery For video mode LCM only. */ // Send TE packet to LCM in a period of n frames and check the response. 
		params->dsi.lcm_int_te_monitor = 0; 
		params->dsi.lcm_int_te_period = 1; // Unit : frames 
 
		// Need longer FP for more opportunity to do int. TE monitor applicably. 
		if(params->dsi.lcm_int_te_monitor) 
			params->dsi.vertical_frontporch *= 2; 
 
		// Monitor external TE (or named VSYNC) from LCM once per 2 sec. (LCM VSYNC must be wired to baseband TE pin.) 
		params->dsi.lcm_ext_te_monitor = 0; 
		// Non-continuous clock 
		params->dsi.noncont_clock = 1; 
		params->dsi.noncont_clock_period = 2; // Unit : frames
}


static void lcm_init(void)
{
	unsigned int data_array[16];
	SET_RESET_PIN(1);
	MDELAY(20);
	SET_RESET_PIN(0);
    MDELAY(50);
    SET_RESET_PIN(1);
    MDELAY(100);
	push_table(lcm_initialization_setting, sizeof(lcm_initialization_setting) / sizeof(struct LCM_setting_table), 1);

}

static void lcm_update(unsigned int x, unsigned int y,
                       unsigned int width, unsigned int height)
{
	unsigned int x0 = x;
	unsigned int y0 = y;
	unsigned int x1 = x0 + width - 1;
	unsigned int y1 = y0 + height - 1;

	unsigned char x0_MSB = ((x0>>8)&0xFF);
	unsigned char x0_LSB = (x0&0xFF);
	unsigned char x1_MSB = ((x1>>8)&0xFF);
	unsigned char x1_LSB = (x1&0xFF);
	unsigned char y0_MSB = ((y0>>8)&0xFF);
	unsigned char y0_LSB = (y0&0xFF);
	unsigned char y1_MSB = ((y1>>8)&0xFF);
	unsigned char y1_LSB = (y1&0xFF);

	unsigned int data_array[16];

	data_array[0]= 0x00053902;
	data_array[1]= (x1_MSB<<24)|(x0_LSB<<16)|(x0_MSB<<8)|0x2a;
	data_array[2]= (x1_LSB);
	dsi_set_cmdq(&data_array, 3, 1);
	//MDELAY(1);

	data_array[0]= 0x00053902;
	data_array[1]= (y1_MSB<<24)|(y0_LSB<<16)|(y0_MSB<<8)|0x2b;
	data_array[2]= (y1_LSB);
	dsi_set_cmdq(&data_array, 3, 1);
	//MDELAY(1);

//	data_array[0]= 0x00290508;
//	dsi_set_cmdq(&data_array, 1, 1);
	//MDELAY(1);

	data_array[0]= 0x002c3909;
	dsi_set_cmdq(&data_array, 1, 0);
	//MDELAY(1);

}

static void lcm_suspend(void)
{
	//push_table(lcm_deep_sleep_mode_in_setting, sizeof(lcm_deep_sleep_mode_in_setting) / sizeof(struct LCM_setting_table), 1);
    unsigned int data_array[16];

    data_array[0]=0x00280500; // Display Off
    dsi_set_cmdq(&data_array, 1, 1);
    MDELAY(60);


 data_array[0]=0x00290500; // Display Off
    dsi_set_cmdq(&data_array, 1, 1);
    MDELAY(60);


data_array[0]=0x00280500; // Display Off
    dsi_set_cmdq(&data_array, 1, 1);
    MDELAY(60);


 data_array[0]=0x00290500; // Display Off
    dsi_set_cmdq(&data_array, 1, 1);
    MDELAY(60);
data_array[0]=0x00280500; // Display Off
    dsi_set_cmdq(&data_array, 1, 1);
    MDELAY(60);


 data_array[0]=0x00290500; // Display Off
    dsi_set_cmdq(&data_array, 1, 1);
    MDELAY(60);
data_array[0]=0x00280500; // Display Off
    dsi_set_cmdq(&data_array, 1, 1);
    MDELAY(60);


    data_array[0] = 0x00100500; // Sleep In
    dsi_set_cmdq(&data_array, 1, 1);
    MDELAY(100);
	//SET_RESET_PIN(0);

}


static void lcm_resume(void)
{

    lcm_init();
#if 0
	push_table(lcm_sleep_out_setting, sizeof(lcm_sleep_out_setting) / sizeof(struct LCM_setting_table), 1);
#endif
}


static unsigned int lcm_compare_id(void)
{
	unsigned int id = 0;
	unsigned char buffer[5];
	unsigned int array[5];
	//Do reset here
	SET_RESET_PIN(1);
	SET_RESET_PIN(0);
	MDELAY(10);

	SET_RESET_PIN(1);
	MDELAY(100);

	push_table(lcm_compare_id_setting, sizeof(lcm_compare_id_setting) / sizeof(struct LCM_setting_table), 1);

	array[0] = 0x00033700;// read id return two byte,version and id
	dsi_set_cmdq(array, 1, 1);
	read_reg_v2(0xc5, buffer, 3);
	id = ((buffer[0] << 8) | buffer[1]); //we only need ID
#if defined(BUILD_LK)
	printf("%s, id1 = 0x%x,id2 = 0x%x,id3 = 0x%x\n", __func__, buffer[0],buffer[1],buffer[2]);
#endif
	//printk("%s, id1 = 0x%x,id2 = 0x%x,id3 = 0x%x\n", __func__, buffer[0],buffer[1],buffer[2]);
	return (LCM_NT35516_ID == id)?1:0;

}


#if (0)
static int __esd_check_count = 0;
static unsigned int lcm_esd_check(void)
{
    unsigned char buffer[5];
    unsigned int id = 0;
#ifndef BUILD_LK
        read_reg_v2(0xc5, buffer, 3);
        id = ((buffer[0] << 8) | buffer[1]);
        if(LCM_NT35516_ID == id)
        {
            __esd_check_count = 0;
            return 0;
        }
        else
        {
        #if 1
        __esd_check_count ++;
        if(__esd_check_count > 6){
            __esd_check_count = 0;
            return 1;
        }
        __esd_check_count = 0;
        return 0;
        #else
            return 1;
        #endif
        }
#endif
}

static unsigned int lcm_esd_recover(void)
{
    unsigned char para = 0;

    SET_RESET_PIN(1);
    SET_RESET_PIN(0);
    MDELAY(40);
    SET_RESET_PIN(1);
    MDELAY(80);
	  push_table(lcm_initialization_setting, sizeof(lcm_initialization_setting) / sizeof(struct LCM_setting_table), 1);
#if 0
    MDELAY(10);
	  push_table(lcm_sleep_out_setting, sizeof(lcm_sleep_out_setting) / sizeof(struct LCM_setting_table), 1);
    MDELAY(10);
    dsi_set_cmdq_V2(0x35, 1, &para, 1);     ///enable TE
    MDELAY(10);
#endif

    return 1;
}

#endif
LCM_DRIVER nt35516_lg50_yushun_qhd_drv=
{
	.name		= "nt35516_lg50_yushun_qhd",
	.set_util_funcs = lcm_set_util_funcs,
	.get_params     = lcm_get_params,
	.init           = lcm_init,
	.suspend        = lcm_suspend,
	.resume         = lcm_resume,
	.compare_id    = lcm_compare_id,
#if (LCM_DSI_CMD_MODE)
	.update         = lcm_update,
#endif
#if (0)
     .esd_check = lcm_esd_check,
     .esd_recover = lcm_esd_recover,
#endif
};
