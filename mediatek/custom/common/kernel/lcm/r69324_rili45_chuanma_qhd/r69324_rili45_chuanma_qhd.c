/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/


#ifdef BUILD_LK
#else
#include <linux/string.h>
#if defined(BUILD_UBOOT)
#include <asm/arch/mt_gpio.h>
#else
#include <mach/mt_gpio.h>
#endif
#endif
#include "lcm_drv.h"

#if defined(BUILD_LK)
#else

#include <linux/proc_fs.h>   //proc file use 
#endif


// ---------------------------------------------------------------------------
//  Local Constants
// ---------------------------------------------------------------------------

#define FRAME_WIDTH  										(540)
#define FRAME_HEIGHT 										(960)

#define REGFLAG_DELAY             							0XFE
#define REGFLAG_END_OF_TABLE      							0xFFF   // END OF REGISTERS MARKER

#define LCM_ID       0x9324
#define LCM_DSI_CMD_MODE									0

#ifndef TRUE
    #define   TRUE     1
#endif
 
#ifndef FALSE
    #define   FALSE    0
#endif

// ---------------------------------------------------------------------------
//  Local Variables
// ---------------------------------------------------------------------------

static LCM_UTIL_FUNCS lcm_util = {0};

static int lcd_id_test  = 0;  


#define SET_RESET_PIN(v)    								(lcm_util.set_reset_pin((v)))

#define UDELAY(n) 											(lcm_util.udelay(n))
#define MDELAY(n) 											(lcm_util.mdelay(n))

// ---------------------------------------------------------------------------
//  Local Functions
// ---------------------------------------------------------------------------

#define dsi_set_cmdq_V2(cmd, count, ppara, force_update)	lcm_util.dsi_set_cmdq_V2(cmd, count, ppara, force_update)
#define dsi_set_cmdq(pdata, queue_size, force_update)		lcm_util.dsi_set_cmdq(pdata, queue_size, force_update)
#define wrtie_cmd(cmd)										lcm_util.dsi_write_cmd(cmd)
#define write_regs(addr, pdata, byte_nums)					lcm_util.dsi_write_regs(addr, pdata, byte_nums)
#define read_reg											lcm_util.dsi_read_reg()
#define read_reg_v2(cmd, buffer, buffer_size)				lcm_util.dsi_dcs_read_lcm_reg_v2(cmd, buffer, buffer_size)

 struct LCM_setting_table {
    unsigned cmd;
    unsigned char count;
    unsigned char para_list[64];
};


static struct LCM_setting_table lcm_initialization_setting[] = {
	
  /*  {0xB0,	1,	{0x04}},		// Manufacture Command Accesse Protect 
	
	{0xB3,	1,	{0x00}},		// Number of Source Outputs and Pixel Format setting
	
	{0xB6,	2,	{0x32,0x82}},	// DSI Control 
	
	{0xC0,	1,	{0x00}},		// Panel Driving Setting 1
	
	{0xC1,	20,	{0x00,0xc0,0x03,0x5A,0x5A,0x04,0x0B,0x02,0x08,0x45,0x21,0x21,0x03,0x03,0x18,0x00,0x3e,0x3e,0x08,0x08}},			// Panel Driving Setting 2

	{0xC2,	6,	{0x48,0x01,0x00,0x00,0x61,0x02}},		// Display V-Timing Setting

	{0xC8,	22,	{0x00,0x05,0x03,0x09,0x69,0x06,0x08,0x03,0x02,0x04,0x04,0x00,0x05,0x03,0x09,0x69,0x06,0x08,0x03,0x02,0x04,0x04}},	// Gamma Set A

	{0xC9,	22,	{0x00,0x05,0x03,0x09,0x69,0x06,0x08,0x03,0x02,0x04,0x04,0x00,0x05,0x03,0x09,0x69,0x06,0x08,0x03,0x02,0x04,0x04}},	// Gamma Set B

	{0xCA,	22,	{0x00,0x05,0x03,0x09,0x69,0x06,0x08,0x03,0x02,0x04,0x04,0x00,0x05,0x03,0x09,0x69,0x06,0x08,0x03,0x02,0x04,0x04}},	// Gamma Set C
	
	{0xD0,	7,	{0x33,0x00,0x9C,0xBC,0xB2,0x10,0x08}},		// Power Setting(Charge Pump Setting)

	{0xD1,	7,	{0x18,0x0C,0x12,0x05,0x46,0x00,0x24}},		// Test Mode 6(Swiching Regulator Setting)

	{0xD2,	2,	{0xC1,0x00}},		// Test Mode 7(Power Setting for Internal Power Supply Circuit)

	{0xD3,	2,	{0x23,0x03}},		// Power Setting for Internal Power Supply Circuit

	{0xD5,	2,	{0x0A,0x0A}},		// VREG Setting

	{0xD6,	1,	{0x81}},		// Test Mode 8

	{0xDA,	1,	{0x01}},	// Manual Sequecer Control 1

	{0xDB,	1,	{0x01}},		// Manual Sequecer Control 2

	{0xDE,	2,	{0x01,0x53}},			// VCOMDC Setting

	{0xE1,	4,	{0x01,0x01,0x01,0x01}},	// Set DDB
		
	{0xB0,	1,	{0x03}},*/
	{0x36,	1,	{0x48}},
	
	//{0x35,	1,	{0x00}},	

  {0x11,1,{0x00}},//SLEEP OUT
	{REGFLAG_DELAY,120,{}},
	                                 				                                                                                
	{0x29,1,{0x00}},//Display ON 
	{REGFLAG_DELAY,20,{}},

	{REGFLAG_END_OF_TABLE, 0x00, {}}

};



static struct LCM_setting_table lcm_set_window[] = {
	{0x2A,	4,	{0x00, 0x00, (FRAME_WIDTH>>8), (FRAME_WIDTH&0xFF)}},
	{0x2B,	4,	{0x00, 0x00, (FRAME_HEIGHT>>8), (FRAME_HEIGHT&0xFF)}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_sleep_out_setting[] = {
    // Sleep Out
	{0x11, 1, {0x00}},
    {REGFLAG_DELAY, 200, {}},

    // Display ON
	{0x29, 1, {0x00}},
	{REGFLAG_DELAY, 50, {}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_deep_sleep_mode_in_setting[] = {
	// Display off sequence
	{0x28, 1, {0x00}},
	{REGFLAG_DELAY, 50, {}},

    // Sleep Mode On
	{0x10, 1, {0x00}},
	{REGFLAG_DELAY, 200, {}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_backlight_level_setting[] = {
	{0x51, 1, {0xFF}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};
 

static void push_table(struct LCM_setting_table *table, unsigned int count, unsigned char force_update)
{
	unsigned int i;

    for(i = 0; i < count; i++) {
		
        unsigned cmd;
        cmd = table[i].cmd;
		
        switch (cmd) {
			
            case REGFLAG_DELAY :
                MDELAY(table[i].count);
                break;
				
            case REGFLAG_END_OF_TABLE :
                break;
				
            default:
				dsi_set_cmdq_V2(cmd, table[i].count, table[i].para_list, force_update);
       	}
    }
	
}


// ---------------------------------------------------------------------------
//  LCM Driver Implementations
// ---------------------------------------------------------------------------

static void lcm_set_util_funcs(const LCM_UTIL_FUNCS *util)
{
    memcpy(&lcm_util, util, sizeof(LCM_UTIL_FUNCS));
}
static unsigned int lcm_compare_id(void)
{
	int array[4];
	char buffer[5];
	char id_high=0;
	char id_low=0;
	int id=0;

	SET_RESET_PIN(1);
	SET_RESET_PIN(0);
	MDELAY(10);
	SET_RESET_PIN(1);
	MDELAY(200);
	array[0]=0x00022902;
	array[1]=0x000004b0;//cmd mode
	dsi_set_cmdq(array, 2, 1);
	MDELAY(10);
	array[0] = 0x00053700;
	dsi_set_cmdq(array, 1, 1);
	read_reg_v2(0xbf, buffer, 5);

	id_high = buffer[2];
	id_low = buffer[3];
	id = (id_high<<8) | id_low;

	#if defined(BUILD_LK)
		printf("R69324 uboot %s \n", __func__);
		printf("%s id = 0x%08x \n", __func__, id);
	#else
		printk("R69324 kernel %s \n", __func__);
		printk("%s id = 0x%08x \n", __func__, id);
	#endif
   // if(LCM_ID_OTM8018B == id){
	//	while(1){}
	//}
	lcd_id_test = id;
	return (LCM_ID== id)?1:0;
}


static void lcm_get_params(LCM_PARAMS *params)
	{
		 #if 1
	   memset(params, 0, sizeof(LCM_PARAMS));
		
		params->type   = LCM_TYPE_DSI;
		
		params->width  = FRAME_WIDTH;
		params->height = FRAME_HEIGHT;
		
		// enable tearing-free
		params->dbi.te_mode 			= LCM_DBI_TE_MODE_DISABLED;
		params->dbi.te_edge_polarity		= LCM_POLARITY_RISING;
		
		params->dsi.mode   = SYNC_EVENT_VDO_MODE;
		
		// DSI
		/* Command mode setting */
		params->dsi.LANE_NUM				= LCM_TWO_LANE;
		
		//The following defined the fomat for data coming from LCD engine.
		params->dsi.data_format.color_order = LCM_COLOR_ORDER_RGB;
		params->dsi.data_format.trans_seq	= LCM_DSI_TRANS_SEQ_MSB_FIRST;
		params->dsi.data_format.padding 	= LCM_DSI_PADDING_ON_LSB;
		params->dsi.data_format.format	  = LCM_DSI_FORMAT_RGB888;
		
		// Video mode setting		
		params->dsi.intermediat_buffer_num = 2;
		
		params->dsi.PS=LCM_PACKED_PS_24BIT_RGB888;
		
		params->dsi.word_count=540*3;	//DSI CMD mode need set these two bellow params, different to 6577
		//params->dsi.vertical_active_line=854;
	
		params->dsi.vertical_sync_active				= 4;
		params->dsi.vertical_backporch					= 6;
		params->dsi.vertical_frontporch 				= 16;//16
		params->dsi.vertical_active_line				= FRAME_HEIGHT;
		
		params->dsi.horizontal_sync_active				= 4;
		params->dsi.horizontal_backporch				= 40;//37
		params->dsi.horizontal_frontporch				= 40;//37
		//params->dsi.horizontal_blanking_pixel 			= 60;
		params->dsi.horizontal_active_pixel 			= FRAME_WIDTH;
		
		// Bit rate calculation
#if 0//def CONFIG_MT6589_FPGA
			params->dsi.pll_div1=2; 	// div1=0,1,2,3;div1_real=1,2,4,4
			params->dsi.pll_div2=2; 	// div2=0,1,2,3;div1_real=1,2,4,4
			params->dsi.fbk_sel=0;		// fbk_sel=0,1,2,3;fbk_sel_real=1,2,4,4
			params->dsi.fbk_div =8; 	// fref=26MHz, fvco=fref*(fbk_div+1)*2/(div1_real*div2_real)
#else
			params->dsi.pll_div1=0; 	// div1=0,1,2,3;div1_real=1,2,4,4
			params->dsi.pll_div2=1; 	// div2=0,1,2,3;div2_real=1,2,4,4
			//params->dsi.fbk_sel=1;		 // fbk_sel=0,1,2,3;fbk_sel_real=1,2,4,4
			params->dsi.fbk_div =15;		// fref=26MHz, fvco=fref*(fbk_div+1)*2/(div1_real*div2_real)	 32
		//		params->dsi.pll_select=1;	//0: MIPI_PLL; 1: LVDS_PLL
   		//	params->dsi.PLL_CLOCK = LCM_DSI_6589_PLL_CLOCK_429;//this value must be in MTK suggested table
			
#endif
		
#endif
#if 0	
				memset(params, 0, sizeof(LCM_PARAMS));
		
				params->type   = LCM_TYPE_DSI;
		
				params->width  = FRAME_WIDTH;
				params->height = FRAME_HEIGHT;
		
				// enable tearing-free
			//	params->dbi.te_mode 				= LCM_DBI_TE_MODE_VSYNC_ONLY;
			//	params->dbi.te_edge_polarity		= LCM_POLARITY_RISING;
		
#if (LCM_DSI_CMD_MODE)
				params->dsi.mode   = CMD_MODE;
#else
				params->dsi.mode   = SYNC_PULSE_VDO_MODE;
#endif
			
				// DSI
				/* Command mode setting */
				params->dsi.LANE_NUM				= LCM_TWO_LANE;
				//The following defined the fomat for data coming from LCD engine.
				params->dsi.data_format.color_order = LCM_COLOR_ORDER_RGB;
				params->dsi.data_format.trans_seq	= LCM_DSI_TRANS_SEQ_MSB_FIRST;
				params->dsi.data_format.padding 	= LCM_DSI_PADDING_ON_LSB;
				params->dsi.data_format.format		= LCM_DSI_FORMAT_RGB888;
		
				// Highly depends on LCD driver capability.
				// Not support in MT6573
				params->dsi.packet_size=256;
		
				// Video mode setting		
				params->dsi.intermediat_buffer_num = 2;
		
				params->dsi.PS=LCM_PACKED_PS_24BIT_RGB888;
				//params->dsi.word_count=480*3; 
		
				params->dsi.vertical_sync_active				= 4;
				params->dsi.vertical_backporch					= 8;
				params->dsi.vertical_frontporch 				= 8;
				params->dsi.vertical_active_line				= FRAME_HEIGHT; 
		
				params->dsi.horizontal_sync_active				= 6;//6
				params->dsi.horizontal_backporch				= 35;//37
				params->dsi.horizontal_frontporch				= 35;//37
				params->dsi.horizontal_active_pixel 			= FRAME_WIDTH;
		
				// Bit rate calculation
				//params->dsi.pll_div1=29;		// fref=26MHz, fvco=fref*(div1+1)	(div1=0~63, fvco=500MHZ~1GHz)
				//params->dsi.pll_div2=1;		// div2=0~15: fout=fvo/(2*div2)
				params->dsi.pll_div1=1; 	// div1=0,1,2,3;div1_real=1,2,4,4 ----0: 546Mbps  1:273Mbps
				params->dsi.pll_div2=1; 	// div2=0,1,2,3;div1_real=1,2,4,4	
				params->dsi.fbk_div =20;	// fref=26MHz, fvco=fref*(fbk_div+1)*2/(div1_real*div2_real)	
#endif
		}





static void lcm_init(void)
{
    SET_RESET_PIN(1);
	MDELAY(10);
    SET_RESET_PIN(0);
    MDELAY(50);
    SET_RESET_PIN(1);
    MDELAY(120);
    //lcm_compare_id();
	push_table(lcm_initialization_setting, sizeof(lcm_initialization_setting) / sizeof(struct LCM_setting_table), 1);
}


static void lcm_suspend(void)
{
	SET_RESET_PIN(1);
	SET_RESET_PIN(0);
	MDELAY(10);
	SET_RESET_PIN(1);
	MDELAY(20);
	//lcm_init();
	push_table(lcm_deep_sleep_mode_in_setting, sizeof(lcm_deep_sleep_mode_in_setting) / sizeof(struct LCM_setting_table), 1);
}



static void lcm_resume(void)
{
unsigned int data_array[16];
   SET_RESET_PIN(1);
	SET_RESET_PIN(0);
	MDELAY(10);
	SET_RESET_PIN(1);
	MDELAY(20);


	data_array[0] = 0x00110500; // Sleep Out
	dsi_set_cmdq(data_array, 1, 1);
	MDELAY(100);
	data_array[0] = 0x00290500; // Display On
	dsi_set_cmdq(data_array, 1, 1);
	MDELAY(10);
	//lcm_compare_id();
	lcm_init();
	
	
push_table(lcm_sleep_out_setting, sizeof(lcm_sleep_out_setting) / sizeof(struct LCM_setting_table), 1);
}


static void lcm_update(unsigned int x, unsigned int y,
                       unsigned int width, unsigned int height)
{
	unsigned int x0 = x;
	unsigned int y0 = y;
	unsigned int x1 = x0 + width - 1;
	unsigned int y1 = y0 + height - 1;

	unsigned char x0_MSB = ((x0>>8)&0xFF);
	unsigned char x0_LSB = (x0&0xFF);
	unsigned char x1_MSB = ((x1>>8)&0xFF);
	unsigned char x1_LSB = (x1&0xFF);
	unsigned char y0_MSB = ((y0>>8)&0xFF);
	unsigned char y0_LSB = (y0&0xFF);
	unsigned char y1_MSB = ((y1>>8)&0xFF);
	unsigned char y1_LSB = (y1&0xFF);

	unsigned int data_array[16];

	data_array[0]= 0x00053902;
	data_array[1]= (x1_MSB<<24)|(x0_LSB<<16)|(x0_MSB<<8)|0x2a;
	data_array[2]= (x1_LSB);
	data_array[3]= 0x00053902;
	data_array[4]= (y1_MSB<<24)|(y0_LSB<<16)|(y0_MSB<<8)|0x2b;
	data_array[5]= (y1_LSB);
	data_array[6]= 0x002c3909;

	dsi_set_cmdq(data_array, 7, 0);

}


static void lcm_setbacklight(unsigned int level)
{
	unsigned int default_level = 145;
	unsigned int mapped_level = 0;

	//for LGE backlight IC mapping table
	if(level > 255) 
			level = 255;

	if(level >0) 
			mapped_level = default_level+(level)*(255-default_level)/(255);
	else
			mapped_level=0;

	// Refresh value of backlight level.
	lcm_backlight_level_setting[0].para_list[0] = mapped_level;

	push_table(lcm_backlight_level_setting, sizeof(lcm_backlight_level_setting) / sizeof(struct LCM_setting_table), 1);
}



LCM_DRIVER r69324_rili45_chuanma_qhd_lcm_drv = 
{
    .name			= "r69324_rili45_chuanma_qhd",
	.set_util_funcs = lcm_set_util_funcs,
	.get_params     = lcm_get_params,
	.init           = lcm_init,
	.suspend        = lcm_suspend,
	.resume         = lcm_resume,
	.compare_id    = lcm_compare_id,	
#if (LCM_DSI_CMD_MODE)
	.set_backlight	= lcm_setbacklight,
    .update         = lcm_update,
#endif
};
