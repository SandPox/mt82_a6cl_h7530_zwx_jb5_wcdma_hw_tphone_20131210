#ifdef BUILD_LK
#else
#include <linux/string.h>

#ifdef BUILD_UBOOT
//
#else
#include <linux/kernel.h>//for printk
#endif
#endif
 #include "lcm_drv.h"

// ---------------------------------------------------------------------------
//  Local Constants
// ---------------------------------------------------------------------------

#define FRAME_WIDTH  										(540)
#define FRAME_HEIGHT 										(960)

#define REGFLAG_DELAY             							0XFE
#define REGFLAG_END_OF_TABLE      							0xfff   // END OF REGISTERS MARKER

#define LCM_DSI_CMD_MODE									0

#define LCM_ID_HX8389B 0x89

// ---------------------------------------------------------------------------
//  Local Variables
// ---------------------------------------------------------------------------

static LCM_UTIL_FUNCS lcm_util = {0};

#define SET_RESET_PIN(v)    								(lcm_util.set_reset_pin((v)))

#define UDELAY(n) 											(lcm_util.udelay(n))
#define MDELAY(n) 											(lcm_util.mdelay(n))


// ---------------------------------------------------------------------------
//  Local Functions
// ---------------------------------------------------------------------------

#define dsi_set_cmdq_V2(cmd, count, ppara, force_update)	lcm_util.dsi_set_cmdq_V2(cmd, count, ppara, force_update)
#define dsi_set_cmdq(pdata, queue_size, force_update)		lcm_util.dsi_set_cmdq(pdata, queue_size, force_update)
#define wrtie_cmd(cmd)										lcm_util.dsi_write_cmd(cmd)
#define write_regs(addr, pdata, byte_nums)					lcm_util.dsi_write_regs(addr, pdata, byte_nums)
#define read_reg											lcm_util.dsi_read_reg()
#define read_reg_v2(cmd, buffer, buffer_size)   			lcm_util.dsi_dcs_read_lcm_reg_v2(cmd, buffer, buffer_size)    
       

static struct LCM_setting_table {
    unsigned cmd;
    unsigned char count;
    unsigned char para_list[64];
};



static struct LCM_setting_table lcm_set_window[] = {
	{0x2A,	4,	{0x00, 0x00, (FRAME_WIDTH>>8), (FRAME_WIDTH&0xFF)}},
	{0x2B,	4,	{0x00, 0x00, (FRAME_HEIGHT>>8), (FRAME_HEIGHT&0xFF)}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_sleep_out_setting[] = {
    // Sleep Out
	{0x11, 0, {0x00}},
    {REGFLAG_DELAY, 120, {}},

    // Display ON
	{0x29, 0, {0x00}},
	{REGFLAG_DELAY, 10, {}},
	
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_sleep_in_setting[] = {
	// Display off sequence
	{0x28, 0, {0x00}},

    // Sleep Mode On
	{0x10, 0, {0x00}},

	{REGFLAG_END_OF_TABLE, 0x00, {}}
};

static void push_table(struct LCM_setting_table *table, unsigned int count, unsigned char force_update)
{
	unsigned int i;

    for(i = 0; i < count; i++) {
		
        unsigned cmd;
        cmd = table[i].cmd;
		
        switch (cmd) {
			
            case REGFLAG_DELAY :
                MDELAY(table[i].count);
                break;
				
            case REGFLAG_END_OF_TABLE :
                break;
				
            default:
				dsi_set_cmdq_V2(cmd, table[i].count, table[i].para_list, force_update);
       	}
    }
	
}


// ---------------------------------------------------------------------------
//  LCM Driver Implementations
// ---------------------------------------------------------------------------

static void lcm_set_util_funcs(const LCM_UTIL_FUNCS *util)
{
    memcpy(&lcm_util, util, sizeof(LCM_UTIL_FUNCS));
}


static void lcm_get_params(LCM_PARAMS *params)
{
		memset(params, 0, sizeof(LCM_PARAMS));
	
		params->type   = LCM_TYPE_DSI;

		params->width  = FRAME_WIDTH;
		params->height = FRAME_HEIGHT;

		// enable tearing-free
		//params->dbi.te_mode 				= LCM_DBI_TE_MODE_VSYNC_ONLY;
		//params->dbi.te_edge_polarity		= LCM_POLARITY_RISING;

#if (LCM_DSI_CMD_MODE)
		params->dsi.mode   = CMD_MODE;
#else
		params->dsi.mode   = SYNC_PULSE_VDO_MODE;
#endif
	
		// DSI
		/* Command mode setting */
		params->dsi.LANE_NUM				= LCM_TWO_LANE;
		//The following defined the fomat for data coming from LCD engine.
		params->dsi.data_format.color_order = LCM_COLOR_ORDER_RGB;
		params->dsi.data_format.trans_seq   = LCM_DSI_TRANS_SEQ_MSB_FIRST;
		params->dsi.data_format.padding     = LCM_DSI_PADDING_ON_LSB;
		params->dsi.data_format.format      = LCM_DSI_FORMAT_RGB888;

		// Highly depends on LCD driver capability.
		// Not support in MT6573
		params->dsi.packet_size=256;

		// Video mode setting		
		params->dsi.intermediat_buffer_num = 2;

		params->dsi.PS=LCM_PACKED_PS_24BIT_RGB888;
		params->dsi.vertical_sync_active				= 9;
		params->dsi.vertical_backporch					= 8;
		params->dsi.vertical_frontporch					= 8;
		params->dsi.vertical_active_line				= FRAME_HEIGHT; 

		params->dsi.horizontal_sync_active				= 20;//96;	///===100
		params->dsi.horizontal_backporch				= 46;//96;	///===120
		params->dsi.horizontal_frontporch				= 21;//48;	///===170
		params->dsi.horizontal_active_pixel				= FRAME_WIDTH;

		params->dsi.pll_div1=1;		// div1=0,1,2,3;div1_real=1,2,4,4 ----0: 546Mbps  1:273Mbps
		params->dsi.pll_div2=1;		// div2=0,1,2,3;div1_real=1,2,4,4	
		params->dsi.fbk_div =31;    // fref=26MHz, fvco=fref*(fbk_div+1)*2/(div1_real*div2_real//20)	///==24

		/* ESD or noise interference recovery For video mode LCM only. */ // Send TE packet to LCM in a period of n frames and check the response. 
		params->dsi.lcm_int_te_monitor = 0; 
		params->dsi.lcm_int_te_period = 1; // Unit : frames 
 
		// Need longer FP for more opportunity to do int. TE monitor applicably. 
		if(params->dsi.lcm_int_te_monitor) 
			params->dsi.vertical_frontporch *= 2; 
 
		// Monitor external TE (or named VSYNC) from LCM once per 2 sec. (LCM VSYNC must be wired to baseband TE pin.) 
		params->dsi.lcm_ext_te_monitor = 0; 
		// Non-continuous clock 
		params->dsi.noncont_clock = 1; 
		params->dsi.noncont_clock_period = 2; // Unit : frames

}
static void init_lcm_registers(void)
{
        unsigned int data_array[30];
      /*  data_array[0]= 0x00043902; 
        data_array[1]= 0x8983FFB9; 
        dsi_set_cmdq(&data_array, 2, 1); 
        MDELAY(1); 	

  
		
	     data_array[0] = 0x08C61500;        //exit sleep mode 
	     dsi_set_cmdq(&data_array, 1, 1); 
	     MDELAY(20);	
		


	    data_array[0]= 0x00143902; 
      data_array[1]= 0x070000B1; 
     	data_array[2]= 0x111058EC; 
     	data_array[3]= 0x3E36F5F5; 
   	  data_array[4]= 0x01422525;
    	data_array[5]= 0xE600F75A;
      dsi_set_cmdq(&data_array, 6, 1); 
      MDELAY(1); 	

	    data_array[0]= 0x00083902; 
        data_array[1]= 0x780000B2; 
     	data_array[2]= 0x503F070C; 
        dsi_set_cmdq(&data_array, 3, 1); 
        MDELAY(1); 

	data_array[0]= 0x00183902; 
    data_array[1]= 0x000892B4; 
	data_array[2]= 0x32041032; 
	data_array[3]= 0x10320010; 
	data_array[4]= 0x400A3700;
	data_array[5]= 0x40013708;
	data_array[6]= 0x0B584718;
	dsi_set_cmdq(&data_array, 7, 1); 
        MDELAY(1); 	

	data_array[0]= 0x00393902; 
	data_array[1]= 0x000000D5; 
	data_array[2]= 0x00060100; 
	data_array[3]= 0x99006000; 
	data_array[4]= 0x88998888;
	data_array[5]= 0x88018823;
	data_array[6]= 0x01458867;
	data_array[7]= 0x88888823; 
	data_array[8]= 0x99888888; 
	data_array[9]= 0x54888899; 
	data_array[10]= 0x10887688; 
	data_array[11]= 0x10323288;
	data_array[12]= 0x88888888;
	data_array[13]= 0x00043C88;
	data_array[14]= 0x00000000;
	data_array[15]= 0x00000000;
	dsi_set_cmdq(&data_array, 15, 1); 
	MDELAY(1); 	
	
		data_array[0]= 0x00053902; 
	data_array[1]= 0x006700B6; 
	data_array[2]= 0x0000006B; 
	dsi_set_cmdq(&data_array, 3, 1); 
	MDELAY(1);
	
	data_array[0]= 0x00233902; 
	data_array[1]= 0x060000E0; 
	data_array[2]= 0x241E1010; 
	data_array[3]= 0x120D0735; 
	data_array[4]= 0x17161917;
	data_array[5]= 0x00001912;
	data_array[6]= 0x1E101006;
	data_array[7]= 0x0D073524; 			
	data_array[8]= 0x16191712;   
	data_array[9]= 0x00191217; 
	dsi_set_cmdq(&data_array, 10, 1); 
	MDELAY(1); 	


	data_array[0] = 0x02CC1500;        //exit sleep mode 
	dsi_set_cmdq(&data_array, 1, 1); 
	MDELAY(20);	
	
	data_array[0]= 0x00043902; 
	data_array[1]= 0xD00500B7;
	dsi_set_cmdq(&data_array, 2, 1); 
	MDELAY(1);  */
	data_array[0]=0x00043902;//Enable external Command
	data_array[1]=0x8983FFB9; 
	dsi_set_cmdq(&data_array, 2, 1);
	MDELAY(1);
	
	data_array[0]=0x00083902;//Enable external Command//3
	data_array[1]=0x009341ba; 
	data_array[2]=0x1800a416; 
	dsi_set_cmdq(&data_array, 3, 1);
	MDELAY(1);
	
	data_array[0]=0x00143902;
	data_array[1]=0x060000B1;
	data_array[2]=0x111059e8;
	data_array[3]=0x453df1d1;  ///0x2b23d1d1 //0x332bf272   2a22f272
	data_array[4]=0x01422e2e; 
	data_array[5]=0xe600f75a;  
	dsi_set_cmdq(&data_array, 6, 1);	
	MDELAY(1);
	
	data_array[0]=0x00083902;
	data_array[1]=0x780000B2;
	data_array[2]=0x303f070c;
	dsi_set_cmdq(&data_array, 3, 1);	
	MDELAY(1);
	
	data_array[0]=0x00183902;//Enable external Command//3
	data_array[1]=0x000880b4; //0x0008adb4;   //0x000480b4
	data_array[2]=0x32041032; 
	data_array[3]=0x10320010; 
	data_array[4]=0x400a3700; 
	data_array[5]=0x40023708; //0x380a1301
	data_array[6]=0x0a585014; 
	dsi_set_cmdq(&data_array, 7, 1);	
	MDELAY(1);
	
	data_array[0]=0x00393902;//Enable external Command//3
	data_array[1]=0x000000d5; 
	data_array[2]=0x00000100; 
	data_array[3]=0x99006000; 
	data_array[4]=0x88bbaa88; 
	data_array[5]=0x88018823; 
	data_array[6]=0x01458867; 
	data_array[7]=0x88888823; 
	data_array[8]=0x99888888; 
	data_array[9]=0x54aabb88; 
	data_array[10]=0x10887688; 
	data_array[11]=0x10323288; 
	data_array[12]=0x88888888; 
	data_array[13]=0x00040088; 
	data_array[14]=0x00000000; 
	data_array[15]=0x00000000; 
	dsi_set_cmdq(&data_array, 16, 1);
	MDELAY(1);

	data_array[0]=0x00023902;//Enable external Command
	data_array[1]=0x0000e0c6; 
	dsi_set_cmdq(&data_array, 2, 1);
	MDELAY(1);
		
	data_array[0]=0x00043902;//Enable external Command//3
	data_array[1]=0x500000b7;
	dsi_set_cmdq(&data_array, 2, 1);	
	MDELAY(1);
		
	
		
	
	
	
	
	//data_array[0]=0x00033902;
	//data_array[1]=0x001743c0; 
	//dsi_set_cmdq(&data_array, 2, 1);	
	//MDELAY(1);
	
	data_array[0]=0x00053902;//Enable external Command
	data_array[1]=0x008800b6; //84 86
	data_array[2]=0x00000088; 

        //data_array[1]=vcom1; //84
	//data_array[2]=vcom2; 
	dsi_set_cmdq(&data_array, 3, 1);
       //vcom1+=0x003f0000;
       //vcom2+=0x0000003f;	
	MDELAY(1);
		 
	data_array[0]=0x00233902;
	data_array[1]=0x252300e0; 
	data_array[2]=0x2d3f3e3c; 
	data_array[3]=0x0e0e0747; 
	data_array[4]=0x110f1311; 
	data_array[5]=0x23001911; 
	data_array[6]=0x3f3e3c25; 
	data_array[7]=0x0e07472d; 
	data_array[8]=0x0f13110e; 
	data_array[9]=0x00191111; 
	dsi_set_cmdq(&data_array, 10, 1);	
	MDELAY(1);
		
	data_array[0]=0x00023902;//e
	data_array[1]=0x000002cc;//0e
	dsi_set_cmdq(&data_array, 2, 1);	
	MDELAY(1);	



	data_array[0] = 0x00110500;        //exit sleep mode 
	dsi_set_cmdq(&data_array, 1, 1); 
	MDELAY(150); 

	data_array[0] = 0x00290500;        //exit sleep mode 
	dsi_set_cmdq(&data_array, 1, 1); 
	MDELAY(20);	
}

static void lcm_init(void)
{
#if 1
    SET_RESET_PIN(1);
    MDELAY(1);
    SET_RESET_PIN(0);
    MDELAY(10);
    SET_RESET_PIN(1);
    MDELAY(50);
#endif
    init_lcm_registers();


}

static void lcm_suspend(void)
{
	SET_RESET_PIN(1);
	MDELAY(10);
	SET_RESET_PIN(0);	
	MDELAY(10);	
	SET_RESET_PIN(1);
	MDELAY(120);	

	//push_table(lcm_sleep_in_setting, sizeof(lcm_sleep_in_setting) / sizeof(struct LCM_setting_table), 1);

}


static void lcm_resume(void)
{

	lcm_init();
	//push_table(lcm_sleep_out_setting, sizeof(lcm_sleep_out_setting) / sizeof(struct LCM_setting_table), 1);
}


static void lcm_update(unsigned int x, unsigned int y,
                       unsigned int width, unsigned int height)
{
	unsigned int x0 = x;
	unsigned int y0 = y;
	unsigned int x1 = x0 + width - 1;
	unsigned int y1 = y0 + height - 1;

	unsigned char x0_MSB = ((x0>>8)&0xFF);
	unsigned char x0_LSB = (x0&0xFF);
	unsigned char x1_MSB = ((x1>>8)&0xFF);
	unsigned char x1_LSB = (x1&0xFF);
	unsigned char y0_MSB = ((y0>>8)&0xFF);
	unsigned char y0_LSB = (y0&0xFF);
	unsigned char y1_MSB = ((y1>>8)&0xFF);
	unsigned char y1_LSB = (y1&0xFF);

	unsigned int data_array[16];

	data_array[0]= 0x00053902;
	data_array[1]= (x1_MSB<<24)|(x0_LSB<<16)|(x0_MSB<<8)|0x2a;
	data_array[2]= (x1_LSB);
	data_array[3]= 0x00053902;
	data_array[4]= (y1_MSB<<24)|(y0_LSB<<16)|(y0_MSB<<8)|0x2b;
	data_array[5]= (y1_LSB);
	data_array[6]= 0x002c3909;

	dsi_set_cmdq(&data_array, 7, 0);

}


static unsigned int lcm_compare_id(void)
{
	unsigned int id=0;
	unsigned char buffer[2];
	unsigned int array[16];  

    //return 1;

    SET_RESET_PIN(1);
    MDELAY(10);
    SET_RESET_PIN(0);
    MDELAY(10);
    SET_RESET_PIN(1);
    MDELAY(20);//Must over 6 ms

	array[0]=0x00043902;
	array[1]=0x8983FFB9;
	dsi_set_cmdq(&array, 2, 1);
	MDELAY(10);

	array[0]=0x00083902;
	array[1]=0x009341BA;
	array[2]=0x1800a416;
	dsi_set_cmdq(&array, 3, 1);
	MDELAY(10);

	array[0] = 0x00023700;// return byte number
	dsi_set_cmdq(&array, 1, 1);
	MDELAY(10);

	read_reg_v2(0xF4, buffer, 2);
	id = buffer[0]; 
	
#if defined(BUILD_LK)
	printf("%s, id = 0x%08x\n", __func__, id);
#endif

	return (LCM_ID_HX8389B == id)?1:0;

}




LCM_DRIVER hx8389b_hsd50_jingtai_qhd_lcm_drv = 
{
    .name			= "hx8389b_hsd50_jingtai_qhd",
	.set_util_funcs = lcm_set_util_funcs,
	.get_params     = lcm_get_params,
	.init           = lcm_init,
	.suspend        = lcm_suspend,
	.resume         = lcm_resume,
	.compare_id     = lcm_compare_id,
#if (LCM_DSI_CMD_MODE)
	.set_backlight	= lcm_setbacklight,
    .update         = lcm_update,
#endif
};

