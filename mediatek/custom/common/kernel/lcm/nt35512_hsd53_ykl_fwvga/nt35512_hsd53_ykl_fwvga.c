/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifdef BUILD_LK
#else
    #include <linux/string.h>
    #if defined(BUILD_LK)
        #include <asm/arch/mt_gpio.h>
    #else
        #include <mach/mt_gpio.h>
    #endif
#endif
#include "lcm_drv.h"

// ---------------------------------------------------------------------------
//  Local Constants
// ---------------------------------------------------------------------------

#define FRAME_WIDTH  										(480)
#define FRAME_HEIGHT 										(854)
#define LCM_ID       (0x5512)

#define REGFLAG_DELAY             							0XFE
#define REGFLAG_END_OF_TABLE      							0xFFF   // END OF REGISTERS MARKER


#define LCM_DSI_CMD_MODE									1

#ifndef TRUE
    #define TRUE 1
#endif

#ifndef FALSE
    #define FALSE 0
#endif

// ---------------------------------------------------------------------------
//  Local Variables
// ---------------------------------------------------------------------------

static LCM_UTIL_FUNCS lcm_util = {0};

#define SET_RESET_PIN(v)    								(lcm_util.set_reset_pin((v)))

#define UDELAY(n) 											(lcm_util.udelay(n))
#define MDELAY(n) 											(lcm_util.mdelay(n))

static unsigned int lcm_esd_test = FALSE;      ///only for ESD test

// ---------------------------------------------------------------------------
//  Local Functions
// ---------------------------------------------------------------------------

#define dsi_set_cmdq_V2(cmd, count, ppara, force_update)	lcm_util.dsi_set_cmdq_V2(cmd, count, ppara, force_update)
#define dsi_set_cmdq(pdata, queue_size, force_update)		lcm_util.dsi_set_cmdq(pdata, queue_size, force_update)
#define wrtie_cmd(cmd)										lcm_util.dsi_write_cmd(cmd)
#define write_regs(addr, pdata, byte_nums)					lcm_util.dsi_write_regs(addr, pdata, byte_nums)
#define read_reg(cmd)											lcm_util.dsi_dcs_read_lcm_reg(cmd)
#define read_reg_v2(cmd, buffer, buffer_size)				lcm_util.dsi_dcs_read_lcm_reg_v2(cmd, buffer, buffer_size)

 struct LCM_setting_table {
    unsigned cmd;
    unsigned char count;
    unsigned char para_list[64];
};

static struct LCM_setting_table lcm_initialization_setting[] = {

	/*
	   Note :

	   Data ID will depends on the following rule.

	   count of parameters > 1 => Data ID = 0x39
	   count of parameters = 1 => Data ID = 0x15
	   count of parameters = 0 => Data ID = 0x05

	   Structure Format :

	   {DCS command, count of parameters, {parameter list}}
	   {REGFLAG_DELAY, milliseconds of time, {}},

	   ...

	   Setting ending by predefined flag

	   {REGFLAG_END_OF_TABLE, 0x00, {}}
	 */

//LV2 Page 1 enable
{0xF0,  5,  {0x55,0xAA,0x52,0x08,0x01}},
//AVDD Set AVDD 5.2V
{0xB0,  3,  {0x0A,0x0A,0x0A}},
//AVDD ratio
{0xB6,  3,  {0x44,0x44,0x44}},
//AVEE  -5.2V
{0xB1,  3,  {0x0A,0x0A,0x0A}},
//AVEE ratio
{0xB7,  3,  {0x34,0x34,0x34}},
//VCL  -2.5V
{0xB2,  3,  {0x00,0x00,0x00}},
//VCL ratio
{0xB8,  3,  {0x34,0x34,0x34}},
//VGH 15V  (Free pump)
{0xBF,  1,  {0x01}},
{0xB3,  3,  {0x08,0x08,0x08}},
//VGH ratio
{0xB9,  3,  {0x34,0x34,0x34}},
//VGL_REG -10V
{0xB5,  3,  {0x08,0x08,0x08}},
{0xC2,  1,  {0x03}},
//VGLX ratio
{0xBA,  3,  {0x24,0x24,0x24}},
//VGMP/VGSP 4.5V/0V
{0xBC,  3,  {0x00,0x78,0x00}},
{0xBD,  3,  {0x00,0x78,0x00}},
//VCOM  -0.9625V
{0xBE,  2,  {0x00,0x57}},//VCOM 4B
//Gamma Setting
//    0  1    3     5      7   11   15   23   31  47    63  95   127     128     160    192     208      224      232    240      244       248      250   
{0xD1,  52,  {0x00,0x00,0x00,0x01,0x00,0x02,0x00,0x07,0x00,0x1A,0x00,0x4D,0x00,0x7F,0x00,0xC9,0x00,0xFD,0x01,0x48,0x01,0x7A,0x01,0xC4,0x01,0xFA,0x01,0xFB,0x02,0x2A,0x02,0x59,0x02,0x73,0x02,0x90,0x02,0xA2,0x02,0xB7,0x02,0xC6,0x02,0xDB,0x02,0xEB,0x03,0x01,0x03,0x35,0x03,0xFF}},
{0xD2,  52,  {0x00,0x00,0x00,0x01,0x00,0x02,0x00,0x07,0x00,0x1A,0x00,0x4D,0x00,0x7F,0x00,0xC9,0x00,0xFD,0x01,0x48,0x01,0x7A,0x01,0xC4,0x01,0xFA,0x01,0xFB,0x02,0x2A,0x02,0x59,0x02,0x73,0x02,0x90,0x02,0xA2,0x02,0xB7,0x02,0xC6,0x02,0xDB,0x02,0xEB,0x03,0x01,0x03,0x35,0x03,0xFF}},
          
{0xD3,  52,  {0x00,0x00,0x00,0x01,0x00,0x02,0x00,0x07,0x00,0x1A,0x00,0x4D,0x00,0x7F,0x00,0xC9,0x00,0xFD,0x01,0x48,0x01,0x7A,0x01,0xC4,0x01,0xFA,0x01,0xFB,0x02,0x2A,0x02,0x59,0x02,0x73,0x02,0x90,0x02,0xA2,0x02,0xB7,0x02,0xC6,0x02,0xDB,0x02,0xEB,0x03,0x01,0x03,0x35,0x03,0xFF}},
          
{0xD4,  52,  {0x00,0x00,0x00,0x01,0x00,0x02,0x00,0x07,0x00,0x1A,0x00,0x4D,0x00,0x7F,0x00,0xC9,0x00,0xFD,0x01,0x48,0x01,0x7A,0x01,0xC4,0x01,0xFA,0x01,0xFB,0x02,0x2A,0x02,0x59,0x02,0x73,0x02,0x90,0x02,0xA2,0x02,0xB7,0x02,0xC6,0x02,0xDB,0x02,0xEB,0x03,0x01,0x03,0x35,0x03,0xFF}},
          
{0xD5,  52,  {0x00,0x00,0x00,0x01,0x00,0x02,0x00,0x07,0x00,0x1A,0x00,0x4D,0x00,0x7F,0x00,0xC9,0x00,0xFD,0x01,0x48,0x01,0x7A,0x01,0xC4,0x01,0xFA,0x01,0xFB,0x02,0x2A,0x02,0x59,0x02,0x73,0x02,0x90,0x02,0xA2,0x02,0xB7,0x02,0xC6,0x02,0xDB,0x02,0xEB,0x03,0x01,0x03,0x35,0x03,0xFF}},
          
{0xD6,  52,  {0x00,0x00,0x00,0x01,0x00,0x02,0x00,0x07,0x00,0x1A,0x00,0x4D,0x00,0x7F,0x00,0xC9,0x00,0xFD,0x01,0x48,0x01,0x7A,0x01,0xC4,0x01,0xFA,0x01,0xFB,0x02,0x2A,0x02,0x59,0x02,0x73,0x02,0x90,0x02,0xA2,0x02,0xB7,0x02,0xC6,0x02,0xDB,0x02,0xEB,0x03,0x01,0x03,0x35,0x03,0xFF}},

//LV2 Page 0 enable
{0xF0,  5,  {0x55,0xAA,0x52,0x08,0x00}},
//480x854
{0xB1,  2,  {0xFC,0x00}},
//Display control
{0xB5,  1,  {0x6B}},
//Source hold time
{0xB6,  1,  {0x05}},
//Gate EQ control
{0xB7,  2,  {0x70,0x70}},
//Source EQ control (Mode 2)
{0xB8,  4,  {0x01,0x03,0x03,0x03}},
//Inversion mode  (2-dot)
{0xBC,  3,  {0x00,0x00,0x00}},
//Timing control 4H w/ 4-delay 
{0xC9,  5,  {0xD0,0x02,0x50,0x50,0x50}},

	{0x35,    1,    {0x00}},//TE
        {0x36,    1,    {0x00}},
	
	{0x11,    1,    {0x00}},
	{REGFLAG_DELAY, 150, {}},

	{0x29,    1,   {0x00}},
	{REGFLAG_DELAY, 100, {}},
	
	{0x2C,    1,   {0x00}},
	{REGFLAG_DELAY, 20, {}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_sleep_out_setting[] = {
    // Sleep Out
	{0x11, 1, {0x00}},
    {REGFLAG_DELAY, 120, {}},

    // Display ON
	{0x29, 1, {0x00}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static struct LCM_setting_table lcm_deep_sleep_mode_in_setting[] = {
	// Display off sequence
	{0x28, 1, {0x00}},

    // Sleep Mode On
	{0x10, 1, {0x00}},

	{REGFLAG_END_OF_TABLE, 0x00, {}}
};

static struct LCM_setting_table lcm_compare_id_setting[] = {
	// Display off sequence
	{0xB9,	3,	{0xFF, 0x83, 0x69}},
	{REGFLAG_DELAY, 10, {}},

    // Sleep Mode On
//	{0xC3, 1, {0xFF}},

	{REGFLAG_END_OF_TABLE, 0x00, {}}
};

static struct LCM_setting_table lcm_backlight_level_setting[] = {
	{0x51, 1, {0xFF}},
	{REGFLAG_END_OF_TABLE, 0x00, {}}
};


static void push_table(struct LCM_setting_table *table, unsigned int count, unsigned char force_update)
{
	unsigned int i;

    for(i = 0; i < count; i++) {

        unsigned cmd;
        cmd = table[i].cmd;

        switch (cmd) {

            case REGFLAG_DELAY :
                MDELAY(table[i].count);
                break;

            case REGFLAG_END_OF_TABLE :
                break;

            default:
				dsi_set_cmdq_V2(cmd, table[i].count, table[i].para_list, force_update);
				/*
                if (cmd != 0xFF && cmd != 0x2C && cmd != 0x3C) {	//wqtao. copy from nt35510
                    while(read_reg(cmd) != table[i].para_list[0]);
       			}*/
    	}
    }
}


// ---------------------------------------------------------------------------
//  LCM Driver Implementations
// ---------------------------------------------------------------------------

static void lcm_set_util_funcs(const LCM_UTIL_FUNCS *util)
{
    memcpy(&lcm_util, util, sizeof(LCM_UTIL_FUNCS));
}


static void lcm_get_params(LCM_PARAMS *params)
{
	memset(params, 0, sizeof(LCM_PARAMS));
	params->type   = LCM_TYPE_DSI;
	params->width  = FRAME_WIDTH;
	params->height = FRAME_HEIGHT;

	// enable tearing-free
	//params->dbi.te_mode 				= LCM_DBI_TE_MODE_DISABLED;
	params->dbi.te_mode 				= LCM_DBI_TE_MODE_VSYNC_ONLY;
	params->dbi.te_edge_polarity		= LCM_POLARITY_RISING;

#if (LCM_DSI_CMD_MODE)
	params->dsi.mode   = CMD_MODE;
#else
	params->dsi.mode   = SYNC_PULSE_VDO_MODE;
#endif

			// DSI
		/* Command mode setting */
		params->dsi.LANE_NUM				= LCM_TWO_LANE;

		//The following defined the fomat for data coming from LCD engine.
		params->dsi.data_format.color_order = LCM_COLOR_ORDER_RGB;
		params->dsi.data_format.trans_seq	= LCM_DSI_TRANS_SEQ_MSB_FIRST;
		params->dsi.data_format.padding 	= LCM_DSI_PADDING_ON_LSB;
		params->dsi.data_format.format		= LCM_DSI_FORMAT_RGB888;

		params->dsi.intermediat_buffer_num = 2;//because DSI/DPI HW design change, this parameters should be 0 when video mode in MT658X; or memory leakage

		params->dsi.PS=LCM_PACKED_PS_24BIT_RGB888;
		params->dsi.word_count=720*3;


                params->dsi.vertical_sync_active                                = 2;  //---3
                params->dsi.vertical_backporch                                  = 10; //---14
                params->dsi.vertical_frontporch                                 = 44;  //----8
                params->dsi.vertical_active_line                                = FRAME_HEIGHT;

                params->dsi.horizontal_sync_active                              = 2;  //----2
                params->dsi.horizontal_backporch                                = 60; //----28
                params->dsi.horizontal_frontporch                               = 20; //----50
                params->dsi.horizontal_active_pixel                             = FRAME_WIDTH;
                params->dsi.lcm_ext_te_enable=1;



                // Bit rate calculation
                //1 Every lane speed
                params->dsi.pll_div1=1;		// div1=0,1,2,3;div1_real=1,2,4,4 ----0: 546Mbps  1:273Mbps
                params->dsi.pll_div2=1;		// div2=0,1,2,3;div1_real=1,2,4,4
                params->dsi.fbk_div =18;//18;    // fref=26MHz, fvco=fref*(fbk_div+1)*2/(div1_real*div2_real)
                //params->dsi.compatibility_for_nvk=1;

}


static void lcm_init(void)
{
    SET_RESET_PIN(1);
    MDELAY(10);
    SET_RESET_PIN(0);
    MDELAY(10);
    SET_RESET_PIN(1);
    MDELAY(100);

	push_table(lcm_initialization_setting, sizeof(lcm_initialization_setting) / sizeof(struct LCM_setting_table), 1);
}


static void lcm_suspend(void)
{
	SET_RESET_PIN(1);
    MDELAY(10);
	SET_RESET_PIN(0);
	MDELAY(10);
	SET_RESET_PIN(1);
	MDELAY(20);
	push_table(lcm_deep_sleep_mode_in_setting, sizeof(lcm_deep_sleep_mode_in_setting) / sizeof(struct LCM_setting_table), 1);	//wqtao. enable
}


static void lcm_resume(void)
{
	lcm_init();
}


static void lcm_update(unsigned int x, unsigned int y,
                       unsigned int width, unsigned int height)
	{//wqtao. copy from nt35110
		unsigned int x0 = x;
		unsigned int y0 = y;
		unsigned int x1 = x0 + width - 1;
		unsigned int y1 = y0 + height - 1;

		unsigned char x0_MSB = ((x0>>8)&0xFF);
		unsigned char x0_LSB = (x0&0xFF);
		unsigned char x1_MSB = ((x1>>8)&0xFF);
		unsigned char x1_LSB = (x1&0xFF);
		unsigned char y0_MSB = ((y0>>8)&0xFF);
		unsigned char y0_LSB = (y0&0xFF);
		unsigned char y1_MSB = ((y1>>8)&0xFF);
		unsigned char y1_LSB = (y1&0xFF);

		unsigned int data_array[16];


		data_array[0]= 0x00053902;
		data_array[1]= (x1_MSB<<24)|(x0_LSB<<16)|(x0_MSB<<8)|0x2a;
		data_array[2]= (x1_LSB);
		dsi_set_cmdq(data_array, 3, 1);

		data_array[0]= 0x00053902;
		data_array[1]= (y1_MSB<<24)|(y0_LSB<<16)|(y0_MSB<<8)|0x2b;
		data_array[2]= (y1_LSB);
		dsi_set_cmdq(data_array, 3, 1);

	//	data_array[0]= 0x00290508; //HW bug, so need send one HS packet
	//	dsi_set_cmdq(data_array, 1, 1);

		data_array[0]= 0x002c3909;
		dsi_set_cmdq(data_array, 1, 0);
	}



#if 1	//wqtao.
static void lcm_setbacklight(unsigned int level)
{
	unsigned int default_level = 145;
	unsigned int mapped_level = 0;

	//for LGE backlight IC mapping table
	if(level > 255)
			level = 255;

	if(level >0)
			mapped_level = default_level+(level)*(255-default_level)/(255);
	else
			mapped_level=0;

	// Refresh value of backlight level.
	lcm_backlight_level_setting[0].para_list[0] = mapped_level;

	push_table(lcm_backlight_level_setting, sizeof(lcm_backlight_level_setting) / sizeof(struct LCM_setting_table), 1);
}
#endif

static unsigned int lcm_esd_check(void)
{
#ifndef BUILD_LK
        if(lcm_esd_test)
        {
            lcm_esd_test = FALSE;
            return TRUE;
        }

        /// please notice: the max return packet size is 1
        /// if you want to change it, you can refer to the following marked code
        /// but read_reg currently only support read no more than 4 bytes....
        /// if you need to read more, please let BinHan knows.
        /*
                unsigned int data_array[16];
                unsigned int max_return_size = 1;

                data_array[0]= 0x00003700 | (max_return_size << 16);

                dsi_set_cmdq(&data_array, 1, 1);
        */

        if(read_reg(0x0a) == 0x9c)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
#endif
}

static unsigned int lcm_esd_recover(void)
{
    unsigned char para = 0;

    SET_RESET_PIN(1);
    SET_RESET_PIN(0);
    MDELAY(1);
    SET_RESET_PIN(1);
    MDELAY(120);
	  push_table(lcm_initialization_setting, sizeof(lcm_initialization_setting) / sizeof(struct LCM_setting_table), 1);
    MDELAY(10);
	  push_table(lcm_sleep_out_setting, sizeof(lcm_sleep_out_setting) / sizeof(struct LCM_setting_table), 1);
    MDELAY(10);
    dsi_set_cmdq_V2(0x35, 1, &para, 1);     ///enable TE
    MDELAY(10);

    return TRUE;
}


static unsigned int lcm_compare_id(void)
{
	unsigned int id = 0;
	unsigned char buffer[2];
	unsigned int array[16];
	SET_RESET_PIN(1);	//NOTE:should reset LCM firstly
	MDELAY(10);
	SET_RESET_PIN(0);
	MDELAY(50);
	SET_RESET_PIN(1);
	MDELAY(50);

#ifdef BUILD_LK
	DSI_clk_HS_mode(1);
	MDELAY(10);
	DSI_clk_HS_mode(0);
#endif
	array[0] = 0x00063902;	// read id return two byte,version and id
	array[1] = 0x52aa55f0;	// read id return two byte,version and id
	array[2] = 0x00000108;	// read id return two byte,version and id
	dsi_set_cmdq(array, 3, 1);
	MDELAY(10);
	array[0] = 0x00023700;	// read id return two byte,version and id
	dsi_set_cmdq(array, 1, 1);
	MDELAY(10);

	read_reg_v2(0xc5, buffer, 2);
	id = ((buffer[0] << 8) | buffer[1]);	//we only need ID
#if defined(BUILD_LK)
	printf("%s, id1 = 0x%x,id2 = 0x%x,id3 = 0x%x\n", __func__, id,
	       buffer[0], buffer[1]);
#endif
	return (LCM_ID == id) ? 1 : 0;
}

LCM_DRIVER nt35512_hsd53_ykl_fwvga_lcm_drv = {
	.name = "nt35512_hsd53_ykl_fwvga",
	.set_util_funcs = lcm_set_util_funcs,
	.get_params = lcm_get_params,
	.init = lcm_init,
	.suspend = lcm_suspend,
	.resume = lcm_resume,
#if (LCM_DSI_CMD_MODE)
	.update = lcm_update,
#endif
        .compare_id = lcm_compare_id,
};
