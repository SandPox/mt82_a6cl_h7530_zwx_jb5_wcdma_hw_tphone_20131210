/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
#include "tpd.h"
#include <linux/interrupt.h>
#include <cust_eint.h>
#include <linux/i2c.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/rtpm_prio.h>
#include <linux/wait.h>
#include <linux/time.h>
#include <linux/delay.h>

#include "tpd_custom_msg2133.h"

#include <mach/mt_pm_ldo.h>
#include <mach/mt_typedefs.h>
#include <mach/mt_boot.h>

#include "cust_gpio_usage.h"


// added 20120321 maliejun
#ifdef TPD_HAVE_BUTTON
static int tpd_keys_local[TPD_KEY_COUNT] = TPD_KEYS;
static int tpd_keys_dim_local[TPD_KEY_COUNT][4] = TPD_KEYS_DIM;
#endif
#if (defined(TPD_WARP_START) && defined(TPD_WARP_END))
static int tpd_wb_start_local[TPD_WARP_CNT] = TPD_WARP_START;
static int tpd_wb_end_local[TPD_WARP_CNT]   = TPD_WARP_END;
#endif
#if (defined(TPD_HAVE_CALIBRATION) && !defined(TPD_CUSTOM_CALIBRATION))
static int tpd_calmat_local[8]     = TPD_CALIBRATION_MATRIX;
static int tpd_def_calmat_local[8] = TPD_CALIBRATION_MATRIX;
#endif

extern struct tpd_device *tpd;

static struct i2c_client *i2c_client = NULL;
static struct task_struct *thread = NULL;

static DECLARE_WAIT_QUEUE_HEAD(waiter);


static void tpd_eint_interrupt_handler(void);




static int __devinit tpd_probe(struct i2c_client *client, const struct i2c_device_id *id);
static int tpd_detect(struct i2c_client *client, int kind, struct i2c_board_info *info);
static int __devexit tpd_remove(struct i2c_client *client);
static int touch_event_handler(void *unused);

static int tpd_flag = 0;
static int point_num = 0;
static int p_point_num = 0;

#define	GPIO_CTP_RST_PIN_M_GPIO		0			//add in 2012-07-07 by [Hally]
//#define TPD_CLOSE_POWER_IN_SLEEP
//#define TP_DEBUG  //add by xuzhoubin
#define TP_FIRMWARE_UPDATE
#define TP_PROXIMITY_SENSOR  //add  by xuzhoubin
#define TPD_OK 0
// debug macros
#if defined(TP_DEBUG)
#define SSL_PRINT(x...)		printk("MSG2133:"x)
#else
#define SSL_PRINT(format, args...)  do {} while (0)
#endif

#ifdef TP_PROXIMITY_SENSOR
char ps_data_state[1] = {0};
enum
{
    DISABLE_CTP_PS,
    ENABLE_CTP_PS,
    RESET_CTP_PS
};
char tp_proximity_state = DISABLE_CTP_PS;
#endif
///int SMC_SWITCH=0;
#ifdef TP_FIRMWARE_UPDATE
#define U8 unsigned char
#define S8 signed char
#define U16 unsigned short
#define S16 signed short
#define U32 unsigned int
#define S32 signed int
#define TOUCH_ADDR_MSG20XX   0x4C
#define FW_ADDR_MSG20XX      0xC4
#define FW_UPDATE_ADDR_MSG20XX   0x92
static  char *fw_version;
#define DWIIC_MODE_ISP    0
#define DWIIC_MODE_DBBUS  1
static U8 temp[94][1024];
static int FwDataCnt;
//static int FwVersion;
struct class *firmware_class;
struct device *firmware_cmd_dev;

static int update_switch = 0;
#define ENABLE_DMA      0
#if ENABLE_DMA
static u8 *gpDMABuf_va = NULL;
static u32 gpDMABuf_pa = NULL;
#endif

#endif

struct touch_info
{
    unsigned short y[3];
    unsigned short x[3];
    unsigned short p[3];
    unsigned short count;
};

typedef struct
{
    unsigned short pos_x;
    unsigned short pos_y;
    unsigned short pos_x2;
    unsigned short pos_y2;
    unsigned short temp2;
    unsigned short temp;
    short dst_x;
    short dst_y;
    unsigned char checksum;

} SHORT_TOUCH_STATE;

struct msg_ts_priv
{
    unsigned int buttons;
    int      prev_x[4];
    int	prev_y[4];
};

struct msg_ts_priv msg2133_priv;


static const struct i2c_device_id tpd_id[] = {{TPD_DEVICE, 0}, {}};
unsigned short force[] = {TPD_I2C_NUMBER, TOUCH_ADDR_MSG20XX, I2C_CLIENT_END, I2C_CLIENT_END};
static const unsigned short *const forces[] = { force, NULL };
//static struct i2c_client_address_data addr_data = { .forces = forces, };
static struct i2c_board_info __initdata i2c_tpd = { I2C_BOARD_INFO(TPD_DEVICE, (0x4C >> 1))};

#ifdef TP_FIRMWARE_UPDATE
static void i2c_write(u8 addr, u8 *pbt_buf, int dw_lenth)
{
    int ret;
    i2c_client->addr = addr;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    ret = i2c_master_send(i2c_client, pbt_buf, dw_lenth);
    i2c_client->addr = TOUCH_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;

    if(ret <= 0)
    {
        printk("i2c_write_interface error line = %d, ret = %d\n", __LINE__, ret);
    }
}

static void i2c_read(u8 addr, u8 *pbt_buf, int dw_lenth)
{
    int ret;
    i2c_client->addr = addr;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    ret = i2c_master_recv(i2c_client, pbt_buf, dw_lenth);
    i2c_client->addr = TOUCH_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;

    if(ret <= 0)
    {
        printk("i2c_read_interface error line = %d, ret = %d\n", __LINE__, ret);
    }
}

static void i2c_write_update_msg2033(u8 *pbt_buf, int dw_lenth)
{
    int ret;
    i2c_client->addr = FW_UPDATE_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    ret = i2c_master_send(i2c_client, pbt_buf, dw_lenth);
    i2c_client->addr = TOUCH_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;

    // ret = i2c_smbus_write_i2c_block_data(i2c_client, *pbt_buf, dw_lenth-1, pbt_buf+1);
    if(ret <= 0)
    {
        printk("i2c_write_interface error line = %d, ret = %d\n", __LINE__, ret);
    }
}

static void i2c_write_msg2033(u8 *pbt_buf, int dw_lenth)
{
    int ret;
    i2c_client->timing = 40;
    i2c_client->addr = FW_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    ret = i2c_master_send(i2c_client, pbt_buf, dw_lenth);
    i2c_client->addr = TOUCH_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    i2c_client->timing = 240;

    // ret = i2c_smbus_write_i2c_block_data(i2c_client, *pbt_buf, dw_lenth-1, pbt_buf+1);
    if(ret <= 0)
    {
        printk("i2c_write_interface error line = %d, ret = %d\n", __LINE__, ret);
    }
}
static void i2c_read_update_msg2033(u8 *pbt_buf, int dw_lenth)
{
    int ret;
    i2c_client->addr = FW_UPDATE_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    ret = i2c_master_recv(i2c_client, pbt_buf, dw_lenth);
    i2c_client->addr = TOUCH_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    //  ret=i2c_smbus_read_i2c_block_data(i2c_client, *pbt_buf, dw_lenth-1, pbt_buf+1);

    if(ret <= 0)
    {
        printk("i2c_read_interface error line = %d, ret = %d\n", __LINE__, ret);
    }
}

static void i2c_read_msg2033(u8 *pbt_buf, int dw_lenth)
{
    int ret;
    i2c_client->timing = 40;
    i2c_client->addr = FW_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    ret = i2c_master_recv(i2c_client, pbt_buf, dw_lenth);
    i2c_client->addr = TOUCH_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    i2c_client->timing = 240;
    //  ret=i2c_smbus_read_i2c_block_data(i2c_client, *pbt_buf, dw_lenth-1, pbt_buf+1);

    if(ret <= 0)
    {
        printk("i2c_read_interface error line = %d, ret = %d\n", __LINE__, ret);
    }
}

#if ENABLE_DMA
ssize_t msg2033_dma_read_m_byte(u8 *returnData_va, u32 returnData_pa, U16 len)
{
    char     readData = 0;
    int     ret = 0, read_count = 0, read_length = 0;
    int    i, total_count = len;

    if(len == 0)
    {
        printk("[Error]msg2033_dma_read Read Len should not be zero!! \n");
        return 0;
    }

    //gpDMABuf_va = returnData_va; //mtk
    i2c_client->addr = FW_UPDATE_ADDR_MSG20XX;
    i2c_client->addr |= I2C_DMA_FLAG;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    returnData_va[0] = 0x11;
    ret = i2c_master_send(i2c_client, returnData_pa, 1);

    if(ret < 0)
    {
        printk("[Error]MATV sends command error!! \n");
        i2c_client->addr = TOUCH_ADDR_MSG20XX;
        i2c_client->addr|=I2C_ENEXT_FLAG;
        return 0;
    }

    ret = i2c_master_recv(i2c_client, returnData_pa, len); // mtk
    i2c_client->addr = TOUCH_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;

    if(ret < 0)
    {
        printk("[Error]msg2033_dma_read reads data error!! \n");
        return 0;
    }

    //for (i = 0; i< total_count; i++)
    //    MATV_LOGD("[MATV]I2C ReadData[%d] = %x\n",i,returnData_va[i]);
    return 1;
}

ssize_t msg2033_dma_write_m_byte(u8 *Data, U16 len)
{
    char    write_data[8] = {0};
    int    i, ret = 0, write_count = 0, write_length = 0;
    int    total_count = len;

    if(len == 0)
    {
        printk("[Error] msg2033_dma_write Len should not be zero!! \n");
        return 0;
    }

    //SSL_PRINT("mt5192_dma_write_m_byte, va=%x, pa=%x, len = %x\n",writeData_va,writeData_pa,len);
    gpDMABuf_va = Data;
    i2c_client->addr = FW_UPDATE_ADDR_MSG20XX;
    i2c_client->addr |= I2C_DMA_FLAG;
    i2c_client->addr|=I2C_ENEXT_FLAG;
    ret = i2c_master_send(i2c_client, gpDMABuf_pa, len + 1);
    i2c_client->addr = TOUCH_ADDR_MSG20XX;
    i2c_client->addr|=I2C_ENEXT_FLAG;

    if(ret < 0)
    {
        printk("[Error] msg2033_dma_write data error!! \n");
        return 0;
    }

    return 1;
}

U8 drvISP_DMA_Read(U8 *pDataToRead, U32 pa_addr, U8 n)    //First it needs send 0x11 to notify we want to get flash data back.
{
    //    U8 Read_cmd = 0x11;
    //    unsigned char dbbus_rx_data[2] = {0};
    //    i2c_write_update_msg2033( &Read_cmd, 1);
    msg2033_dma_read_m_byte(pDataToRead, pa_addr, n);
    return 0;
}

#endif

void Get_Chip_Version(void)
{
    unsigned char dbbus_tx_data[3];
    unsigned char dbbus_rx_data[2];
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0xCE;
    i2c_write_msg2033(&dbbus_tx_data[0], 3);
    i2c_read_msg2033(&dbbus_rx_data[0], 2);

    if(dbbus_rx_data[1] == 0)
    {
        // it is Catch2
        SSL_PRINT("*** Catch2 ***\n");
        //FwVersion  = 2;// 2 means Catch2
    }
    else
    {
        // it is catch1
        SSL_PRINT("*** Catch1 ***\n");
        //FwVersion  = 1;// 1 means Catch1
    }
}

void dbbusDWIICEnterSerialDebugMode(void)
{
    U8 data[5];
    // Enter the Serial Debug Mode
    data[0] = 0x53;
    data[1] = 0x45;
    data[2] = 0x52;
    data[3] = 0x44;
    data[4] = 0x42;
    i2c_write_msg2033(data, 5);
}

void dbbusDWIICStopMCU(void)
{
    U8 data[1];
    // Stop the MCU
    data[0] = 0x37;
    i2c_write_msg2033(data, 1);
}

void dbbusDWIICIICUseBus(void)
{
    U8 data[1];
    // IIC Use Bus
    data[0] = 0x35;
    i2c_write_msg2033(data, 1);
}

void dbbusDWIICIICReshape(void)
{
    U8 data[1];
    // IIC Re-shape
    data[0] = 0x71;
    i2c_write_msg2033(data, 1);
}

void dbbusDWIICIICNotUseBus(void)
{
    U8 data[1];
    // IIC Not Use Bus
    data[0] = 0x34;
    i2c_write_msg2033(data, 1);
}

void dbbusDWIICNotStopMCU(void)
{
    U8 data[1];
    // Not Stop the MCU
    data[0] = 0x36;
    i2c_write_msg2033(data, 1);
}

void dbbusDWIICExitSerialDebugMode(void)
{
    U8 data[1];
    // Exit the Serial Debug Mode
    data[0] = 0x45;
    i2c_write_msg2033(data, 1);
    // Delay some interval to guard the next transaction
    //udelay ( 200 );        // delay about 0.2ms
}

void drvISP_EntryIspMode(void)
{
    U8 bWriteData[5] =
    {
        0x4D, 0x53, 0x54, 0x41, 0x52
    };
    i2c_write_update_msg2033(bWriteData, 5);
    mdelay(10);           // delay about 1ms
}

U8 drvISP_Read(U8 n, U8 *pDataToRead)    //First it needs send 0x11 to notify we want to get flash data back.
{
    U8 Read_cmd = 0x11;
    U8 i = 0;
    unsigned char dbbus_rx_data[16] = {0};
    i2c_write_update_msg2033(&Read_cmd, 1);
    //if (n == 1)
    {
        i2c_read_update_msg2033(&dbbus_rx_data[0], n + 1);

        for(i = 0; i < n; i++)
        {
            *(pDataToRead + i) = dbbus_rx_data[i + 1];
        }
    }
    //else
    {
        //     i2c_read_update_msg2033(pDataToRead, n);
    }
    return 0;
}

void drvISP_WriteEnable(void)
{
    U8 bWriteData[2] =
    {
        0x10, 0x06
    };
    U8 bWriteData1 = 0x12;
    i2c_write_update_msg2033(bWriteData, 2);
    i2c_write_update_msg2033(&bWriteData1, 1);
}


void drvISP_ExitIspMode(void)
{
    U8 bWriteData = 0x24;
    i2c_write_update_msg2033(&bWriteData, 1);
}

U8 drvISP_ReadStatus(void)
{
    U8 bReadData = 0;
    U8 bWriteData[2] =
    {
        0x10, 0x05
    };
    U8 bWriteData1 = 0x12;
    i2c_write_update_msg2033(bWriteData, 2);
    drvISP_Read(1, &bReadData);
    i2c_write_update_msg2033(&bWriteData1, 1);
    return bReadData;
}


void drvISP_BlockErase(U32 addr)
{
    U8 bWriteData[5] = { 0x00, 0x00, 0x00, 0x00, 0x00 };
    U8 bWriteData1 = 0x12;
    SSL_PRINT("The drvISP_ReadStatus0=%d\n", drvISP_ReadStatus());
    drvISP_WriteEnable();
    SSL_PRINT("The drvISP_ReadStatus1=%d\n", drvISP_ReadStatus());
    //Enable write status register
    bWriteData[0] = 0x10;
    bWriteData[1] = 0x50;
    i2c_write_update_msg2033(bWriteData, 2);
    i2c_write_update_msg2033(&bWriteData1, 1);
    //Write Status
    bWriteData[0] = 0x10;
    bWriteData[1] = 0x01;
    bWriteData[2] = 0x00;
    i2c_write_update_msg2033(bWriteData, 3);
    i2c_write_update_msg2033(&bWriteData1, 1);
    //Write disable
    bWriteData[0] = 0x10;
    bWriteData[1] = 0x04;
    i2c_write_update_msg2033(bWriteData, 2);
    i2c_write_update_msg2033(&bWriteData1, 1);

    while((drvISP_ReadStatus() & 0x01) == 0x01)
    {
        ;
    }

    SSL_PRINT("The drvISP_ReadStatus3=%d\n", drvISP_ReadStatus());
    drvISP_WriteEnable();
    SSL_PRINT("The drvISP_ReadStatus4=%d\n", drvISP_ReadStatus());
    bWriteData[0] = 0x10;
    bWriteData[1] = 0xC7;        //Block Erase
    //bWriteData[2] = ((addr >> 16) & 0xFF) ;
    //bWriteData[3] = ((addr >> 8) & 0xFF) ;
    // bWriteData[4] = (addr & 0xFF) ;
    i2c_write_update_msg2033(bWriteData, 2);
    //i2c_write_update_msg2033( &bWriteData, 5);
    i2c_write_update_msg2033(&bWriteData1, 1);

    while((drvISP_ReadStatus() & 0x01) == 0x01)
    {
        ;
    }
}

void drvISP_Program(U16 k, U8 *pDataToWrite)
{
    U16 i = 0;
    U16 j = 0;
    //U16 n = 0;
    U8 TX_data[133];
    U8 bWriteData1 = 0x12;
    U32 addr = k * 1024;
#if ENABLE_DMA

    for(j = 0; j < 8; j++)    //128*8 cycle
    {
        TX_data[0] = 0x10;
        TX_data[1] = 0x02;// Page Program CMD
        TX_data[2] = (addr + 128 * j) >> 16;
        TX_data[3] = (addr + 128 * j) >> 8;
        TX_data[4] = (addr + 128 * j);

        for(i = 0; i < 128; i++)
        {
            TX_data[5 + i] = pDataToWrite[j * 128 + i];
        }

        while((drvISP_ReadStatus() & 0x01) == 0x01)
        {
            ;    //wait until not in write operation
        }

        drvISP_WriteEnable();
        //        i2c_write_update_msg2033( TX_data, 133);   //write 133 byte per cycle
        msg2033_dma_write_m_byte(TX_data, 133);
        i2c_write_update_msg2033(&bWriteData1, 1);
    }

#else

    for(j = 0; j < 512; j++)    //128*8 cycle
    {
        TX_data[0] = 0x10;
        TX_data[1] = 0x02;// Page Program CMD
        TX_data[2] = (addr + 2 * j) >> 16;
        TX_data[3] = (addr + 2 * j) >> 8;
        TX_data[4] = (addr + 2 * j);

        for(i = 0; i < 2; i++)
        {
            TX_data[5 + i] = pDataToWrite[j * 2 + i];
        }

        while((drvISP_ReadStatus() & 0x01) == 0x01)
        {
            ;    //wait until not in write operation
        }

        drvISP_WriteEnable();
        i2c_write_update_msg2033(TX_data, 7);    //write 133 byte per cycle
        i2c_write_update_msg2033(&bWriteData1, 1);
    }

#endif
}


void drvISP_Verify(U16 k, U8 *pDataToVerify)
{
    U16 i = 0, j = 0;
    U8 bWriteData[5] =
    {
        0x10, 0x03, 0, 0, 0
    };
    U8 bWriteData1 = 0x12;
    U32 addr = k * 1024;
    U8 index = 0;
#if ENABLE_DMA
    U8 *RX_data = gpDMABuf_va; //mtk

    for(j = 0; j < 8; j++)    //128*8 cycle
    {
        bWriteData[2] = (U8)((addr + j * 128) >> 16);
        bWriteData[3] = (U8)((addr + j * 128) >> 8);
        bWriteData[4] = (U8)(addr + j * 128);

        while((drvISP_ReadStatus() & 0x01) == 0x01)
        {
            ;    //wait until not in write operation
        }

        i2c_write_update_msg2033(bWriteData, 5);     //write read flash addr
        drvISP_DMA_Read(gpDMABuf_va, gpDMABuf_pa, 128); //mtk
        i2c_write_update_msg2033(&bWriteData1, 1);     //cmd end

        for(i = 0; i < 128; i++)    //log out if verify error
        {
            if((RX_data[i] != 0) && index < 10)
            {
                SSL_PRINT("j=%d,RX_data[%d]=0x%x\n", j, i, RX_data[i]);
                index++;
            }

            if(RX_data[i] != pDataToVerify[128 * j + i])
            {
                SSL_PRINT("k=%d,j=%d,i=%d===============Update Firmware Error================", k, j, i);
            }
        }
    }

#else
    U8 RX_data[128];

    for(j = 0; j < 256; j++)    //128*8 cycle
    {
        bWriteData[2] = (U8)((addr + j * 4) >> 16);
        bWriteData[3] = (U8)((addr + j * 4) >> 8);
        bWriteData[4] = (U8)(addr + j * 4);

        while((drvISP_ReadStatus() & 0x01) == 0x01)
        {
            ;    //wait until not in write operation
        }

        i2c_write_update_msg2033(bWriteData, 5);     //write read flash addr
        drvISP_Read(4, RX_data);
        i2c_write_update_msg2033(&bWriteData1, 1);     //cmd end

        for(i = 0; i < 4; i++)    //log out if verify error
        {
            if((RX_data[i] != 0) && index < 10)
            {
                SSL_PRINT("j=%d,RX_data[%d]=0x%x\n", j, i, RX_data[i]);
                index++;
            }

            if(RX_data[i] != pDataToVerify[4 * j + i])
            {
                SSL_PRINT("k=%d,j=%d,RX_data[%d]=0x%x===============Update Firmware Error================", k, j, i, RX_data[i]);
            }
        }
    }

#endif
}

static ssize_t firmware_update_show(struct device *dev,
                                    struct device_attribute *attr, char *buf)
{
    return sprintf(buf, "%s\n", fw_version);
}

static ssize_t firmware_update_store(struct device *dev,
                                     struct device_attribute *attr, const char *buf, size_t size)
{
    U8 i;
    U8 dbbus_tx_data[4];
    unsigned char dbbus_rx_data[2] = {0};
    update_switch = 1;
    SSL_PRINT("\n");
    //drvISP_EntryIspMode();
    //drvISP_BlockErase(0x00000);
    //M by cheehwa _HalTscrHWReset();
    mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
	mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
    mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ZERO);
    msleep(100);
    mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
    //changed in 2012-07-07 by [Hally]
#ifdef TPD_RSTPIN_1V8
    mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_IN);
    mt_set_gpio_pull_enable(GPIO_CTP_RST_PIN, GPIO_PULL_DISABLE);
#else
	mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
    mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ONE);
#endif

    mdelay(500);
    //msctpc_LoopDelay ( 100 );        // delay about 100ms*****
    // Enable slave's ISP ECO mode
    dbbusDWIICEnterSerialDebugMode();
    dbbusDWIICStopMCU();
    dbbusDWIICIICUseBus();
    dbbusDWIICIICReshape();
    SSL_PRINT("dbbusDWIICIICReshape\n");
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x08;
    dbbus_tx_data[2] = 0x0c;
    dbbus_tx_data[3] = 0x08;
    // Disable the Watchdog
    i2c_write_msg2033(dbbus_tx_data, 4);
    //Get_Chip_Version();
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x11;
    dbbus_tx_data[2] = 0xE2;
    dbbus_tx_data[3] = 0x00;
    i2c_write_msg2033(dbbus_tx_data, 4);
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x3C;
    dbbus_tx_data[2] = 0x60;
    dbbus_tx_data[3] = 0x55;
    i2c_write_msg2033(dbbus_tx_data, 4);
    SSL_PRINT("update\n");
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x3C;
    dbbus_tx_data[2] = 0x61;
    dbbus_tx_data[3] = 0xAA;
    i2c_write_msg2033(dbbus_tx_data, 4);
    //Stop MCU
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x0F;
    dbbus_tx_data[2] = 0xE6;
    dbbus_tx_data[3] = 0x01;
    i2c_write_msg2033(dbbus_tx_data, 4);
    //Enable SPI Pad
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0x02;
    i2c_write_msg2033(dbbus_tx_data, 3);
    i2c_read_msg2033(&dbbus_rx_data[0], 2);
    SSL_PRINT("dbbus_rx_data[0]=0x%x", dbbus_rx_data[0]);
    dbbus_tx_data[3] = (dbbus_rx_data[0] | 0x20);  //Set Bit 5
    i2c_write_msg2033(dbbus_tx_data, 4);
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0x25;
    i2c_write_msg2033(dbbus_tx_data, 3);
    dbbus_rx_data[0] = 0;
    dbbus_rx_data[1] = 0;
    i2c_read_msg2033(&dbbus_rx_data[0], 2);
    SSL_PRINT("dbbus_rx_data[0]=0x%x", dbbus_rx_data[0]);
    dbbus_tx_data[3] = dbbus_rx_data[0] & 0xFC;  //Clear Bit 1,0
    i2c_write_msg2033(dbbus_tx_data, 4);
    /*
    //------------
    // ISP Speed Change to 400K
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x11;
    dbbus_tx_data[2] = 0xE2;
    i2c_write_msg2033( dbbus_tx_data, 3);
    i2c_read_msg2033( &dbbus_rx_data[3], 1);
    //SSL_PRINT("dbbus_rx_data[0]=0x%x", dbbus_rx_data[0]);
    dbbus_tx_data[3] = dbbus_tx_data[3]&0xf7;  //Clear Bit3
    i2c_write_msg2033( dbbus_tx_data, 4);
    */
    //WP overwrite
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0x0E;
    dbbus_tx_data[3] = 0x02;
    i2c_write_msg2033(dbbus_tx_data, 4);
    //set pin high
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0x10;
    dbbus_tx_data[3] = 0x08;
    i2c_write_msg2033(dbbus_tx_data, 4);
    dbbusDWIICIICNotUseBus();
    dbbusDWIICNotStopMCU();
    dbbusDWIICExitSerialDebugMode();
    ///////////////////////////////////////
    // Start to load firmware
    ///////////////////////////////////////
    drvISP_EntryIspMode();
    SSL_PRINT("entryisp\n");
    drvISP_BlockErase(0x00000);
    //msleep(1000);
    SSL_PRINT("FwVersion=2");

    for(i = 0; i < 94; i++)    // total  94 KB : 1 byte per R/W
    {
        SSL_PRINT("drvISP_Program\n");
        drvISP_Program(i, temp[i]);    // program to slave's flash
    }

    SSL_PRINT("update OK\n");
    drvISP_ExitIspMode();
    FwDataCnt = 0;
    mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
    mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
    mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ZERO);
    msleep(100);
    mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
    //changed in 2012-07-07 by [Hally]
#ifdef TPD_RSTPIN_1V8
    mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_IN);
    mt_set_gpio_pull_enable(GPIO_CTP_RST_PIN, GPIO_PULL_DISABLE);
#else
	mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
    mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ONE);
#endif
    msleep(500);
    update_switch = 0;
    return size;
}

static DEVICE_ATTR(update, 0777, firmware_update_show, firmware_update_store);

/*test=================*/
static ssize_t firmware_clear_show(struct device *dev,
                                   struct device_attribute *attr, char *buf)
{
    U16 k = 0, i = 0, j = 0;
    U8 bWriteData[5] =
    {
        0x10, 0x03, 0, 0, 0
    };
    U8 RX_data[256];
    U8 bWriteData1 = 0x12;
    U32 addr = 0;
    SSL_PRINT("\n");

    for(k = 0; k < 94; i++)    // total  94 KB : 1 byte per R/W
    {
        addr = k * 1024;

        for(j = 0; j < 8; j++)    //128*8 cycle
        {
            bWriteData[2] = (U8)((addr + j * 128) >> 16);
            bWriteData[3] = (U8)((addr + j * 128) >> 8);
            bWriteData[4] = (U8)(addr + j * 128);

            while((drvISP_ReadStatus() & 0x01) == 0x01)
            {
                ;    //wait until not in write operation
            }

            i2c_write_update_msg2033(bWriteData, 5);     //write read flash addr
            drvISP_Read(128, RX_data);
            i2c_write_update_msg2033(&bWriteData1, 1);    //cmd end

            for(i = 0; i < 128; i++)    //log out if verify error
            {
                if(RX_data[i] != 0xFF)
                {
                    SSL_PRINT("k=%d,j=%d,i=%d===============erase not clean================", k, j, i);
                }
            }
        }
    }

    SSL_PRINT("read finish\n");
    return sprintf(buf, "%s\n", fw_version);
}

static ssize_t firmware_clear_store(struct device *dev,
                                    struct device_attribute *attr, const char *buf, size_t size)
{
    U8 dbbus_tx_data[4];
    unsigned char dbbus_rx_data[2] = {0};
    //msctpc_LoopDelay ( 100 );        // delay about 100ms*****
    // Enable slave's ISP ECO mode
    /*
    dbbusDWIICEnterSerialDebugMode();
    dbbusDWIICStopMCU();
    dbbusDWIICIICUseBus();
    dbbusDWIICIICReshape();*/
    SSL_PRINT("\n");
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x08;
    dbbus_tx_data[2] = 0x0c;
    dbbus_tx_data[3] = 0x08;
    // Disable the Watchdog
    i2c_write_msg2033(dbbus_tx_data, 4);
    //Get_Chip_Version();
    //FwVersion  = 2;
    //if (FwVersion  == 2)
    {
        dbbus_tx_data[0] = 0x10;
        dbbus_tx_data[1] = 0x11;
        dbbus_tx_data[2] = 0xE2;
        dbbus_tx_data[3] = 0x00;
        i2c_write_msg2033(dbbus_tx_data, 4);
    }
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x3C;
    dbbus_tx_data[2] = 0x60;
    dbbus_tx_data[3] = 0x55;
    i2c_write_msg2033(dbbus_tx_data, 4);
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x3C;
    dbbus_tx_data[2] = 0x61;
    dbbus_tx_data[3] = 0xAA;
    i2c_write_msg2033(dbbus_tx_data, 4);
    //Stop MCU
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x0F;
    dbbus_tx_data[2] = 0xE6;
    dbbus_tx_data[3] = 0x01;
    i2c_write_msg2033(dbbus_tx_data, 4);
    //Enable SPI Pad
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0x02;
    i2c_write_msg2033(dbbus_tx_data, 3);
    i2c_read_msg2033(&dbbus_rx_data[0], 2);
    SSL_PRINT("dbbus_rx_data[0]=0x%x", dbbus_rx_data[0]);
    dbbus_tx_data[3] = (dbbus_rx_data[0] | 0x20);  //Set Bit 5
    i2c_write_msg2033(dbbus_tx_data, 4);
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0x25;
    i2c_write_msg2033(dbbus_tx_data, 3);
    dbbus_rx_data[0] = 0;
    dbbus_rx_data[1] = 0;
    i2c_read_msg2033(&dbbus_rx_data[0], 2);
    SSL_PRINT("dbbus_rx_data[0]=0x%x", dbbus_rx_data[0]);
    dbbus_tx_data[3] = dbbus_rx_data[0] & 0xFC;  //Clear Bit 1,0
    i2c_write_msg2033(dbbus_tx_data, 4);
    //WP overwrite
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0x0E;
    dbbus_tx_data[3] = 0x02;
    i2c_write_msg2033(dbbus_tx_data, 4);
    //set pin high
    dbbus_tx_data[0] = 0x10;
    dbbus_tx_data[1] = 0x1E;
    dbbus_tx_data[2] = 0x10;
    dbbus_tx_data[3] = 0x08;
    i2c_write_msg2033(dbbus_tx_data, 4);
    dbbusDWIICIICNotUseBus();
    dbbusDWIICNotStopMCU();
    dbbusDWIICExitSerialDebugMode();
    ///////////////////////////////////////
    // Start to load firmware
    ///////////////////////////////////////
    drvISP_EntryIspMode();
    SSL_PRINT("chip erase+\n");
    drvISP_BlockErase(0x00000);
    SSL_PRINT("chip erase-\n");
    drvISP_ExitIspMode();
    return size;
}

static DEVICE_ATTR(clear, 0777, firmware_clear_show, firmware_clear_store);

/*test=================*/
/*Add by Tracy.Lin for update touch panel firmware and get fw version*/

static ssize_t firmware_version_show(struct device *dev,
                                     struct device_attribute *attr, char *buf)
{
    SSL_PRINT("*** firmware_version_show fw_version = %s***\n", fw_version);
    return sprintf(buf, "%s\n", fw_version);
}

static ssize_t firmware_version_store(struct device *dev,
                                      struct device_attribute *attr, const char *buf, size_t size)
{
    unsigned char dbbus_tx_data[3];
    unsigned char dbbus_rx_data[4] ;
    unsigned short major = 0, minor = 0;
    dbbusDWIICEnterSerialDebugMode();
    dbbusDWIICStopMCU();
    dbbusDWIICIICUseBus();
    dbbusDWIICIICReshape();
    fw_version = kzalloc(sizeof(char), GFP_KERNEL);
    SSL_PRINT("\n");
    //Get_Chip_Version();
    dbbus_tx_data[0] = 0x53;
    dbbus_tx_data[1] = 0x00;
    dbbus_tx_data[2] = 0x74;
    i2c_write(TOUCH_ADDR_MSG20XX, &dbbus_tx_data[0], 3);
    i2c_read(TOUCH_ADDR_MSG20XX, &dbbus_rx_data[0], 4);
    major = (dbbus_rx_data[1] << 8) + dbbus_rx_data[0];
    minor = (dbbus_rx_data[3] << 8) + dbbus_rx_data[2];
    SSL_PRINT("***major = %d ***\n", major);
    SSL_PRINT("***minor = %d ***\n", minor);
    sprintf(fw_version, "%03d%03d", major, minor);
    SSL_PRINT("***fw_version = %s ***\n", fw_version);
	printk("\n\n====[%s]====[%s]====[%d]====major = [%d]===minor = [%d]===\n\n",
	__FILE__,__FUNCTION__,__LINE__,major,minor);
    return size;
}
static DEVICE_ATTR(version, 0777, firmware_version_show, firmware_version_store);

static ssize_t firmware_data_show(struct device *dev,
                                  struct device_attribute *attr, char *buf)
{
    return FwDataCnt;
}

static ssize_t firmware_data_store(struct device *dev,
                                   struct device_attribute *attr, const char *buf, size_t size)
{
    int i;
    SSL_PRINT("***FwDataCnt = %d ***\n", FwDataCnt);

    for(i = 0; i < 1024; i++)
    {
        memcpy(temp[FwDataCnt], buf, 1024);
    }

    FwDataCnt++;
    return size;
}
static DEVICE_ATTR(data, 0777, firmware_data_show, firmware_data_store);
#endif

#ifdef TP_PROXIMITY_SENSOR
static ssize_t show_proximity_sensor(struct device *dev, struct device_attribute *attr, char *buf)
{
    static char temp=2;
        if (buf != NULL)
        sprintf(buf, "%dhl !!!\n", ps_data_state[0]);//mijianfeng //*buf = ps_data_state[0];
    if(temp!=*buf)
    {
    printk("proximity_sensor_show: buf=%d\n\n", *buf);
    temp=*buf;
    }

    return sizeof("hl\n") + 1;
}

static ssize_t store_proximity_sensor(struct device *dev, struct device_attribute *attr, const char *buf, size_t size)
{
    U8 ps_store_data[4];

    if(buf != NULL && size != 0)
    {
        if(DISABLE_CTP_PS == *buf)
        {
            printk("DISABLE_CTP_PS buf=%d,size=%d\n", *buf, size);
            ps_store_data[0] = 0x52;
            ps_store_data[1] = 0x00;
	    ps_store_data[2] = 0x78;  //0x62 modif by xuzhoubin
            ps_store_data[3] = 0xa1;
            i2c_write(TOUCH_ADDR_MSG20XX, &ps_store_data[0], 4);
            msleep(2000);
            printk("RESET_CTP_PS buf=%d\n", *buf);
            mt_eint_mask(CUST_EINT_TOUCH_PANEL_NUM);
            mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
            mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
            mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ZERO);
            msleep(100);
            mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
            //changed in 2012-07-07 by [Hally]
		#ifdef TPD_RSTPIN_1V8
    		mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_IN);
    		mt_set_gpio_pull_enable(GPIO_CTP_RST_PIN, GPIO_PULL_DISABLE);
		#else
			mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
    		mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ONE);
		#endif
            mdelay(500);
            mt_eint_unmask(CUST_EINT_TOUCH_PANEL_NUM);
        }
        else if(ENABLE_CTP_PS == *buf)
        {
            printk("ENABLE_CTP_PS buf=%d,size=%d\n", *buf, size);
            ps_store_data[0] = 0x52;
            ps_store_data[1] = 0x00;
	    ps_store_data[2] = 0x78; //0x62 modif by xuzhoubin
            ps_store_data[3] = 0xa0;
            i2c_write(TOUCH_ADDR_MSG20XX, &ps_store_data[0], 4);
        }
                tp_proximity_state = *buf;
    }

    return size;
}
static DEVICE_ATTR(proximity_sensor, 0777, show_proximity_sensor, store_proximity_sensor);
#endif

static struct i2c_driver tpd_i2c_driver =
{
    .driver = {
        .name = TPD_DEVICE,
        .owner = THIS_MODULE,
    },
    .probe = tpd_probe,
    .remove = __devexit_p(tpd_remove),
    .id_table = tpd_id,
    .detect = tpd_detect,
   // .address_data = &addr_data,
   .address_list = (const unsigned short *) forces,
};

static  void tpd_down(int x, int y)
{
    input_report_abs(tpd->dev, ABS_PRESSURE, 128);
    input_report_key(tpd->dev, BTN_TOUCH, 1);
    input_report_abs(tpd->dev, ABS_MT_TOUCH_MAJOR, 128);
    input_report_abs(tpd->dev, ABS_MT_WIDTH_MAJOR, 128);
    input_report_abs(tpd->dev, ABS_MT_POSITION_X, x);
    input_report_abs(tpd->dev, ABS_MT_POSITION_Y, y);
    input_mt_sync(tpd->dev);
    SSL_PRINT("MSG2133 D[%4d %4d %4d] ", x, y,1);
    printk("MSG2133 D[%4d %4d %4d] ", x, y,1);
}

static void tpd_up(int x, int y)
{
    input_report_abs(tpd->dev, ABS_PRESSURE, 0);
    input_report_key(tpd->dev, BTN_TOUCH, 0);
    input_report_abs(tpd->dev, ABS_MT_TOUCH_MAJOR, 0);
    input_report_abs(tpd->dev, ABS_MT_WIDTH_MAJOR, 0);
    input_report_abs(tpd->dev, ABS_MT_POSITION_X, x);
    input_report_abs(tpd->dev, ABS_MT_POSITION_Y, y);
    input_mt_sync(tpd->dev);
    SSL_PRINT("MSG2133 U[%4d %4d %4d] ", x, y, 0);
    printk("MSG2133 U[%4d %4d %4d] ", x, y, 0);

}


unsigned char tpd_check_sum(unsigned char *pval)
{
    int i, sum = 0;

    for(i = 0; i < 7; i++)
    {
        sum += pval[i];
    }

    return (unsigned char)((-sum) & 0xFF);
}

static bool msg2033_i2c_read(char *pbt_buf, int dw_lenth)
{
    int ret;
    //SSL_PRINT("The msg_i2c_client->addr=0x%x i2c_client->timing=%d\n",i2c_client->addr,i2c_client->timing);
    i2c_client->addr|=I2C_ENEXT_FLAG;
    ret = i2c_master_recv(i2c_client, pbt_buf, dw_lenth);
    //SSL_PRINT("msg_i2c_read_interface ret=%d pbt_buf[0]=0x%x pbt_buf[1]=0x%x pbt_buf[2]=0x%x pbt_buf[3]=0x%x pbt_buf[4]=0x%x pbt_buf[5]=0x%x pbt_buf[6]=0x%x pbt_buf[7]=0x%x \n",ret,pbt_buf[0],pbt_buf[1],pbt_buf[2],pbt_buf[3],pbt_buf[4],pbt_buf[5],pbt_buf[6],pbt_buf[7]);

    if(ret <= 0)
    {
        SSL_PRINT("msg_i2c_read_interface error\n");
        return false;
    }

    return true;
}

static int tpd_touchinfo(struct touch_info *cinfo)
{
    SHORT_TOUCH_STATE ShortTouchState;
    BYTE reg_val[8] = {0};
    unsigned int  temp = 0;
    u32 bitmask;
    int i;

    if(update_switch)
    {
        return false;
    }

    msg2033_i2c_read(reg_val, 8);
    SSL_PRINT("received raw data from touch panel as following in fun %s\n", __func__);

    ShortTouchState.pos_x = ((reg_val[1] & 0xF0) << 4) | reg_val[2];
    ShortTouchState.pos_y = ((reg_val[1] & 0x0F) << 8) | reg_val[3];
    ShortTouchState.dst_x = ((reg_val[4] & 0xF0) << 4) | reg_val[5];
    ShortTouchState.dst_y = ((reg_val[4] & 0x0F) << 8) | reg_val[6];

    if((ShortTouchState.dst_x) & 0x0800)
    {
        ShortTouchState.dst_x |= 0xF000;
    }

    if((ShortTouchState.dst_y) & 0x0800)
    {
        ShortTouchState.dst_y |= 0xF000;
    }

    ShortTouchState.pos_x2 = ShortTouchState.pos_x + ShortTouchState.dst_x;
    ShortTouchState.pos_y2 = ShortTouchState.pos_y + ShortTouchState.dst_y;
    SSL_PRINT("MSG_ID=0x%x,X= %d ,Y=%d\n", reg_val[0], ShortTouchState.pos_x, ShortTouchState.pos_x);
    temp = tpd_check_sum(reg_val);
    SSL_PRINT("check_sum=%d,reg_val_7=%d\n", temp, reg_val[7]);

    if(temp == reg_val[7])
    {
        SSL_PRINT("TP_PS \nreg_val[1]=0x%x\nreg_val[2]=0x%x\nreg_val[3]=0x%x\nreg_val[4]=0x%x\nreg_val[5]=0x%x\nreg_val[6]=0x%x\nreg_val[7]=0x%x by cheehwa\n", reg_val[1], reg_val[2], reg_val[3], reg_val[4], reg_val[5], reg_val[6], reg_val[7]);

        if(reg_val[0] == 0x52) //CTP  ID
        {
#if (defined(TPD_HAVE_BUTTON) && !defined(SIMULATED_BUTTON))
	      if(reg_val[1] == 0xFF && reg_val[2] == 0xFF&& reg_val[3] == 0xFF && reg_val[4] == 0xFF)  //modif by xuzhoubin  add && reg_val[4] == 0xFF
	     {
		 	if(reg_val[5] == 0x0||reg_val[5] == 0xFF)
			{
				i = TPD_KEY_COUNT;
			}
		#ifdef TP_PROXIMITY_SENSOR
				else if(reg_val[5] == 0x80) // close to
				{
					SSL_PRINT("TP_PROXIMITY_SENSOR VVV by cheehwa\n");
					ps_data_state[0] = 1;
				}
				else if(reg_val[5] == 0x40) // leave
				{
					SSL_PRINT("TP_PROXIMITY_SENSOR XXX by cheehwa\n");
					ps_data_state[0] = 0;
				}
		#endif
			else
			{
				for(i=0;i<TPD_KEY_COUNT;i++)
				{
					bitmask=1<<i;
					if(reg_val[5] & bitmask)
					{ // button[i] on
						if(msg2133_priv.buttons & bitmask)
						{	//button[i] still on
							//repeat button[i]
							SSL_PRINT("Button%d repeat.\n",i);
						}
						else
						{
							//button[i] down
							SSL_PRINT("Button%d down.\n",i);
							cinfo->x[0]  = tpd_keys_dim_local[i][0];
							cinfo->y[0]=  tpd_keys_dim_local[i][1];
							point_num = 1;
							break;
						}
					}
					else
					{
						if(msg2133_priv.buttons & bitmask)
						{
							//button[i] up
							SSL_PRINT("Button%d up.\n",i);
						}
					}
				}
			}
			if(i == TPD_KEY_COUNT)
			{
				point_num = 0;
			}

			msg2133_priv.buttons = reg_val[5];
	     }
#else
	     if(reg_val[1] == 0xFF && reg_val[2] == 0xFF&& reg_val[3] == 0xFF&& reg_val[4] == 0xFF&& reg_val[5] == 0xFF && reg_val[6] == 0xFF)
            {
		  	point_num = 0;
	     }
#endif
            else if((ShortTouchState.dst_x == 0) && (ShortTouchState.dst_y == 0))
            {
		  #if defined(TPD_XY_INVERT)

				  #if defined(TPD_X_INVERT)
		                cinfo->x[0] = (2048-ShortTouchState.pos_y) * TPD_RES_X / 2048;
				  #else
				  		cinfo->x[0] = (ShortTouchState.pos_y) * TPD_RES_X / 2048;
				  #endif

				  #if defined(TPD_Y_INVERT)
		                cinfo->y[0] = (2048-ShortTouchState.pos_x) * TPD_RES_Y / 2048;
				  #else
		                cinfo->y[0] = ShortTouchState.pos_x * TPD_RES_Y / 2048;
				  #endif

		  #else

				  #if defined(TPD_X_INVERT)
		                cinfo->x[0] = (2048-ShortTouchState.pos_x) * TPD_RES_X / 2048;
				  #else
		                cinfo->x[0] = ShortTouchState.pos_x * TPD_RES_X / 2048;
				  #endif

				  #if defined(TPD_Y_INVERT)
		                cinfo->y[0] = (2048-ShortTouchState.pos_y) * TPD_RES_Y / 2048;
				  #else
		                cinfo->y[0] = ShortTouchState.pos_y * TPD_RES_Y / 2048;
				  #endif

		  #endif
                point_num = 1;
            }
            else
            {
                #if defined(TPD_XY_INVERT)

				  #if defined(TPD_X_INVERT)
				   		cinfo->x[0] = (2048-ShortTouchState.pos_y) * TPD_RES_X / 2048;
				  #else
		                cinfo->x[0] = ShortTouchState.pos_y * TPD_RES_X / 2048;
				  #endif

				  #if defined(TPD_Y_INVERT)
		                cinfo->y[0] = (2048-ShortTouchState.pos_x) * TPD_RES_Y / 2048;
				  #else
		                cinfo->y[0] = ShortTouchState.pos_x * TPD_RES_Y / 2048;
				  #endif

				  #if defined(TPD_X_INVERT)
		                cinfo->x[1] = (2048-ShortTouchState.pos_y2) * TPD_RES_X / 2048;
				  #else
		                cinfo->x[1] = ShortTouchState.pos_y2 * TPD_RES_X / 2048;
				  #endif

				  #if defined(TPD_Y_INVERT)
		                cinfo->y[1] = (2048-ShortTouchState.pos_x2) * TPD_RES_Y / 2048;
				  #else
		                cinfo->y[1] = ShortTouchState.pos_x2 * TPD_RES_Y / 2048;
				  #endif

		  #else

				  #if defined(TPD_X_INVERT)
				  		cinfo->x[0] = (2048-ShortTouchState.pos_x) * TPD_RES_X / 2048;
				  #else
		                cinfo->x[0] = ShortTouchState.pos_x * TPD_RES_X / 2048;
				  #endif

				  #if defined(TPD_Y_INVERT)
		                cinfo->y[0] = (2048-ShortTouchState.pos_y) * TPD_RES_Y / 2048;
				  #else
		                cinfo->y[0] = ShortTouchState.pos_y * TPD_RES_Y / 2048;
				  #endif

				  #if defined(TPD_X_INVERT)
		                cinfo->x[1] = (2048-ShortTouchState.pos_x2) * TPD_RES_X / 2048;
				  #else
		                cinfo->x[1] = ShortTouchState.pos_x2 * TPD_RES_X / 2048;
				  #endif

				  #if defined(TPD_Y_INVERT)
		                cinfo->y[1] = (2048-ShortTouchState.pos_y2) * TPD_RES_Y / 2048;
				  #else
		                cinfo->y[1] = ShortTouchState.pos_y2 * TPD_RES_Y / 2048;
				  #endif

		  #endif
                point_num = 2;
            }
        }
        return true;
    }
    else
    {
        SSL_PRINT("tpd_check_sum_ XXXX\n");
        return  false;
    }

}

static int touch_event_handler(void *unused)
{
    struct touch_info cinfo;
    int touch_state = 3;
    unsigned long time_eclapse;
    struct sched_param param = { .sched_priority = RTPM_PRIO_TPD };
    sched_setscheduler(current, SCHED_RR, &param);

    do
    {
        mt_eint_unmask(CUST_EINT_TOUCH_PANEL_NUM);
        set_current_state(TASK_INTERRUPTIBLE);
        wait_event_interruptible(waiter, tpd_flag != 0);
        tpd_flag = 0;
        set_current_state(TASK_RUNNING);

        if(tpd_touchinfo(&cinfo))
        {
            SSL_PRINT("MSG_X = %d,MSG_Y = %d\n", cinfo.x[0], cinfo.y[0]);

            if(point_num == 1)
            {
                tpd_down(cinfo.x[0], cinfo.y[0]);
                SSL_PRINT("msg_press 1 point--->\n");
                input_sync(tpd->dev);
            }
            else if(point_num == 2)
            {
                //SSL_PRINT("MSG_X2 = %d,MSG_Y2 = %d\n", cinfo.x[1], cinfo.y[1]);
                tpd_down(cinfo.x[0], cinfo.y[0]);
                tpd_down(cinfo.x[1], cinfo.y[1]);
                SSL_PRINT("msg_press 2 points--->\n");
                input_sync(tpd->dev);
            }
            else if(point_num == 0)
            {
                SSL_PRINT("release --->\n");
                input_mt_sync(tpd->dev);
                input_sync(tpd->dev);
            }
        }
    }
    while(!kthread_should_stop());

    return 0;
}

static int tpd_detect(struct i2c_client *client, int kind, struct i2c_board_info *info)
{
    strcpy(info->type, TPD_DEVICE);
    return 0;
}

static void tpd_eint_interrupt_handler(void)
{
    //printk("MSG2133 TPD interrupt has been triggered\n");
    tpd_flag = 1;
    wake_up_interruptible(&waiter);
}

static int __devinit tpd_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    int retval = TPD_OK;
	int err = 0;
	BYTE reg_val[8] = {0};
    client->addr |= I2C_ENEXT_FLAG;
    i2c_client = client;
    printk("In tpd_probe_ ,the i2c addr=0x%x", client->addr);

	msleep(100);///////////////////////////////////////
	hwPowerOn(TPD_POWER_SOURCE,VOL_2800,"TP");
	hwPowerOn(TPD_POWER_IO,VOL_1800,"TP");

		 mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
		 mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
		 mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ZERO);
		 msleep(10);
		 mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
		 mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_IN);
		 mt_set_gpio_pull_enable(GPIO_CTP_RST_PIN, GPIO_PULL_DISABLE);
        msleep(250);

	msleep(100);

    mt_set_gpio_mode(GPIO_CTP_EINT_PIN, GPIO_CTP_EINT_PIN_M_EINT);
    mt_set_gpio_dir(GPIO_CTP_EINT_PIN, GPIO_DIR_IN);
    mt_set_gpio_pull_enable(GPIO_CTP_EINT_PIN, GPIO_PULL_ENABLE);
    mt_set_gpio_pull_select(GPIO_CTP_EINT_PIN, GPIO_PULL_UP);

    //mt_eint_set_sens(CUST_EINT_TOUCH_PANEL_NUM, CUST_EINT_TOUCH_PANEL_SENSITIVE);
    //mt_eint_set_hw_debounce(CUST_EINT_TOUCH_PANEL_NUM, CUST_EINT_TOUCH_PANEL_DEBOUNCE_CN);
    mt_eint_registration(CUST_EINT_TOUCH_PANEL_NUM, EINTF_TRIGGER_FALLING, tpd_eint_interrupt_handler, 1);
    mt_eint_unmask(CUST_EINT_TOUCH_PANEL_NUM);
    msleep(100);

    err=msg2033_i2c_read(reg_val, 1);
	if(!err)
	{
        printk("msg_i2c_interface error\n");
		return -1;
	}
    tpd_load_status = 1;
    thread = kthread_run(touch_event_handler, 0, TPD_DEVICE);

    if(IS_ERR(thread))
    {
        retval = PTR_ERR(thread);
        SSL_PRINT(TPD_DEVICE " failed to create kernel thread: %d\n", retval);
    }

#ifdef TP_FIRMWARE_UPDATE
#if ENABLE_DMA
    gpDMABuf_va = (u8 *)dma_alloc_coherent(NULL, 4096, &gpDMABuf_pa, GFP_KERNEL);

    if(!gpDMABuf_va)
    {
        printk("[MATV][Error] Allocate DMA I2C Buffer failed!\n");
    }

#endif
    firmware_class = class_create(THIS_MODULE, "ms-touchscreen-msg20xx");

    if(IS_ERR(firmware_class))
    {
        pr_err("Failed to create class(firmware)!\n");
    }

    firmware_cmd_dev = device_create(firmware_class,
                                     NULL, 0, NULL, "device");

    if(IS_ERR(firmware_cmd_dev))
    {
        pr_err("Failed to create device(firmware_cmd_dev)!\n");
    }

    // version
    if(device_create_file(firmware_cmd_dev, &dev_attr_version) < 0)
    {
        pr_err("Failed to create device file(%s)!\n", dev_attr_version.attr.name);
    }

    // update
    if(device_create_file(firmware_cmd_dev, &dev_attr_update) < 0)
    {
        pr_err("Failed to create device file(%s)!\n", dev_attr_update.attr.name);
    }

    // data
    if(device_create_file(firmware_cmd_dev, &dev_attr_data) < 0)
    {
        pr_err("Failed to create device file(%s)!\n", dev_attr_data.attr.name);
    }

	//clear
    if(device_create_file(firmware_cmd_dev, &dev_attr_clear) < 0)
    {
        pr_err("Failed to create device file(%s)!\n", dev_attr_clear.attr.name);
    }

#endif
#ifdef TP_PROXIMITY_SENSOR

    if(device_create_file(firmware_cmd_dev, &dev_attr_proximity_sensor) < 0) // /sys/class/mtk-tpd/device/proximity_sensor
    {
        pr_err("Failed to create device file(%s)!\n", dev_attr_proximity_sensor.attr.name);
    }

#endif
    //SSL_PRINT("create device file:%s\n", dev_attr_proximity_sensor.attr.name);
    printk("Touch Panel Device Probe %s\n", (retval < TPD_OK) ? "FAIL" : "PASS");
    return 0;
}

static int __devexit tpd_remove(struct i2c_client *client)

{
    printk("TPD removed\n");
#ifdef TP_FIRMWARE_UPDATE
#if ENABLE_DMA

    if(gpDMABuf_va)
    {
        dma_free_coherent(NULL, 4096, gpDMABuf_va, gpDMABuf_pa);
        gpDMABuf_va = NULL;
        gpDMABuf_pa = NULL;
    }

#endif
#endif
    return 0;
}


static int tpd_local_init(void)
{
    printk(" MSG2033 I2C Touchscreen Driver (Built %s @ %s)\n", __DATE__, __TIME__);

    if(i2c_add_driver(&tpd_i2c_driver) != 0)
    {
        printk("unable to add i2c driver.\n");
        return -1;
    }

#ifdef TPD_HAVE_BUTTON
    tpd_button_setting(TPD_KEY_COUNT, tpd_keys_local, tpd_keys_dim_local);// initialize tpd button data
#endif
#if (defined(TPD_WARP_START) && defined(TPD_WARP_END))
    TPD_DO_WARP = 1;
    memcpy(tpd_wb_start, tpd_wb_start_local, TPD_WARP_CNT * 4);
    memcpy(tpd_wb_end, tpd_wb_start_local, TPD_WARP_CNT * 4);
#endif
#if (defined(TPD_HAVE_CALIBRATION) && !defined(TPD_CUSTOM_CALIBRATION))
    memcpy(tpd_calmat, tpd_def_calmat_local, 8 * 4);
    memcpy(tpd_def_calmat, tpd_def_calmat_local, 8 * 4);
#endif
    SSL_PRINT("end %s, %d\n", __FUNCTION__, __LINE__);
    tpd_type_cap = 1;
    msg2133_priv.buttons=0;
    return 0;
}

static int tpd_resume(struct i2c_client *client)
{
    int retval = TPD_OK;
    printk("TPD wake up\n");
#ifdef TPD_CLOSE_POWER_IN_SLEEP
    hwPowerOn(TPD_POWER_SOURCE, VOL_2800, "TP");
    hwPowerOn(TPD_POWER_IO, VOL_1800, "TP");
#else
	mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
	//changed in 2012-07-07 by [Hally]
#ifdef TPD_RSTPIN_1V8
    mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_IN);
    mt_set_gpio_pull_enable(GPIO_CTP_RST_PIN, GPIO_PULL_DISABLE);
#else
	mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
    mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ONE);
#endif
#endif
    msleep(300);													//changed in 2012-07-07 by [Hally]
    mt_eint_unmask(CUST_EINT_TOUCH_PANEL_NUM);
    return retval;
}

static int tpd_suspend(struct i2c_client *client, pm_message_t message)
{
    int retval = TPD_OK;
    static char data = 0x3;
    printk("TPD enter sleep\n");

#ifdef TP_PROXIMITY_SENSOR
        if (tp_proximity_state == ENABLE_CTP_PS){
            // resume ......
            return retval;
        }
#endif

    mt_eint_mask(CUST_EINT_TOUCH_PANEL_NUM);
    mt_set_gpio_mode(GPIO_CTP_RST_PIN, GPIO_CTP_RST_PIN_M_GPIO);
    mt_set_gpio_dir(GPIO_CTP_RST_PIN, GPIO_DIR_OUT);
    mt_set_gpio_out(GPIO_CTP_RST_PIN, GPIO_OUT_ZERO);

#ifdef TPD_CLOSE_POWER_IN_SLEEP
    hwPowerDown(TPD_POWER_SOURCE, "TP");
    hwPowerDown(TPD_POWER_IO, "TP");
#else
    //i2c_smbus_write_i2c_block_data(i2c_client, 0xA5, 1, &data);  //TP enter sleep mode
    //mt_set_gpio_mode(GPIO_CTP_EN_PIN, GPIO_CTP_EN_PIN_M_GPIO);
    //mt_set_gpio_dir(GPIO_CTP_EN_PIN, GPIO_DIR_OUT);
    //mt_set_gpio_out(GPIO_CTP_EN_PIN, GPIO_OUT_ZERO);
#endif
    return retval;
}


static struct tpd_driver_t tpd_device_driver =
{
    .tpd_device_name = "MSG2033",
    .tpd_local_init = tpd_local_init,
    .suspend = tpd_suspend,
    .resume = tpd_resume,
#ifdef TPD_HAVE_BUTTON
    .tpd_have_button = 1,
#else
    .tpd_have_button = 0,
#endif
};
/* called when loaded into kernel */
static int __init tpd_driver_init(void)
{
    printk("MediaTek MSG2033 touch panel driver init\n");
    printk("\n\n++++++[%s]++++++++[%s]+++++++[%d]\n\n",__FILE__,__FUNCTION__,__LINE__);
#if defined(TPD_I2C_NUMBER)
		i2c_register_board_info(TPD_I2C_NUMBER, &i2c_tpd, 1);
#else
			i2c_register_board_info(0, &i2c_tpd, 1);
#endif
    if(tpd_driver_add(&tpd_device_driver) < 0)
    {
        printk("add MSG2033 driver failed\n");
    }

    return 0;
}

/* should never be called */
static void __exit tpd_driver_exit(void)
{
    printk("MediaTek MSG2033 touch panel driver exit\n");
    //input_unregister_device(tpd->dev);
    tpd_driver_remove(&tpd_device_driver);
}

module_init(tpd_driver_init);
module_exit(tpd_driver_exit);


