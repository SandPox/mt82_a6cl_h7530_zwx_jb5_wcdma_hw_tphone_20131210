/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef TOUCHPANEL_H__
#define TOUCHPANEL_H__

/* Pre-defined definition */
#define TPD_TYPE_CAPACITIVE
//#define TPD_TYPE_RESISTIVE
#define TPD_POWER_SOURCE         MT6323_POWER_LDO_VGP1
#define TPD_POWER_SOURCE_1800 	MT6323_POWER_LDO_VGP3

#define TPD_I2C_NUMBER           0
#define TPD_WAKEUP_TRIAL         60
#define TPD_WAKEUP_DELAY         100

//cyc start
#define TPD_POWER_IO          MT6323_POWER_LDO_VGP3
//cyc end

#define TPD_DELAY                (2*HZ/100)

#if (defined(FWVGA))

#define TPD_X_RES (480)
#define TPD_Y_RES (854)

#elif(defined(QHD))

#define TPD_X_RES (540)
#define TPD_Y_RES (960)

#elif(defined(LQHD))

#define TPD_X_RES (640)
#define TPD_Y_RES (960)

#elif defined(HD)

#define TPD_X_RES (720)
#define TPD_Y_RES (1280)

#elif defined(WVGA)

#define TPD_X_RES (480)
#define TPD_Y_RES (800)

#elif defined(HVGA)

#define TPD_X_RES (320)
#define TPD_Y_RES (480)

#else

#define TPD_X_RES (480)
#define TPD_Y_RES (800)

#endif

#define TPD_CALIBRATION_MATRIX  {-256, 0, 1310720, 0, 287, 0,0,0};

#define TPD_HAVE_CALIBRATION

#define TPD_HAVE_TREMBLE_ELIMINATION

#define TPD_HAVE_POWER_ON_OFF

#define PRESSURE_FACTOR	10

#define TPD_HAVE_BUTTON
#define TPD_BUTTON_HEIGHT	810

#define TPD_KEY_COUNT           4
#define TPD_KEYS                {KEY_MENU, KEY_HOME, KEY_BACK, KEY_SEARCH}
#define TPD_KEYS_DIM            {{60,TPD_Y_RES+50,60,50},{180,TPD_Y_RES + 50,60,50},{300,TPD_Y_RES + 50,60,50},{420,TPD_Y_RES + 50,60,50}}
//Add in 2012-07-07 by [Hally]
#define	TPD_XY_INVERT		//交换X和Y
//#define	TPD_X_INVERT		//X翻转(对称)
//#define	TPD_Y_INVERT		//Y翻转(对称)

//Add in 2012-07-07 by [Hally]
//#define	TPD_RSTPIN_1V8		//RESET PIN为1.8V时，要打开这个

#endif /* TOUCHPANEL_H__ */
