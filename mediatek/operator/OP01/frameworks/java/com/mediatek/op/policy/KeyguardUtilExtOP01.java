
package com.mediatek.op.policy;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.mediatek.common.policy.IKeyguardUtilExt;
import com.mediatek.common.policy.INewEventController;

/**
 * Interface that defines all methos which are implemented in ConnectivityService
 * {@hide} 
 */
public class KeyguardUtilExtOP01 implements IKeyguardUtilExt
{
    private static final String TAG = "KeyguardUtilExtOP";

    private static final boolean DEBUG = false;

    public KeyguardUtilExtOP01(){
        if (DEBUG) Log.d(TAG, "OP01 constructor");
    }
            
    public boolean shouldShowEmergencyBtnForVoiceOn() {
        if (DEBUG) Log.d(TAG, "shouldShowEmergencyBtnForVoiceOn return false");
        return true;
    }
    
    public void updateNewEventControllerVisibility(View viewContainer, int newEventViewId) {
        View newEventView = (View)viewContainer.findViewById(newEventViewId);
        if (newEventView != null) {
            INewEventController controller = (INewEventController)newEventView;
            controller.setUpdateEnabled(true); 
            controller.updateNewEvent();
        }
    }

    public boolean needShowPassToast() {
        if (DEBUG) Log.d(TAG, "needshowPassToast return false");
        return false;
    }
}
