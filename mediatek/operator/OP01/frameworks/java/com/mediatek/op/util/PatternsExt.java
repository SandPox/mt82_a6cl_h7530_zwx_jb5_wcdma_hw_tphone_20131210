package com.mediatek.op.util;

import com.mediatek.op.util.DefaultPatterns;

import java.util.regex.Pattern;

public class PatternsExt extends DefaultPatterns {

    public static Pattern getWebUrl(final String engIriChar, final String goodIriChar,
            final String topLevelDomainStrForWebUrlExpand) {
            return Pattern.compile(
                    "((?:(ftp|http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
                    + "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
                    + "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
                    + "((?:(?:[" + engIriChar + "][" + engIriChar + "\\-]{0,64}\\.)+"   // named host
                    + topLevelDomainStrForWebUrlExpand
                    + "|(?:(?:25[0-5]|2[0-4]" // or ip address
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]"
                    + "|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9])))"
                    + "(?:\\:\\d{1,5})?)" // plus option port number
                    + "(\\/(?:(?:[" + goodIriChar + "\\;\\/\\?\\:\\@\\&\\=\\#\\~"  // plus option query params
                    + "\\-\\.\\+\\!\\*\\'\\(\\)\\_])|(?:\\%[a-fA-F0-9]{2}))*)?",   //change for OP01 URL rule.
                    Pattern.CASE_INSENSITIVE); /// M: ignore case
        
    }

}
