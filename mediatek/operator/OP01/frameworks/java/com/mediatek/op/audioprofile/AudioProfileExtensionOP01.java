package com.mediatek.op.audioprofile;

import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;

import com.mediatek.audioprofile.AudioProfileManager;
import com.mediatek.audioprofile.AudioProfileService;
import com.mediatek.audioprofile.AudioProfileState;
import com.mediatek.audioprofile.AudioProfileManager.ProfileSettings;
import com.mediatek.audioprofile.AudioProfileManager.Scenario;
import com.mediatek.common.audioprofile.IAudioProfileService;

public class AudioProfileExtensionOP01 extends DefaultAudioProfileExtension {
    private static final String TAG = "AudioProfileExtension_Op01";
    private AudioProfileService mService;
    private Context mContext;
    private ContentResolver mContentResolver;
    private AudioManager mAudioManager;

    @Override
    public void init(IAudioProfileService service, Context context) {
        // 1. retain the type cast at present, for if I won't do this, public methods would be
        // declared in aidl
        // and furthermore, I had to declare aidl for public methods related types (eg.
        // AudioProfileState)
        // 2. another way to avoid type cast is declare another abstract class extending from
        // IAudioProfileService.stub
        // in the common package
        mService = (AudioProfileService) service;
        mContext = context;
        mContentResolver = mContext.getContentResolver();
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public boolean onNotificationChange(boolean selfChange) {
        String activeProfileKey = mService.getActiveProfileKey();
        Scenario activeScenario = AudioProfileManager.getScenario(activeProfileKey);
        AudioProfileState activeState = mService.getProfileState(activeProfileKey);
        if (!Scenario.SILENT.equals(activeScenario)) {
            // If notification has been changed and the active profile is non-silent
            // profile, synchronize the current system notification to active profile.
            String uriString = Settings.System.getString(mContentResolver, Settings.System.NOTIFICATION_SOUND);
            Uri systemUri = (uriString == null ? AudioProfileService.SILENT_NOTIFICATION_URI : Uri.parse(uriString));

            if ((activeState.mNotificationStream == null && systemUri != null)
                    || (activeState.mNotificationStream != null && !activeState.mNotificationStream.equals(systemUri))) {
                activeState.mNotificationStream = systemUri;
                mService.persistRingtoneUriToDatabase(mService.getActiveProfileKey(), AudioProfileManager.TYPE_NOTIFICATION,
                        systemUri);
                Log.d(TAG, "Notification changed by other app in non-silent profile,"
                        + " synchronize to active profile: new uri = " + systemUri);
            } else {
                Log.d(TAG, "Notification changed by itself, do nothing!");
            }
        } else {
            mService.setShouldSyncToSystemFlag(AudioProfileManager.TYPE_NOTIFICATION, true);
            Log.d(TAG, "Notification changed in silent profile, need sync to system when switch to last active profile.");
        }
        return true;
    }

    @Override
    public boolean onRingerModeChanged(int newRingerMode) {
        String activeProfileKey = mService.getActiveProfileKey();
        Scenario activeScenario = AudioProfileManager.getScenario(activeProfileKey);
        Log.d(TAG, "CMCC: onRingerModeChanged: ringermode changed by other app," + " change profile! ringerMode = "
                + newRingerMode);
        switch (newRingerMode) {
            case AudioManager.RINGER_MODE_SILENT:
            case AudioManager.RINGER_MODE_VIBRATE:
                String silentProfileKey = AudioProfileManager.getProfileKey(Scenario.SILENT);
                // When ringermode change in silent mode, we should set vibration setting to match
                // current mode.
                // Silent is disabled and Vibrate is enabled, beside we need not to set ringermode again.
                mService.setVibrationEnabled(silentProfileKey, newRingerMode == AudioManager.RINGER_MODE_VIBRATE, false);

                // RingerMode has been changed to be silent or vibrate, if profile
                // is not silent, change active to silent.
                if (!Scenario.SILENT.equals(activeScenario)) {
                    Log.d(TAG, "CMCC: RingerMode change to SILENT or VIBRATE change profile to silent!");
                    mService.setActiveProfile(silentProfileKey, false);
                }
                break;

            case AudioManager.RINGER_MODE_NORMAL:
                // RingerMode has been changed to be normal, if profile is silent,
                // set active to last active profile.
                if (Scenario.SILENT.equals(activeScenario)) {
                    Log.d(TAG, "CMCC: RingerMode change to NORMAL," + " change profile to last active profile!");
                    mService.setActiveProfile(mService.getLastActiveProfileKey(), false);
                }
                break;

            default:
                Log.e(TAG, "CMCC: undefined RingerMode!");
                break;
        }
        return true;
    }

    @Override
    public boolean onRingerVolumeChanged(int oldVolume, int newVolume, String extra) {
        String activeProfileKey = mService.getActiveProfileKey();
        AudioProfileState activeProfileState = mService.getProfileState(activeProfileKey);
        Scenario activeScenario = AudioProfileManager.getScenario(activeProfileKey);
        if ((!Scenario.SILENT.equals(activeScenario)) && (activeProfileState.mRingerVolume != newVolume)) {
            mService.notifyRingerVolumeChanged(oldVolume, newVolume, activeProfileKey);
            mService.syncRingerVolumeToProfile(activeProfileKey, newVolume);
            Log.d(TAG, "CMCC: onRingerVolumeChanged: ringer volume changed," + " sysn to active profile except silent!");
        }
        return true;
    }

    @Override
    public boolean onRingtoneChange(boolean selfChange) {
        String activeProfileKey = mService.getActiveProfileKey();
        Scenario activeScenario = AudioProfileManager.getScenario(activeProfileKey);
        AudioProfileState activeState = mService.getProfileState(activeProfileKey);
        if (!Scenario.SILENT.equals(activeScenario)) {
            // If ringtone has been changed and the active profile is non-silent profile,
            // synchronize the current system ringtone to active profile.
            String uriString = Settings.System.getString(mContentResolver, Settings.System.RINGTONE);
            Uri systemUri = (uriString == null ? null : Uri.parse(uriString));

            if ((activeState.mRingerStream == null && systemUri != null)
                    || (activeState.mRingerStream != null && !activeState.mRingerStream.equals(systemUri))) {
                activeState.mRingerStream = systemUri;
                mService.persistRingtoneUriToDatabase(activeProfileKey, AudioProfileManager.TYPE_RINGTONE, systemUri);
                Log.d(TAG, "Ringtone changed by other app in non-silent profile, synchronize to active profile: new uri = "
                        + systemUri);
            } else {
                Log.d(TAG, "Ringtone changed by itself, do nothing!");
            }
        } else {
            mService.setShouldSyncToSystemFlag(AudioProfileManager.TYPE_RINGTONE, true);
            Log.d(TAG, "Ringtone changed in silent profile, need sync to system when switch to last active profile.");
        }
        return true;
    }

    @Override
    public boolean persistStreamVolumeToSystem(int streamType) {
        int flags = 0;
        int volume = 0;
        String activeProfileKey = mService.getActiveProfileKey();
        AudioProfileState activeState = mService.getProfileState(activeProfileKey);
        switch (streamType) {
            case AudioProfileManager.STREAM_RING:
                volume = activeState.mRingerVolume;
                mAudioManager.setAudioProfileStreamVolume(AudioManager.STREAM_RING, volume, flags);
                break;

            case AudioProfileManager.STREAM_NOTIFICATION:
                volume = activeState.mNotificationVolume;
                mAudioManager.setAudioProfileStreamVolume(AudioManager.STREAM_NOTIFICATION, volume, flags);
                break;

            case AudioProfileManager.STREAM_ALARM:
                volume = activeState.mAlarmVolume;
                mAudioManager.setAudioProfileStreamVolume(AudioManager.STREAM_ALARM, volume, flags);
                break;

            default:
                Log.e(TAG, "CMCC: persistStreamVolumeToSystem with unsupport type!");
                break;
        }
        Log.d(TAG, "CMCC: persistStreamVolumeToSystem: streamType = " + streamType + ", volume = " + volume);
        return true;
    }

    @Override
    public IActiveProfileChangeInfo getActiveProfileChangeInfo(boolean shouldSetRingerMode, String oldProfileKey,
            String newProfileKey, boolean customActiveProfileDeleted) {
        Scenario newScenario = AudioProfileManager.getScenario(newProfileKey);
        Scenario oldScenario = AudioProfileManager.getScenario(oldProfileKey);
        ActiveProfileChangeInfo apcInfo = new ActiveProfileChangeInfo();
        // Set RingerMode to match different AudioProfiles to different RingerModes
        int ringerMode = mAudioManager.getRingerMode();

        switch (newScenario) {
            case SILENT:
                // Set RingerMode: if vibration is enabled, set to VIBRATE, is not
                // set to SILENT
                if (shouldSetRingerMode) {
                    if (mService.getProfileState(newProfileKey).mVibrationEnabled) {
                        apcInfo.mRingerModeToUpdate = AudioManager.RINGER_MODE_VIBRATE;
                    } else {
                        apcInfo.mRingerModeToUpdate = AudioManager.RINGER_MODE_SILENT;
                    }
                }
                // Persisted settings to override system
                apcInfo.mShouldSetLastActiveKey = true;
                apcInfo.mShouldOverrideSystem = false;
                break;

            default:
                // Set RingerMode:
                if (shouldSetRingerMode) {
                    if (ringerMode != AudioManager.RINGER_MODE_NORMAL) {
                        apcInfo.mRingerModeToUpdate = AudioManager.RINGER_MODE_NORMAL;
                    }
                }
                // Persisted settings to override system
                if (Scenario.SILENT.equals(oldScenario) && newProfileKey.equals(mService.getLastActiveProfileKey())) {
                    // If the new profile is same as last active profile when set silent
                    // to non-silent, we should not override system except last active
                    // profile key has been reset caused by custom active profile delete
                    apcInfo.mShouldOverrideSystem = customActiveProfileDeleted;

                    // In this case, when user changed the last active profile volumes
                    // and ringtones, we should persist it to system
                    apcInfo.mShouldSyncToSystem = true;
                }
                break;
        }

        return apcInfo;
    }

    @Override
    public boolean shouldCheckDefaultProfiles() {
        return IS_SUPPORT_OUTDOOR_EDITABLE;
    }

    @Override
    public boolean shouldSyncGeneralRingtoneToOutdoor() {
        return false;
    }
}
