package com.mediatek.settings.plugin;

import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;

import com.mediatek.settings.ext.DefaultApnSettingsExt;
import com.mediatek.xlog.Xlog;

public class ApnSettingsExt extends DefaultApnSettingsExt {
    
    private static final String TAG = "OP01ApnSettingsExt";
    
    public boolean isAllowEditPresetApn(String type, String apn, String numeric, int sourcetype) {
        boolean ret = true;
        boolean isCmccCard = numeric.equals("46000") || numeric.equals("46002") || numeric.equals("46007"); 
        if ("cmwap".equals(apn) && (!"mms".equals(type)) && isCmccCard) {
            ret = false;
         }
         return ret || (sourcetype != 0);

    }

}

