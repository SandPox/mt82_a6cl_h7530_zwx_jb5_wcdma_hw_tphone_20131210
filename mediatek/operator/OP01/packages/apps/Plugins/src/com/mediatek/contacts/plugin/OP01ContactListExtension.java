package com.mediatek.contacts.plugin;

//import android.content.Context;
//import android.os.ServiceManager;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.MenuItem;

import com.android.contacts.ext.ContactListExtension;
import com.android.contacts.ext.ContactPluginDefault;


public class OP01ContactListExtension extends ContactListExtension {
    private static final String TAG = "OP01ContactListExtension";
    
    @Override
    public String getCommand() {
        return ContactPluginDefault.COMMD_FOR_OP01;
    }
    
    @Override
    public void setMenuItem(MenuItem blockVoiceCallmenu, boolean mOptionsMenuOptions, String commd) {
        if (! ContactPluginDefault.COMMD_FOR_OP01.equals(commd)){
            return ;
        }
        Log.i(TAG, "[setMenuItem]");
        blockVoiceCallmenu.setVisible(false);
       
    }
    
//    @Override
//    public boolean [] setAllRejectedCall() {
//        ITelephony iTel = ITelephony.Stub.asInterface(ServiceManager
//                .getService(Context.TELEPHONY_SERVICE));
//        boolean [] result = null;
//        //try {
//            //result[0] = iTel.isRejectAllVoiceCall();
//            //result[1] = iTel.isRejectAllVideoCall();
//        //} catch (Exception e) {
//            //Log.i(TAG, "[setAllCall]: Exception=" + e.toString());
//        //}
//        Log.i(TAG, "[setAllCall] result is : " + result);
//        return result;
//    }

    @Override
    public void setLookSimStorageMenuVisible(MenuItem lookSimStorageMenu, boolean flag, String commd) {
        if (! ContactPluginDefault.COMMD_FOR_OP01.equals(commd)){
            return ;
        }
        Log.i(TAG, "PeopleActivity: [setLookSimStorageMenuVisible()]"); 
        if (flag) {
            lookSimStorageMenu.setVisible(true);
        } else {
            lookSimStorageMenu.setVisible(false);
        }
    }

    @Override
    public String getReplaceString(final String src, String commd) {
        if (! ContactPluginDefault.COMMD_FOR_OP01.equals(commd)){
            return null;
        }
        Log.i(TAG, "AbstractStartSIMService: [getReplaceString()]");
        return src.replace(PhoneNumberUtils.PAUSE, 'p').replace(PhoneNumberUtils.WAIT, 'w');
    }

}
