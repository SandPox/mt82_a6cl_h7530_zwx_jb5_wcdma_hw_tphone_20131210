#!/usr/bin/env python

import os
import sys
import shutil
import shlex
import subprocess


sys.path.insert(0, '.')

if __name__ == '__main__':
    while not os.path.exists('.repo'):
        if os.getcwd() == '/':
            print('I am not in a repo')
            sys.exit(1)
        os.chdir('..')

    tag = 'vendor/vanzo_custom/patch_done_tag.txt'
    if os.path.exists(tag):
        sys.exit(0)

    if os.path.exists('vanzo_team2'):
        shutil.rmtree('vanzo_team2')

    try:
        if not os.path.exists('vanzo_common2.py'):
            cmd = 'git clone vanzo:tools/vanzo_team2'
            subprocess.check_call(shlex.split(cmd))
            shutil.move('vanzo_team2/vanzo_common2.py', '.')

        from vanzo_common2 import do_update_overlay
        do_update_overlay()

        shutil.copy('build/tools/update_overlay_files.py', '.')
        subprocess.check_call(shlex.split('touch ' + tag))
    finally:
        if os.path.exists('vanzo_team2'):
            shutil.rmtree('vanzo_team2')
