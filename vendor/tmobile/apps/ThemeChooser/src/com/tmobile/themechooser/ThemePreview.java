/*
 * Copyright (C) 2010, T-Mobile USA, Inc.
 * This code has been modified.  Portions copyright (C) 2012, ParanoidAndroid Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tmobile.themechooser;

import java.util.ArrayList;
import java.util.HashMap;

import com.tmobile.themes.provider.Themes.ThemeColumns;
import com.tmobile.themechooser.ChangeThemeHelper;
import com.tmobile.themes.ThemeManager;
import com.tmobile.themes.provider.ThemeItem;
import com.tmobile.themes.provider.Themes;
import com.tmobile.themes.widget.ThemeAdapter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.view.animation.TranslateAnimation;

public class ThemePreview extends Activity implements OnClickListener {
    private static final String TAG = ThemePreview.class.getSimpleName();
    private static final int DIALOG_APPLY = 0;
    private static final int DIALOG_MISSING_HOST_DENSITY = 1;
    private static final int DIALOG_MISSING_THEME_PACKAGE_SCOPE = 2;
    private final ChangeThemeHelper mChangeHelper = new ChangeThemeHelper(this,
            DIALOG_APPLY);

    private static final int MENU_APPLY = Menu.FIRST;
    private static final int MENU_UNINSTALL = MENU_APPLY + 1;
    private static final int MENU_ABOUT = MENU_UNINSTALL + 1;
    private Button mApplyBut;
    private final int ANIM_DURATION = 500;
    private final int MSG_SINGLECLICK = 1;
    private final int MSG_DOUBLECLICK = 2;
    private static final int GET_CODE = 0;
    private static final int DOUBLE_CLICK_TIME = 350;
    private boolean mShowGlanceOver = true;
    private boolean mWaitDouble = true;
    private Button mApplyBtn, mBackBtn;

    private FrameLayout mPreviewLayout;
    private RelativeLayout mApplyLayout;
    private RelativeLayout mGlanceOverTitle;

    private ScrollLayout mScroll;
    private int mLastIdx = -1;
    private ArrayList<HashMap<String, Object>> mPreviewListItems;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.preview);

        this.overridePendingTransition(R.anim.push_in, R.anim.push_out);

        initialization();
        mChangeHelper.dispatchOnCreate();
    }

    public void initialization() {
        mPreviewListItems = new ArrayList<HashMap<String, Object>>();
        Intent intent = this.getIntent();
        int sumItems = intent.getIntExtra("sumItems", 0);
        int currPreviewItem = intent.getIntExtra("currPreviewItem", 0);
        for (int i = 0; i < sumItems; i++) {
            HashMap map = new HashMap<String, Object>();
            map.put("itemImgId", intent.getIntExtra("itemImgId" + i, -1));
            map.put("itemPkgName", intent.getStringExtra("itemPkgName" + i));
            map.put("itemName", intent.getStringExtra("itemName" + i));
            mPreviewListItems.add(map);
        }

        mBackBtn = (Button) findViewById(R.id.back_btn);

        mApplyBtn = (Button) findViewById(R.id.apply_btn);

        mApplyLayout = (RelativeLayout) findViewById(R.id.apply);
        mGlanceOverTitle = (RelativeLayout) findViewById(R.id.glanceover_title);
        mScroll = (ScrollLayout) findViewById(R.id.scroll);

        mPreviewLayout = (FrameLayout) findViewById(R.id.preview_layout);

        mBackBtn.setOnClickListener(this);
        mApplyBtn.setOnClickListener(this);
        mApplyLayout.setOnClickListener(this);
        mGlanceOverTitle.setOnClickListener(this);

        initScroll();

        genPagesDelayed(currPreviewItem);
        mScroll.setToScreen(currPreviewItem);
        mScroll.setPageListener(new PageListener());

    }

    private void genPagesDelayed(int idx) {
        if (mLastIdx == idx)
            return;
        // first create
        if (mLastIdx == -1) {
            genPage(idx - 1);
            genPage(idx);
            genPage(idx + 1);
        } else {
            if (idx > mLastIdx) {
                genPage(idx + 1);
            } else {
                genPage(idx - 1);
            }
        }
        mLastIdx = idx;
    }

    private void genPage(int idx) {
        if (idx < 0 || idx >= mPreviewListItems.size())
            return;
        HashMap map = new HashMap<String, Object>();
        map = mPreviewListItems.get(idx);
        int resId = (Integer) map.get("itemImgId");
        if (resId < 0) {
            return;
        }
        ((ImageView) mScroll.getChildAt(idx)).setImageResource(resId);
    }

    public void initScroll() {
        ImageView mScrollImg;
        for (int i = 0; i < mPreviewListItems.size(); i++) {
            mScrollImg = new ImageView(this);
            mScroll.addView(mScrollImg);
            mScrollImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    if (mWaitDouble == true) {
                        mWaitDouble = false;
                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(DOUBLE_CLICK_TIME);
                                    if (mWaitDouble == false) {
                                        mWaitDouble = true;
                                        Message message = new Message();
                                        message.what = MSG_SINGLECLICK;
                                        mHandler.sendMessage(message);
                                    }
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();
                    } else {
                        mWaitDouble = true;
                        Message message = new Message();
                        message.what = MSG_DOUBLECLICK;
                        mHandler.sendMessage(message);
                    }
                }
            });
        }
    }

    class PageListener implements ScrollLayout.PageListener {
        @Override
        public void page(int page) {
            genPagesDelayed(page);
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SINGLECLICK:
                    singleClick();
                    break;
                case MSG_DOUBLECLICK:
                    doubleClick();
                    break;
            }
        };
    };

    private void singleClick() {
        if (mShowGlanceOver) {
            showGlanceover();
        } else {
            showApply();
        }
    }

    private void doubleClick() {
    }

    public void showGlanceover() {
        Animation mApplyanimation = new TranslateAnimation(0.0f, 0.0f, 0.0f,
                mApplyLayout.getHeight());
        mApplyanimation.setDuration(ANIM_DURATION);
        mApplyanimation.setFillAfter(true);
        mApplyLayout.startAnimation(mApplyanimation);

        Animation mTitleanimation = new TranslateAnimation(0.0f, 0.0f, 0.0f,
                -mGlanceOverTitle.getHeight());
        mTitleanimation.setDuration(ANIM_DURATION);
        mTitleanimation.setFillAfter(true);
        mGlanceOverTitle.startAnimation(mTitleanimation);

        final Animation anim = AnimationUtils.loadAnimation(this,
                R.anim.push_in);
        mApplyBtn.setVisibility(View.GONE);
        mShowGlanceOver = false;
    }

    public void showApply() {
        Animation mApplyanimation = new TranslateAnimation(0.0f, 0.0f,
                mApplyLayout.getHeight(), 0.0f);
        mApplyanimation.setDuration(ANIM_DURATION);
        mApplyanimation.setFillAfter(true);
        mApplyLayout.startAnimation(mApplyanimation);

        Animation mTitleanimation = new TranslateAnimation(0.0f, 0.0f,
                -mGlanceOverTitle.getHeight(), 0.0f);
        mTitleanimation.setDuration(ANIM_DURATION);
        mTitleanimation.setFillAfter(true);
        mGlanceOverTitle.startAnimation(mTitleanimation);

        final Animation anim = AnimationUtils.loadAnimation(this,
                R.anim.push_out);
        mApplyBtn.setVisibility(View.VISIBLE);
        mShowGlanceOver = true;
    }

    private static class ViewHolder {
        public ImageView preview;

        public ViewHolder(View row) {
            preview = (ImageView) row.findViewById(R.id.theme_preview);
        }
    }

    public void exitAnimation() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.exit_anim);
        anim.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {

            }
        });
        anim.setFillAfter(true);
        mPreviewLayout.startAnimation(anim);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.apply_btn:
                if (mLastIdx >= 0 && mLastIdx < mPreviewListItems.size()) {
                    HashMap map = new HashMap<String, Object>();
                    String pkgName = "";
                    map = mPreviewListItems.get(mLastIdx);
                    pkgName = (String) map.get("itemPkgName");
                    setTheme(pkgName);
                }

                break;
            case R.id.glanceover_title:
                break;
            case R.id.apply:
                break;
            default:
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        boolean finishing = mChangeHelper
                .dispatchOnConfigurationChanged(newConfig);
        if (!finishing) {
            Bundle state = new Bundle();
            onSaveInstanceState(state);
            onRestoreInstanceState(state);
        }
    }

    @Override
    protected void onResume() {
        mChangeHelper.dispatchOnResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mChangeHelper.dispatchOnPause();
        super.onPause();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog.Builder builder;
        switch (id) {
            case DIALOG_MISSING_HOST_DENSITY:
                builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.dialog_theme_error_title);
                builder.setMessage(R.string.dialog_missing_host_density_msg);
                builder.setPositiveButton(R.string.dialog_apply_anyway_btn,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder.setNegativeButton(R.string.dialog_bummer_btn, null);
                return builder.create();
            case DIALOG_MISSING_THEME_PACKAGE_SCOPE:
                builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.dialog_theme_error_title);
                builder.setMessage(R.string.dialog_missing_theme_package_scope_msg);
                builder.setPositiveButton(android.R.string.ok, null);
                return builder.create();
            default:
                return mChangeHelper.dispatchOnCreateDialog(id);
        }
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        mChangeHelper.dispatchOnPrepareDialog(id, dialog);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return true;
    }

    private String getThemeName() {
        String themeName = "";
        HashMap map = new HashMap<String, Object>();
        if (mLastIdx >= 0 && mLastIdx < mPreviewListItems.size()) {
            map = mPreviewListItems.get(mLastIdx);
            themeName = (String) map.get("itemName");
        }

        return themeName;
    }

    public void setTheme(String pkgName) {
        String themePackage = "";
        String packageName = "";
        String appName = "";
        Uri uri = null;

        if (pkgName == null) {
            return;
        }

        if (pkgName.equals("com.android.systemui")) {
            pkgName = "";
        }

        ContentResolver cr = this.getContentResolver();

        Cursor cursor = cr.query(ThemeColumns.CONTENT_PLURAL_URI, null, null,
                null, ThemeColumns.NAME);

        cursor.moveToPosition(-1);
        while (cursor != null && cursor.moveToNext()) {
            packageName = cursor.getString(cursor
                    .getColumnIndex(ThemeColumns.THEME_PACKAGE));
            if (packageName.equals(pkgName)) {
                themePackage = cursor.getString(cursor
                        .getColumnIndex(ThemeColumns.THEME_PACKAGE));
                uri = Themes.getThemeUri(this, themePackage,
                        cursor.getString(cursor
                                .getColumnIndex(ThemeColumns.THEME_ID)));
                break;
            }
        }

        appName = getThemeName();
        mChangeHelper.beginChange(appName);
        if (getResources().getBoolean(R.bool.config_change_style_only)) {
            Themes.changeStyle(this, uri);
        } else {
            Themes.changeTheme(this, uri);
        }
        cursor.close();
    }

}
