package com.tmobile.themechooser;

import com.tmobile.themes.provider.Themes;
import com.tmobile.themes.provider.Themes.ThemeColumns;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class QuickThemeChooser extends Activity {

    private static final int DIALOG_APPLY = 0;
    private final ChangeThemeHelper mChangeHelper = new ChangeThemeHelper(this, DIALOG_APPLY);
    private LinearLayout mLayout;
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.dialog);
        mLayout = (LinearLayout) findViewById(R.id.main_layout);
        mChangeHelper.dispatchOnCreate();
        doApply();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        boolean finishing = mChangeHelper.dispatchOnConfigurationChanged(newConfig);
        if (!finishing) {
            Bundle state = new Bundle();
            onSaveInstanceState(state);
            onRestoreInstanceState(state);
        }
    }

    private void setBackground(String name) {
        if (name.equals("Pink")) {
            mLayout.setBackgroundResource(R.drawable.preview_pink);
        } else if (name.equals("Lights")) {
            mLayout.setBackgroundResource(R.drawable.preview_lights);
        } else if (name.equals("Sky")) {
            mLayout.setBackgroundResource(R.drawable.preview_sky);
        } else if (name.equals("System")) {
            mLayout.setBackgroundResource(R.drawable.preview_system);
        }
    }
    @Override
    protected void onResume() {
        mChangeHelper.dispatchOnResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mChangeHelper.dispatchOnPause();
        super.onPause();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        return mChangeHelper.dispatchOnCreateDialog(id);
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        mChangeHelper.dispatchOnPrepareDialog(id, dialog);
    }
    public void doApply() {
        String themePackage = null;
        String packageName = null;
        Uri uri = null;

        Cursor cursor = managedQuery(ThemeColumns.CONTENT_PLURAL_URI,
                                null, null, ThemeColumns.NAME);
        cursor.moveToPosition(-1);
        while (cursor != null && cursor.moveToNext()) {

            int applied = cursor.getInt(cursor.getColumnIndex(ThemeColumns.IS_APPLIED));
            if (applied == 1) {
                if (!cursor.moveToNext()) {
                   cursor.moveToFirst();
                }
                themePackage = cursor.getString(cursor.getColumnIndex(ThemeColumns.THEME_PACKAGE));
                packageName = cursor.getString(cursor.getColumnIndex(ThemeColumns.NAME));
                setBackground(packageName);
                uri = Themes.getThemeUri(this, themePackage,
                        cursor.getString(cursor.getColumnIndex(ThemeColumns.THEME_ID)));
                break;
            }
        }
        mChangeHelper.beginChange(packageName);
        if (getResources().getBoolean(R.bool.config_change_style_only)) {
            Themes.changeStyle(this, uri);
        } else {
            Themes.changeTheme(this, uri);
        }
        cursor.close();
    }
}
