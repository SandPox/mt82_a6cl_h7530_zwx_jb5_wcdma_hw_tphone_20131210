
package com.tmobile.themechooser;

import java.util.ArrayList;

import com.tmobile.themes.provider.Themes.ThemeColumns;
import com.tmobile.themechooser.ChangeThemeHelper;
import com.tmobile.themes.ThemeManager;
import com.tmobile.themes.provider.ThemeItem;
import com.tmobile.themes.provider.Themes;
import com.tmobile.themes.widget.ThemeAdapter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.os.SystemProperties;

public class MainActivity extends Activity implements OnClickListener {
    private static final String TAG = ThemePreview.class.getSimpleName();
    private ArrayList<HashMap<String, Object>> mListGridViewItems;
    private HashMap mSupportThemItemMap;
    private GridView mGridView;

    private static final String THEME_PKG_NAME_ARRAY[] = {
            "", // system
            "com.tmobile.theme.Lights", // Lights
            "com.tmobile.theme.Pink", // Pink
            "com.tmobile.theme.Sky", // Sky
    };

    public static final int IMG_DRAWABLE_IDS[] = {
            R.drawable.system,
            R.drawable.light,
            R.drawable.pink,
            R.drawable.sky,
    };

    public static final int IMG_USE_IDS[] = {
            R.drawable.systemuse,
            R.drawable.lightuse,
            R.drawable.pinkuse,
            R.drawable.skyuse,
    };

    public static final int IMG_PREVIEW_IDS[] = {
            R.drawable.systempreview,
            R.drawable.lightpreview,
            R.drawable.pinkpreview,
            R.drawable.skypreview,
    };

    public static final int STRING_RES_IDS[] = {
            R.string.system_theme,
            R.string.light_theme,
            R.string.pink_theme,
            R.string.sky_theme,
    };

    public static String sPreviewThemePkgName = "";
    public static Integer sPreviewImgId = 0;
    public static String sPreviewThemeName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGridView = (GridView) findViewById(R.id.gridview);
        initSupportThemeData();

        mListGridViewItems = new ArrayList<HashMap<String, Object>>();
        setGridViewAdapter();

        mGridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
                HashMap<String, Object> item = (HashMap<String, Object>) arg0
                        .getItemAtPosition(arg2);
                sPreviewThemePkgName = (String) item.get("PackageName");
                sPreviewImgId = (Integer) item.get("ImagePreviewId");
                sPreviewThemeName = (String) item.get("ItemText");
                startPreviewActivity();

            }

        });
    }

    private boolean isSupport(String pkgName) {
        boolean isSupport = false;
        for (int i = 0; i < THEME_PKG_NAME_ARRAY.length; i++) {
            if (pkgName.equals(THEME_PKG_NAME_ARRAY[i])) {
                isSupport = true;
                break;
            }
        }

        return isSupport;
    }

    private void initSupportThemeData() {
        mSupportThemItemMap = new HashMap<String, Object>();
        for (int i = 0; i < THEME_PKG_NAME_ARRAY.length; i++) {
            mSupportThemItemMap.put("ImgId:" + THEME_PKG_NAME_ARRAY[i],
                    IMG_DRAWABLE_IDS[i]);
            mSupportThemItemMap.put("ImgUseId:" + THEME_PKG_NAME_ARRAY[i],
                    IMG_USE_IDS[i]);
            mSupportThemItemMap.put("ImgPreviewId:" + THEME_PKG_NAME_ARRAY[i],
                    IMG_PREVIEW_IDS[i]);
            mSupportThemItemMap.put("StringId:" + THEME_PKG_NAME_ARRAY[i],
                    STRING_RES_IDS[i]);
        }
    }

    private void setGridViewAdapter() {
        SimpleAdapter simpleAdapter;
        updateGridViewItemData();
        simpleAdapter = new SimpleAdapter(this,
                mListGridViewItems,
                R.layout.item,
                new String[] {
                        "ItemImage", "ItemText"
                },
                new int[] {
                        R.id.imageView, R.id.textView
                });
        mGridView.setAdapter(simpleAdapter);

    }

    private boolean isShowingSystemTheme() {
        boolean ret = false;
        if (SystemProperties.getBoolean("persist.sys.showSystemTheme", true)) {
            ret = true;
        }
        return ret;
    }

    private void updateGridViewItemData() {
        ContentResolver cr = this.getContentResolver();
        Cursor cursor = cr.query(ThemeColumns.CONTENT_PLURAL_URI, null, null,
                null, ThemeColumns.NAME);

        if (cursor == null) {
            return;
        }

        mListGridViewItems.clear();
        cursor.moveToPosition(-1);
        while (cursor != null && cursor.moveToNext()) {
            String themePkgName = cursor.getString(cursor
                    .getColumnIndex(ThemeColumns.THEME_PACKAGE));
            Log.i(TAG, "themePkgName:" + themePkgName);
            if (themePkgName.equals("")) {
                if (!isShowingSystemTheme()) {
                    continue;
                }
            }
            if (!isSupport(themePkgName)) {
                continue;
            }
            String themeName = "";
            HashMap<String, Object> map = new HashMap<String, Object>();
            Integer imgId = (Integer) mSupportThemItemMap.get("ImgId:" + themePkgName);
            Integer imgUseId = (Integer) mSupportThemItemMap.get("ImgUseId:" + themePkgName);
            Integer imgPreviewId = (Integer) mSupportThemItemMap
                    .get("ImgPreviewId:" + themePkgName);
            Integer stringId = (Integer) mSupportThemItemMap.get("StringId:" + themePkgName);
            themeName = this.getResources().getString(stringId);

            int applied = cursor.getInt(cursor
                    .getColumnIndex(ThemeColumns.IS_APPLIED));
            if (applied == 1) {
                map.put("ItemImage", imgUseId);
            } else {
                map.put("ItemImage", imgId);
            }

            map.put("ImagePreviewId", imgPreviewId);
            map.put("PackageName", themePkgName);
            map.put("ItemText", themeName);
            mListGridViewItems.add(map);
        }

        cursor.close();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setGridViewAdapter();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void startPreviewActivity() {
        Intent intent = new Intent("com.tmobile.intent.action.SET_THEME");
        int sumItems = mListGridViewItems.size();

        intent.putExtra("sumItems", sumItems);
        for (int i = 0; i < sumItems; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            Integer imgPreviewId;
            String pkgName = "";
            String themeName = "";
            map = mListGridViewItems.get(i);
            imgPreviewId = (Integer) map.get("ImagePreviewId");
            pkgName = (String) map.get("PackageName");
            themeName = (String) map.get("ItemText");
            intent.putExtra("itemImgId" + i, imgPreviewId);
            intent.putExtra("itemPkgName" + i, pkgName);
            intent.putExtra("itemName" + i, themeName);
            if (pkgName.equals(sPreviewThemePkgName)) {
                intent.putExtra("currPreviewItem", i);
            }
        }

        this.startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }

}
