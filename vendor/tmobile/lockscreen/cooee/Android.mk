ifeq ($(VANZO_COOEE_LOCKSCREEN_SUPPORT), yes)
LOCAL_PATH:= $(call my-dir)

copy_from :=  \
    ParticleHYLZ.apk \
    ParticleJL.apk \
    S4.apk \
    Zheshan.apk \
    Lockbox.apk

$(call add-prebuilt-files, APPS, $(copy_from))

endif
