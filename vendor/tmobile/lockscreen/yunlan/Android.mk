ifeq ($(VANZO_YUNLAN_LOCKSCREEN_SUPPORT), yes)
LOCAL_PATH:= $(call my-dir)

copy_from :=  \
    DanceDays.apk \
    FantasyBubbles.apk \
    IqiyooV1.5.0_fanzhuo.apk \
    Meatball.apk \
    OpeningsPrizes.apk \
    RainbowCastle.apk \
    SleepingGuitar.apk \
    SnowyEvening.apk \
    Stealing.apk \
    SteelCounter.apk \
    UnLock_V1302.apk

$(call add-prebuilt-files, APPS, $(copy_from))

endif
