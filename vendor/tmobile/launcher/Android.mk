ifeq ($(VANZO_COOEE_MIUI_LAUNCHER_SUPPORT), yes)
COOEE_LAUNCHER = yes
endif

ifeq ($(VANZO_COOEE_COCO_LAUNCHER_SUPPORT), yes)
COOEE_LAUNCHER = yes
endif

ifeq ($(COOEE_LAUNCHER), yes)

LOCAL_PATH:= $(call my-dir)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/libcoo.so:system/lib/libcoo.so \
    $(LOCAL_PATH)/libcoogl20.so:system/lib/libcoogl20.so \
    $(LOCAL_PATH)/libcut.so:system/lib/libcut.so

ifeq ($(VANZO_COOEE_MIUI_LAUNCHER_SUPPORT), yes)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/ui_default_layout.xml:system/launcher/ui_default_layout.xml
endif

ifeq ($(VANZO_COOEE_COCO_LAUNCHER_SUPPORT), yes)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/coco_default_layout.xml:system/launcher/coco_default_layout.xml
endif

copy_from :=  \
    3D_clock.apk \
    3D_desert.apk \
    3D_music.apk \
    CocoLauncher.apk \
    kuwa.apk \
    milauncher.apk

$(call add-prebuilt-files, APPS, $(copy_from))

endif
