
PRODUCT_PACKAGES += \
    3D_clock \
    3D_desert \
    3D_music \
    kuwa \
    libcoo.so \
    libcoogl20.so \
    libcut.so

ifeq ($(VANZO_COOEE_MIUI_LAUNCHER_SUPPORT), yes)
PRODUCT_PACKAGES += \
    milauncher
endif

ifeq ($(VANZO_COOEE_COCO_LAUNCHER_SUPPORT), yes)
PRODUCT_PACKAGES += \
    CocoLauncher
endif

