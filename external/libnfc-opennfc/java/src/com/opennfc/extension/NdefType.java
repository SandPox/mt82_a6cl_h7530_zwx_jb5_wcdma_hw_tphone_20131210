/*
 * Copyright (c) 2007-2012 Inside Secure, All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*******************************************************************************
 File auto-generated with the autogen.exe tool - Do not modify manually
 The autogen.exe binary tool, the generation scripts and the files used
 for the source of the generation are available under Apache License, Version 2.0
 ******************************************************************************/

package com.opennfc.extension;


final class NdefType {

  private NdefType() {}

  static final int W_NDEF_TNF_EMPTY = 0;
  static final int W_NDEF_TNF_WELL_KNOWN = 1;
  static final int W_NDEF_TNF_MEDIA = 2;
  static final int W_NDEF_TNF_ABSOLUTE_URI = 3;
  static final int W_NDEF_TNF_EXTERNAL = 4;
  static final int W_NDEF_TNF_UNKNOWN = 5;
  static final int W_NDEF_TNF_UNCHANGED = 6;
  static final int W_NDEF_TNF_ANY_TYPE = 8;
}
