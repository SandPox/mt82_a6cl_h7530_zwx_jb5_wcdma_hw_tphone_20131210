#ifndef __COM_MEDIATEK_NFC_DEMO_UTILITIES_H__
#define __COM_MEDIATEK_NFC_DEMO_UTILITIES_H__

#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void RecvIRQCallBack( void);

#ifdef __cplusplus
}
#endif

#endif /* __COM_MEDIATEK_NFC_DEMO_UTILITIES_H__ */

