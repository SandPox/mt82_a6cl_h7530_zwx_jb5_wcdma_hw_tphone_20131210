/*
 *  ion.c
 *
 * Memory Allocator functions for ion
 *
 *   Copyright 2011 Google, Inc
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#define LOG_TAG "ion"

#include <cutils/log.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unwind.h>
#include <linux/ion.h>
#include <ion/ion.h>
#include <linux/ion_drv.h>
#include <pthread.h>
#include "ion_debug.h"
#ifdef HAVE_UNWIND_CONTEXT_STRUCT
typedef struct _Unwind_Context __unwind_context;
#else
typedef _Unwind_Context __unwind_context;
#endif
extern struct map_info_t *g_milist;
extern pthread_mutex_t ion_debug_lock;
unsigned int ion_debug_lock_init = 0; 
typedef struct
{
    size_t count;
    intptr_t* addrs;
} stack_crawl_state_t;

typedef struct
{
  intptr_t start_address;
  char *name;  
} mapping_info_t;
static _Unwind_Reason_Code trace_function(__unwind_context *context, void *arg)
{
    stack_crawl_state_t* state = (stack_crawl_state_t*)arg;
    if (state->count) {
        intptr_t ip = (intptr_t)_Unwind_GetIP(context);
        if (ip) {
            state->addrs[0] = ip;
            state->addrs++;
            state->count--;
            return _URC_NO_REASON;
        }
    }
    /*
     * If we run out of space to record the address or 0 has been seen, stop
     * unwinding the stack.
     */
    return _URC_END_OF_STACK;
}

static inline
int get_backtrace(int *fp, intptr_t* addrs, size_t max_entries)
{
    stack_crawl_state_t state;
    state.count = max_entries;
    state.addrs = (intptr_t*)addrs;
    _Unwind_Backtrace(trace_function, (void*)&state);
    return max_entries - state.count;
}
int ion_open()
{
        int fd = open("/dev/ion", O_RDONLY);
        if (fd < 0)
                ALOGE("open /dev/ion failed!\n");
	else
	{
#if ION_DEBUGGER
		if((!ion_debug_lock_init)&&(g_milist == NULL))
		{
			ALOGE("init mutex lock  g_milist and link notify_function\n");
			ion_debug_lock_init = 1;
			pthread_mutex_init(&ion_debug_lock,NULL);
                        ALOGE("[ion_open] ion_debug_lock 0x%x",&ion_debug_lock);
                        acquire_my_map_info_list();
                        dl_register_notify_function(add_system_map_entry,remove_system_map_entry);
		}
		else
		{
		        acquire_my_map_info_list();
		}
		
		ion_record_debug_info(fd,NULL,NULL,0,ION_FUNCTION_OPEN);	
#endif
	}
        return fd;
}

int ion_close(int fd)
{
	int ret;

#if ION_DEBUGGER
	ion_record_debug_info(fd,NULL,NULL,0,ION_FUNCTION_CLOSE);
	release_my_map_info_list(g_milist);
	if(g_milist == NULL)
	{
		ALOGE("unregister notify_function\n");
		dl_unregister_notify_function();
                ion_debug_lock_init = 0;
	}
#endif
        ret = close(fd);
        return ret;
}

static int ion_ioctl(int fd, int req, void *arg)
{
        int ret = ioctl(fd, req, arg);
        if (ret < 0) {
                ALOGE("ioctl %x failed with code %d: %s\n", req,
                       ret, strerror(errno));
                return -errno;
        }
        return ret;
}

int ion_alloc(int fd, size_t len, size_t align, unsigned int heap_mask,
	      unsigned int flags, struct ion_handle **handle)
{
        int ret;
        struct ion_allocation_data data = {
                .len = len,
                .align = align,
		.heap_mask = heap_mask,
                .flags = flags,
        };

        ret = ion_ioctl(fd, ION_IOC_ALLOC, &data);
        if (ret < 0)
                return ret;
        *handle = data.handle;
    
#if ION_DEBUGGER
	ion_record_debug_info(fd,data.handle,NULL,0,ION_FUNCTION_ALLOC);
#endif
        return ret;
}

int ion_alloc_mm(int fd, size_t len, size_t align, unsigned int flags,
              struct ion_handle **handle)
{
        int ret;
        struct ion_allocation_data data = {
                .len = len,
                .align = align,
                .flags = flags,
                .heap_mask = ION_HEAP_MULTIMEDIA_MASK
        };

        ret = ion_ioctl(fd, ION_IOC_ALLOC, &data);
        if (ret < 0)
                return ret;
        *handle = data.handle;
#if 0
    //sample code for debug info
    {
        int ion_fd = fd;
        struct ion_mm_data mm_data;
        
        mm_data.mm_cmd = ION_MM_SET_DEBUG_INFO;
        mm_data.config_buffer_param.handle = *handle;
        strncpy(mm_data.buf_debug_info_param.dbg_name, "buf_for_k", ION_MM_DBG_NAME_LEN);
        mm_data.buf_debug_info_param.value1 = 1;//getpid();
        mm_data.buf_debug_info_param.value2 = 2;
        mm_data.buf_debug_info_param.value3 = 3;
        mm_data.buf_debug_info_param.value4 = 4;
        if(ion_custom_ioctl(ion_fd, ION_CMD_MULTIMEDIA, &mm_data))
        {
            //config error
            ALOGE("[ion_dbg] config debug info error\n");
            return -1;
        }

    }
 #endif
        
#if ION_DEBUGGER
	ion_record_debug_info(fd,data.handle,NULL,0,ION_FUNCTION_ALLOC_MM);
#endif
        return ret;
}

int ion_alloc_syscontig(int fd, size_t len, size_t align, unsigned int flags, struct ion_handle **handle)
{
        int ret;
        struct ion_allocation_data data = {
                .len = len,
                .align = align,
                .flags = flags,
                .heap_mask = ION_HEAP_SYSTEM_CONTIG_MASK
        };

        ret = ion_ioctl(fd, ION_IOC_ALLOC, &data);
        if (ret < 0)
                return ret;
        *handle = data.handle;
#if ION_DEBUGGER
	ion_record_debug_info(fd,data.handle,NULL,0,ION_FUNCTION_ALLOC_CONT);
#endif 
        return ret;
}

int ion_free(int fd, struct ion_handle *handle)
{
        struct ion_handle_data data = {
                .handle = handle,
        };
#if ION_DEBUGGER
	ion_record_debug_info(fd,handle,NULL,0,ION_FUNCTION_FREE);
#endif
        return ion_ioctl(fd, ION_IOC_FREE, &data);
}

int ion_map(int fd, struct ion_handle *handle, size_t length, int prot,
            int flags, off_t offset, unsigned char **ptr, int *map_fd)
{
        struct ion_fd_data data = {
                .handle = handle,
        };

        int ret = ion_ioctl(fd, ION_IOC_SHARE, &data);
        if (ret < 0)
                return ret;
        *map_fd = data.fd;
        if (*map_fd < 0) {
                ALOGE("map ioctl returned negative fd\n");
                return -EINVAL;
        }
        *ptr = mmap(NULL, length, prot, flags, *map_fd, offset);
        if (*ptr == MAP_FAILED) {
                ALOGE("mmap failed: %s\n", strerror(errno));
                return -errno;
        }
        return ret;
}

void* ion_mmap(int fd, void *addr, size_t length, int prot, int flags, int share_fd, off_t offset)
{
    void *mapping_address = NULL; 

    mapping_address =  mmap(addr, length, prot, flags, share_fd, offset);
#if ION_DEBUGGER
    {
	int tmp_fd;
	tmp_fd = fd | share_fd << 16;
	ALOGE("ION_MMAP\n");
    	ion_record_debug_info(tmp_fd,NULL,mapping_address,length,ION_FUNCTION_MMAP);
    }
#endif
    return mapping_address;
}

int ion_munmap(int fd, void *addr, size_t length)
{
#if ION_DEBUGGER
    ion_record_debug_info(fd,NULL,addr,length,ION_FUNCTION_MUNMAP);
#endif
    return munmap(addr, length);
}

int ion_share(int fd, struct ion_handle *handle, int *share_fd)
{
        int map_fd;
        struct ion_fd_data data = {
                .handle = handle,
        };

        int ret = ion_ioctl(fd, ION_IOC_SHARE, &data);
        if (ret < 0)
                return ret;
        *share_fd = data.fd;
        if (*share_fd < 0) {
                ALOGE("share ioctl returned negative fd\n");
                return -EINVAL;
        }
#if ION_DEBUGGER
	{
		int tmp_fd;
		tmp_fd = fd | *share_fd << 16;
		ion_record_debug_info(tmp_fd,handle,NULL,0,ION_FUNCTION_SHARE);
	}
#endif
        return ret;
}

int ion_alloc_fd(int fd, size_t len, size_t align, unsigned int heap_mask,
		 unsigned int flags, int *handle_fd) {
	struct ion_handle *handle;
	int ret;

	ret = ion_alloc(fd, len, align, heap_mask, flags, &handle);
	if (ret < 0)
		return ret;
	ret = ion_share(fd, handle, handle_fd);
	ion_free(fd, handle);
	return ret;
}

int ion_share_close(int fd, int share_fd)
{
#if ION_DEBUGGER 
	if(fd != -1){
                int tmp_fd;
                tmp_fd = fd | share_fd << 16;
                ion_record_debug_info(tmp_fd,NULL,NULL,0,ION_FUNCTION_SHARE_CLOSE);
	}

#endif
    return close(share_fd);
}

int ion_import(int fd, int share_fd, struct ion_handle **handle)
{
        struct ion_fd_data data = {
                .fd = share_fd,
        };

        int ret = ion_ioctl(fd, ION_IOC_IMPORT, &data);
        if (ret < 0)
                return ret;
        *handle = data.handle;
#if ION_DEBUGGER 
	ion_record_debug_info(fd,data.handle,NULL,0,ION_FUNCTION_IMPORT);
#endif
        return ret;
}
int ion_record_debug_info(int fd,struct ion_handle *handle,unsigned int address, unsigned int length, unsigned int function_name)
{
	unsigned int cmd,i;
	int ret = 0;
        ion_sys_data_t arg;
	map_info_t* tmp;

        //set second level command
        cmd = ION_CMD_SYSTEM;

        //set third leyel command
        arg.sys_cmd = ION_SYS_RECORD;

        //record ION function name for record fucntion
        arg.record_param.action = function_name;
        arg.record_param.pid = getpid();
	arg.record_param.group_id = 0;
	if((ION_FUNCTION_MMAP == function_name) || (ION_FUNCTION_SHARE == function_name) || (ION_FUNCTION_SHARE_CLOSE == function_name))
	{
		arg.record_param.fd  = (fd>>16)&0xffff;
	}
	else
	{
		arg.record_param.fd  = fd;
	}
	arg.record_param.handle = handle;
	arg.record_param.buffer = NULL;
	arg.record_param.client = NULL;

	if(address > 0)
	{
		arg.record_param.address = address;
		arg.record_param.length = length;
		arg.record_param.address_type = ADDRESS_USER_VIRTUAL;
	}
	else
	{
		arg.record_param.address = 0;
                arg.record_param.length = 0;
                arg.record_param.address_type = ADDRESS_MAX;
	}

        //get backtrace
        arg.record_param.backtrace_num = get_backtrace(__builtin_frame_address(0), arg.record_param.backtrace, BACKTRACE_SIZE);
	if(arg.record_param.backtrace_num == 0)
	{
		ALOGE("[ion_record_debug_info] ERROR!!!! can't get backtrace !!!!!!!!!!!!!!!!!!!!!!!!!!");
	}
	pthread_mutex_lock(&ion_debug_lock);

	//get mapping info
        for(i = 0;i < arg.record_param.backtrace_num;i++)
        {
        	//ALOGE("ION_%d: %x",arg.record_param.action,arg.record_param.backtrace[i]);
        	tmp = find_map_info(g_milist,arg.record_param.backtrace[i]);
		if(tmp != NULL)
		{
                	arg.record_param.mapping_record[i].name = tmp->name;
                	arg.record_param.mapping_record[i].address = tmp->start;
			arg.record_param.mapping_record[i].size = tmp->end - tmp->start +1;
                	//ALOGE("start address 0x%x name %s\n",arg.record_param.mapping_record[i].address, arg.record_param.mapping_record[i].name);
		}
		else
		{
			ALOGE("Address 0x%x can't get libinfo from system map\n",arg.record_param.backtrace[i]);
		}
        }
	pthread_mutex_unlock(&ion_debug_lock);
	if((ION_FUNCTION_MMAP == function_name) || (ION_FUNCTION_SHARE == function_name) || (ION_FUNCTION_SHARE_CLOSE == function_name))
	{
        	ret = ion_custom_ioctl(fd&0xffff,cmd,(void *)&arg);
	}
	else
	{
		ret = ion_custom_ioctl(fd,cmd,(void *)&arg);
	}
	return ret;
}
int ion_custom_ioctl(int fd, unsigned int cmd, void* arg)
{
    struct ion_custom_data custom_data;
    custom_data.cmd = cmd;
    custom_data.arg = (unsigned long) arg;
    return ioctl(fd, ION_IOC_CUSTOM, &custom_data);
}

int ion_sync_fd(int fd, int handle_fd)
{
    struct ion_fd_data data = {
        .fd = handle_fd,
    };
    return ion_ioctl(fd, ION_IOC_SYNC, &data);
}
